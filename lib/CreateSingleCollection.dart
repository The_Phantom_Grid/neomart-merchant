import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:merchant/ChoosInventory.dart';
import 'package:merchant/Network/httpRequests.dart';

import 'Components/ImagePick.dart';
import 'constants.dart';

class CreateSingleCollection extends StatefulWidget {
  @override
  _CreateSingleCollectionState createState() => _CreateSingleCollectionState();
}

class _CreateSingleCollectionState extends State<CreateSingleCollection> {
  var mColor = Color(0xFFfc0c5b);
  var cNameController = TextEditingController();
  var cDescription = TextEditingController();
  String cNameError, cDescError;
  String image1, image2, image3;
  HttpRequests requests = HttpRequests();
  String collectionId;
  var ratio = [
    CropAspectRatioPreset.square,
  ];

  setCollection() async {
    var res = await requests.setCollection(cNameController.text.toString(),
        cDescription.text.toString(), image1, image2, image3);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      collectionId = res['info']['collection_id'].toString();
      Get.to(ChooseInventory(collectionId));
    }
  }

  validateDetails() {
    setState(() {
      if (cNameController.text == "" || cNameController.text.length < 3) {
        cNameError = "Please provide valid collection name";
      } else if (cDescription.text == "" || cDescription.text.length < 3) {
        cDescError = "Description must be at least 10 characters";
      } else if (image1 == null && image2 == null && image3 == null) {
        Fluttertoast.showToast(msg: "Please provide images for collection");
      } else
        setCollection();
    });
  }

  setImage(int imageType, String imageString) {
    switch (imageType) {
      case 1:
        image1 = imageString;
        break;
      case 2:
        image2 = imageString;
        break;
      case 3:
        image3 = imageString;
        break;
    }
  }

  openBottomSheet(int imageType) {
    String base64String;
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (context) {
          return Container(
              height: MediaQuery.of(context).size.height * .2,
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      image = await getImage(ImageSource.camera, ratio);
                      if (image != null) {
                        base64String = convertImageToBase64(image);
                        setImage(imageType, base64String);
                      }
                      setState(() {});
                    },
                    leading: Icon(Icons.camera_alt),
                    title: Text("Camera"),
                  ),
                  ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      image = await getImage(ImageSource.gallery, ratio);
                      if (image != null) {
                        base64String = convertImageToBase64(image);
                        setImage(imageType, base64String);
                      }
                      setState(() {});
                    },
                    leading: Icon(Icons.photo_library),
                    title: Text("Gallery"),
                  ),
                ],
              ));
        });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: textBoxFillColor,
      appBar: AppBar(
        centerTitle: true,
        title: Text("Create Collection"),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Card(
            elevation: 0.0,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 14),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Collection Name",
                    textAlign: TextAlign.left,
                    style: subHeading,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  TextFormField(
                    style: regularText,
                    controller: cNameController,
                    enabled: true,
                    onChanged: (val) {
                      setState(() {
                        cNameError = null;
                      });
                    },
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(12),
                        errorText: cNameError,
                        isDense: true,
                        hasFloatingPlaceholder: true,
                        hintText: "Example - Diwali Special",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    "About Collection",
                    textAlign: TextAlign.left,
                    style: subHeading,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  TextFormField(
                    style: regularText,
                    controller: cDescription,
                    enabled: true,
                    onChanged: (val) {
                      setState(() {
                        cDescError = null;
                      });
                    },
                    maxLines: 3,
                    maxLength: 150,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(12),
                        errorText: cDescError,
                        isDense: true,
                        hasFloatingPlaceholder: true,
                        hintText: "Write short description about the collection",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    "Upload Images",
                    textAlign: TextAlign.left,
                    style: subHeading,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7),
                        border: Border.all(color: Colors.black, width: 1)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          GestureDetector(
                            onTap: () {
                              WidgetsBinding.instance.focusManager.primaryFocus.unfocus();
                              openBottomSheet(1);
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(50),
                              child: Container(
                                height: 100,
                                width: 100,
                                color: Colors.black12,
                                child: image1 == null ?
                                Center(child: Icon(Icons.add_photo_alternate,
                                    color: Colors.black)) :
                                Image.memory(Base64Decoder().convert(image1),
                                  fit: BoxFit.cover,),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              WidgetsBinding.instance.focusManager.primaryFocus.unfocus();
                              openBottomSheet(2);
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(50),
                              child: Container(
                                height: 100,
                                width: 100,
                                color: Colors.black12,
                                child: image2 == null ?
                                Center(child: Icon(Icons.add_photo_alternate,
                                    color: Colors.black)) :
                                Image.memory(Base64Decoder().convert(image2),
                                  fit: BoxFit.cover,),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              WidgetsBinding.instance.focusManager.primaryFocus.unfocus();
                              openBottomSheet(3);
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(50),
                              child: Container(
                                height: 100,
                                width: 100,
                                color: Colors.black12,
                                child: image3 == null ?
                                Center(child: Icon(Icons.add_photo_alternate,
                                    color: Colors.black)) :
                                Image.memory(Base64Decoder().convert(image3),
                                  fit: BoxFit.cover,),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            validateDetails();
//            Navigator.push(context, MaterialPageRoute(builder: (context) => ChooseInventory()));
          },
          label: Row(
            children: <Widget>[Text("NEXT"), Icon(Icons.arrow_forward)],
          )
      ),
    );
  }
}
