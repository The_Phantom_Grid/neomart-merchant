import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:merchant/CollectionSetting.dart';
import 'package:merchant/Components/StoreInventoryProductCard.dart';
import 'package:merchant/Getter/GetFilteredProduct.dart';
import 'package:merchant/Getter/GetProductCategory.dart';
import 'package:merchant/Getter/GetProductSubCategory.dart';
import 'package:merchant/Network/httpRequests.dart';

import 'Components/SearchProductPos.dart';
import 'Getter/GetSystemFilter.dart';
import 'MyInventory.dart';
import 'constants.dart';

_ChooseInventoryState chooseInventoryState;

class ChooseInventory extends StatefulWidget {
  String collectionId;

  ChooseInventory(this.collectionId);

  @override
  _ChooseInventoryState createState() {
    chooseInventoryState = _ChooseInventoryState();
    return chooseInventoryState;
  }
}

class _ChooseInventoryState extends State<ChooseInventory> with SingleTickerProviderStateMixin {
  var _tabController;
  int selectedChip = 0;
  bool pageState = false, loading = true, loadMore = false;
  PageController pageController = PageController();
  HttpRequests requests = HttpRequests();
  SystemFilter systemFilter;
  String filterName, catId, subCatId;
  ProductCategory productCategory;
  ProductSubCategory productSubCategory;
  Product filteredProduct, tempProducts;
  Map<int, dynamic> products = {};
  int MIN = 0, MAX = 10;

  selectedOption(int index) {
    setState(() {
      selectedChip = index;
      pageState = false;
      catId = productCategory.info[index].prodCategoryId.toString();
      pageState = false;
      pageController.animateToPage(0,
          duration: Duration(milliseconds: 200), curve: Curves.decelerate);
    });
    getSubCategory();
  }

  getFilters() async {
    var res = await requests.getSystemFilters();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        systemFilter = SystemFilter.fromJson(res);
        filterName = systemFilter.info[0].filterName;
        _tabController = TabController(
            length: systemFilter.info.length, initialIndex: 0, vsync: this);
        getProductCategories();
      });
    }
  }

  getProductCategories() async {
    var res = await requests.getProductCategories(filterName);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        productCategory = ProductCategory.fromJson(res);
        catId = productCategory.info[0].prodCategoryId.toString();
        getSubCategory();
      });
    }
  }

  getSubCategory() async {
    var res = await requests.getSubCategories(catId);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        productSubCategory = ProductSubCategory.fromJson(res);
        subCatId = productSubCategory.info[0].subcatId.toString();
        loading = false;
      });
    }
  }

  getProdcuts() async {
    print(filterName);
    var res = await requests.getFilteredProducts(
        filterName, catId, subCatId, "10", "0");
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        filteredProduct = Product.fromJson(res);
        pageState = true;
        pageController.animateToPage(1,
            duration: Duration(milliseconds: 200), curve: Curves.decelerate);
      });
    }
  }

  getMoreProducts() async {
    print(filterName);
    MIN += MAX;
    tempProducts = null;
    var res = await requests.getFilteredProducts(
        filterName, catId, subCatId, MAX.toString(), MIN.toString());
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        tempProducts = Product.fromJson(res);
        if(tempProducts.info.data.isEmpty)
          showToast("No more products", Colors.black);
        filteredProduct.info.data.addAll(tempProducts.info.data);
        loadMore = false;
        pageState = true;
        pageController.animateToPage(1,
            duration: Duration(milliseconds: 200), curve: Curves.decelerate);
      });
    }else {
      setState(() {
        loadMore = false;
      });
    }
  }

  addProductsToCollection() async {
    print("JSON STRING: ${products.values.toList()}");

    var res = await requests.addProductToCollection(
        widget.collectionId, "1", products.values.toList(), "0");
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CollectionSetting(widget.collectionId)));
    }
  }

  Widget showMore() {
    return GestureDetector(
      onTap: () {
        setState(() {
          loadMore = true;
        });
        getMoreProducts();
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        padding: EdgeInsets.all(16),
        child: loadMore
            ? Text(
                "Loading...",
                textAlign: TextAlign.center,
              )
            : Text(
                "Show more",
                textAlign: TextAlign.center,
              ),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    print("Collection ID: ${widget.collectionId}");
    getFilters();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: textBoxFillColor,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Choose Inventory",
              style: TextStyle(fontSize: 13),
            ),
            Text(
              storeName,
              style: storeNameTextStyle,
            )
          ],
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              Get.to(SearchProductPos("", "", false));
            },
          )
        ],
        bottom: systemFilter == null ? PreferredSize(
          child: SizedBox(),
          preferredSize: Size(0, 0),
        ) : TabBar(
          isScrollable: true,
          indicatorColor: mColor,
          indicatorWeight: 3,
          tabs: systemFilter.info.map((e) =>
              Tab(child: Text(e.filterName, style: regularText,),)).toList(),
          controller: _tabController,
          onTap: (val) {
            setState(() {
              MIN = 0;
              filterName = systemFilter.info[val].filterName;
              loading = true;
              productCategory = null;
              productSubCategory = null;
              print("FILTER: $filterName");
              pageState = false;
              pageController.animateToPage(
                  0, duration: Duration(milliseconds: 200),
                  curve: Curves.decelerate);
              getProductCategories();
            });
          },
        ),
      ),
      body: loading ? loadingCircular() : Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Container(
                height: 25,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: productCategory.info.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        selectedOption(index);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: selectedChip == index ? Colors.black : Colors
                                .grey,
                            borderRadius: BorderRadius.circular(8)
                        ),
                        padding: EdgeInsets.symmetric(horizontal: 8),
                        margin: EdgeInsets.symmetric(horizontal: 8),
                        child: Center(child: Text(
                          productCategory.info[index].prodCategoryName,
                          style: TextStyle(fontSize: 11,
                              color: selectedChip == index
                                  ? Colors.white
                                  : Colors.black),)),
                      ),
                    );
                  },
                ),
              ),
            ),

            !pageState? Container(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text("My Brands: 4 items"),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.arrow_drop_up,
                          size: 30,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ): GestureDetector(
                onTap: (){
                  setState(() {
                    MIN = 0;
                    pageState = false;
                  });
                  pageController.animateToPage(0, duration: Duration(milliseconds: 200), curve: Curves.decelerate);
                },
                child: SizedBox(height: 30, child: Icon(Icons.keyboard_backspace))),
            Expanded(
              child: PageView(
                physics: NeverScrollableScrollPhysics(),
                scrollDirection: Axis.horizontal,
                controller: pageController,
                children: <Widget>[
                  myBrandList(),
                  filteredProduct == null ?
                  SizedBox() :
                  ListView.separated(
                      separatorBuilder: (context, index) {
                        return SizedBox(height: 8,);
                      },
                      padding: EdgeInsets.only(bottom: 70),
                      itemCount: filteredProduct.info.data.length < MIN
                          ? filteredProduct.info.data.length
                          : filteredProduct.info.data.length + 1,
                      itemBuilder: (context, index) {
                        return index == filteredProduct.info.data.length
                            ? showMore()
                            : CollectionProductCard(
                            filteredProduct.info.data[index]);
                      })
                ],
              ),
            )
          ],
        ),
      ),

      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            addProductsToCollection();
          },
          label: Row(
            children: <Widget>[Text("NEXT"), Icon(Icons.arrow_forward)],
          )
      ),
    );
  }

  myBrandList() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView.separated(
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: 5,
                );
              },
              itemCount: productSubCategory.info.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      subCatId = productSubCategory.info[index].subcatId
                          .toString();
//                      filteredProduct = null;
                    });
                    getProdcuts();
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0)),
                    elevation: 2.0,
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(color: mColor, width: 4))),
                      child: ListTile(
                        title: Text("${productSubCategory.info[index]
                            .name}(${productSubCategory.info[index]
                            .productCount})", style: defaultTextStyle,),
                        trailing: Icon(Icons.arrow_forward_ios),
                      ),
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

//class ChooseInventoryProductCard extends StatefulWidget {
//  List<Datum> data;
//  ChooseInventoryProductCard(this.data);
//  @override
//  _ChooseInventoryProductCardState createState() => _ChooseInventoryProductCardState();
//}
//
//class _ChooseInventoryProductCardState extends State<ChooseInventoryProductCard> {
//  @override
//  Widget build(BuildContext context) {
//    return ListView.separated(
//        separatorBuilder: (context, index){
//          return SizedBox(height: 8,);
//        },
//        padding: EdgeInsets.only(bottom: 70),
//        itemCount: widget.data.length,
//        itemBuilder: (context, index){
//          return CollectionProductCard(widget.data[index]);
//        });
////    return ListView.separated(
////        padding: EdgeInsets.only(bottom: 70),
////        separatorBuilder: (context, index){
////          return SizedBox(height: 8,);
////        },
////        itemCount: widget.data.length,
////      itemBuilder: (context, index){
////        return MyInventoryProductCard(widget.data[index]);
////      }
////    );
//  }
//}

