import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:merchant/Components/GetMerchantStores.dart';
import 'package:merchant/Dashboard.dart';
import 'package:merchant/HomePage/HomePage.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/constants.dart';

import 'Components/NotificationHandler.dart';
import 'Components/VariableControllers.dart';

class GetStarted extends StatefulWidget {
  String merchantid, token;

  GetStarted(this.merchantid, this.token);

  @override
  _GetStartedState createState() => _GetStartedState();
}

class _GetStartedState extends State<GetStarted> {
  HttpRequests requests = HttpRequests();
  bool load = true;
  NotificationHandler notificationHandler;

  getMerchantStores() async {
    var res =
        await requests.getAllMerchantStores(widget.merchantid, widget.token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        merchantStores = MerchantStores.fromJson(res);
        storeid = merchantStores.info.allStores[0].id.toString();
        merchantid = widget.merchantid;
        token = widget.token;
        storeName = merchantStores.info.allStores[0].storeName.toString();
        storeLocation =
            merchantStores.info.allStores[0].storeAddress.toString();
        print("MERCHANTID: $merchantid || USERID: $userid || TOKEN: $token");
        load = false;
      });
    }
  }

  @override
  void initState() {
    notificationHandler = NotificationHandler()..setupFirebaseNotification();
    // TODO: implement initState
    getMerchantStores();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    final padding = MediaQuery.of(context).padding.top;
    return Scaffold(
//      appBar: AppBar(
//        automaticallyImplyLeading: false,
//      ),
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Image.asset('assets/neomart_getstarted.png', height: 30,),
        centerTitle: true,
        elevation: 0.0,
        automaticallyImplyLeading: false,
      ),

      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                SizedBox(
                  height: height * .3,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30)),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(30),
                              bottomRight: Radius.circular(30))
                      ),
//                    padding: EdgeInsets.symmetric(vertical: padding),
                      width: width,
                      child: Opacity(
                          opacity: .4,
                          child: FadeInImage.assetNetwork(
                            placeholder: 'assets/neomart_splash_image.png',
                            image: load ? "" : merchantStores.info.allStores[0]
                                .storeImage,
                            fit: BoxFit.cover,)
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: -70,
                  left: 0,
                  right: 0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 80,
                        width: 80,
                        child: Center(child: Text(load ? "" : merchantStores
                            .info.allStores[0].storeName[0].toUpperCase(),
                          style: TextStyle(fontSize: 24),),),
                        decoration: BoxDecoration(
                            color: Colors.yellow,
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.white, width: 5)
                        ),
                      ),
                      Text(
                        load ? "--" : merchantStores.info.allStores[0].storeName
                            .toUpperCase(), style: subHeading,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.center,)
                    ],
                  ),
                )
              ],
            ),
            SizedBox(height: 85,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Stack(
                overflow: Overflow.visible,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 80,
                    child: Container(
                      margin: new EdgeInsets.only(left: 36.0, right: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: textBoxFillColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 30.0),
                        child: Align(alignment: Alignment.centerLeft,child: Text("SETUP YOUR STORE", style: h1, textAlign: TextAlign.start,),),
                      ),
                    ),
                  ),
                  Container(
                    height: 60,
                    width: 60,
                    child: Center(child: Image.asset('assets/getstarted/step-1.png', height: 20, color: Colors.white,)),
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.symmetric(vertical: 12),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 5),
                        color: Colors.teal
                    ),
                  ),
                ],
              ),
            ),

            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Stack(
                overflow: Overflow.visible,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 80,
                    child: Container(
                      margin: new EdgeInsets.only(left: 36.0, right: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: textBoxFillColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 30.0),
                        child: Align(alignment: Alignment.centerLeft,child: Text("PROFILE VERIFICATION AND APPROVAL", style: h1, textAlign: TextAlign.start,),),
                      ),
                    ),
                  ),
                  Container(
                    height: 60,
                    width: 60,
                    child: Center(child: Image.asset('assets/getstarted/step-2.png', height: 20, color: Colors.white,)),
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.symmetric(vertical: 12),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 5),
                        color: Colors.deepOrangeAccent
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Stack(
                overflow: Overflow.visible,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 80,
                    child: Container(
                      margin: new EdgeInsets.only(left: 36.0, right: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: textBoxFillColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 30.0),
                        child: Align(alignment: Alignment.centerLeft,child: Text("CREATE STORE INVENTORY", style: h1, textAlign: TextAlign.start,),),
                      ),
                    ),
                  ),
                  Container(
                    height: 60,
                    width: 60,
                    child: Center(child: Image.asset('assets/getstarted/step-3.png', height: 20, color: Colors.white,)),
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.symmetric(vertical: 12),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 5),
                        color: Colors.indigoAccent
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Stack(
                overflow: Overflow.visible,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 80,
                    child: Container(
                      margin: new EdgeInsets.only(left: 36.0, right: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: textBoxFillColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 30.0),
                        child: Align(alignment: Alignment.centerLeft,child: Text("START SELLING", style: h1, textAlign: TextAlign.start,),),
                      ),
                    ),
                  ),
                  Container(
                    height: 60,
                    width: 60,
                    child: Center(child: Image.asset('assets/getstarted/step-4.png', height: 20, color: Colors.white,)),
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.symmetric(vertical: 12),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 5),
                        color: Colors.amber
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.transparent,
        elevation: 0,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 50, vertical: 10),
          child: Hero(
            tag: 'getStarted',
            child: ButtonTheme(
              height: 45,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25))
                ),
                onPressed: (){
                  Get.offAll(HomePage());
                },
                color: Colors.black,
                textColor: Colors.white,
                child: Text("GET STARTED"),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

