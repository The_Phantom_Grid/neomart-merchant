import 'package:chips_choice/chips_choice.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:merchant/Network/httpRequests.dart';

import 'ChooseCategory.dart';
import 'Getter/GetSubCategory.dart';
import 'constants.dart';

_MyCategoryState myCategoryState;

class MyCategory extends StatefulWidget {
  @override
  _MyCategoryState createState() {
    myCategoryState = _MyCategoryState();
    return myCategoryState;
  }
}

class _MyCategoryState extends State<MyCategory> {
  var searchController = TextEditingController();

  List<String> categories = [];
  Map<int, String> selectedCat = {};
  TextEditingController catNameController = TextEditingController();
  HttpRequests requests = HttpRequests();
  SubCategory subCategory;
  List<Info> subCats = [];
  bool loading = true, onSearchStart = false;

  getSubCategory() async {
    var res = await requests.getMyCategory("1");
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        subCategory = SubCategory.fromJson(res);
        subCats.addAll(subCategory.info);
        loading = false;
        print("LENGTH: ${subCategory.info.length}");
      });
    } else {
      Fluttertoast.showToast(
          msg: res['message'].toString(),
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    }
  }

  addNewCat() async {
    var res = await requests.addNewCat(
        catNameController.text.toString(), merchantid, token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Fluttertoast.showToast(
          msg: res['message'].toString(),
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
      setState(() {
        categories.add(catNameController.text);
        catNameController.text = "";
        selectedCat[res['data']['category_id']] =
            res['data']['category'].toString();
      });
      print("SELECTED CAT: $selectedCat");
    } else {
      Fluttertoast.showToast(
          msg: res['message'].toString(),
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    }
  }

  addProductPrompt() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Text(
              "Add a product category",
              style: subHeading,
            ),
            content: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    controller: catNameController,
                    style: TextStyle(fontSize: 14),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        contentPadding: EdgeInsets.all(8),
                        focusColor: Colors.white38,
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black12),
                            borderRadius: BorderRadius.circular(4)),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black12),
                            borderRadius: BorderRadius.circular(4)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black12),
                            borderRadius: BorderRadius.circular(4)),
                        counterText: "",
                        hintText: "Category name",
                        labelStyle: TextStyle(color: Colors.grey)),
                  ),
                ],
              ),
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
            actions: <Widget>[
              ButtonTheme(
                height: 40,
                minWidth: 100,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                child: RaisedButton(
                  onPressed: () {
                    Navigator.pop(context);
                    addNewCat();
//                    setState(() {
//                      categories.add(catNameController.text);
//                      catNameController.text = "";
//                    });
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                      Text(
                        "Add",
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        });
  }

  addProductContainer() {
    return GestureDetector(
      onTap: () {
        addProductPrompt();
      },
      child: Container(
        height: 100,
        width: 100,
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0, 0),
                  blurRadius: 6,
                  spreadRadius: 2)
            ],
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: Colors.grey)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.add),
            SizedBox(
              height: 5,
            ),
            Text(
              "Add a product category",
              style: smallHeading,
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }

  onSearch(String text) {
    print(text);
    setState(() {
      subCats.clear();
      onSearchStart = true;
      subCategory.info.forEach((element) {
        if (element.subCategoryName.toLowerCase().contains(text)) {
          print(element.subCategoryName);
          subCats.add(element);
        }
      });
    });
    print(subCats);
  }

  String selectedSubCats;

  validate() {
    selectedSubCats = selectedCat.keys
        .toList()
        .toString()
        .replaceAll('[', "")
        .replaceAll(']', "")
        .replaceAll(" ", "");
    print(selectedSubCats);
    if (selectedSubCats == null || selectedSubCats == "") {
      Fluttertoast.showToast(
          msg: "Please select at least one category.",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    } else
      saveProductCat();
  }

  saveProductCat() async {
    var res = await requests.addMoreCat(selectedSubCats);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Fluttertoast.showToast(
          msg: "Categories Saved",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    } else {
      Fluttertoast.showToast(
          msg: "Please select at least one category to proceed.",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getSubCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Choose Category You Wish To Sell",
        ),
        bottom: loading
            ? PreferredSize(
                preferredSize: Size(0, 0),
                child: SizedBox(),
              )
            : PreferredSize(
                preferredSize: Size(double.infinity, 60),
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: searchController,
                      onChanged: (text) {
                        onSearch(text.toString().toLowerCase());
                      },
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                          filled: true,
                          fillColor: textBoxFillColor,
                          focusColor: Colors.white38,
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(4)),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(4)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(4)),
                          counterText: "",
                          suffixIcon: Icon(Icons.search, color: Colors.black),
                          hintText: "Search category",
                          labelStyle: TextStyle(color: Colors.grey)),
                    ),
                  ),
                ),
              ),
      ),
      extendBody: true,
      body: loading
          ? loadingCircular()
          : SingleChildScrollView(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).padding.bottom + 60),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
//            Padding(
//              padding: const EdgeInsets.all(16.0),
//              child: TextFormField(
//                decoration: InputDecoration(
//                    contentPadding: EdgeInsets.all(16),
//                    filled: true,
//                    fillColor: textBoxFillColor,
//                    focusColor: Colors.white38,
//                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
//                    border: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
//                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
//                    counterText: "",
//                    suffixIcon: Icon(Icons.search, color: Colors.black),
//                    hintText: "Search Category",
//                    labelStyle: TextStyle(color: Colors.grey)),
//                controller: searchController,
//              ),
//            ),
                    SizedBox(
                      height: 5,
                    ),
                    ChipsChoice.single(
                        value: categories,
                        isWrapped: true,
                        itemConfig: ChipsChoiceItemConfig(
                          showCheckmark: true,
                          spacing: 5,
                          unselectedColor: mColor,
                        ),
                        options: ChipsChoiceOption.listFrom<String, String>(
                          source: categories,
                          value: (i, v) => v,
                          label: (i, v) => v,
                        ),
                        onChanged: (val) {
                          setState(() {
                            categories.removeWhere((element) => element == val);
                            selectedCat
                                .removeWhere((key, value) => value == val);
                          });
                        }),
                    subCategory == null
                        ? SizedBox()
                        : Wrap(
                            spacing: 8,
                            alignment: WrapAlignment.start,
                            runSpacing: 12,
                            children:
                                List.generate(subCats.length + 1, (index) {
                              return index == 0
                                  ? addProductContainer()
                                  : buildCatContainer(subCats[index - 1]);
                            }),
                          )
                  ],
                ),
              ),
            ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ButtonTheme(
          height: 50,
          splashColor: Colors.white38,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: RaisedButton(
            onPressed: () {
              validate();
            },
            child: Text(
              "UPDATE",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}

class buildCatContainer extends StatefulWidget {
  Info info;

  buildCatContainer(this.info);

  @override
  _buildCatContainerState createState() => _buildCatContainerState();
}

class _buildCatContainerState extends State<buildCatContainer> {
  bool selected = false;

  selectedOption() {
    setState(() {
      if (selected) {
        selected = false;
        myCategoryState.selectedCat
            .removeWhere((key, value) => key == widget.info.subcategoryId);
      } else {
        selected = true;
        myCategoryState.selectedCat[widget.info.subcategoryId] =
            widget.info.subCategoryName.toString();
      }
    });
    print(myCategoryState.selectedCat
        .toString()
        .replaceAll('[', "")
        .replaceAll(']', "")
        .replaceAll(" ", ""));
  }

  checkSelected() {
    setState(() {
//      if(myCategoryState.selectedCat.toString().replaceAll('[', "").replaceAll(']', "").replaceAll(" ", "").contains(widget.info.subcategoryId.toString()))
//        selected = true;
      selected = widget.info.checkStatus;
      if (selected)
        myCategoryState.selectedCat[widget.info.subcategoryId] =
            widget.info.subCategoryName.toString();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    checkSelected();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        selectedOption();
      },
      child: Container(
        height: 100,
        width: 100,
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0, 0),
                  blurRadius: 6,
                  spreadRadius: 2)
            ],
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: selected ? mColor : Colors.grey)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(),
                Icon(
                  Icons.fiber_manual_record,
                  color: selected ? mColor : Colors.grey,
                  size: 15,
                ),
              ],
            ),
            Image.network(
              widget.info.categoryImage.toString(),
              height: 34,
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                widget.info.subCategoryName.toString(),
                style: smallHeading,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
