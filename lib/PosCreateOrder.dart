import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/model.dart';
import 'package:get/get.dart';
import 'package:merchant/Components/VariableControllers.dart';
import 'package:merchant/Getter/GetCustomerAddress.dart';
import 'package:merchant/Getter/GetCustomerData.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/OffersByCategory.dart';
import 'package:toggle_switch/toggle_switch.dart';

import 'SearchLocation.dart';
import 'constants.dart';

class PosCreateOrder extends StatefulWidget {
  String number;

  PosCreateOrder(this.number);

  @override
  _PosCreateOrderState createState() => _PosCreateOrderState();
}

class _PosCreateOrderState extends State<PosCreateOrder> {
  var house = TextEditingController(),
      streetName = TextEditingController(),
      fullNAme = TextEditingController(),
      pincode = TextEditingController(),
      city = TextEditingController(),
      state = TextEditingController(),
      storeLocation = TextEditingController(),
      deliveryDate = TextEditingController(),
      deliveryTime = TextEditingController(),
      landmark = TextEditingController();
  int optionValue = 0;
  String nameError;
  HttpRequests requests = HttpRequests();
  GetCustomerData customerData;
  CustomerAddress customerAddress;
  String userid, deliveryType = "Homedelivery";
  List<Address> address;

  getCustomerData() async {
    var res = await requests.searchCustomerNumber(widget.number, token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      customerData = GetCustomerData.fromJson(res);
      getUserid();
    }
  }

  getUserid() {
    for (int x = 0; x < customerData.data.length; x++) {
      if (customerData.data[x].userNumber.toString() == widget.number) {
        userid = customerData.data[x].userid.toString();
        break;
      }
    }
    print("USERID: $userid");
    getUserAddress();
  }

  getUserAddress() async {
    var res = await requests.getUserAddress(userid, token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      customerAddress = CustomerAddress.fromJson(res);
      if (customerAddress.details.length != 0) {
        initControllers();
      }
    }
  }

  updateCustomerDetails() async {
    var res = await requests.updateCustomerDetails(
        deliveryType,
        fullNAme.text.toString(),
        userid,
        customerAddress.details[0].address.toString(),
        customerAddress.details[0].id.toString(),
        customerAddress.details[0].lat.toString(),
        customerAddress.details[0].lng.toString(),
        house.text.toString(),
        pincode.text.toString(),
        token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      proceed();
    }
  }

  updateCustomerPickupDetails() async {
    var res = await requests.updateCustomerDetailsPickup(
        userid,
        merchantStores.info.allStores[0].storeAddress.toString(),
        fullNAme.text.toString(),
        deliveryType);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      proceed();
    }
  }

  proceed() {
    Get.to(OffersByCategory(userid, deliveryType, widget.number));
  }

  initControllers() {
    setState(() {
      fullNAme.text = customerAddress.details[0].fullName;
      house.text = customerAddress.details[0].flatHouseno;
      streetName.text = customerAddress.details[0].colonyStreet;
      pincode.text = customerAddress.details[0].pincode.toString();
      landmark.text = customerAddress.details[0].landmark;
    });
  }

  updateDetail(){
    setState(() {
      streetName.text = "${address.first.subLocality}, ${address.first.locality}";
      pincode.text = address.first.postalCode;
    });
  }

  validateDetails() {
    if (optionValue == 0) {
      if (fullNAme.text == "" || fullNAme.text.length < 3)
        Fluttertoast.showToast(msg: "Invalid detail: Customer Name");
      else if (house.text == "")
        Fluttertoast.showToast(msg: "Invalid detail: Delivery Address");
      else if (streetName.text == "")
        Fluttertoast.showToast(msg: "Invalid detail: Delivery Address");
      else if (pincode.text == "" || pincode.text.length < 6)
        Fluttertoast.showToast(msg: "Invalid detail: Pincode");
      else if (landmark.text == "")
        Fluttertoast.showToast(msg: "Invalid detail: Delivery Address");
      else if (deliveryDate.text == "")
        Fluttertoast.showToast(msg: "Invalid detail: Delivery Date");
      else if (deliveryTime.text == "")
        Fluttertoast.showToast(msg: "Invalid detail: Delivery Time");
      else
        updateCustomerDetails();
    } else if (optionValue == 1) {
      if (fullNAme.text == "" || fullNAme.text.length < 3)
        Fluttertoast.showToast(msg: "Invalid detail: Customer Name");
      else if (deliveryDate.text == "")
        Fluttertoast.showToast(msg: "Invalid detail: Delivery Date");
      else if (deliveryTime.text == "")
        Fluttertoast.showToast(msg: "Invalid detail: Delivery Time");
      else
        updateCustomerPickupDetails();
    } else if (optionValue == 2) {
      if (fullNAme.text == "" || fullNAme.text.length < 3)
        Fluttertoast.showToast(msg: "Invalid detail: Customer Name");
      else
        updateCustomerPickupDetails();
    }
  }

  toggleOption(int option) {
    setState(() {
      optionValue = option;

      if (optionValue == 1) {
        deliveryType = "Pickup";
        house.text = "";
        streetName.text = "";
        pincode.text = "";
        landmark.text = "";
      } else if (optionValue == 2) {
        deliveryType = "counter purchase";
        house.text = "";
        streetName.text = "";
        pincode.text = "";
        landmark.text = "";
        deliveryTime.text = "";
        deliveryDate.text = "";
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    getCustomerData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("POS"),
        automaticallyImplyLeading: true,
      ),
      extendBody: true,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).padding.bottom + 50),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Name",
                  textAlign: TextAlign.left,
                  style: subHeading,
                ),
                SizedBox(
                  height: 5,
                ),
                TextFormField(
                  style: regularText,
                  controller: fullNAme,
                  enabled: true,
                  onChanged: (val) {
                    setState(() {
                      nameError = null;
                    });
                  },
                  decoration: InputDecoration(
                      errorText: nameError,
                      contentPadding: EdgeInsets.symmetric(
                          horizontal: 12, vertical: 12),
                      isDense: true,
                      hintText: "Full Name",
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black))),
                ),
                SizedBox(
                  height: 15,
                ),
                deliverToOptions(),
                SizedBox(height: 15,),
                optionValue == 0?
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Deliver to:",
                      textAlign: TextAlign.left,
                      style: subHeading,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    addressField(),
                  ],
                ): SizedBox(),
                optionValue == 0 || optionValue == 1?
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Date of Delivery",
                      textAlign: TextAlign.left,
                      style: subHeading,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      style: regularText,
                      onTap: () {
                        DatePicker.showDatePicker(context, onConfirm: (date) {
                          if (!date.isBefore(DateTime.now())) {
                            deliveryDate.text = date.day.toString();
                            deliveryDate.text += "/" + date.month.toString();
                            deliveryDate.text += "/" + date.year.toString();
                          } else {
                            Flushbar(
                              duration: Duration(seconds: 2),
                              message:
                                  "Date Selected must be after ${DateTime.now().day}/${DateTime.now().month}/${DateTime.now().year}",
                            ).show(context);
                          }
                        });
                      },
                      controller: deliveryDate,
                      enabled: true,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
                          isDense: true,
                          hasFloatingPlaceholder: true,
                          hintText: "Tap to Set",
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black))),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Time of Delivery",
                      textAlign: TextAlign.left,
                      style: subHeading,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      style: regularText,
                      onTap: () {
                        DatePicker.showTime12hPicker(
                          context,
                          onConfirm: (date) {
                            if (date.isAfter(DateTime.now())) {
                              deliveryTime.text = date.hour.toString();
                              deliveryTime.text += ":" + date.minute.toString();
                            } else {
                              Flushbar(
                                duration: Duration(seconds: 2),
                                message:
                                    "Time Selected must br after ${DateTime.now().hour}:${DateTime.now().minute}",
                              ).show(context);
                            }
                          },
                          currentTime: DateTime.now(),
                        );
                      },
                      controller: deliveryTime,
                      enabled: true,
                      decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
                          isDense: true,
                          hasFloatingPlaceholder: true,
                          hintText: "Tap to Set",
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black))),
                    ),
                  ],
                ): SizedBox(),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ButtonTheme(
          height: 50,
          splashColor: Colors.white38,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: RaisedButton(
            onPressed: () {
              validateDetails();
//              Navigator.push(context, MaterialPageRoute(builder: (context) => OffersByCategory()));
            },
            child: Text(
              "CREATE ORDER",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }

  deliverToOptions (){
    return ToggleSwitch(
      initialLabelIndex: optionValue,
      fontSize: 11,
      minHeight: 30,
      minWidth: double.infinity,
      labels: ["Home", "Store Pickup", "Counter Sales"],
      activeBgColor: Colors.black,
      onToggle: (val) {
        toggleOption(val);
      },
    );
//    return Row(
//      mainAxisAlignment: MainAxisAlignment.spaceAround,
//      mainAxisSize: MainAxisSize.min,
//      crossAxisAlignment: CrossAxisAlignment.center,
//      children: <Widget>[
//        Expanded(
//          child: GestureDetector(
//            onTap: (){
//              setState(() {
//                optionValue = 1;
//              });
//            },
//            child: Container(
//              padding: EdgeInsets.all(4),
//              decoration: BoxDecoration(
//                  color: optionValue == 1? Colors.black: Colors.white,
//                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(6), topLeft: Radius.circular(6)),
//                  border: Border.all(color: Colors.black)
//              ),
//              child: Center(child: Text("Home", style: TextStyle(color: optionValue == 1? Colors.white: Colors.black, fontSize: 11),)),
//            ),
//          ),
//        ),
//        Expanded(
//          child: GestureDetector(
//            onTap: (){
//              setState(() {
//                optionValue = 2;
//              });
//            },
//            child: Container(
//              padding: EdgeInsets.all(4),
//              decoration: BoxDecoration(
//                  color: optionValue == 2? Colors.black: Colors.white,
//                  border: Border(top: BorderSide(color: Colors.black), bottom: BorderSide(color: Colors.black))
//              ),
//              child: Center(child: Text("Store Pickup", style: TextStyle(color: optionValue == 2? Colors.white: Colors.black, fontSize: 11))),
//            ),
//          ),
//        ),
//        Expanded(
//          child: GestureDetector(
//            onTap: (){
//              setState(() {
//                optionValue = 3;
//              });
//            },
//            child: Container(
//              padding: EdgeInsets.all(4),
//              decoration: BoxDecoration(
//                  color: optionValue == 3? Colors.black: Colors.white,
//                  borderRadius: BorderRadius.only(topRight: Radius.circular(6), bottomRight: Radius.circular(6)),
//                  border: Border.all(color: Colors.black)
//              ),
//              child: Center(child: Text("Counter Sales", style: TextStyle(color: optionValue == 3? Colors.white: Colors.black, fontSize: 11))),
//            ),
//          ),
//        )
//      ],
//    );
  }

  addressField(){
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          border: Border.all(color: Colors.black)),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Align(
              alignment: Alignment.topRight,
              child: RichText(
                text: TextSpan(children: <TextSpan>[
                  TextSpan(
                      text: "*", style: TextStyle(color: mColor)),
                  TextSpan(
                      text: " Mandatory fields",
                      style: TextStyle(color: Colors.black, fontSize: 11)),
                ]),
              ),
            ),
            TextField(
              style: regularText,
              controller: house,
              scrollPadding: EdgeInsets.all(8.0),
              inputFormatters: [
                WhitelistingTextInputFormatter(
                    RegExp("[a-zA-Z 0-9 - ,]"))
              ],
              decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(vertical: 12),
                hintText: "Flat / House no. / Floor / Building *",
              ),
            ),
            SizedBox(
              height: 15,
            ),
            TextField(
              style: regularText,
              controller: streetName,
              scrollPadding: EdgeInsets.all(8.0),
              inputFormatters: [
                WhitelistingTextInputFormatter(
                    RegExp("[a-zA-Z 0-9 - ,]"))
              ],
              decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(vertical: 12),
                hintText: "Colony / Street / Locality*",
              ),
            ),
            SizedBox(
              height: 15,
            ),
            TextField(
              style: regularText,
              controller: pincode,
              maxLength: 6,
              maxLengthEnforced: true,
              onChanged: (val) {
                if (val.length == 6) {
                  FocusScope.of(context).requestFocus(FocusNode());
                }
              },
              inputFormatters: [
                WhitelistingTextInputFormatter(RegExp("[0-9]"))
              ],
              keyboardType: TextInputType.numberWithOptions(),
              scrollPadding: EdgeInsets.all(8.0),
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(vertical: 12),
                counterText: "",
                hintText: "Pincode*",
              ),
            ),
            SizedBox(
              height: 15,
            ),
            TextField(
              style: regularText,
              controller: landmark,
              scrollPadding: EdgeInsets.all(8.0),
              inputFormatters: [
                WhitelistingTextInputFormatter(
                    RegExp("[a-zA-Z 0-9 - ,]"))
              ],
              decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(vertical: 12),
                hintText: "Landmark*",
              ),
            ),
            SizedBox(height: 20,),
            Row(
              children: <Widget>[
                Expanded(child: Divider(height: 0, thickness: 1, color: Colors.black54, indent: 30,)),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("OR", style: subHeading,),
                ),
                Expanded(child: Divider(height: 0, thickness: 1, color: Colors.black54, endIndent: 30,)),
              ],
            ),
            SizedBox(height: 10,),
            GestureDetector(
              onTap: () async {
                address = await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => SearchLocation()));
                if (address != null) updateDetail();
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Use ", style: subHeading,),
                  Icon(FlutterIcons.google_maps_mco, color: Colors.black, size: 25,),
                  Text("Maps", style: subHeading,)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
