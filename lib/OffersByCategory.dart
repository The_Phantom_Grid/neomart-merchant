import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:merchant/Components.dart';
import 'package:merchant/Components/SearchProductPos.dart';
import 'package:merchant/Components/VariableControllers.dart';
import 'package:merchant/Getter/GetBarcodeScanResult.dart';
import 'package:merchant/Getter/GetPosCategory.dart';
import 'package:merchant/Getter/GetPosOffer.dart';
import 'package:merchant/Getter/GetPosProduct.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/constants.dart';
import 'package:permission_handler/permission_handler.dart';

import 'AddOwnBrand.dart';
import 'Components/ProductCard.dart';
import 'PosCheckout.dart';
import 'ScanResult.dart';

_OffersByCategoryState offersByCategoryState;

class OffersByCategory extends StatefulWidget {
  String customerUserid, deliveryType, number;

  OffersByCategory(this.customerUserid, this.deliveryType, this.number);

  @override
  _OffersByCategoryState createState() {
    offersByCategoryState = _OffersByCategoryState();
    return offersByCategoryState;
  }
}

class _OffersByCategoryState extends State<OffersByCategory> with SingleTickerProviderStateMixin{

  TabController tabController;
  HttpRequests requests = HttpRequests();
  PosCategory posCategory;
  bool loading = true, loadMore = false, noOffer = false, showOffer = false;
  String subCatId, catId;
  PosProduct posProduct, tempProducts;
  int productsInCart = 0, MIN = 0, MAX = 10;
  BarcodeScanResult barcodeScanResult;
  final VarController v = Get.put(VarController());
  List<String> tabNames = ["Offers"];
  PosOffer posOffer;

  getPosOffers() async {
    var res = await requests.getPosOffers();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        posOffer = PosOffer.fromJson(res);
        if(posOffer.info.isEmpty)
          noOffer = true;
        else {
          noOffer = false;
        }
      });
    }
  }

  getPosCat() async {
    var res =
        await requests.getPosCat(widget.customerUserid, merchantid, token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        posCategory = PosCategory.fromJson(res);
        tabController = TabController(
            initialIndex: 1,
            length: posCategory.info.subCategory.length + 1,
            vsync: this);
        subCatId = posCategory.info.subCategory[0].subcatId.toString();
        catId = posCategory.info.subCategory[0].categoryId.toString();
        loading = false;
        posCategory.info.subCategory.forEach((element) {
          tabNames.add(element.name.toString());
        });
        getProducts();
      });
    }
//    posCategory.info.category.map((e) => Tab(child: Text(e.prodCategoryName),)).toList()
  }

  getProducts() async {
    var res = await requests.getPosProduct(
        catId, subCatId, widget.customerUserid, "10", "0");
    posProduct = null;
    setState(() {
      posProduct = PosProduct.fromJson(res);
      print("CART COUNT: ${posProduct.info.data[0].cartCount}");
    });
  }

  getMoreProducts() async {
    MIN += MAX;
    tempProducts = null;
    var res = await requests.getPosProduct(
        catId, subCatId, widget.customerUserid, MAX.toString(), MIN.toString());

    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        tempProducts = PosProduct.fromJson(res);
        if (tempProducts.info.data.isEmpty)
          showToast("No more products", Colors.black);
        posProduct.info.data.addAll(tempProducts.info.data);
        loadMore = false;
      });
    } else {
      setState(() {
        loadMore = false;
      });
    }
  }

  setPageState() {
    setState(() {});
  }

  Widget showMore() {
    return GestureDetector(
      onTap: () {
        setState(() {
          loadMore = true;
        });
        getMoreProducts();
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        padding: EdgeInsets.all(16),
        child: loadMore
            ? Text(
          "Loading...",
          textAlign: TextAlign.center,
        )
            : Text(
          "Show more",
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Future _scan() async {
    PermissionStatus status = await Permission.camera.status;
    if (!status.isGranted)
      await Permission.camera.request();
    else {
      String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          mColor.toString(), "Cancel", true, ScanMode.DEFAULT);
      HapticFeedback.mediumImpact();
      if (barcodeScanRes != null) getScanResult(barcodeScanRes);
//      String barcode = await Get.to(QrScan());
//      print(barcode);
//      if(barcode != null)
//        Navigator.push(context,
//            MaterialPageRoute(builder: (context) => ScanResult(barcode)));
    }
//    }
  }

  getScanResult(String barCode) async {
    var res = await requests.getBarcodeScanResult(barCode);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      barcodeScanResult = BarcodeScanResult.fromJson(res);
      if (barcodeScanResult.product.isNotEmpty) {
        showScanResult(barcodeScanResult.product[0]);
      } else {
        Get.snackbar(
            "Not Found",
            "Do you wish to add a new product to your inventory?",
            snackPosition: SnackPosition.BOTTOM,
            isDismissible: true,
            duration: Duration(seconds: 10),
            mainButton: FlatButton(
              child: Text("Yes"),
              onPressed: () {
                Get.to(AddOwnBrand());
              },
            )
        );
      }
    }
  }


  addProdToCart(Product product) async {
    if (qty > product.totalLeft)
      showToast("Could not add quantity more than ${product.totalLeft}",
          Colors.redAccent);
    else {
      var res = await requests.addToCartPos(
          widget.deliveryType,
          product.masterProductId.toString(),
          product.merchantlistId.toString(),
          qty.toString(),
          widget.customerUserid,
          token);
      if (res != null && res['status'].toString().toLowerCase() == "success") {
        offersByCategoryState.setPageState();
        showToast("Added to cart", Colors.green);
      }
    }
  }

  var mrpController = TextEditingController();
  var srpController = TextEditingController();
  var qtyController = TextEditingController();
  double mrp, srp;
  int qty;

  showScanResult(Product product) {
    mrpController.text = product.mrp;
    srpController.text = product.mrp;
    qty = 0;

    Get.bottomSheet(StatefulBuilder(
      builder: (context, setState) {
        return Container(
          padding: EdgeInsets.all(16),
          width: double.infinity,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(8), topLeft: Radius.circular(8))),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 2,
                          color: textBoxFillColor,
                          spreadRadius: 2,
                        )
                      ]),
                  child: FadeInImage.assetNetwork(
                    placeholder: 'assets/tsp.png',
                    image: product.icon,
                    height: 200,
                    width: double.infinity,
                    fit: BoxFit.fitHeight,
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  product.productname,
                  style: subHeading,
                ),
                Text(
                  product.productdescription,
                  style: TextStyle(fontSize: 11, color: Colors.grey),
                ),
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: TextField(
                              enabled: false,
                              keyboardType: TextInputType.numberWithOptions(),
                              scrollPadding: EdgeInsets.all(8.0),
                              maxLength: 10,
                              maxLengthEnforced: true,
                              controller: mrpController,
                              inputFormatters: [
                                WhitelistingTextInputFormatter(RegExp("[0-9]")),
                              ],
                              onChanged: (value) {
                                setState(() {
                                  mrp = double.tryParse(double.tryParse(value)
                                      .toStringAsFixed(2));
                                });
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Icon(FlutterIcons.rupee_faw,
                                      size: 15, color: Colors.black),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 8),
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: true,
                                  fillColor: textBoxFillColor,
                                  focusColor: Colors.white38,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  border: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  counterText: "",
                                  hintText: "Enter MRP",
                                  labelText: "MRP"),
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(),
                              scrollPadding: EdgeInsets.all(8.0),
                              maxLength: 10,
                              controller: srpController,
                              maxLengthEnforced: true,
                              inputFormatters: [
                                WhitelistingTextInputFormatter(RegExp("[0-9]"))
                              ],
                              onChanged: (value) {
                                setState(() {
                                  srp = double.tryParse(double.tryParse(value)
                                      .toStringAsFixed(2));
                                });
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Icon(FlutterIcons.rupee_faw,
                                      size: 15, color: Colors.black),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 8),
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: true,
                                  fillColor: textBoxFillColor,
                                  focusColor: Colors.white38,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  border: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  counterText: "",
                                  hintText: "Enter Selling Price",
                                  labelText: "SRP"),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Row(
                              children: <Widget>[
                                Text(
                                  "Quantity: ",
                                  style: regularText,
                                ),
//                          IconButton(
//                            onPressed: qty <= 0? null: (){
//                              setState(() {
//                                qty--;
//                                qtyController.text = qty.toString();
//                              });
//                            },
//                            icon: Icon(Icons.remove, size: 15, color: Colors.black,),
//                          ),
                                SizedBox(
                                  width: 60,
                                  child: TextField(
                                    keyboardType:
                                    TextInputType.numberWithOptions(),
                                    scrollPadding: EdgeInsets.all(8.0),
                                    maxLength: 10,
                                    maxLengthEnforced: true,
                                    controller: qtyController,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter(
                                          RegExp("[0-9]"))
                                    ],
                                    onChanged: (value) {
                                      setState(() {
                                        qty = int.tryParse(value);
                                      });
                                    },
                                    decoration: InputDecoration(
                                      isDense: true,
//                              prefixIcon: Icon(Icons.remove, size: 15, color: Colors.black,),
//                              suffixIcon: Icon(Icons.add, size: 15, color: Colors.black,),
                                      contentPadding: EdgeInsets.symmetric(
                                          horizontal: 8, vertical: 8),
                                      labelStyle: TextStyle(
                                          color: Colors.black.withOpacity(.4)),
                                      filled: true,
                                      fillColor: textBoxFillColor,
                                      focusColor: Colors.white38,
                                      focusedBorder: OutlineInputBorder(
                                          borderSide:
                                          BorderSide(color: Colors.white),
                                          borderRadius:
                                          BorderRadius.circular(4)),
                                      border: OutlineInputBorder(
                                          borderSide:
                                          BorderSide(color: Colors.white),
                                          borderRadius:
                                          BorderRadius.circular(4)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide:
                                          BorderSide(color: Colors.white),
                                          borderRadius:
                                          BorderRadius.circular(4)),
                                      counterText: "",
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),

//                          IconButton(
//                            onPressed: (){
//                              print("ADD");
//                              setState(() {
//                                qty++;
//                                qtyController.text = qty.toString();
//                              });
//                            },
//                            icon: Icon(Icons.add, size: 15, color: Colors.black,),
//                          ),
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: ButtonTheme(
                              height: 35,
                              child: FlatButton(
                                onPressed: () {
                                  Get.back();
                                },
                                color: textBoxFillColor,
                                textColor: Colors.black,
                                child: Text("Cancel"),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: ButtonTheme(
                              height: 35,
                              child: FlatButton(
                                onPressed: () {
//                                  validateDetails();
                                  Get.back();
                                  addProdToCart(product);
                                },
                                color: Colors.black,
                                textColor: textBoxFillColor,
                                child: Text("Add to cart"),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    ));
  }

  @override
  void initState() {
    // TODO: implement initState
    print("DEL TYPE: ${widget.deliveryType}");
    v.pCount.value = 0;
    getPosOffers();
    getPosCat();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: textBoxFillColor,
        appBar: AppBar(
          title: Text("Offers By Category"),
          centerTitle: true,
          automaticallyImplyLeading: true,
          actions: <Widget>[
            IconButton(
              onPressed: () async {
                await Get.to(SearchProductPos(
                    widget.deliveryType, widget.customerUserid, true));
//                await Navigator.push(context, MaterialPageRoute(builder: (context) => SearchProductPos(widget.deliveryType, widget.customerUserid, true)));
                getProducts();
              },
              icon: Icon(Icons.search, color: Colors.white,),
            ),
            IconButton(
              onPressed: () {
                _scan();
              },
              icon: Icon(FlutterIcons.qrcode_scan_mco, color: Colors.white,),
            )
          ],
          bottom: loading ? PreferredSize(
            child: SizedBox(),
            preferredSize: Size(0, 0),
          ) : TabBar(
            indicatorColor: mColor,
            indicatorWeight: 3,
            isScrollable: true,
            controller: tabController,
            onTap: (val) {
              print("VALUE: $val");
              if(val == 0) {
                getPosOffers();
                showOffer = true;
              }
              else {
                showOffer = false;
                subCatId =
                    posCategory.info.subCategory[val - 1].subcatId.toString();
                catId = posCategory.info.subCategory[val - 1].categoryId.toString();
                print("SUBCATID: $subCatId|| CATId: $catId");
                setState(() {
                  posProduct = null;
                  MIN = 0;
                });
                getProducts();
              }
            },
            tabs: tabNames.map((e) => Tab(child: Text(e))).toList(),
//          tabs: <Widget>[
//            Tab(child: Text("Offers"),),
//            Tab(child: Text("Tea Leaf"),),
//            Tab(child: Text("Edible Oil"),),
//            Tab(child: Text("Coffee Powder"),),
//            Tab(child: Text("Besan"),),
//          ],
          ),
        ),

        body: loading ? loadingCircular() : showOffer? Container(
          child: noOffer? Center(child: Text("No Offers Available", style: subHeading,),): ListView.separated(
              padding: EdgeInsets.only(bottom: 70, top: 16),
              scrollDirection: Axis.vertical,
              itemCount: posOffer.info.length,
              separatorBuilder: (context, index) {
                return SizedBox(height: 8,);
              },
              itemBuilder: (context, index) {
                return PosOfferCard(
                    widget.deliveryType, widget.customerUserid, posOffer.info[index]);
              }
          ),
        ): Container(
          child: posProduct == null ? loadingCircular() : ListView.separated(
              padding: EdgeInsets.only(bottom: 70, top: 16),
              scrollDirection: Axis.vertical,
              itemCount: posProduct.info.data.length < MIN ? posProduct.info
                  .data.length : posProduct.info.data.length + 1,
              separatorBuilder: (context, index) {
                return SizedBox(height: 8,);
              },
              itemBuilder: (context, index) {
                return index == posProduct.info.data.length
                    ? showMore()
                    : productCard(
                    posProduct.info.data[index], widget.deliveryType,
                    widget.customerUserid);
              }
          ),
        ),

        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: Obx(() =>
            FloatingActionButton(
              onPressed: v.pCount.value <= 0 ? null : () {
                Navigator.push(context, MaterialPageRoute(builder: (context) =>
                    PosCheckout(widget.deliveryType, widget.customerUserid,
                        widget.number)));
              },
              child: Icon(Icons.shopping_cart, color: Colors.white,),
            ),)
    );
  }
}
