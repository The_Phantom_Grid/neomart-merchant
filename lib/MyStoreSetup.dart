import 'package:flutter/material.dart';
import 'package:merchant/DeliverySettings.dart';
import 'package:merchant/EditAddress.dart';

import 'FillStoreDetails.dart';
import 'MyCategory.dart';

class MyStoreSetup extends StatefulWidget {
  @override
  _MyStoreSetupState createState() => _MyStoreSetupState();
}

class _MyStoreSetupState extends State<MyStoreSetup> {
  var mColor = Color(0xFFfc0c5b);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("My Store Setup"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MyCategory()));
              },
              child: Container(
                height: 100,
                child: Card(
                  elevation: 2.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: BorderSide(color: mColor)),
                  child: Center(
                    child: ListTile(
                      leading: Icon(
                        Icons.category,
                        size: 30,
                      ),
                      title: Text("My Category"),
                      subtitle: Text(
                        "View/Edit",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => FillStoreDetails()));
              },
              child: Container(
                height: 100,
                child: Card(
                  elevation: 2.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: BorderSide(color: mColor)),
                  child: Center(
                    child: ListTile(
                      leading: Icon(
                        Icons.verified_user,
                        size: 30,
                      ),
                      title: Text("Store Documents and Verification"),
                      subtitle: Text(
                        "View/Edit",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => EditAddress()));
              },
              child: Container(
                height: 100,
                child: Card(
                  elevation: 2.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: BorderSide(color: mColor)),
                  child: Center(
                    child: ListTile(
                      leading: Icon(
                        Icons.location_city,
                        size: 30,
                      ),
                      title: Text("Shop Address"),
                      subtitle: Text(
                        "View/Edit",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DeliverySettings()));
              },
              child: Container(
                height: 100,
                child: Card(
                  elevation: 2.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: BorderSide(color: mColor)),
                  child: Center(
                    child: ListTile(
                      leading: Icon(
                        Icons.settings_applications,
                        size: 30,
                      ),
                      title: Text("Shipping Settings"),
                      subtitle: Text(
                        "View/Edit",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                      trailing: Icon(Icons.keyboard_arrow_right),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
