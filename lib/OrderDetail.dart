import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:io';
import 'package:merchant/Components/ProductCard.dart';
import 'package:merchant/Getter/GetOrderByStatus.dart';
import 'package:merchant/Getter/GetOrderSummary.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/constants.dart';

import 'Components/Invoice.dart';

class OrderDetail extends StatefulWidget {
  OrderByStatusDetail orderDetail;

  OrderDetail(this.orderDetail);

  @override
  _OrderDetailState createState() => _OrderDetailState();
}

class _OrderDetailState extends State<OrderDetail> {
  HttpRequests requests = HttpRequests();
  OrderSummary orderSummary;
  bool loading = true, storeDelivery = false;
  String invoiceUrl;

  getOrderSummary() async {
    if (widget.orderDetail.delivertype.toLowerCase() == "pickup" ||
        widget.orderDetail.delivertype.toLowerCase() == "counter purchase")
      storeDelivery = true;
    var res =
        await requests.getOrderSummary(widget.orderDetail.orderId.toString());
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        orderSummary = OrderSummary.fromJson(res);
        loading = false;
      });
    }
  }
  getInvoice() async {
    var res = await requests.getInvoice(widget.orderDetail.orderId.toString());
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      invoiceUrl = res['info'].toString();
      print("invoice" + invoiceUrl);
      if (Platform.isIOS) {
        Get.to(Invoice(invoiceUrl, widget.orderDetail.orderId.toString()));
      } else if (Platform.isAndroid) {
        Get.to(Invoice(invoiceUrl, widget.orderDetail.orderId.toString()));
        // getAppDirectory(invoiceUrl, widget.orderDetail.orderId.toString());
      }
      // PDFDocument.fromURL(invoiceUrl);
//      getAppDirectory();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getOrderSummary();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: textBoxFillColor,
      appBar: AppBar(
        centerTitle: true,
        title: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Orders",
              style: TextStyle(fontSize: 13),
            ),
            Text(
              storeName,
              style: TextStyle(color: mColor, fontSize: 16),
            )
          ],
        ),
      ),
      body: loading ? loadingCircular() : SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Card(
                elevation: 0.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  widget.orderDetail.firstname,
                                  style: subHeading,
                                ),
                                storeDelivery ? Text(storeLocation, style: defaultTextStyle,)
                                    : Text(widget.orderDetail.deliveryAddress, style: defaultTextStyle,)
                              ],
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  "Order Instruction",
                                  style: subHeading,
                                ),
                                Text(
                                  orderSummary.data[0].orderInstructions,
                                  style: defaultTextStyle,
                                ),
                              ],
                            ),
                    ),
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Schedule Delivery Date & Time",
                            style: subHeading,
                          ),
                          RichText(
                            text: TextSpan(
                                children: [
                                  TextSpan(
                                      text: "${orderSummary.data[0].timeSlot
                                          .split(',')[0]} | ",
                                      style: TextStyle(
                                        fontSize: 13, color: Colors.black,)),
                                  TextSpan(
                                      text: "${orderSummary.data[0].timeSlot
                                          .split(',')[1]}",
                                      style: TextStyle(
                                          fontSize: 13, color: mColor)),
                                ]
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("Order ID", style: subHeading,),
                          Row(
                            children: <Widget>[
                              Text("${widget.orderDetail.orderId} | ",
                                style: defaultTextStyle,),
                              Text("${widget.orderDetail.delivertype
                                  .toUpperCase()}", style: defaultTextStyle,),
                            ],
                          ),
                          ButtonTheme(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(6)
                            ),
                            minWidth: double.infinity,
                            height: 35,
                            child: FlatButton(
                              onPressed: () {
                                print(widget.orderDetail.mobile);
                              },
                              textColor: Colors.white,
                              color: Colors.black,
                              child: Text("CALL CUSTOMER", style: regularText,),
                            ),
                          ),
                          SizedBox(height: 8,),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(height: 4,),
              Card(
                elevation: 0.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text("CONFIRM & PACK", style: subHeading,),
                          Text("Product Details", style: defaultTextStyle,),
                        ],
                      ),
                    ),
                    Divider(),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: ListView.separated(
                        shrinkWrap: true,
                        itemCount: orderSummary.data.length,
                        physics: NeverScrollableScrollPhysics(),
                        separatorBuilder: (BuildContext context, int index) {
                          return Divider();
                        },
                        itemBuilder: (BuildContext context, int index) {
                          return OrderSummaryProductCard(
                              orderSummary.data[index]);
                        },
                      ),
                    ),
                    Divider(
                      indent: 8,
                      endIndent: 8,
                      color: textBoxFillColor,
                      thickness: 2,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Text("Total item : ${orderSummary.data
                                .length}/${orderSummary.data.length}",
                              style: smallHeading,),
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("Total", style: smallHeading,),
                                    Text("₹ ${widget.orderDetail.subtotal}",
                                        style: TextStyle(
                                            fontSize: 13, color: mColor)),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("Tax", style: smallHeading,),
                                    Text("₹ ${orderSummary.data[0]
                                        .taxCollected}", style: TextStyle(
                                        fontSize: 13, color: mColor)),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("Shipping", style: smallHeading,),
                                    Text("₹ ${widget.orderDetail.shipping}",
                                        style: TextStyle(
                                            fontSize: 13, color: mColor)),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("Discount", style: smallHeading,),
                                    Text("₹ ${widget.orderDetail
                                        .discountAmount}", style: TextStyle(
                                        fontSize: 13, color: mColor)),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(flex: 1, child: SizedBox(),),
                          Expanded(
                            flex: 1,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Bill amount", style: smallHeading,),
                                Text("₹ ${widget.orderDetail.grandtotal}",
                                    style: TextStyle(
                                        fontSize: 13, color: mColor)),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: ButtonTheme(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)
                        ),
                        height: 35,
                        child: FlatButton(
                          onPressed: () {
                          getInvoice();
                          },
                          textColor: Colors.white,
                          color: Colors.black,
                          child: Text(
                            "Invoice No. ${widget.orderDetail.invoiceNumber}",
                            style: smallHeading,),
                        ),
                      ),
                    ),
                    SizedBox(height: 14,)
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
