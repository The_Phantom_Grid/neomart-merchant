import 'package:flutter/material.dart';
import 'package:merchant/OrdersComponents/ActiveOrders.dart';
import 'package:merchant/OrdersComponents/CancelledOrders.dart';
import 'package:merchant/OrdersComponents/DeliveredOrders.dart';

import 'constants.dart';

_OrdersState ordersState;

class Orders extends StatefulWidget {
  @override
  _OrdersState createState() {
    ordersState = _OrdersState();
    return ordersState;
  }
}

class _OrdersState extends State<Orders> with SingleTickerProviderStateMixin {
  var scrollViewController = ScrollController();
  var _tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 3, initialIndex: 1, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: textBoxFillColor,
      resizeToAvoidBottomInset: true,
      extendBodyBehindAppBar: false,

//      appBar: AppBar(
//        centerTitle: true,
//        title: Column(
//          mainAxisSize: MainAxisSize.min,
//          mainAxisAlignment: MainAxisAlignment.center,
//          children: <Widget>[
//            Text("Orders", style: TextStyle(fontSize: 13),),
//            Text("Rahul Test", style: TextStyle(color: mColor, fontSize: 17),)
//          ],
//        ),
//      ),

      body: NestedScrollView(
        controller: scrollViewController,
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              title: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "Orders",
                    style: TextStyle(fontSize: 13),
                  ),
                  Text(
                    storeName,
                    style: TextStyle(color: mColor, fontSize: 16),
                  ),
                ],
              ),
              pinned: true,
              floating: true,
              centerTitle: true,
              automaticallyImplyLeading: true,
              forceElevated: innerBoxIsScrolled,
              bottom: TabBar(
                tabs: <Tab>[
                  Tab(text: "Active"),
                  Tab(text: "Delivered"),
                  Tab(text: "Cancelled"),
                ],
                controller: _tabController,
              ),
            ),
          ];
        },
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            ActiveOrders(),
            DeliveredOrders(),
            CancelledOrders(),
          ],
          controller: _tabController,
        ),
      ),
    );
  }
}
