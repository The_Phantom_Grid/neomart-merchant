import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:merchant/Components/SearchMasterProduct.dart';
import 'package:merchant/Getter/GetProductCategory.dart';
import 'package:merchant/Getter/GetProductSubCategory.dart';
import 'package:merchant/Network/httpRequests.dart';

import 'Components/StoreInventoryProductCard.dart';
import 'Getter/GetFilteredProduct.dart';
import 'MyInventory.dart';
import 'constants.dart';

_AddProductState addProductState;

class AddProduct extends StatefulWidget {
  @override
  _AddProductState createState() {
    addProductState = _AddProductState();
    return addProductState;
  }
}

class _AddProductState extends State<AddProduct>
    with SingleTickerProviderStateMixin {
  int selectedChip = 0, selectedRow = -1, temp = 0;
  ScrollController scrollController = ScrollController();
  HttpRequests requests = HttpRequests();
  ProductCategory productCategory;
  ProductSubCategory productSubCategory;
  String catId, subCatId, productCount;
  bool loading = true, pageState = false, loadMore = false;
  PageController pageController = PageController();
  Product filteredProduct, tempProducts;
  Map<int, dynamic> products = {};
  int MIN = 0, MAX = 10;

  selectedOption(int index) {
    temp = index;
    if (selectedChip != index && products.isNotEmpty) {
      showSaveChages();
    } else {
      setState(() {
        MIN = 0;
        selectedChip = index;
        catId = productCategory.info[index].prodCategoryId.toString();
        pageState = false;
        pageController.animateToPage(0,
            duration: Duration(milliseconds: 200), curve: Curves.decelerate);
      });
      getSubCategory();
    }
  }

  showSaveChages() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return Container(
            child: CupertinoAlertDialog(
              content:
                  Text("Do you want to save changes and update inventory."),
              actions: <Widget>[
                CupertinoDialogAction(
                  child: Text("Save"),
                  onPressed: () {
                    Get.back();
                    addProductsToInventory();
                  },
                  isDefaultAction: true,
                ),
                CupertinoDialogAction(
                  child: Text("Discard"),
                  onPressed: () {
                    setState(() {
                      Get.back();
                      products.clear();
                      selectedOption(temp);
                    });
                  },
                  isDestructiveAction: true,
                )
              ],
            ),
          );
        });
  }

  selectedTile(int index) {
    if (selectedRow == index) {
      setState(() {
        selectedRow = -1;
      });
    } else {
      setState(() {
        selectedRow = index;
      });
    }
  }

  getProductCategories() async {
    var res = await requests.getProductCategories("");
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        productCategory = ProductCategory.fromJson(res);
        catId = productCategory.info[0].prodCategoryId.toString();
        getSubCategory();
      });
    }
  }

  getSubCategory() async {
    var res = await requests.getSubCategories(catId);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        productSubCategory = ProductSubCategory.fromJson(res);
        subCatId = productSubCategory.info[0].subcatId.toString();
        loading = false;
      });
    }
  }

  getProdcuts() async {
    print(catId);
    print(subCatId);
    var res = await requests.getFilteredProducts(
        "Instock", catId, subCatId, "10", "0");
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        filteredProduct = Product.fromJson(res);
        pageState = true;
        pageController.animateToPage(1,
            duration: Duration(milliseconds: 200), curve: Curves.decelerate);
      });
    }
  }

  getMoreProducts() async {
    print(catId);
    print(subCatId);
    MIN += MAX;
    tempProducts = null;
    var res = await requests.getFilteredProducts(
        "Instock", catId, subCatId, MAX.toString(), MIN.toString());
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        tempProducts = Product.fromJson(res);
        if(tempProducts.info.data.isEmpty)
          showToast("No more products", Colors.black);
        filteredProduct.info.data.addAll(tempProducts.info.data);
        loadMore = false;
        pageState = true;
        pageController.animateToPage(1,
            duration: Duration(milliseconds: 200), curve: Curves.decelerate);
      });
    }else {
      setState(() {
        loadMore = false;
      });
    }
  }

  addProductsToInventory() async {
    var res = await requests.addProductToInventoryNew(products.values.toList());
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Fluttertoast.showToast(
          msg: "Inventory Updated.",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
      setState(() {
        products = {};
        filteredProduct = null;
      });
      if (temp != selectedChip) {
        selectedOption(temp);
      } else
        getProdcuts();
    }
  }

  getProductCount() async {
    var res = await requests.getProductCount();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        productCount = res['info']['system_inventory'].toString();
      });
    }
  }

  setPageState() {
    setState(() {});
  }

  Widget showMore() {
    return GestureDetector(
      onTap: () {
        setState(() {
          loadMore = true;
        });
        getMoreProducts();
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        padding: EdgeInsets.all(16),
        child: loadMore
            ? Text(
                "Loading...",
                textAlign: TextAlign.center,
              )
            : Text(
                "Show more",
                textAlign: TextAlign.center,
              ),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    getProductCategories();
    getProductCount();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Add Product",
              style: TextStyle(fontSize: 13),
            ),
            Text(
              storeName,
              style: TextStyle(color: mColor, fontSize: 14),
            ),
          ],
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              Get.to(SearchMasterProduct());
            },
          )
        ],
      ),
      body: loading ? loadingCircular() : Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Container(
              height: 30,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: productCategory.info.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      selectedOption(index);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: selectedChip == index ? Colors.black : Colors
                              .grey,
                          borderRadius: BorderRadius.circular(8)
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      margin: EdgeInsets.symmetric(horizontal: 4),
                      child: Center(child: Text("${productCategory.info[index]
                          .prodCategoryName} (${productCategory.info[index]
                          .productCount})", style: TextStyle(fontSize: 11,
                          color: selectedChip == index ? Colors.white : Colors
                              .black),)),
                    ),
                  );
                },
              ),
            ),
          ),
          !pageState ? Container(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4.0, vertical: 4),
              child: Text("Available Inventory: ${productCount == null
                  ? "--"
                  : productCount}", style: regularText,),
            ),
          ) : GestureDetector(
              onTap: () {
                setState(() {
                  MIN = 0;
                  pageState = false;
                });
                pageController.animateToPage(
                    0, duration: Duration(milliseconds: 200),
                    curve: Curves.decelerate);
              },
              child: SizedBox(
                  height: 30, child: Icon(Icons.keyboard_backspace))),
          Expanded(
            child: PageView(
              physics: NeverScrollableScrollPhysics(),
              scrollDirection: Axis.horizontal,
              controller: pageController,
              children: <Widget>[
                myBrandList(),
                filteredProduct == null ? SizedBox() :
                filteredProduct.info.data.isEmpty ? Center(
                  child: Text("No Products.", style: subHeading,),) :
                ListView.separated(
                    padding: EdgeInsets.only(bottom: 70),
                    separatorBuilder: (context, index) {
                      return SizedBox(height: 8,);
                    },
                    itemCount: filteredProduct.info.data.length < MIN
                        ? filteredProduct.info.data.length
                        : filteredProduct.info.data.length + 1,
                    itemBuilder: (context, index) {
                      return index == filteredProduct.info.data.length
                          ? showMore()
                          : AddProductCard(filteredProduct.info.data[index]);
                    }
                )
              ],
            ),
          )
        ],
      ),

      floatingActionButton: products.values.length == 0
          ? SizedBox()
          : FloatingActionButton.extended(
          onPressed: () {
            addProductsToInventory();
          },
          label: Text("Update (${products.length})")
      ),
    );
  }

  myBrandList() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView.separated(
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: 5,
                );
              },
              itemCount: productSubCategory.info.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      subCatId = productSubCategory.info[index].subcatId
                          .toString();
//                      filteredProduct = null;
                    });
                    getProdcuts();
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0)),
                    elevation: 2.0,
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(color: mColor, width: 4))),
                      child: ListTile(
                        title: Text("${productSubCategory.info[index]
                            .name}(${productSubCategory.info[index]
                            .productCount})", style: defaultTextStyle,),
                        trailing: Icon(Icons.arrow_forward_ios),
                      ),
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class buildItemList extends StatefulWidget {

  @override
  _buildItemListState createState() => _buildItemListState();
}

class _buildItemListState extends State<buildItemList> {

  Container itemCard = Container(
    child: Row(
      children: <Widget>[
        Expanded(
          child: Container(
            height: 100,
            width: 100,
            color: Colors.deepPurple,
          ),
        ),
        Expanded(
          flex: 3,
          child: Container(
            padding: EdgeInsets.all(4),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Product name with max 2 lines hgjhgj hgj gjgjhgjhg gjh", overflow: TextOverflow.ellipsis, maxLines: 2,),
                SizedBox(height: 5,),
                Text("100 Gm"),
                Row(
                  children: <Widget>[
                    Expanded(child: Text("Mrp: Rs.49")),
                    Container(
                      height: 40,
                      width: 120,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(color: Colors.black)
                      ),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: IconButton(
                              onPressed: (){},
                              icon: Icon(Icons.remove),
                            ),
                          ),
                          Expanded(
                              child: Center(child: Text("0"))
                          ),
                          Expanded(
                            child: IconButton(
                              onPressed: (){},
                              icon: Icon(Icons.add),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        )
      ],
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 2.0,
        child: ListView.separated(
            physics: ScrollPhysics(),
            itemCount: 10,
            shrinkWrap: true,
            separatorBuilder: (context, index){
              return Divider();
            },
            itemBuilder: (context, index){
              return itemCard;
            }
        )
    );
  }
}

