import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:merchant/ChoosInventory.dart';
import 'package:merchant/Components.dart';
import 'package:merchant/Components/ProductCard.dart';
import 'package:merchant/Dashboard.dart';
import 'package:merchant/Getter/GetCollectionInfo.dart';
import 'package:merchant/HomePage/HomePage.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:get/get.dart';

import 'constants.dart';

class CollectionSetting extends StatefulWidget {
  String collectionId;

  CollectionSetting(this.collectionId);

  @override
  _CollectionSettingState createState() => _CollectionSettingState();
}

class _CollectionSettingState extends State<CollectionSetting> {


  var cDescription = TextEditingController();
  var nameController = TextEditingController();
  var spController = TextEditingController();
  var comboController = TextEditingController();
  String cNameError, cDescError, cId;
  double sellingPrice;
  int comboPacks;
  DateTime sDate, sTime, cDate, cTime;
  String startDate = "DD/MM/YYYY",
      startTime = "HH:MM",
      closeDate = "DD/MM/YYYY",
      closeTime = "HH:MM";
  HttpRequests requests = HttpRequests();
  CollectionInfo collectionInfo;
  bool loading = true;

  getCollectionInfo() async {
    var res = await requests.getOfferInfo(widget.collectionId);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      collectionInfo = CollectionInfo.fromJson(res);
      initControllers();
    }
  }

  initControllers() {
    setState(() {
      nameController.text = collectionInfo.info[0].collectionName;
      cDescription.text = collectionInfo.info[0].collectionDescr;
      loading = false;
    });
  }

  setCollectionSetting() async {
    print("SELLING PRICE: $sellingPrice || PACKS: $comboPacks");
    var res = await requests.addProductToCollection(
        widget.collectionId,
        comboPacks.toString(),
        chooseInventoryState.products.values.toList(),
        sellingPrice.toString());
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      publish();
      print("publishing...");
    }
  }

  publish() async {
    print("$startDate $startTime || $closeDate $closeTime");
    var res = await requests.publishCollection(
        widget.collectionId,
        "$startDate $startTime",
        "$closeDate $closeTime");
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Fluttertoast.showToast(msg: "Collection Published",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
      Get.offAll(HomePage());
    }
  }

  validateDetails() {
    if (nameController.text == "" || nameController.text.length < 3) {
      Fluttertoast.showToast(msg: "Invalid Detail: Collection Name");
    } else if (cDescription.text == "" || cDescription.text.length < 3) {
      Fluttertoast.showToast(msg: "Invalid Detail: Collection Name");
    } else if (sellingPrice == null || sellingPrice < 1) {
      Fluttertoast.showToast(msg: "Invalid Detail: Selling Price cannot be 0");
    } else if (comboPacks == null || comboPacks < 1) {
      Fluttertoast.showToast(
          msg: "Invalid Detail: Combo Packs cannot be less then 1");
    } else if (sDate == null || sTime == null) {
      Fluttertoast.showToast(msg: "Invalid Detail: Select Start Date & Time");
    } else if (cDate == null || cTime == null) {
      Fluttertoast.showToast(msg: "Invalid Detail: Select End Date & Time");
    } else
      setCollectionSetting();
  }

  @override
  void initState() {
    // TODO: implement initState
    getCollectionInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: textBoxFillColor,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Create Collection",
              style: TextStyle(fontSize: 13),
            ),
            Text(
              "Rahul Test",
              style: TextStyle(color: mColor, fontSize: 17),
            ),
          ],
        ),
        centerTitle: true,
      ),

      body: loading ? loadingCircular() : SingleChildScrollView(
        padding: EdgeInsets.only(bottom: 60),
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              padding: EdgeInsets.only(left: 2, right: 2),
              child: CarouselSlider(
                height: 200,
                initialPage: 0,
                enlargeCenterPage: true,
                pauseAutoPlayOnTouch: Duration(seconds: 5),
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                autoPlayCurve: Curves.fastOutSlowIn,
                autoPlay: true,
                autoPlayInterval: Duration(seconds: 3),
                enableInfiniteScroll: true,
                items: collectionInfo.info[0].collectionImages.map((e) =>
                    ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: FadeInImage.assetNetwork(
                      width: 180,
                      height: 180,
                      fit: BoxFit.contain,
                      placeholder: 'assets/tsp.png',
                      image: e.imageUrl.toString()),
                    )).toList(),
              ),
            ),

            SizedBox(
              height: 10,
            ),
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0)
              ),
              margin: EdgeInsets.all(0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Collection Name",
                      textAlign: TextAlign.left,
                      textScaleFactor: 1,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      controller: nameController,
                      enabled: true,
                      decoration: InputDecoration(
                          errorText: cNameError,
                          isDense: true,
                          hasFloatingPlaceholder: true,
                          hintText: "Enter collection name",
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black))),
                    ),
                    SizedBox(height: 10,),
                    Text(
                      "About Collection",
                      textAlign: TextAlign.left,
                      textScaleFactor: 1,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      controller: cDescription,
                      enabled: true,
                      maxLines: 3,
                      maxLength: 150,
                      decoration: InputDecoration(
                          errorText: cDescError,
                          isDense: true,
                          hasFloatingPlaceholder: true,
                          hintText: "Write short description about the store",
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black))),
                    ),
                  ],
                ),
              ),
            ),

            SizedBox(
              height: 10,
            ),

            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0)
              ),
              margin: EdgeInsets.all(0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Product Added",
                      textAlign: TextAlign.left,
                      textScaleFactor: 1,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Divider(),
                    ListView.separated(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        separatorBuilder: (context, index) {
                          return Divider();
                        },
                        itemBuilder: (context, index) {
                          return CollectionProductCard(collectionInfo.info[0]
                              .collectionProducts[index]);
                        },
                        itemCount: collectionInfo.info[0].collectionProducts
                            .length
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 10,),
            Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0)
              ),
              margin: EdgeInsets.all(0),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Create Offer Running Price, Quantity, Date & Time",
                      textAlign: TextAlign.left,
                      textScaleFactor: 1,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Divider(),
                    SizedBox(height: 5,),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black),
                                borderRadius: BorderRadius.circular(12)
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Selling Price",
                                    textAlign: TextAlign.left,
                                    textScaleFactor: 1,
                                    style: TextStyle(fontWeight: FontWeight.bold),
                                  ),
//                          SizedBox(height: 5,),
                                  TextFormField(
                                    keyboardType: TextInputType
                                        .numberWithOptions(),
                                    enabled: true,
                                    controller: spController,
                                    onChanged: (val) {
                                      sellingPrice = double.tryParse(
                                          spController.text.toString());
                                      print(sellingPrice);
                                    },
                                    inputFormatters: onlyNumberInputFormatter,
                                    decoration: InputDecoration(
                                        contentPadding: EdgeInsets.symmetric(
                                            vertical: 8),
                                        isDense: true,
                                        hasFloatingPlaceholder: true,
                                        hintText: "Enter selling price here...",
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.white)),
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.white)),
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.white)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.white))),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          flex: 1,
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black),
                                borderRadius: BorderRadius.circular(12)
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "No. of combo packs",
                                    textAlign: TextAlign.left,
                                    textScaleFactor: 1,
                                    style: TextStyle(fontWeight: FontWeight.bold),
                                  ),
//                          SizedBox(height: 5,),
                                  TextFormField(
                                    enabled: true,
                                    keyboardType: TextInputType
                                        .numberWithOptions(),
                                    controller: comboController,
                                    onChanged: (val) {
                                      comboPacks =
                                          int.tryParse(comboController.text);
                                      print(comboPacks);
                                    },
                                    inputFormatters: onlyNumberInputFormatter,
                                    decoration: InputDecoration(
                                        contentPadding: EdgeInsets.symmetric(
                                            vertical: 8),
                                        isDense: true,
                                        hasFloatingPlaceholder: true,
                                        hintText: "Enter value here...",
                                        disabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.white)),
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.white)),
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.white)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.white))),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 15,),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black),
                                borderRadius: BorderRadius.circular(12)
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Start Data & Time",
                                    textAlign: TextAlign.left,
                                    textScaleFactor: 1,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(height: 5,),
                                  GestureDetector(
                                    onTap: () {
                                      DatePicker.showDatePicker(
                                          context, onConfirm: (time) {
                                        if (time.isBefore(
                                            DateTime.now().subtract(
                                                Duration(days: 1)))) {
                                          Fluttertoast.showToast(
                                              msg: "Please select correct date");
                                        } else {
                                          setState(() {
                                            startDate =
                                                DateFormat('yyyy-MM-dd').format(
                                                    time);
                                            sDate = time;
                                          });
                                        }
                                      });
                                    },
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment
                                          .start,
                                      children: <Widget>[
                                        Icon(
                                            Icons.calendar_today, color: mColor,
                                            size: 20),
                                        SizedBox(width: 5,),
                                        Text(startDate)
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: 5,),
                                  GestureDetector(
                                    onTap: () {
                                      DatePicker.showTime12hPicker(
                                          context, onConfirm: (time) {
                                        if (time.isBefore(
                                            DateTime.now().subtract(
                                                Duration(minutes: 1)))) {
                                          Fluttertoast.showToast(
                                              msg: "Please select correct time");
                                        } else {
                                          setState(() {
                                            startTime =
                                                DateFormat('hh:mm').format(
                                                    time);
                                            sTime = time;
                                          });
                                        }
                                      });
                                    },
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment
                                          .start,
                                      children: <Widget>[
                                        Icon(Icons.timer, color: mColor,
                                          size: 20,),
                                        SizedBox(width: 5,),
                                        Text(startTime)
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          child: IgnorePointer(
                            ignoring: sDate == null || sTime == null
                                ? true
                                : false,
                            child: Container(
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black),
                                  borderRadius: BorderRadius.circular(12)
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "End Date & Time",
                                      textAlign: TextAlign.left,
                                      textScaleFactor: 1,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(height: 5,),
                                    GestureDetector(
                                      onTap: () {
                                        DatePicker.showDatePicker(
                                            context, onConfirm: (time) {
                                          if (time.isAfter(sDate)) {
                                            setState(() {
                                              closeDate =
                                                  DateFormat('yyyy-MM-dd')
                                                      .format(time);
                                              cDate = time;
                                            });
                                          } else if (time.isBefore(sDate)) {
                                            Fluttertoast.showToast(
                                                msg: "End Date & Time should be after Start Date");
                                          }
                                        });
                                      },
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .start,
                                        children: <Widget>[
                                          Icon(Icons.calendar_today,
                                              color: mColor, size: 20),
                                          SizedBox(width: 5,),
                                          Text(closeDate)
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 5,),
                                    GestureDetector(
                                      onTap: () {
                                        DatePicker.showTime12hPicker(
                                            context, onConfirm: (time) {
                                          setState(() {
                                            closeTime =
                                                DateFormat('hh:mm').format(
                                                    time);
                                            cTime = time;
                                          });
                                        });
                                      },
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment
                                            .start,
                                        children: <Widget>[
                                          Icon(Icons.timer, color: mColor,
                                            size: 20,),
                                          SizedBox(width: 5,),
                                          Text(closeTime)
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),

            SizedBox(height: 20,),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    child: ButtonTheme(
                      height: 50,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25),
                          side: BorderSide(color: Colors.black)
                      ),
                      child: RaisedButton(
                        elevation: 8,
                        onPressed: (){},
                        color: Colors.white,
                        textColor: Colors.black,
                        child: Text("Cancel", style: TextStyle(fontWeight: FontWeight.bold)),
                      ),
                    ),
                  ),
                  SizedBox(width: 5,),
                  Expanded(
                    child: ButtonTheme(
                      height: 50,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25),
                      ),
                      child: RaisedButton(
                        elevation: 8,
                        onPressed: () {
                          validateDetails();
                        },
                        color: Colors.black,
                        textColor: Colors.white,
                        child: Text("Publish", style: TextStyle(
                            fontWeight: FontWeight.bold),),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
