import 'package:flutter/material.dart';

class AddDeliveryBoy extends StatefulWidget {
  @override
  _AddDeliveryBoyState createState() => _AddDeliveryBoyState();
}

class _AddDeliveryBoyState extends State<AddDeliveryBoy> {
  var nameController = TextEditingController();
  var numberController = TextEditingController();
  var emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Add Delivery Boy"),
      ),
      body: Container(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                ClipOval(
                  child: Container(
                    color: Colors.grey,
                    height: 150,
                    width: 150,
                    child: Center(
                      child: Text("+Add Photo"),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller: nameController,
                  enabled: true,
                  decoration: InputDecoration(
                    isDense: true,
                    hasFloatingPlaceholder: true,
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black)),
                    labelText: "Name",
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller: numberController,
                  enabled: true,
                  decoration: InputDecoration(
                    isDense: true,
                    hasFloatingPlaceholder: true,
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black)),
                    labelText: "Enter Mobile Number",
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                TextFormField(
                  controller: emailController,
                  enabled: true,
                  decoration: InputDecoration(
                    isDense: true,
                    hasFloatingPlaceholder: true,
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black)),
                    labelText: "E-mail",
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {},
          label: Row(
            children: <Widget>[Text("NEXT"), Icon(Icons.arrow_forward)],
          )),
    );
  }
}
