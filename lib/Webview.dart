import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:flutter_html/flutter_html.dart';

import 'constants.dart';

class TnC extends StatefulWidget {
  @override
  _TnCState createState() => _TnCState();
}

class _TnCState extends State<TnC> {
  HttpRequests requests = HttpRequests();
  String pString;
  bool loading = true;

  getTnC() async {
    var res = await requests.getTnC();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        loading = false;
        pString = res['info'][0]['text'].toString();
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getTnC();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Terms & Conditions"),
        ),
        body: loading
            ? loadingCircular()
            : SingleChildScrollView(
          child: Html(
            data: pString,
            shrinkWrap: true,
          ),
        ));
  }
}

class PrivacyPolicy extends StatefulWidget {
  @override
  _PrivacyPolicyState createState() => _PrivacyPolicyState();
}

class _PrivacyPolicyState extends State<PrivacyPolicy> {
  HttpRequests requests = HttpRequests();
  String pString;
  bool loading = true;

  getTnC() async {
    var res = await requests.getPrivacyPolicy();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        loading = false;
        pString = res['info'][0]['text'].toString();
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getTnC();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Privacy Policy"),
        ),
        body: loading
            ? loadingCircular()
            : SingleChildScrollView(
                child: Html(
                  data: pString,
                  shrinkWrap: true,
                ),
              ));
  }
}

class FAQ extends StatefulWidget {
  @override
  _FAQState createState() => _FAQState();
}

class _FAQState extends State<FAQ> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  StreamSubscription<double> _onProgressChanged;
  final _history = [];
  bool loading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _onProgressChanged =
        flutterWebViewPlugin.onProgressChanged.listen((double progress) {
      if (mounted) {
        setState(() {
          print("Progress: $progress");
          _history.add('onProgressChanged: $progress');
          if (progress == 1.0) loading = false;
        });
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _onProgressChanged.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: loading
              ? Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 15,
                      width: 15,
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        backgroundColor: Colors.white,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text("Loading..."),
                  ],
                )
              : Text("FAQ"),
        ),
        body: WebviewScaffold(
          url: "https://www.neomart.com/frontend/web/index.php/site/data",
          withJavascript: true,
          withZoom: true,
        ));
  }
}

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  StreamSubscription<double> _onProgressChanged;
  final _history = [];
  bool loading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _onProgressChanged =
        flutterWebViewPlugin.onProgressChanged.listen((double progress) {
      if (mounted) {
        setState(() {
          print("Progress: $progress");
          _history.add('onProgressChanged: $progress');
          if (progress == 1.0) loading = false;
        });
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _onProgressChanged.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: loading
              ? Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 15,
                      width: 15,
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        backgroundColor: Colors.white,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text("Loading..."),
                  ],
                )
              : Text("Contact Us"),
        ),
        body: WebviewScaffold(
          url: "https://www.neomart.com/frontend/web/index.php/site/contactusdata",
          withJavascript: true,
          withZoom: true,
        ));
  }
}

class CancellationAndRefund extends StatefulWidget {
  @override
  _CancellationAndRefundState createState() => _CancellationAndRefundState();
}

class _CancellationAndRefundState extends State<CancellationAndRefund> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  StreamSubscription<double> _onProgressChanged;
  final _history = [];
  bool loading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _onProgressChanged =
        flutterWebViewPlugin.onProgressChanged.listen((double progress) {
      if (mounted) {
        setState(() {
          print("Progress: $progress");
          _history.add('onProgressChanged: $progress');
          if (progress == 1.0) loading = false;
        });
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _onProgressChanged.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: loading
              ? Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 15,
                      width: 15,
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        backgroundColor: Colors.white,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text("Loading..."),
                  ],
                )
              : Text("Cancellation & Refund"),
        ),
        body: WebviewScaffold(
          url: "https://www.neomart.com/frontend/web/index.php/site/returns_user",
          withJavascript: true,
          withZoom: true,
        ));
  }
}

class SaleSummary extends StatefulWidget {
  @override
  _SaleSummaryState createState() => _SaleSummaryState();
}

class _SaleSummaryState extends State<SaleSummary> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  StreamSubscription<double> _onProgressChanged;
  final _history = [];
  bool loading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _onProgressChanged =
        flutterWebViewPlugin.onProgressChanged.listen((double progress) {
          if (mounted) {
            setState(() {
              print("Progress: $progress");
              _history.add('onProgressChanged: $progress');
              if (progress == 1.0) loading = false;
            });
          }
        });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _onProgressChanged.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: loading
              ? Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 15,
                width: 15,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  backgroundColor: Colors.white,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text("Loading..."),
            ],
          )
              : Text("Sales Summary"),
        ),
        body: WebviewScaffold(
          url: "https://wwww.neomart.com/frontend/web/index.php/paymentxns/index?id=$storeid",
          withJavascript: true,
          withZoom: true,
        ));
  }
}

