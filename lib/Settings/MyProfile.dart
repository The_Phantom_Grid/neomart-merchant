import 'dart:io';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:merchant/Components/ImagePick.dart';
import 'package:merchant/Components/VariableControllers.dart';

import '../SearchLocation.dart';
import '../constants.dart';

class MyProfile extends StatefulWidget {
  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {

  File image;
  String profileImage, fullName, eMail, locality;
  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var localityController = TextEditingController();
  var numberController = TextEditingController();
  String nameError, emailError, addressError, numberError, base64ImageString,
      currentImage;
  var status;
  bool loading = false,
      ignore = true;
  var ratio = [
    CropAspectRatioPreset.square,
    CropAspectRatioPreset.ratio3x2,
    CropAspectRatioPreset.original,
    CropAspectRatioPreset.ratio4x3,
    CropAspectRatioPreset.ratio16x9
  ];
  double lat = 28.4894676,
      lng = 77.1715731;
  List<Address> address;

//  getImageFromCamera() async {
//    var i = await ImagePicker.pickImage(source: ImageSource.camera);
//    setState(() {
//      if (i != null) image = i;
//    });
//  }
//
//  getImageFromGallery() async {
//    var i = await ImagePicker.pickImage(source: ImageSource.gallery);
//    setState(() {
//      if (i != null) image = i;
//    });
//  }

  validateInfo() {
    setState(() {
      print("Validating...");
      if (nameController.text.isEmpty)
        nameError = "Name Field cannot be empty!";
      else if (emailController.text.isEmpty ||
          !EmailValidator.validate(emailController.text))
        emailError = "Invalid email format!";
      else if (numberController.text.isEmpty)
        numberError = "Invalid email format!";
      else if (localityController.text.isEmpty)
        addressError = "Enter your locality";
      else if (currentImage == null)
        Fluttertoast.showToast(msg: "Upload Profile Image");
      else {
        setState(() {
          ignore = true;
        });
        updateProfile();
      }
    });
  }

  openBottomSheet() {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (context) {
          return Container(
              height: MediaQuery.of(context).size.height * .2,
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      image = await getImage(ImageSource.camera, ratio);
                      if (image != null) {
                        base64ImageString = convertImageToBase64(image);
                      }
                      setState(() {});
                    },
                    leading: Icon(Icons.camera_alt),
                    title: Text("Camera"),
                  ),
                  ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      image = await getImage(ImageSource.gallery, ratio);
                      if (image != null) {
                        base64ImageString = convertImageToBase64(image);
                      }
                      setState(() {});
                    },
                    leading: Icon(Icons.photo_library),
                    title: Text("Gallery"),
                  ),
                ],
              ));
        });
  }

  initControllers() async {
    setState(() {
      nameController.text = userProfile.profileData[0].firstname.toString();
      numberController.text = userProfile.profileData[0].mobile.toString();
      emailController.text =
          userProfile.profileData[0].email.toString() == "null"
              ? ""
              : userProfile.profileData[0].email.toString();
      localityController.text = userProfile.profileData[0].city.toString();
      lat = userProfile.profileData[0].lat;
      lng = userProfile.profileData[0].lng;
    });
    currentImage = await getImageFromNetwork(
        "${userProfile.profileData[0].avatarBaseUrl}${userProfile.profileData[0].avatarPath}");
    setState(() {
      ignore = false;
    });
  }

  updateProfile() async {
    var res = await requests.updateMerchantProfile(
        nameController.text.toString(),
        emailController.text.toString(),
        numberController.text.toString(),
        lat.toString(),
        lng.toString(),
        localityController.text.toString(),
        base64ImageString != null ? base64ImageString : currentImage);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Fluttertoast.showToast(msg: "Profile updated");

      await getMerchantProfile();
      setState(() {
        ignore = false;
        loading = false;
      });
    }
  }

  updateDetail() {
    setState(() {
      localityController.text =
          address.first.subLocality + ", " + address.first.adminArea;
      lat = address.first.coordinates.latitude;
      lng = address.first.coordinates.longitude;
    });
  }

  @override
  void initState() {
    // TODO: implemexnt initState
    initControllers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: ignore,
      child: Scaffold(
          appBar: AppBar(
            title: Text(
              "My Account",
            ),
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            child: Center(
                child: Container(
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Column(
                            children: <Widget>[
                              Stack(
                                children: <Widget>[
                                  Hero(
                                      tag: 'neomart_logo',
                                      child: Container(
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            border: Border.all(
                                                color: Colors.black)),
                                        width: 200,
                                        height: 200,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(
                                              100),
                                          child: image != null ? Image.file(
                                              image) : FadeInImage.assetNetwork(
                                              width: 180,
                                              height: 180,
                                              fit: BoxFit.cover,
                                              placeholder: 'assets/tsp.png',
                                              image: "${userProfile
                                                  .profileData[0]
                                                  .avatarBaseUrl}${userProfile
                                                  .profileData[0].avatarPath}"),
                                        ),
                                      )
                                  ),
                                  Positioned(
                                    bottom: 0,
                                    right: 15,
                                    child: GestureDetector(
                                      onTap: openBottomSheet,
                                      child: CircleAvatar(
                                        radius: 25,
                                        child: Icon(Icons.camera_alt),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              TextFormField(
                                controller: nameController,
//                                    "${profile.prifiledata[0].firstname}",
                                enabled: true,
                                onChanged: (val) {
                                  setState(() {
                                    nameError = null;
                                  });
                                },
                                decoration: InputDecoration(
                                  errorText: nameError,
                                  isDense: true,
                                  hasFloatingPlaceholder: true,
                                  border: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: mColor, width: 5.0)),
                                  labelText: "Name",
                                ),
                              ),
                              SizedBox(
                                height: 25,
                              ),
                              TextFormField(
                                style: regularText,
//                                initialValue: "${profile.prifiledata[0].email}",
                                enabled: true,
                                controller: numberController,
                                onChanged: (val) {
                                  setState(() {
                                    numberError = null;
                                  });
                                },
                                decoration: InputDecoration(
                                  errorText: numberError,
                                  isDense: true,
                                  hasFloatingPlaceholder: true,
                                  border: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: mColor, width: 5.0)),
                                  labelText: "Phone Number",
                                ),
                              ),
                              SizedBox(
                                height: 25,
                              ),
                              TextFormField(
                                style: regularText,
//                                initialValue: "${profile.prifiledata[0].email}",
                                enabled: true,
                                controller: emailController,
                                onChanged: (val) {
                                  setState(() {
                                    emailError = null;
                                  });
                                },
                                decoration: InputDecoration(
                                  errorText: emailError,
                                  isDense: true,
                                  hasFloatingPlaceholder: true,
                                  border: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: mColor, width: 5.0)),
                                  labelText: "E-mail address",
                                ),
                              ),
                              SizedBox(
                                height: 25,
                              ),
                              TextFormField(
                                controller: localityController,
                                //                                initialValue: "${profile.prifiledata[0].city}",
                                onChanged: (val) {
                                  setState(() {
                                    addressError = null;
                                  });
                                },
                                onTap: () async {
                                  address = await Navigator.push(context,
                                      MaterialPageRoute(builder: (context) =>
                                          SearchLocation()));
                                  if (address != null)
                                    updateDetail();
                                },
                                enabled: true,
                                decoration: InputDecoration(
                                    errorText: addressError,
                                    isDense: true,
                                    hasFloatingPlaceholder: true,
                                    border: OutlineInputBorder(
                                        borderSide:
                                        BorderSide(color: mColor, width: 5.0)),
                                    labelText: "Your Locality",
                                    enabled: true),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: ButtonTheme(
                              minWidth: double.infinity,
                              height: 50.0,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(27)),
                              child: IgnorePointer(
                                ignoring: ignore,
                                child: RaisedButton(
                                  child: loading
                                      ? Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Container(
                                        height: 15,
                                        width: 15,
                                        child: CircularProgressIndicator(
                                          strokeWidth: 2,
                                          backgroundColor: Colors.white,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text("Updating Profile..."),
                                    ],
                                  )
                                      : Text("UPDATE"),
                                  onPressed: () {
                                    validateInfo();
                                  },
                                  textColor: Colors.white,
                                  color: Colors.black,
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                )),
          )),
    );
  }
}
