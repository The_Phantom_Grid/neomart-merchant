//import 'package:flushbar/flushbar.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:merchant/Getter/GetReferral.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/constants.dart';
import 'package:wc_flutter_share/wc_flutter_share.dart';

class ReferralPage extends StatefulWidget {
  @override
  _ReferralPageState createState() => _ReferralPageState();
}

class _ReferralPageState extends State<ReferralPage> {
  var mColor = Color(0xFFfc0c5b);
  String rCode;
  String referUrl = "https://www.neomart.com/frontend/web/put?c=";
  int paytmNo;
  HttpRequests requests = HttpRequests();
  Referral referral;
  bool loading = true;

  Copy2Clipboard() {
    Clipboard.setData(ClipboardData(
        text: "http://nukkadse.com/frontend/web/put?c=nsmrahul9272"));
    print("copied");
    Flushbar(
      message: "Referral Link Copied",
      duration: Duration(seconds: 2),
    )..show(context);
  }

  getReferral() async {
    var res = await requests.getReferralInfo();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      referral = Referral.fromJson(res);
      setState(() {
        loading = false;
      });
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  void initState() {
    getReferral();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Refer & Earn",
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: loading || referral == null ? loadingCircular() : Center(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: <Widget>[
              Icon(
                Icons.monetization_on,
                size: 100,
              ),
              SizedBox(
                height: 10,
              ),
              Card(
                  elevation: 0,
                  child: Container(
                    padding: EdgeInsets.all(16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SizedBox(
                          width: 50,
                        ),
                        GestureDetector(
                          onTap: Copy2Clipboard,
                          child: Column(
                            children: <Widget>[
                              Text(
                                referral.data[0].referalCode,
                                style: TextStyle(color: mColor, fontSize: 20),
                              ),
                              Text("Tap to copy", style: subHeading,)
                            ],
                          ),
                        ),
                        IconButton(
                          icon: Icon(Icons.share),
                          onPressed: () async {
                            print("hit..");
                            WcFlutterShare.share(
                                sharePopupTitle: 'Share',
                                mimeType: 'text/plain',
                                text:
                                "Click on the url and download the app to enjoy our services.\n"
                                    "$referUrl$rCode",
                                subject: ""
                            );
                          },
                        )
                      ],
                    ),
                  )),
              Card(
                elevation: 0,
                child: Padding(
                  padding: const EdgeInsets.only(top: 8, bottom: 8),
                  child: Column(
                    children: <Widget>[
                      ListTile(
                        leading: Icon(
                          Icons.arrow_right,
                          color: mColor,
                        ),
                        title: Text(
                          "Refer and Earn Rs. 50/- for each consumer Referral on the Neomart Platform",
                          style: regularText,),
                      ),
                      ListTile(
                          leading: Icon(
                            Icons.arrow_right,
                            color: mColor,
                          ),
                          title: Text(
                            "The Refer & Earn program will be stopped once 25 Referrals are made.",
                            style: regularText,))
                    ],
                  ),
                ),
              ),
              Card(
                elevation: 0,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SizedBox(
                        width: 50,
                      ),
                      Column(
                        children: <Widget>[
                          Text(
                            referral.data[0].paytmNo,
                            style: TextStyle(color: mColor, fontSize: 20),
                          ),
                          Text("Your Paytm Number", style: subHeading,)
                        ],
                      ),
                      SizedBox(
                        width: 50,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
