import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:merchant/Getter/GetPaytmConfig.dart';
import 'package:merchant/Network/httpRequests.dart';

import '../constants.dart';

class IntegratePaymentMethod extends StatefulWidget {
  @override
  _IntegratePaymentMethodState createState() => _IntegratePaymentMethodState();
}

class _IntegratePaymentMethodState extends State<IntegratePaymentMethod> {
  TextEditingController merchantKeyController = TextEditingController();
  TextEditingController midController = TextEditingController();
  TextEditingController channelController = TextEditingController();
  TextEditingController industryTypeController = TextEditingController();

  String merchantKeyError = "";
  String midError = "";
  String channelError = "";
  String industryTypeError = "";

  PaytmConfig paytmConfig;
  HttpRequests requests = HttpRequests();

  bool loading = false;

  getPaytmConfig() async {
    var res = await requests.getPaytmConfig();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      paytmConfig = PaytmConfig.fromJson(res);
      initControllers();
    }
  }

  updateDetails() async {
    var res = await requests.updateMerchantSettings(
        channelController.text.toString(),
        industryTypeController.text.toString(),
        midController.text.toString(),
        merchantKeyController.text.toString());
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        loading = false;
        showToast("Success", Colors.black);
      });
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  initControllers() {
    setState(() {
      merchantKeyController.text =
      paytmConfig.info.merchantKey == "null" ? "" : paytmConfig.info
          .merchantKey;
      midController.text =
          paytmConfig.info.mId == "null" ? "" : paytmConfig.info.mId;
      channelController.text = paytmConfig.info.channelId == "null"
          ? ""
          : paytmConfig.info.channelId;
      industryTypeController.text = paytmConfig.info.industryTypeId == "null"
          ? ""
          : paytmConfig.info.industryTypeId;
    });
  }

  validate() {
    if (merchantKeyController.text == "")
      showToast("Invalid Detail: Merchant Key", Colors.black);
    else if (midController.text == "")
      showToast("Invalid Detail: MID", Colors.black);
    else if (channelController.text == "")
      showToast("Invalid Detail: Channel", Colors.black);
    else if (industryTypeController.text == "")
      showToast("Invalid Detail: Industry Type", Colors.black);
    else {
      setState(() {
        loading = true;
      });
      updateDetails();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getPaytmConfig();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: loading,
      child: Scaffold(
          appBar: AppBar(
            title: Text(
              "Paytm Configuration",
            ),
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            child: Center(
                child: Container(
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Merchant Key",
                            style: h1,
                          ),
                          TextFormField(
                            controller: merchantKeyController,
                            decoration: InputDecoration(
                              errorText: merchantKeyError,
                              isDense: true,
                              hasFloatingPlaceholder: true,
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: mColor, width: 5.0)),
                              hintText: "Enter merchant key",
                            ),
                          ),
                          Text(
                            "MID",
                            style: h1,
                          ),
                          TextFormField(
//                                initialValue: "${profile.prifiledata[0].email}",
                            controller: midController,
                            decoration: InputDecoration(
                              errorText: midError,
                              isDense: true,
                              hasFloatingPlaceholder: true,
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: mColor, width: 5.0)),
                              hintText: "Enter MID",
                            ),
                          ),
                          Text(
                            "Channel",
                            style: h1,
                          ),
                          TextFormField(
//                                initialValue: "${profile.prifiledata[0].email}",
                            enabled: true,
                            controller: channelController,
                            decoration: InputDecoration(
                              errorText: channelError,
                              isDense: true,
                              hasFloatingPlaceholder: true,
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: mColor, width: 5.0)),
                              hintText: "Enter channel",
                            ),
                          ),
                          Text(
                            "Industry Type",
                            style: h1,
                          ),
                          TextFormField(
                            controller: industryTypeController,
//                                initialValue: "${profile.prifiledata[0].city}",
                            enabled: true,
                            decoration: InputDecoration(
                                errorText: industryTypeError,
                                isDense: true,
                                hasFloatingPlaceholder: true,
                                border: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: mColor, width: 5.0)),
                                hintText: "Enter industry type",
                                enabled: true),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: ButtonTheme(
                          minWidth: 180,
                          height: 45.0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(27)),
                          child: RaisedButton(
                            child: loading
                                ? SizedBox(
                                    height: 25,
                                    width: 25,
                                    child: SpinKitCircle(
                                      color: Colors.white,
                                      size: 25,
                                    ))
                                : Text("UPDATE"),
                            onPressed: () {
//                                updateDetails();
                              validate();
                            },
                            textColor: Colors.white,
                            color: Colors.black,
                          )),
                    )
                  ],
                ),
              ),
            )),
          )),
    );
  }
}
