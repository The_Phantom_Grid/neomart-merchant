// To parse this JSON data, do
//
//     final orderCountByStatus = orderCountByStatusFromJson(jsonString);

import 'dart:convert';

OrderCountByStatus orderCountByStatusFromJson(String str) =>
    OrderCountByStatus.fromJson(json.decode(str));

String orderCountByStatusToJson(OrderCountByStatus data) =>
    json.encode(data.toJson());

class OrderCountByStatus {
  OrderCountByStatus({
    this.status,
    this.meta,
  });

  String status;
  Meta meta;

  factory OrderCountByStatus.fromJson(Map<String, dynamic> json) =>
      OrderCountByStatus(
        status: json["status"],
        meta: Meta.fromJson(json["meta"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "meta": meta.toJson(),
      };
}

class Meta {
  Meta({
    this.orderCount,
  });

  List<OrderCount> orderCount;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        orderCount: List<OrderCount>.from(
            json["order_count"].map((x) => OrderCount.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "order_count": List<dynamic>.from(orderCount.map((x) => x.toJson())),
      };
}

class OrderCount {
  OrderCount({
    this.orderCount,
    this.orderStatus,
  });

  int orderCount;
  String orderStatus;

  factory OrderCount.fromJson(Map<String, dynamic> json) => OrderCount(
        orderCount: json["order_count"],
        orderStatus: json["order_status"],
      );

  Map<String, dynamic> toJson() => {
        "order_count": orderCount,
        "order_status": orderStatus,
      };
}
