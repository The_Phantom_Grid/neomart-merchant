// To parse this JSON data, do
//
//     final storeDetails = storeDetailsFromJson(jsonString);

import 'dart:convert';

StoreDetails storeDetailsFromJson(String str) =>
    StoreDetails.fromJson(json.decode(str));

String storeDetailsToJson(StoreDetails data) => json.encode(data.toJson());

class StoreDetails {
  StoreDetails({
    this.info,
    this.status,
  });

  Info info;
  String status;

  factory StoreDetails.fromJson(Map<String, dynamic> json) => StoreDetails(
        info: Info.fromJson(json["info"]),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": info.toJson(),
        "status": status,
      };
}

class Info {
  Info({
    this.storeDetail,
    this.storeImages,
    this.status,
  });

  StoreDetail storeDetail;
  List<dynamic> storeImages;
  String status;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        storeDetail: StoreDetail.fromJson(json["store_detail"]),
        storeImages: List<dynamic>.from(json["store_images"].map((x) => x)),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "store_detail": storeDetail.toJson(),
        "store_images": List<dynamic>.from(storeImages.map((x) => x)),
        "status": status,
      };
}

class StoreDetail {
  StoreDetail({
    this.id,
    this.merchantId,
    this.storeName,
    this.address,
    this.location,
    this.state,
    this.city,
    this.pincode,
    this.lat,
    this.lng,
    this.locality,
    this.country,
    this.storeManager,
    this.storeDescr,
    this.mCategoryIDs,
    this.systemCategoryId,
    this.createdAt,
    this.updatedAt,
    this.isactive,
    this.panCardImg,
    this.panCardBackImg,
    this.aadharCardImg,
    this.aadharCardBackImg,
    this.gst,
    this.status,
    this.aadharCard,
    this.panCard,
    this.rating,
    this.phoneNo,
    this.storeTypeId,
    this.deliveryTime,
    this.deliveryRadius,
    this.deliveryCharge,
    this.minOrder,
    this.storeOpenTime,
    this.storeCloseTime,
    this.logo,
    this.gstImage,
    this.storeImageAvail,
    this.documentsAvail,
    this.fssai,
    this.netOrders,
    this.netCustomers,
    this.csrComments,
    this.categoryFlag,
    this.uploadPrescriptionFlag,
    this.defaultInventory,
    this.liveStore,
    this.orderEnabled,
  });

  int id;
  int merchantId;
  String storeName;
  String address;
  String location;
  String state;
  String city;
  String pincode;
  double lat;
  double lng;
  String locality;
  String country;
  String storeManager;
  String storeDescr;
  String mCategoryIDs;
  int systemCategoryId;
  String createdAt;
  String updatedAt;
  int isactive;
  String panCardImg;
  String panCardBackImg;
  String aadharCardImg;
  String aadharCardBackImg;
  String gst;
  String status;
  String aadharCard;
  String panCard;
  int rating;
  String phoneNo;
  int storeTypeId;
  String deliveryTime;
  String deliveryRadius;
  String deliveryCharge;
  String minOrder;
  String storeOpenTime;
  String storeCloseTime;
  String logo;
  String gstImage;
  int storeImageAvail;
  int documentsAvail;
  String fssai;
  String netOrders;
  String netCustomers;
  String csrComments;
  String categoryFlag;
  String uploadPrescriptionFlag;
  int defaultInventory;
  int liveStore;
  int orderEnabled;

  factory StoreDetail.fromJson(Map<String, dynamic> json) => StoreDetail(
        id: json["id"],
        merchantId: json["merchant_id"],
        storeName: json["store_name"].toString(),
        address: json["address"].toString(),
        location: json["location"].toString(),
        state: json["state"].toString(),
        city: json["city"].toString(),
        pincode: json["pincode"].toString(),
        lat: json["lat"].toDouble(),
        lng: json["lng"].toDouble(),
        locality: json["locality"].toString(),
        country: json["country"].toString(),
        storeManager: json["store_manager"].toString(),
        storeDescr: json["store_descr"].toString(),
        mCategoryIDs: json["m_category_IDs"].toString(),
        systemCategoryId: json["system_category_id"],
        createdAt: json["created_at"].toString(),
        updatedAt: json["updated_at"].toString(),
        isactive: json["isactive"],
        panCardImg: json["pan_card_img"].toString(),
        panCardBackImg: json["pan_card_back_img"].toString(),
        aadharCardImg: json["aadhar_card_img"].toString(),
        aadharCardBackImg: json["aadhar_card_back_img"].toString(),
        gst: json["gst"].toString(),
        status: json["status"],
        aadharCard: json["aadhar_card"].toString(),
        panCard: json["pan_card"].toString(),
        rating: json["rating"],
        phoneNo: json["phone_no"].toString(),
        storeTypeId: json["store_type_id"],
        deliveryTime: json["delivery_time"].toString(),
        deliveryRadius: json["delivery_radius"].toString(),
        deliveryCharge: json["delivery_charge"].toString(),
        minOrder: json["min_order"].toString(),
        storeOpenTime: json["store_open_time"].toString(),
        storeCloseTime: json["store_close_time"].toString(),
        logo: json["logo"].toString(),
        gstImage: json["gst_image"].toString(),
        storeImageAvail: json["store_image_avail"],
        documentsAvail: json["documents_avail"],
        fssai: json["fssai"].toString(),
        netOrders: json["net_orders"].toString(),
        netCustomers: json["net_customers"].toString(),
        csrComments: json["csr_comments"].toString(),
        categoryFlag: json["category_flag"].toString(),
        uploadPrescriptionFlag: json["upload_prescription_flag"].toString(),
        defaultInventory: json["default_inventory"],
        liveStore: json["live_store"],
        orderEnabled: json["order_enabled"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "merchant_id": merchantId,
        "store_name": storeName,
        "address": address,
        "location": location,
        "state": state,
        "city": city,
        "pincode": pincode,
        "lat": lat,
        "lng": lng,
        "locality": locality,
        "country": country,
        "store_manager": storeManager,
        "store_descr": storeDescr,
        "m_category_IDs": mCategoryIDs,
        "system_category_id": systemCategoryId,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "isactive": isactive,
        "pan_card_img": panCardImg,
        "pan_card_back_img": panCardBackImg,
        "aadhar_card_img": aadharCardImg,
        "aadhar_card_back_img": aadharCardBackImg,
        "gst": gst,
        "status": status,
        "aadhar_card": aadharCard,
        "pan_card": panCard,
        "rating": rating,
        "phone_no": phoneNo,
        "store_type_id": storeTypeId,
        "delivery_time": deliveryTime,
        "delivery_radius": deliveryRadius,
        "delivery_charge": deliveryCharge,
        "min_order": minOrder,
        "store_open_time": storeOpenTime,
        "store_close_time": storeCloseTime,
        "logo": logo,
        "gst_image": gstImage,
        "store_image_avail": storeImageAvail,
        "documents_avail": documentsAvail,
        "fssai": fssai,
        "net_orders": netOrders,
        "net_customers": netCustomers,
        "csr_comments": csrComments,
        "category_flag": categoryFlag,
        "upload_prescription_flag": uploadPrescriptionFlag,
        "default_inventory": defaultInventory,
        "live_store": liveStore,
        "order_enabled": orderEnabled,
      };
}
