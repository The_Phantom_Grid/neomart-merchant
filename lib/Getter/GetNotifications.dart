// To parse this JSON data, do
//
//     final notification = notificationFromJson(jsonString);

import 'dart:convert';

UserNotifications notificationFromJson(String str) =>
    UserNotifications.fromJson(json.decode(str));

String notificationToJson(UserNotifications data) => json.encode(data.toJson());

class UserNotifications {
  UserNotifications({
    this.data,
    this.message,
    this.status,
  });

  List<Datum> data;
  String message;
  String status;

  factory UserNotifications.fromJson(Map<String, dynamic> json) =>
      UserNotifications(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        message: json["message"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "message": message,
        "status": status,
      };
}

class Datum {
  Datum({
    this.id,
    this.sender,
    this.senderId,
    this.orderId,
    this.templateId,
    this.channelId,
    this.eventId,
    this.subject,
    this.messageContent,
    this.receiverId,
    this.currentStatus,
    this.sentAt,
    this.deliveredAt,
    this.readAt,
    this.resendAt,
    this.username,
    this.email,
    this.mobile,
    this.address,
    this.city,
    this.lat,
    this.lng,
    this.userId,
    this.firstname,
    this.middlename,
    this.lastname,
    this.avatarBaseUrl,
    this.avatarPath,
    this.gender,
    this.notificationId,
  });

  String id;
  String sender;
  String senderId;
  String orderId;
  String templateId;
  String channelId;
  String eventId;
  String subject;
  String messageContent;
  String receiverId;
  String currentStatus;
  String sentAt;
  String deliveredAt;
  String readAt;
  String resendAt;
  String username;
  String email;
  String mobile;
  String address;
  String city;
  double lat;
  double lng;
  String userId;
  String firstname;
  String middlename;
  String lastname;
  String avatarBaseUrl;
  String avatarPath;
  String gender;
  String notificationId;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"].toString(),
        sender: json["sender"].toString(),
        senderId: json["sender_id"].toString(),
        orderId: json["order_id"].toString(),
        templateId: json["template_id"].toString(),
        channelId: json["channel_id"].toString(),
        eventId: json["event_id"].toString(),
        subject: json["subject"].toString(),
        messageContent: json["message_content"].toString(),
        receiverId: json["receiver_id"].toString(),
        currentStatus: json["current_status"].toString(),
        sentAt: json["sent_at"].toString(),
        deliveredAt: json["delivered_at"].toString(),
        readAt: json["read_at"].toString(),
        resendAt: json["resend_at"].toString(),
        username: json["username"].toString(),
        email: json["email"].toString(),
        mobile: json["mobile"].toString(),
        address: json["address"].toString(),
        city: json["city"].toString(),
        lat: json["lat"].toDouble(),
        lng: json["lng"].toDouble(),
        userId: json["user_id"].toString(),
        firstname: json["firstname"].toString(),
        middlename: json["middlename"].toString(),
        lastname: json["lastname"].toString(),
        avatarBaseUrl: json["avatar_base_url"].toString(),
        avatarPath: json["avatar_path"].toString(),
        gender: json["gender"].toString(),
        notificationId: json["notification_id"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "sender": sender,
        "sender_id": senderId,
        "order_id": orderId,
        "template_id": templateId,
        "channel_id": channelId,
        "event_id": eventId,
        "subject": subject,
        "message_content": messageContent,
        "receiver_id": receiverId,
        "current_status": currentStatus,
        "sent_at": sentAt,
        "delivered_at": deliveredAt,
        "read_at": readAt,
        "resend_at": resendAt,
        "username": username,
        "email": email,
        "mobile": mobile,
        "address": address,
        "city": city,
        "lat": lat,
        "lng": lng,
        "user_id": userId,
        "firstname": firstname,
        "middlename": middlename,
        "lastname": lastname,
        "avatar_base_url": avatarBaseUrl,
        "avatar_path": avatarPath,
        "gender": gender,
        "notification_id": notificationId,
      };
}
