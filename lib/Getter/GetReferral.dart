// To parse this JSON data, do
//
//     final referral = referralFromJson(jsonString);

import 'dart:convert';

Referral referralFromJson(String str) => Referral.fromJson(json.decode(str));

String referralToJson(Referral data) => json.encode(data.toJson());

class Referral {
  Referral({
    this.message,
    this.status,
    this.data,
  });

  String message;
  String status;
  List<Datum> data;

  factory Referral.fromJson(Map<String, dynamic> json) => Referral(
        message: json["message"].toString(),
        status: json["status"].toString(),
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "status": status,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.referalCode,
    this.paytmNo,
  });

  String referalCode;
  String paytmNo;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        referalCode: json["referal_code"].toString(),
        paytmNo: json["paytm_no"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "referal_code": referalCode,
        "paytm_no": paytmNo,
      };
}
