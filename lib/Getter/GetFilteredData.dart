// To parse this JSON data, do
//
//     final filteredData = filteredDataFromJson(jsonString);

import 'dart:convert';

FilteredData filteredDataFromJson(String str) =>
    FilteredData.fromJson(json.decode(str));

String filteredDataToJson(FilteredData data) => json.encode(data.toJson());

class FilteredData {
  FilteredData({
    this.info,
    this.status,
  });

  Info info;
  String status;

  factory FilteredData.fromJson(Map<String, dynamic> json) => FilteredData(
        info: Info.fromJson(json["info"]),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": info.toJson(),
        "status": status,
      };
}

class Info {
  Info({
    this.category,
    this.subCategory,
    this.status,
  });

  List<Category> category;
  List<SubCategory> subCategory;
  String status;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        category: List<Category>.from(
            json["category"].map((x) => Category.fromJson(x))),
        subCategory: List<SubCategory>.from(
            json["sub_category"].map((x) => SubCategory.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "category": List<dynamic>.from(category.map((x) => x.toJson())),
        "sub_category": List<dynamic>.from(subCategory.map((x) => x.toJson())),
        "status": status,
      };
}

class Category {
  Category({
    this.prodCategoryId,
    this.prodCategoryName,
    this.colorcode,
    this.icons,
    this.productCount,
  });

  String prodCategoryId;
  String prodCategoryName;
  String colorcode;
  String icons;
  String productCount;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        prodCategoryId: json["prod_category_id"].toString(),
        prodCategoryName: json["prod_category_name"].toString(),
        colorcode: json["colorcode"].toString(),
        icons: json["icons"].toString(),
        productCount: json["product_count"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "prod_category_id": prodCategoryId,
        "prod_category_name": prodCategoryName,
        "colorcode": colorcode,
        "icons": icons,
        "product_count": productCount,
      };
}

class SubCategory {
  SubCategory({
    this.subcatId,
    this.name,
    this.description,
    this.categoryId,
    this.productCount,
  });

  String subcatId;
  String name;
  String description;
  String categoryId;
  String productCount;

  factory SubCategory.fromJson(Map<String, dynamic> json) => SubCategory(
        subcatId: json["subcat_id"].toString(),
        name: json["name"].toString(),
        description: json["description"].toString(),
        categoryId: json["category_id"].toString(),
        productCount: json["product_count"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "subcat_id": subcatId,
        "name": name,
        "description": description,
        "category_id": categoryId,
        "product_count": productCount,
      };
}
