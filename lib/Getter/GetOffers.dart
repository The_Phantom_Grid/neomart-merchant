// To parse this JSON data, do
//
//     final offer = offerFromJson(jsonString);

import 'dart:convert';

Offer offerFromJson(String str) => Offer.fromJson(json.decode(str));

String offerToJson(Offer data) => json.encode(data.toJson());

class Offer {
  Offer({
    this.info,
    this.status,
  });

  List<Info> info;
  String status;

  factory Offer.fromJson(Map<String, dynamic> json) => Offer(
        info: List<Info>.from(json["info"].map((x) => Info.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": List<dynamic>.from(info.map((x) => x.toJson())),
        "status": status,
      };
}

class Info {
  Info({
    this.collectionId,
    this.collectionName,
    this.collectionDescr,
    this.validFrom,
    this.validTo,
    this.comboPrice,
    this.inventory,
    this.isPublished,
    this.collectionProducts,
    this.collectionImages,
  });

  int collectionId;
  String collectionName;
  String collectionDescr;
  String validFrom;
  String validTo;
  int comboPrice;
  int inventory;
  int isPublished;
  List<CollectionProduct> collectionProducts;
  List<CollectionImage> collectionImages;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        collectionId: json["collection_id"],
        collectionName: json["collection_name"].toString(),
        collectionDescr: json["collection_descr"].toString(),
        validFrom: json["valid_from"].toString(),
        validTo: json["valid_to"].toString(),
        comboPrice: json["combo_price"],
        inventory: json["inventory"],
        isPublished: json["is_published"],
        collectionProducts: List<CollectionProduct>.from(
            json["collection_products"]
                .map((x) => CollectionProduct.fromJson(x))),
        collectionImages: List<CollectionImage>.from(
            json["collection_images"].map((x) => CollectionImage.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "collection_id": collectionId,
        "collection_name": collectionName,
        "collection_descr": collectionDescr,
        "valid_from": validFrom,
        "valid_to": validTo,
        "combo_price": comboPrice,
        "inventory": inventory,
        "is_published": isPublished,
        "collection_products":
            List<dynamic>.from(collectionProducts.map((x) => x.toJson())),
        "collection_images":
            List<dynamic>.from(collectionImages.map((x) => x.toJson())),
      };
}

class CollectionImage {
  CollectionImage({
    this.id,
    this.collectionId,
    this.imageUrl,
    this.createdAt,
    this.updatedAt,
    this.isactive,
  });

  int id;
  int collectionId;
  String imageUrl;
  DateTime createdAt;
  dynamic updatedAt;
  int isactive;

  factory CollectionImage.fromJson(Map<String, dynamic> json) =>
      CollectionImage(
        id: json["id"],
        collectionId: json["collection_id"],
        imageUrl: json["image_url"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"],
        isactive: json["isactive"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "collection_id": collectionId,
        "image_url": imageUrl,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt,
        "isactive": isactive,
      };
}

class CollectionProduct {
  CollectionProduct({
    this.id,
    this.collectionId,
    this.masterProductId,
    this.productName,
    this.productDescr,
    this.productImage,
    this.merchantProductId,
    this.merchantId,
    this.storeId,
    this.catId,
    this.subcatId,
    this.catName,
    this.subcatName,
    this.productPrice,
    this.createdId,
    this.updatedAt,
    this.isactive,
    this.actualProductPrice,
  });

  int id;
  int collectionId;
  int masterProductId;
  String productName;
  String productDescr;
  String productImage;
  int merchantProductId;
  int merchantId;
  int storeId;
  int catId;
  int subcatId;
  String catName;
  String subcatName;
  int productPrice;
  String createdId;
  String updatedAt;
  int isactive;
  String actualProductPrice;

  factory CollectionProduct.fromJson(Map<String, dynamic> json) =>
      CollectionProduct(
        id: json["id"],
        collectionId: json["collection_id"],
        masterProductId: json["master_product_id"],
        productName: json["product_name"].toString(),
        productDescr: json["product_descr"].toString(),
        productImage: json["product_image"].toString(),
        merchantProductId: json["merchant_product_id"],
        merchantId: json["merchant_id"],
        storeId: json["store_id"],
        catId: json["cat_id"],
        subcatId: json["subcat_id"],
        catName: json["cat_name"].toString(),
        subcatName: json["subcat_name"].toString(),
        productPrice: json["product_price"],
        createdId: json["created_id"].toString(),
        updatedAt: json["updated_at"].toString(),
        isactive: json["isactive"],
        actualProductPrice: json["actual_product_price"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "collection_id": collectionId,
        "master_product_id": masterProductId,
        "product_name": productName,
        "product_descr": productDescr,
        "product_image": productImage,
        "merchant_product_id": merchantProductId,
        "merchant_id": merchantId,
        "store_id": storeId,
        "cat_id": catId,
        "subcat_id": subcatId,
        "cat_name": catName,
        "subcat_name": subcatName,
        "product_price": productPrice,
        "created_id": createdId,
        "updated_at": updatedAt,
        "isactive": isactive,
        "actual_product_price": actualProductPrice,
      };
}
