// To parse this JSON data, do
//
//     final storeSubCat = storeSubCatFromJson(jsonString);

import 'dart:convert';

StoreSubCat storeSubCatFromJson(String str) =>
    StoreSubCat.fromJson(json.decode(str));

String storeSubCatToJson(StoreSubCat data) => json.encode(data.toJson());

class StoreSubCat {
  StoreSubCat({
    this.data,
    this.status,
  });

  List<Datum> data;
  String status;

  factory StoreSubCat.fromJson(Map<String, dynamic> json) => StoreSubCat(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status,
      };
}

class Datum {
  Datum({
    this.id,
    this.merCatId,
    this.name,
    this.createdAt,
    this.updatedAt,
    this.isactive,
    this.merchantId,
  });

  int id;
  int merCatId;
  String name;
  String createdAt;
  String updatedAt;
  int isactive;
  int merchantId;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        merCatId: json["mer_cat_id"],
        name: json["name"],
        createdAt: json["created_at"].toString(),
        updatedAt: json["updated_at"].toString(),
        isactive: json["isactive"],
        merchantId: json["merchant_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "mer_cat_id": merCatId,
        "name": name,
        "created_at": createdAt.toString(),
        "updated_at": updatedAt.toString(),
        "isactive": isactive,
        "merchant_id": merchantId,
      };
}
