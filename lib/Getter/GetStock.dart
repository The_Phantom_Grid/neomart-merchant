// To parse this JSON data, do
//
//     final revenue = revenueFromJson(jsonString);

import 'dart:convert';

Stock revenueFromJson(String str) => Stock.fromJson(json.decode(str));

String revenueToJson(Stock data) => json.encode(data.toJson());

class Stock {
  Stock({
    this.data,
    this.status,
    this.message,
  });

  Data data;
  String status;
  String message;

  factory Stock.fromJson(Map<String, dynamic> json) => Stock(
        data: Data.fromJson(json["data"]),
        status: json["status"].toString(),
        message: json["message"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "data": data.toJson(),
        "status": status,
        "message": message,
      };
}

class Data {
  Data({
    this.inStock,
    this.outOfStock,
  });

  String inStock;
  String outOfStock;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        inStock: json["in_stock"].toString(),
        outOfStock: json["out_of_stock"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "in_stock": inStock,
        "out_of_stock": outOfStock,
      };
}
