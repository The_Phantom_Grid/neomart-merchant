// To parse this JSON data, do
//
//     final masterProduct = masterProductFromJson(jsonString);

import 'dart:convert';

MasterProduct masterProductFromJson(String str) =>
    MasterProduct.fromJson(json.decode(str));

String masterProductToJson(MasterProduct data) => json.encode(data.toJson());

class MasterProduct {
  MasterProduct({
    this.info,
    this.status,
  });

  List<Info> info;
  String status;

  factory MasterProduct.fromJson(Map<String, dynamic> json) => MasterProduct(
        info: List<Info>.from(json["info"].map((x) => Info.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": List<dynamic>.from(info.map((x) => x.toJson())),
        "status": status,
      };
}

class Info {
  Info({
    this.id,
    this.productname,
    this.productdescription,
    this.avgPrice,
    this.mrp,
    this.manufacture,
    this.colorcode,
    this.icon,
    this.measure,
    this.producttype,
    this.createdAt,
    this.updatedAt,
    this.isactive,
    this.taxStatus,
    this.taxPercent,
    this.manufacturerId,
    this.systemStatus,
    this.catId,
    this.subcatId,
    this.sku,
    this.addedBy,
    this.addedId,
    this.approvedAt,
    this.approvedBy,
    this.remark,
    this.barcode,
    this.hsnNumber,
    this.skuNumber,
    this.isDefault,
    this.productType,
    this.imageName,
    this.imageType,
    this.imagePath,
    this.mproductId,
    this.imageFor,
    this.size,
    this.isFeatured,
    this.storeId,
    this.categoryName,
    this.subcategoryName,
  });

  int id;
  String productname;
  String productdescription;
  String avgPrice;
  String mrp;
  String manufacture;
  String colorcode;
  String icon;
  String measure;
  String producttype;
  String createdAt;
  String updatedAt;
  int isactive;
  String taxStatus;
  String taxPercent;
  int manufacturerId;
  String systemStatus;
  int catId;
  int subcatId;
  String sku;
  String addedBy;
  String addedId;
  String approvedAt;
  String approvedBy;
  String remark;
  String barcode;
  int hsnNumber;
  String skuNumber;
  String isDefault;
  String productType;
  String imageName;
  String imageType;
  String imagePath;
  int mproductId;
  String imageFor;
  String size;
  String isFeatured;
  String storeId;
  String categoryName;
  String subcategoryName;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        id: json["id"],
        productname: json["productname"].toString(),
        productdescription: json["productdescription"].toString(),
        avgPrice: json["avg_price"].toString(),
        mrp: json["mrp"].toString(),
        manufacture: json["manufacture"].toString(),
        colorcode: json["colorcode"].toString(),
        icon: json["icon"].toString(),
        measure: json["measure"].toString(),
        producttype: json["producttype"].toString(),
        createdAt: json["created_at"].toString(),
        updatedAt: json["updated_at"].toString(),
        isactive: json["isactive"],
        taxStatus: json["tax_status"].toString(),
        taxPercent: json["tax_percent"].toString(),
        manufacturerId: json["manufacturerID"],
        systemStatus: json["system_status"],
        catId: json["cat_id"],
        subcatId: json["subcat_id"],
        sku: json["sku"].toString(),
        addedBy: json["added_by"].toString(),
        addedId: json["added_id"].toString(),
        approvedAt: json["approved_at"].toString(),
        approvedBy: json["approved_by"].toString(),
        remark: json["remark"].toString(),
        barcode: json["barcode"].toString(),
        hsnNumber: json["hsn_number"],
        skuNumber: json["sku_number"].toString(),
        isDefault: json["is_default"],
        productType: json["product_type"].toString(),
        imageName: json["image_name"].toString(),
        imageType: json["image_type"].toString(),
        imagePath: json["image_path"].toString(),
        mproductId: json["mproduct_id"],
        imageFor: json["image_for"].toString(),
        size: json["size"].toString(),
        isFeatured: json["is_featured"].toString(),
        storeId: json["store_id"].toString(),
        categoryName: json["category_name"].toString(),
        subcategoryName: json["subcategory_name"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "productname": productname,
        "productdescription": productdescription,
        "avg_price": avgPrice,
        "mrp": mrp,
        "manufacture": manufacture,
        "colorcode": colorcode,
        "icon": icon,
        "measure": measure,
        "producttype": producttype,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "isactive": isactive,
        "tax_status": taxStatus,
        "tax_percent": taxPercent,
        "manufacturerID": manufacturerId,
        "system_status": systemStatus,
        "cat_id": catId,
        "subcat_id": subcatId,
        "sku": sku,
        "added_by": addedBy,
        "added_id": addedId,
        "approved_at": approvedAt,
        "approved_by": approvedBy,
        "remark": remark,
        "barcode": barcode,
        "hsn_number": hsnNumber,
        "sku_number": skuNumber,
        "is_default": isDefault,
        "product_type": productType,
        "image_name": imageName,
        "image_type": imageType,
        "image_path": imagePath,
        "mproduct_id": mproductId,
        "image_for": imageFor,
        "size": size,
        "is_featured": isFeatured,
        "store_id": storeId,
        "category_name": categoryName,
        "subcategory_name": subcategoryName,
      };
}
