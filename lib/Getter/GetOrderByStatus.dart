// To parse this JSON data, do
//
//     final orderByStatus = orderByStatusFromJson(jsonString);

import 'dart:convert';

OrderByStatus orderByStatusFromJson(String str) =>
    OrderByStatus.fromJson(json.decode(str));

String orderByStatusToJson(OrderByStatus data) => json.encode(data.toJson());

class OrderByStatus {
  OrderByStatus({
    this.status,
    this.meta,
  });

  String status;
  Meta meta;

  factory OrderByStatus.fromJson(Map<String, dynamic> json) => OrderByStatus(
        status: json["status"],
        meta: Meta.fromJson(json["meta"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "meta": meta.toJson(),
      };
}

class Meta {
  Meta({
    this.orderCount,
    this.orderDetails,
  });

  List<OrderInfo> orderCount;
  List<OrderByStatusDetail> orderDetails;

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
        orderCount: List<OrderInfo>.from(
            json["order_count"].map((x) => OrderInfo.fromJson(x))),
        orderDetails: List<OrderByStatusDetail>.from(
            json["order_details"].map((x) => OrderByStatusDetail.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "order_count": List<dynamic>.from(orderCount.map((x) => x.toJson())),
        "order_details":
            List<dynamic>.from(orderDetails.map((x) => x.toJson())),
      };
}

class OrderInfo {
  OrderInfo({
    this.orderCount,
  });

  int orderCount;

  factory OrderInfo.fromJson(Map<String, dynamic> json) => OrderInfo(
        orderCount: json["order_count"],
      );

  Map<String, dynamic> toJson() => {
        "order_count": orderCount,
      };
}

class OrderByStatusDetail {
  OrderByStatusDetail({
    this.firstname,
    this.avatarPath,
    this.avatarBaseUrl,
    this.deliveryAddress,
    this.totaldue,
    this.delivertype,
    this.ratings,
    this.paymentStatus,
    this.promocodeApplied,
    this.promocodeAmount,
    this.description,
    this.orderStatus,
    this.totalTimeToDeliver,
    this.prevBalance,
    this.mobile,
    this.email,
    this.expectedDeliveryDate,
    this.paymentMode,
    this.orderDate,
    this.orderId,
    this.customerId,
    this.customerLat,
    this.customerLng,
    this.merchantId,
    this.orderCancelledOrRejectedBy,
    this.storeId,
    this.assignedDeliveryBoyId,
    this.assignedDeliveryBoyName,
    this.assignedDeliveryBoyNumber,
    this.totalTimeToDeliverInMinutes,
    this.orderMadeThrough,
    this.invoiceNumber,
    this.subtotal,
    this.shipping,
    this.deliveredAt,
    this.grandtotal,
    this.totalpaid,
    this.totalrefund,
    this.discountAmount,
    this.discountCode,
    this.discount,
    this.discountFlag,
  });

  String firstname;
  String avatarPath;
  String avatarBaseUrl;
  String deliveryAddress;
  String totaldue;
  String delivertype;
  String ratings;
  String paymentStatus;
  String promocodeApplied;
  String promocodeAmount;
  String description;
  String orderStatus;
  String totalTimeToDeliver;
  String prevBalance;
  String mobile;
  String email;
  String expectedDeliveryDate;
  String paymentMode;
  String orderDate;
  int orderId;
  int customerId;
  double customerLat;
  double customerLng;
  int merchantId;
  String orderCancelledOrRejectedBy;
  int storeId;
  String assignedDeliveryBoyId;
  String assignedDeliveryBoyName;
  String assignedDeliveryBoyNumber;
  String totalTimeToDeliverInMinutes;
  String orderMadeThrough;
  String invoiceNumber;
  String subtotal;
  String shipping;
  String deliveredAt;
  String grandtotal;
  String totalpaid;
  String totalrefund;
  String discountAmount;
  String discountCode;
  String discount;
  String discountFlag;

  factory OrderByStatusDetail.fromJson(
          Map<String, dynamic> json) =>
      OrderByStatusDetail(
          firstname: json["firstname"].toString(),
          avatarPath: json["avatar_path"].toString(),
          avatarBaseUrl: json["avatar_base_url"].toString(),
          deliveryAddress: json["delivery_address"].toString(),
          totaldue: json["totaldue"].toString(),
          delivertype: json["delivertype"].toString(),
          ratings: json["ratings"].toString(),
          paymentStatus: json["paymentStatus"].toString(),
          promocodeApplied: json["promocode_applied"].toString(),
          promocodeAmount: json["promocode_amount"].toString(),
          description: json["description"].toString(),
          orderStatus: json["order_status"].toString(),
          totalTimeToDeliver: json["total_time_to_deliver"].toString(),
          prevBalance: json["prev_balance"].toString(),
          mobile: json["mobile"].toString(),
          email: json["email"] == null ? null : json["email"].toString(),
          expectedDeliveryDate: json["expected_delivery_date"].toString(),
          paymentMode: json["payment_mode"].toString(),
          orderDate: json["order_date"].toString(),
          orderId: json["order_id"],
          customerId: json["customer_id"],
          customerLat: json["customer_lat"].toDouble(),
          customerLng: json["customer_lng"].toDouble(),
          merchantId: json["merchant_id"],
          orderCancelledOrRejectedBy:
              json["order_cancelled_or_rejected_by"].toString(),
          storeId: json["store_id"],
          assignedDeliveryBoyId: json["assigned_delivery_boy_id"].toString(),
          assignedDeliveryBoyName:
              json["assigned_delivery_boy_name"].toString(),
          assignedDeliveryBoyNumber:
              json["assigned_delivery_boy_number"].toString(),
          totalTimeToDeliverInMinutes:
              json["total_time_to_deliver_in_minutes"].toString(),
          orderMadeThrough: json["order_made_through"].toString(),
          invoiceNumber: json["invoiceNumber"].toString(),
          subtotal: json["subtotal"].toString(),
          shipping: json["shipping"].toString(),
          deliveredAt: json["delivered_at"].toString(),
          grandtotal: json["grandtotal"].toString(),
          totalpaid: json["totalpaid"].toString(),
          totalrefund: json["totalrefund"].toString(),
          discountAmount: json["discount_amount"].toString(),
          discountCode: json["discount_code"].toString(),
          discount: json["discount"].toString(),
          discountFlag: json["discount_flag"].toString());

  Map<String, dynamic> toJson() => {
        "firstname": firstname,
        "avatar_path": avatarPath,
        "avatar_base_url": avatarBaseUrl,
        "delivery_address": deliveryAddress,
        "totaldue": totaldue,
        "delivertype": delivertype,
        "ratings": ratings,
        "paymentStatus": paymentStatus,
        "promocode_applied": promocodeApplied,
        "promocode_amount": promocodeAmount,
        "description": description,
        "order_status": orderStatus,
        "total_time_to_deliver": totalTimeToDeliver,
        "prev_balance": prevBalance,
        "mobile": mobile,
        "email": email == null ? null : email,
        "expected_delivery_date": expectedDeliveryDate,
        "payment_mode": paymentMode,
        "order_date": orderDate,
        "order_id": orderId,
        "customer_id": customerId,
        "customer_lat": customerLat,
        "customer_lng": customerLng,
        "merchant_id": merchantId,
        "order_cancelled_or_rejected_by": orderCancelledOrRejectedBy,
        "store_id": storeId,
        "assigned_delivery_boy_id": assignedDeliveryBoyId,
        "assigned_delivery_boy_name": assignedDeliveryBoyName,
        "assigned_delivery_boy_number": assignedDeliveryBoyNumber,
        "total_time_to_deliver_in_minutes": totalTimeToDeliverInMinutes,
        "order_made_through": orderMadeThrough,
        "invoiceNumber": invoiceNumber,
        "subtotal": subtotal,
        "shipping": shipping,
        "delivered_at": deliveredAt == null ? null : deliveredAt,
        "grandtotal": grandtotal,
        "totalpaid": totalpaid,
        "totalrefund": totalrefund,
        "discount_amount": discountAmount,
        "discount_code": discountCode,
        "discount": discount,
        "discount_flag": discountFlag,
      };
}
