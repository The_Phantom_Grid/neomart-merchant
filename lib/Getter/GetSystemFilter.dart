// To parse this JSON data, do
//
//     final systemFilter = systemFilterFromJson(jsonString);

import 'dart:convert';

SystemFilter systemFilterFromJson(String str) =>
    SystemFilter.fromJson(json.decode(str));

String systemFilterToJson(SystemFilter data) => json.encode(data.toJson());

class SystemFilter {
  SystemFilter({
    this.info,
    this.status,
  });

  List<Info> info;
  String status;

  factory SystemFilter.fromJson(Map<String, dynamic> json) => SystemFilter(
        info: List<Info>.from(json["info"].map((x) => Info.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": List<dynamic>.from(info.map((x) => x.toJson())),
        "status": status,
      };
}

class Info {
  Info({
    this.id,
    this.filterName,
    this.displayOrder,
  });

  int id;
  String filterName;
  int displayOrder;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        id: json["id"],
        filterName: json["filter_name"].toString(),
        displayOrder: json["display_order"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "filter_name": filterName,
        "display_order": displayOrder,
      };
}
