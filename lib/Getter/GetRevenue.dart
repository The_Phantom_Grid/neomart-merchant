// To parse this JSON data, do
//
//     final revenue = revenueFromJson(jsonString);

import 'dart:convert';

Revenue revenueFromJson(String str) => Revenue.fromJson(json.decode(str));

String revenueToJson(Revenue data) => json.encode(data.toJson());

class Revenue {
  Revenue({
    this.data,
    this.status,
    this.message,
  });

  Data data;
  String status;
  String message;

  factory Revenue.fromJson(Map<String, dynamic> json) => Revenue(
        data: Data.fromJson(json["data"]),
        status: json["status"].toString(),
        message: json["message"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "data": data.toJson(),
        "status": status,
        "message": message,
      };
}

class Data {
  Data({
    this.totalRevenue,
  });

  String totalRevenue;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        totalRevenue: json["total_revenue"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "total_revenue": totalRevenue,
      };
}
