// To parse this JSON data, do
//
//     final customerAddress = customerAddressFromJson(jsonString);

import 'dart:convert';

CustomerAddress customerAddressFromJson(String str) =>
    CustomerAddress.fromJson(json.decode(str));

String customerAddressToJson(CustomerAddress data) =>
    json.encode(data.toJson());

class CustomerAddress {
  CustomerAddress({
    this.details,
    this.status,
  });

  List<Detail> details;
  String status;

  factory CustomerAddress.fromJson(Map<String, dynamic> json) =>
      CustomerAddress(
        details:
            List<Detail>.from(json["details"].map((x) => Detail.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "details": List<dynamic>.from(details.map((x) => x.toJson())),
        "status": status,
      };
}

class Detail {
  Detail({
    this.id,
    this.userId,
    this.address,
    this.pincode,
    this.city,
    this.state,
    this.lat,
    this.lng,
    this.isDefault,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.fullName,
    this.flatHouseno,
    this.colonyStreet,
    this.landmark,
  });

  int id;
  int userId;
  String address;
  int pincode;
  String city;
  String state;
  double lat;
  double lng;
  int isDefault;
  int isActive;
  String createdAt;
  String updatedAt;
  String fullName;
  String flatHouseno;
  String colonyStreet;
  String landmark;

  factory Detail.fromJson(Map<String, dynamic> json) => Detail(
        id: json["id"],
        userId: json["user_id"],
        address: json["address"].toString(),
        pincode: json["pincode"],
        city: json["city"].toString(),
        state: json["state"].toString(),
        lat: json["lat"].toDouble(),
        lng: json["lng"].toDouble(),
        isDefault: json["is_default"],
        isActive: json["is_active"],
        createdAt: json["created_at"].toString(),
        updatedAt: json["updated_at"].toString(),
        fullName: json["full_name"].toString(),
        flatHouseno: json["flat_houseno"].toString(),
        colonyStreet: json["colony_street"].toString(),
        landmark: json["landmark"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "address": address,
        "pincode": pincode,
        "city": city,
        "state": state,
        "lat": lat,
        "lng": lng,
        "is_default": isDefault,
        "is_active": isActive,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "full_name": fullName,
        "flat_houseno": flatHouseno,
        "colony_street": colonyStreet,
        "landmark": landmark,
      };
}
