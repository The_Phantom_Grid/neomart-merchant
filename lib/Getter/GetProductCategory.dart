// To parse this JSON data, do
//
//     final productCategory = productCategoryFromJson(jsonString);

import 'dart:convert';

ProductCategory productCategoryFromJson(String str) =>
    ProductCategory.fromJson(json.decode(str));

String productCategoryToJson(ProductCategory data) =>
    json.encode(data.toJson());

class ProductCategory {
  ProductCategory({
    this.info,
    this.status,
  });

  List<Info> info;
  String status;

  factory ProductCategory.fromJson(Map<String, dynamic> json) =>
      ProductCategory(
        info: List<Info>.from(json["info"].map((x) => Info.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": List<dynamic>.from(info.map((x) => x.toJson())),
        "status": status,
      };
}

class Info {
  Info({
    this.prodCategoryId,
    this.prodCategoryName,
    this.colorcode,
    this.icons,
    this.productCount,
  });

  String prodCategoryId;
  String prodCategoryName;
  String colorcode;
  String icons;
  int productCount;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        prodCategoryId: json["prod_category_id"].toString(),
        prodCategoryName: json["prod_category_name"].toString(),
        colorcode: json["colorcode"].toString(),
        icons: json["icons"].toString(),
        productCount: json["product_count"],
      );

  Map<String, dynamic> toJson() => {
        "prod_category_id": prodCategoryId,
        "prod_category_name": prodCategoryName,
        "colorcode": colorcode,
        "icons": icons,
        "product_count": productCount,
      };
}
