// To parse this JSON data, do
//
//     final collectionInfo = collectionInfoFromJson(jsonString);

import 'dart:convert';

UserProfile collectionInfoFromJson(String str) =>
    UserProfile.fromJson(json.decode(str));

String collectionInfoToJson(UserProfile data) => json.encode(data.toJson());

class UserProfile {
  UserProfile({
    this.profileData,
    this.status,
  });

  List<ProfileDatum> profileData;
  String status;

  factory UserProfile.fromJson(Map<String, dynamic> json) => UserProfile(
        profileData: List<ProfileDatum>.from(
            json["profile_data"].map((x) => ProfileDatum.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "profile_data": List<dynamic>.from(profileData.map((x) => x.toJson())),
        "status": status,
      };
}

class ProfileDatum {
  ProfileDatum({
    this.id,
    this.username,
    this.email,
    this.lat,
    this.lng,
    this.mobile,
    this.address,
    this.city,
    this.landmark,
    this.pincode,
    this.firstname,
    this.phone,
    this.avatarBaseUrl,
    this.avatarPath,
  });

  int id;
  String username;
  String email;
  double lat;
  double lng;
  String mobile;
  String address;
  String city;
  String landmark;
  String pincode;
  String firstname;
  String phone;
  String avatarBaseUrl;
  String avatarPath;

  factory ProfileDatum.fromJson(Map<String, dynamic> json) => ProfileDatum(
        id: json["id"],
        username: json["username"].toString(),
        email: json["email"].toString() == null ? "" : json["email"].toString(),
        lat: json["lat"].toDouble(),
        lng: json["lng"].toDouble(),
        mobile: json["mobile"].toString(),
        address: json["address"].toString(),
        city: json["city"].toString(),
        landmark: json["landmark"].toString(),
        pincode: json["pincode"].toString(),
        firstname: json["firstname"].toString(),
        phone: json["phone"].toString(),
        avatarBaseUrl: json["avatar_base_url"].toString(),
        avatarPath: json["avatar_path"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "username": username,
        "email": email,
        "lat": lat,
        "lng": lng,
        "mobile": mobile,
        "address": address,
        "city": city,
        "landmark": landmark,
        "pincode": pincode,
        "firstname": firstname,
        "phone": phone,
        "avatar_base_url": avatarBaseUrl,
        "avatar_path": avatarPath,
      };
}
