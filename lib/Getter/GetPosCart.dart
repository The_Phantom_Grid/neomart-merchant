// To parse this JSON data, do
//
//     final posCart = posCartFromJson(jsonString);

import 'dart:convert';

PosCart posCartFromJson(String str) => PosCart.fromJson(json.decode(str));

String posCartToJson(PosCart data) => json.encode(data.toJson());

class PosCart {
  PosCart({
    this.product,
    this.total,
    this.status,
  });

  List<Product> product;
  Total total;
  String status;

  factory PosCart.fromJson(Map<String, dynamic> json) => PosCart(
        product:
            List<Product>.from(json["product"].map((x) => Product.fromJson(x))),
        total: Total.fromJson(json["total"]),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "product": List<dynamic>.from(product.map((x) => x.toJson())),
        "total": total.toJson(),
        "status": status,
      };
}

class Product {
  Product({
    this.tax,
    this.quantity,
    this.productname,
    this.taxStatus,
    this.masterproductid,
    this.merchantlistid,
    this.merchantid,
    this.storeName,
    this.storeIcon,
    this.measure,
    this.icon,
    this.pricewithtax,
    this.pricewithouttax,
    this.sellingPrice,
    this.productMrp,
    this.avgTimeToDeliver,
    this.totalLeft,
    this.shipping,
    this.minOrder,
    this.offerId,
    this.offerName,
    this.offerDescr,
    this.comboPrice,
    this.offerImage,
    this.offerMrp,
  });

  String tax;
  int quantity;
  String productname;
  String taxStatus;
  int masterproductid;
  int merchantlistid;
  int merchantid;
  String storeName;
  String storeIcon;
  String measure;
  String icon;
  String pricewithtax;
  String pricewithouttax;
  String sellingPrice;
  String productMrp;
  String avgTimeToDeliver;
  int totalLeft;
  String shipping;
  String minOrder;
  String offerId;
  String offerName;
  String offerDescr;
  String comboPrice;
  String offerImage;
  String offerMrp;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        tax: json["tax"].toString().replaceAll(',', ""),
        quantity: json["quantity"],
        productname: json["productname"].toString(),
        taxStatus: json["tax_status"].toString(),
        masterproductid: json["masterproductid"],
        merchantlistid: json["merchantlistid"],
        merchantid: json["merchantid"],
        storeName: json["store_name"].toString(),
        storeIcon: json["store_icon"].toString(),
        measure: json["measure"].toString(),
        icon: json["icon"].toString(),
        pricewithtax: json["pricewithtax"].toString().replaceAll(',', ""),
        pricewithouttax: json["pricewithouttax"].toString().replaceAll(',', ""),
        sellingPrice: json["selling_price"].toString().replaceAll(',', ""),
        productMrp: json["product_mrp"].toString().replaceAll(',', ""),
        avgTimeToDeliver: json["avg_time_to_deliver"].toString(),
        totalLeft: json["total_left"],
        shipping: json["shipping"].toString().replaceAll(',', ""),
        minOrder: json["min_order"].toString().replaceAll(',', ""),
        offerId: json["offer_id"].toString(),
        offerName: json["offer_name"].toString(),
        offerDescr: json["offer_descr"].toString(),
        comboPrice: json["combo_price"].toString().replaceAll(',', ""),
        offerImage: json["offer_image"].toString(),
        offerMrp: json["offer_mrp"].toString().replaceAll(',', ""),
      );

  Map<String, dynamic> toJson() => {
        "tax": tax,
        "quantity": quantity,
        "productname": productname,
        "tax_status": taxStatus,
        "masterproductid": masterproductid,
        "merchantlistid": merchantlistid,
        "merchantid": merchantid,
        "store_name": storeName,
        "store_icon": storeIcon,
        "measure": measure,
        "icon": icon,
        "pricewithtax": pricewithtax,
        "pricewithouttax": pricewithouttax,
        "selling_price": sellingPrice,
        "product_mrp": productMrp,
        "avg_time_to_deliver": avgTimeToDeliver,
        "total_left": totalLeft,
        "shipping": shipping,
        "min_order": minOrder,
        "offer_id": offerId,
        "offer_name": offerName,
        "offer_descr": offerDescr,
        "combo_price": comboPrice,
        "offer_image": offerImage,
        "offer_mrp": offerMrp,
      };
}

class Total {
  Total({
    this.paytmAvail,
    this.subtotal,
    this.grandtotal,
    this.totalTax,
    this.shipping,
    this.totaldue,
    this.totalItems,
    this.discount,
  });

  String paytmAvail;
  String subtotal;
  String grandtotal;
  String totalTax;
  String shipping;
  String totaldue;
  int totalItems;
  String discount;

  factory Total.fromJson(Map<String, dynamic> json) => Total(
        paytmAvail: json["paytm_avail"].toString(),
        subtotal: json["subtotal"].toString().replaceAll(',', ""),
        grandtotal: json["grandtotal"].toString().replaceAll(',', ""),
        totalTax: json["total_tax"].toString().replaceAll(',', ""),
        shipping: json["shipping"].toString().replaceAll(',', ""),
        totaldue: json["totaldue"].toString().replaceAll(',', ""),
        totalItems: json["total_items"],
        discount: json["discount"] == null ? "0" : json["discount"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "paytm_avail": paytmAvail,
        "subtotal": subtotal,
        "grandtotal": grandtotal,
        "total_tax": totalTax,
        "shipping": shipping,
        "totaldue": totaldue,
        "total_items": totalItems,
        "discount": discount,
      };
}
