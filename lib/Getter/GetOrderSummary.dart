// To parse this JSON data, do
//
//     final orderSummary = orderSummaryFromJson(jsonString);

import 'dart:convert';

OrderSummary orderSummaryFromJson(String str) =>
    OrderSummary.fromJson(json.decode(str));

String orderSummaryToJson(OrderSummary data) => json.encode(data.toJson());

class OrderSummary {
  OrderSummary({
    this.data,
    this.status,
  });

  List<Datum> data;
  String status;

  factory OrderSummary.fromJson(Map<String, dynamic> json) => OrderSummary(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status,
      };
}

class Datum {
  Datum({
    this.orderitemsid,
    this.masterproductid,
    this.quantity,
    this.unitsellingprice,
    this.taxCollected,
    this.merchantActualSellingPrice,
    this.costToCustomer,
    this.totalCostToCustomer,
    this.comissionAmount,
    this.discountAmount,
    this.discountFlag,
    this.timeSlot,
    this.customerOrderId,
    this.orderInstructions,
    this.cancellationReason,
    this.merchantproductid,
    this.productname,
    this.productdescription,
    this.avgPrice,
    this.mrp,
    this.icon,
    this.measure,
    this.producttype,
    this.catId,
    this.sku,
    this.subcatId,
    this.offerId,
    this.offerName,
    this.offerDescription,
    this.offerImage,
    this.shipping,
    this.deliveredAt,
    this.discountCode,
    this.paymentMode,
    this.xnNumber,
    this.promocodeApplied,
    this.promocodeAmount,
  });

  String orderitemsid;
  String masterproductid;
  int quantity;
  String unitsellingprice;
  String taxCollected;
  String merchantActualSellingPrice;
  String costToCustomer;
  String totalCostToCustomer;
  String comissionAmount;
  String discountAmount;
  String discountFlag;
  String timeSlot;
  String customerOrderId;
  String orderInstructions;
  String cancellationReason;
  String merchantproductid;
  String productname;
  String productdescription;
  String avgPrice;
  String mrp;
  String icon;
  String measure;
  String producttype;
  String catId;
  String sku;
  String subcatId;
  String offerId;
  String offerName;
  String offerDescription;
  String offerImage;
  String shipping;
  String deliveredAt;
  String discountCode;
  String paymentMode;
  String xnNumber;
  String promocodeApplied;
  String promocodeAmount;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        orderitemsid: json["orderitemsid"].toString(),
        masterproductid: json["masterproductid"].toString(),
        quantity: json["quantity"],
        unitsellingprice: json["unitsellingprice"].toString(),
        taxCollected: json["tax_collected"].toString(),
        merchantActualSellingPrice:
            json["merchant_actual_selling_price"].toString(),
        costToCustomer: json["cost_to_customer"].toString(),
        totalCostToCustomer: json["total_cost_to_customer"].toString(),
        comissionAmount: json["comission_amount"].toString(),
        discountAmount: json["discount_amount"] == null
            ? "0"
            : json["discount_amount"].toString(),
        discountFlag: json["discount_flag"].toString(),
        timeSlot: json["time_slot"].toString(),
        customerOrderId: json["customer_order_id"].toString(),
        orderInstructions: json["order_instructions"].toString(),
        cancellationReason: json["cancellation_reason"].toString(),
        merchantproductid: json["merchantproductid"].toString(),
        productname: json["productname"].toString(),
        productdescription: json["productdescription"].toString(),
        avgPrice: json["avg_price"].toString(),
        mrp: json["mrp"].toString(),
        icon: json["icon"].toString(),
        measure: json["measure"].toString(),
        producttype: json["producttype"].toString(),
        catId: json["cat_id"].toString(),
        sku: json["sku"].toString(),
        subcatId: json["subcat_id"].toString(),
        offerId: json["offer_id"].toString(),
        offerName: json["offer_name"].toString(),
        offerDescription: json["offer_description"].toString(),
        offerImage: json["offer_image"].toString(),
        shipping: json["shipping"].toString(),
        deliveredAt: json["delivered_at"].toString(),
        discountCode: json["discount_code"].toString(),
        paymentMode: json["payment_mode"].toString(),
        xnNumber: json["xn_number"].toString(),
        promocodeApplied: json["promocode_applied"].toString(),
        promocodeAmount: json["promocode_amount"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "orderitemsid": orderitemsid,
        "masterproductid": masterproductid,
        "quantity": quantity,
        "unitsellingprice": unitsellingprice,
        "tax_collected": taxCollected,
        "merchant_actual_selling_price": merchantActualSellingPrice,
        "cost_to_customer": costToCustomer,
        "total_cost_to_customer": totalCostToCustomer,
        "comission_amount": comissionAmount,
        "discount_amount": discountAmount,
        "discount_flag": discountFlag,
        "time_slot": timeSlot,
        "customer_order_id": customerOrderId,
        "order_instructions": orderInstructions,
        "cancellation_reason": cancellationReason,
        "merchantproductid": merchantproductid,
        "productname": productname,
        "productdescription": productdescription,
        "avg_price": avgPrice,
        "mrp": mrp,
        "icon": icon,
        "measure": measure,
        "producttype": producttype,
        "cat_id": catId,
        "sku": sku,
        "subcat_id": subcatId,
        "offer_id": offerId,
        "offer_name": offerName,
        "offer_description": offerDescription,
        "offer_image": offerImage,
        "shipping": shipping,
        "delivered_at": deliveredAt,
        "discount_code": discountCode,
        "payment_mode": paymentMode,
        "xn_number": xnNumber,
        "promocode_applied": promocodeApplied,
        "promocode_amount": promocodeAmount,
      };
}
