// To parse this JSON data, do
//
//     final paytmConfig = paytmConfigFromJson(jsonString);

import 'dart:convert';

PaytmConfig paytmConfigFromJson(String str) =>
    PaytmConfig.fromJson(json.decode(str));

String paytmConfigToJson(PaytmConfig data) => json.encode(data.toJson());

class PaytmConfig {
  PaytmConfig({
    this.info,
    this.status,
    this.message,
  });

  Info info;
  String status;
  String message;

  factory PaytmConfig.fromJson(Map<String, dynamic> json) => PaytmConfig(
        info: Info.fromJson(json["info"]),
        status: json["status"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "info": info.toJson(),
        "status": status,
        "message": message,
      };
}

class Info {
  Info({
    this.mId,
    this.channelId,
    this.industryTypeId,
    this.merchantKey,
  });

  String mId;
  String channelId;
  String industryTypeId;
  String merchantKey;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        mId: json["mID"].toString(),
        channelId: json["channelID"].toString(),
        industryTypeId: json["industry_typeID"].toString(),
        merchantKey: json["merchant_key"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "mID": mId,
        "channelID": channelId,
        "industry_typeID": industryTypeId,
        "merchant_key": merchantKey,
      };
}
