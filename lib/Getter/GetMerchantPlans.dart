// To parse this JSON data, do
//
//     final merchantPlan = merchantPlanFromJson(jsonString);

import 'dart:convert';

MerchantPlan merchantPlanFromJson(String str) =>
    MerchantPlan.fromJson(json.decode(str));

String merchantPlanToJson(MerchantPlan data) => json.encode(data.toJson());

class MerchantPlan {
  MerchantPlan({
    this.info,
    this.status,
    this.message,
  });

  List<Info> info;
  String status;
  String message;

  factory MerchantPlan.fromJson(Map<String, dynamic> json) => MerchantPlan(
        info: List<Info>.from(json["info"].map((x) => Info.fromJson(x))),
        status: json["status"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "info": List<dynamic>.from(info.map((x) => x.toJson())),
        "status": status,
        "message": message,
      };
}

class Info {
  Info({
    this.planId,
    this.description,
    this.planAmountMonthly,
    this.planAmountQuarterly,
    this.strikedMrp,
    this.planAmountYear,
    this.planAmountHalfYear,
    this.planFeatures,
  });

  int planId;
  String description;
  int planAmountMonthly;
  int planAmountQuarterly;
  int strikedMrp;
  int planAmountYear;
  int planAmountHalfYear;
  List<PlanFeature> planFeatures;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        planId: json["plan_id"],
        description: json["description"].toString(),
        planAmountMonthly: json["plan_amount_monthly"],
        planAmountQuarterly: json["plan_amount_quarterly"],
        strikedMrp: json["striked_mrp"],
        planAmountYear: json["plan_amount_year"],
        planAmountHalfYear: json["plan_amount_half_year"],
        planFeatures: List<PlanFeature>.from(
            json["plan_features"].map((x) => PlanFeature.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "plan_id": planId,
        "description": description,
        "plan_amount_monthly": planAmountMonthly,
        "plan_amount_quarterly": planAmountQuarterly,
        "striked_mrp": strikedMrp,
        "plan_amount_year": planAmountYear,
        "plan_amount_half_year": planAmountHalfYear,
        "plan_features":
            List<dynamic>.from(planFeatures.map((x) => x.toJson())),
      };
}

class PlanFeature {
  PlanFeature({
    this.id,
    this.planid,
    this.desr,
    this.featureDescr,
    this.allowed,
    this.createdAt,
    this.updatedAt,
    this.isactive,
  });

  int id;
  int planid;
  String desr;
  String featureDescr;
  String allowed;
  String createdAt;
  String updatedAt;
  int isactive;

  factory PlanFeature.fromJson(Map<String, dynamic> json) => PlanFeature(
        id: json["id"],
        planid: json["planid"],
        desr: json["desr"].toString(),
        featureDescr: json["feature_descr"].toString(),
        allowed: json["allowed"].toString(),
        createdAt: json["created_at"].toString(),
        updatedAt: json["updated_at"].toString(),
        isactive: json["isactive"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "planid": planid,
        "desr": desr,
        "feature_descr": featureDescr,
        "allowed": allowed,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "isactive": isactive,
      };
}
