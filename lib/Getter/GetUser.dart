class User {
  Meta meta;
  Data data;

  User({this.meta, this.data});

  User.fromJson(Map<String, dynamic> json) {
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Meta {
  String status;
  String message;

  Meta({this.status, this.message});

  Meta.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}

class Data {
  LoginDetails loginDetails;

  Data({this.loginDetails});

  Data.fromJson(Map<String, dynamic> json) {
    loginDetails = json['login_details'] != null
        ? new LoginDetails.fromJson(json['login_details'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.loginDetails != null) {
      data['login_details'] = this.loginDetails.toJson();
    }
    return data;
  }
}

class LoginDetails {
  String merchantId;
  String authKey;
  String userId;
  String isRegistered;
  String profileSetup;
  String storeSetup;
  String setupStoreAddress;

  LoginDetails(
      {this.merchantId,
      this.authKey,
      this.userId,
      this.isRegistered,
      this.profileSetup,
      this.storeSetup,
      this.setupStoreAddress});

  LoginDetails.fromJson(Map<String, dynamic> json) {
    merchantId = json['merchant_id'].toString();
    authKey = json['auth_key'].toString();
    userId = json['user_id'].toString();
    isRegistered = json['is_registered'].toString();
    profileSetup = json['profile_setup'].toString();
    storeSetup = json['store_setup'].toString();
    setupStoreAddress = json['setup_store_address'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['merchant_id'] = this.merchantId;
    data['auth_key'] = this.authKey;
    data['user_id'] = this.userId;
    data['is_registered'] = this.isRegistered;
    data['profile_setup'] = this.profileSetup;
    data['store_setup'] = this.storeSetup;
    data['setup_store_address'] = this.setupStoreAddress;
    return data;
  }
}
