// To parse this JSON data, do
//
//     final product = productFromJson(jsonString);

import 'dart:convert';

Product productFromJson(String str) => Product.fromJson(json.decode(str));

String productToJson(Product data) => json.encode(data.toJson());

class Product {
  Product({
    this.info,
    this.status,
  });

  Info info;
  String status;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        info: Info.fromJson(json["info"]),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": info.toJson(),
        "status": status,
      };
}

class Info {
  Info({
    this.data,
    this.status,
  });

  List<Datum> data;
  String status;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status,
      };
}

class Datum {
  Datum({
    this.id,
    this.merchantId,
    this.storeId,
    this.productId,
    this.categoryName,
    this.subcatId,
    this.subcatName,
    this.masterProductId,
    this.mrp,
    this.sellingPrice,
    this.taxStatus,
    this.taxPercent,
    this.status,
    this.tags,
    this.createdAt,
    this.updatedAt,
    this.isactive,
    this.totalSold,
    this.totalLeft,
    this.productname,
    this.productdescription,
    this.avgPrice,
    this.originalMrp,
    this.manufacture,
    this.colorcode,
    this.icon,
    this.measure,
    this.producttype,
    this.manufacturerId,
    this.systemStatus,
    this.catId,
    this.sku,
    this.addedBy,
    this.addedId,
    this.approvedAt,
    this.approvedBy,
    this.remark,
    this.barcode,
    this.merchantListId,
  });

  int id;
  int merchantId;
  int storeId;
  int productId;
  String categoryName;
  int subcatId;
  String subcatName;
  int masterProductId;
  String mrp;
  String sellingPrice;
  String taxStatus;
  int taxPercent;
  int status;
  String tags;
  String createdAt;
  String updatedAt;
  int isactive;
  int totalSold;
  int totalLeft;
  String productname;
  String productdescription;
  String avgPrice;
  String originalMrp;
  String manufacture;
  String colorcode;
  String icon;
  String measure;
  String producttype;
  int manufacturerId;
  String systemStatus;
  int catId;
  String sku;
  String addedBy;
  int addedId;
  String approvedAt;
  String approvedBy;
  String remark;
  String barcode;
  int merchantListId;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        merchantId: json["merchant_id"],
        storeId: json["store_id"],
        productId: json["product_id"],
        categoryName: json["category_name"].toString(),
        subcatId: json["subcat_id"],
        subcatName: json["subcat_name"].toString(),
        masterProductId: json["master_product_id"],
        mrp: json["mrp"].toString() == null
            ? "0"
            : json["mrp"].toString().replaceAll(',', ""),
        sellingPrice: json["selling_price"].toString() == null
            ? "0"
            : json["selling_price"].toString().replaceAll(',', ""),
        taxStatus: json["tax_status"],
        taxPercent: json["tax_percent"],
        status: json["status"],
        tags: json["tags"].toString(),
        createdAt: json["created_at"].toString(),
        updatedAt: json["updated_at"].toString(),
        isactive: json["isactive"],
        totalSold: json["total_sold"],
        totalLeft: json["total_left"],
        productname: json["productname"].toString(),
        productdescription: json["productdescription"].toString(),
        avgPrice: json["avg_price"].toString(),
        originalMrp: json["original_mrp"].toString().replaceAll(',', ""),
        manufacture: json["manufacture"].toString(),
        colorcode: json["colorcode"].toString(),
        icon: json["icon"].toString(),
        measure: json["measure"].toString(),
        producttype: json["producttype"].toString(),
        manufacturerId: json["manufacturerID"],
        systemStatus: json["system_status"].toString(),
        catId: json["cat_id"],
        sku: json["sku"].toString(),
        addedBy: json["added_by"].toString(),
        addedId: json["added_id"],
        approvedAt: json["approved_at"].toString(),
        approvedBy: json["approved_by"].toString(),
        remark: json["remark"].toString(),
        barcode: json["barcode"].toString(),
        merchantListId: json["merchant_list_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "merchant_id": merchantId,
        "store_id": storeId,
        "product_id": productId,
        "category_name": categoryName,
        "subcat_id": subcatId,
        "subcat_name": subcatName,
        "master_product_id": masterProductId,
        "mrp": mrp,
        "selling_price": sellingPrice,
        "tax_status": taxStatus,
        "tax_percent": taxPercent,
        "status": status,
        "tags": tags,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "isactive": isactive,
        "total_sold": totalSold,
        "total_left": totalLeft,
        "productname": productname,
        "productdescription": productdescription,
        "avg_price": avgPrice,
        "original_mrp": originalMrp,
        "manufacture": manufacture,
        "colorcode": colorcode,
        "icon": icon,
        "measure": measure,
        "producttype": producttype,
        "manufacturerID": manufacturerId,
        "system_status": systemStatus,
        "cat_id": catId,
        "sku": sku,
        "added_by": addedBy,
        "added_id": addedId,
        "approved_at": approvedAt,
        "approved_by": approvedBy,
        "remark": remark,
        "barcode": barcode,
        "merchant_list_id": merchantListId,
      };
}
