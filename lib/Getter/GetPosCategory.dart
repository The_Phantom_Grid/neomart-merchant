// To parse this JSON data, do
//
//     final posCategory = posCategoryFromJson(jsonString);

import 'dart:convert';

PosCategory posCategoryFromJson(String str) =>
    PosCategory.fromJson(json.decode(str));

String posCategoryToJson(PosCategory data) => json.encode(data.toJson());

class PosCategory {
  PosCategory({
    this.info,
    this.status,
  });

  Info info;
  String status;

  factory PosCategory.fromJson(Map<String, dynamic> json) => PosCategory(
        info: Info.fromJson(json["info"]),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": info.toJson(),
        "status": status,
      };
}

class Info {
  Info({
    this.category,
    this.subCategory,
    this.totalCart,
    this.status,
  });

  List<Category> category;
  List<SubCategory> subCategory;
  int totalCart;
  String status;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        category: List<Category>.from(
            json["category"].map((x) => Category.fromJson(x))),
        subCategory: List<SubCategory>.from(
            json["sub_category"].map((x) => SubCategory.fromJson(x))),
        totalCart: json["total_cart"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "category": List<dynamic>.from(category.map((x) => x.toJson())),
        "sub_category": List<dynamic>.from(subCategory.map((x) => x.toJson())),
        "total_cart": totalCart,
        "status": status,
      };
}

class Category {
  Category({
    this.prodCategoryId,
    this.prodCategoryName,
    this.colorcode,
    this.icons,
    this.productCount,
  });

  int prodCategoryId;
  String prodCategoryName;
  String colorcode;
  String icons;
  int productCount;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        prodCategoryId: json["prod_category_id"],
        prodCategoryName: json["prod_category_name"].toString(),
        colorcode: json["colorcode"].toString(),
        icons: json["icons"].toString(),
        productCount: json["product_count"],
      );

  Map<String, dynamic> toJson() => {
        "prod_category_id": prodCategoryId,
        "prod_category_name": prodCategoryName,
        "colorcode": colorcode,
        "icons": icons,
        "product_count": productCount,
      };
}

class SubCategory {
  SubCategory({
    this.subcatId,
    this.name,
    this.description,
    this.categoryId,
    this.productCount,
  });

  int subcatId;
  String name;
  String description;
  int categoryId;
  int productCount;

  factory SubCategory.fromJson(Map<String, dynamic> json) => SubCategory(
        subcatId: json["subcat_id"],
        name: json["name"].toString(),
        description: json["description"].toString(),
        categoryId: json["category_id"],
        productCount: json["product_count"],
      );

  Map<String, dynamic> toJson() => {
        "subcat_id": subcatId,
        "name": name,
        "description": description,
        "category_id": categoryId,
        "product_count": productCount,
      };
}
