// To parse this JSON data, do
//
//     final posOffer = posOfferFromJson(jsonString);

import 'dart:convert';

PosOffer posOfferFromJson(String str) => PosOffer.fromJson(json.decode(str));

String posOfferToJson(PosOffer data) => json.encode(data.toJson());

class PosOffer {
  PosOffer({
    this.info,
    this.totalCart,
    this.status,
  });

  List<PosOfferInfo> info;
  int totalCart;
  String status;

  factory PosOffer.fromJson(Map<String, dynamic> json) => PosOffer(
    info: List<PosOfferInfo>.from(json["info"].map((x) => PosOfferInfo.fromJson(x))),
    totalCart: json["total_cart"],
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "info": List<dynamic>.from(info.map((x) => x.toJson())),
    "total_cart": totalCart,
    "status": status,
  };
}

class PosOfferInfo {
  PosOfferInfo({
    this.collectionId,
    this.cartCount,
    this.collectionName,
    this.collectionDescr,
    this.validFrom,
    this.validTo,
    this.comboPrice,
    this.inventory,
    this.isPublished,
    this.collectionProducts,
    this.collectionImages,
  });

  int collectionId;
  int cartCount;
  String collectionName;
  String collectionDescr;
  DateTime validFrom;
  DateTime validTo;
  int comboPrice;
  int inventory;
  int isPublished;
  List<CollectionProduct> collectionProducts;
  List<CollectionImage> collectionImages;

  factory PosOfferInfo.fromJson(Map<String, dynamic> json) => PosOfferInfo(
    collectionId: json["collection_id"],
    cartCount: json["cart_count"],
    collectionName: json["collection_name"],
    collectionDescr: json["collection_descr"],
    validFrom: DateTime.parse(json["valid_from"]),
    validTo: DateTime.parse(json["valid_to"]),
    comboPrice: json["combo_price"],
    inventory: json["inventory"],
    isPublished: json["is_published"],
    collectionProducts: List<CollectionProduct>.from(json["collection_products"].map((x) => CollectionProduct.fromJson(x))),
    collectionImages: List<CollectionImage>.from(json["collection_images"].map((x) => CollectionImage.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "collection_id": collectionId,
    "cart_count": cartCount,
    "collection_name": collectionName,
    "collection_descr": collectionDescr,
    "valid_from": validFrom.toIso8601String(),
    "valid_to": validTo.toIso8601String(),
    "combo_price": comboPrice,
    "inventory": inventory,
    "is_published": isPublished,
    "collection_products": List<dynamic>.from(collectionProducts.map((x) => x.toJson())),
    "collection_images": List<dynamic>.from(collectionImages.map((x) => x.toJson())),
  };
}

class CollectionImage {
  CollectionImage({
    this.id,
    this.collectionId,
    this.imageUrl,
    this.createdAt,
    this.updatedAt,
    this.isactive,
  });

  int id;
  int collectionId;
  String imageUrl;
  DateTime createdAt;
  dynamic updatedAt;
  int isactive;

  factory CollectionImage.fromJson(Map<String, dynamic> json) => CollectionImage(
    id: json["id"],
    collectionId: json["collection_id"],
    imageUrl: json["image_url"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"],
    isactive: json["isactive"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "collection_id": collectionId,
    "image_url": imageUrl,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt,
    "isactive": isactive,
  };
}

class CollectionProduct {
  CollectionProduct({
    this.id,
    this.collectionId,
    this.masterProductId,
    this.productName,
    this.productDescr,
    this.productImage,
    this.merchantProductId,
    this.merchantId,
    this.storeId,
    this.catId,
    this.subcatId,
    this.catName,
    this.subcatName,
    this.productPrice,
    this.createdId,
    this.updatedAt,
    this.isactive,
    this.actualProductPrice,
  });

  int id;
  int collectionId;
  int masterProductId;
  String productName;
  String productDescr;
  String productImage;
  int merchantProductId;
  int merchantId;
  int storeId;
  int catId;
  int subcatId;
  String catName;
  String subcatName;
  int productPrice;
  String createdId;
  String updatedAt;
  int isactive;
  String actualProductPrice;

  factory CollectionProduct.fromJson(Map<String, dynamic> json) => CollectionProduct(
    id: json["id"],
    collectionId: json["collection_id"],
    masterProductId: json["master_product_id"],
    productName: json["product_name"].toString(),
    productDescr: json["product_descr"].toString(),
    productImage: json["product_image"].toString(),
    merchantProductId: json["merchant_product_id"],
    merchantId: json["merchant_id"],
    storeId: json["store_id"],
    catId: json["cat_id"],
    subcatId: json["subcat_id"],
    catName: json["cat_name"].toString(),
    subcatName: json["subcat_name"].toString(),
    productPrice: json["product_price"],
    createdId: json["created_id"].toString(),
    updatedAt: json["updated_at"].toString(),
    isactive: json["isactive"],
    actualProductPrice: json["actual_product_price"].toString(),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "collection_id": collectionId,
    "master_product_id": masterProductId,
    "product_name": productName,
    "product_descr": productDescr,
    "product_image": productImage,
    "merchant_product_id": merchantProductId,
    "merchant_id": merchantId,
    "store_id": storeId,
    "cat_id": catId,
    "subcat_id": subcatId,
    "cat_name": catName,
    "subcat_name": subcatName,
    "product_price": productPrice,
    "created_id": createdId,
    "updated_at": updatedAt,
    "isactive": isactive,
    "actual_product_price": actualProductPrice,
  };
}
