// To parse this JSON data, do
//
//     final searchNumber = searchNumberFromJson(jsonString);

import 'dart:convert';

SearchNumber searchNumberFromJson(String str) =>
    SearchNumber.fromJson(json.decode(str));

String searchNumberToJson(SearchNumber data) => json.encode(data.toJson());

class SearchNumber {
  SearchNumber({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  List<Datum> data;

  factory SearchNumber.fromJson(Map<String, dynamic> json) => SearchNumber(
        status: json["status"],
        message: json["message"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.userNumber,
  });

  String userNumber;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        userNumber: json["user_number"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "user_number": userNumber,
      };
}
