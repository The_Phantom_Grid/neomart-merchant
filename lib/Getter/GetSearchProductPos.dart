// To parse this JSON data, do
//
//     final productPos = productPosFromJson(jsonString);

import 'dart:convert';

ProductPos productPosFromJson(String str) =>
    ProductPos.fromJson(json.decode(str));

String productPosToJson(ProductPos data) => json.encode(data.toJson());

class ProductPos {
  ProductPos({
    this.info,
    this.status,
  });

  List<Info> info;
  String status;

  factory ProductPos.fromJson(Map<String, dynamic> json) => ProductPos(
        info: List<Info>.from(json["info"].map((x) => Info.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": List<dynamic>.from(info.map((x) => x.toJson())),
        "status": status,
      };
}

class Info {
  Info({
    this.id,
    this.merchantId,
    this.cartCount,
    this.storeId,
    this.productId,
    this.categoryName,
    this.subcatId,
    this.subcatName,
    this.masterProductId,
    this.productMrp,
    this.sellingPrice,
    this.taxStatus,
    this.taxPercent,
    this.status,
    this.tags,
    this.createdAt,
    this.updatedAt,
    this.isactive,
    this.totalSold,
    this.totalLeft,
    this.productname,
    this.productdescription,
    this.avgPrice,
    this.mrp,
    this.manufacture,
    this.colorcode,
    this.icon,
    this.measure,
    this.producttype,
    this.manufacturerId,
    this.systemStatus,
    this.catId,
    this.sku,
    this.addedBy,
    this.addedId,
    this.approvedAt,
    this.approvedBy,
    this.remark,
    this.barcode,
    this.merchantListId,
  });

  int id;
  int merchantId;
  int cartCount;
  int storeId;
  int productId;
  String categoryName;
  int subcatId;
  String subcatName;
  int masterProductId;
  String productMrp;
  String sellingPrice;
  String taxStatus;
  int taxPercent;
  int status;
  String tags;
  String createdAt;
  String updatedAt;
  int isactive;
  int totalSold;
  int totalLeft;
  String productname;
  String productdescription;
  int avgPrice;
  String mrp;
  String manufacture;
  String colorcode;
  String icon;
  String measure;
  String producttype;
  int manufacturerId;
  String systemStatus;
  int catId;
  String sku;
  String addedBy;
  int addedId;
  String approvedAt;
  String approvedBy;
  String remark;
  String barcode;
  int merchantListId;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        id: json["id"],
        merchantId: json["merchant_id"],
        cartCount: json["cart_count"],
        storeId: json["store_id"],
        productId: json["product_id"],
        categoryName: json["category_name"].toString(),
        subcatId: json["subcat_id"],
        subcatName: json["subcat_name"].toString(),
        masterProductId: json["master_product_id"],
        productMrp: json["product_mrp"].toString().replaceAll(',', ""),
        sellingPrice: json["selling_price"].toString().replaceAll(',', ""),
        taxStatus: json["tax_status"].toString(),
        taxPercent: json["tax_percent"],
        status: json["status"],
        tags: json["tags"].toString(),
        createdAt: json["created_at"].toString(),
        updatedAt: json["updated_at"].toString(),
        isactive: json["isactive"],
        totalSold: json["total_sold"],
        totalLeft: json["total_left"],
        productname: json["productname"].toString(),
        productdescription: json["productdescription"].toString(),
        avgPrice: json["avg_price"],
        mrp: json["mrp"].toString().replaceAll(',', ""),
        manufacture: json["manufacture"].toString(),
        colorcode: json["colorcode"].toString(),
        icon: json["icon"].toString(),
        measure: json["measure"].toString(),
        producttype: json["producttype"].toString(),
        manufacturerId: json["manufacturerID"],
        systemStatus: json["system_status"],
        catId: json["cat_id"],
        sku: json["sku"].toString(),
        addedBy: json["added_by"].toString(),
        addedId: json["added_id"],
        approvedAt: json["approved_at"].toString(),
        approvedBy: json["approved_by"].toString(),
        remark: json["remark"].toString(),
        barcode: json["barcode"].toString(),
        merchantListId: json["merchant_list_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "merchant_id": merchantId,
        "cart_count": cartCount,
        "store_id": storeId,
        "product_id": productId,
        "category_name": categoryName,
        "subcat_id": subcatId,
        "subcat_name": subcatName,
        "master_product_id": masterProductId,
        "product_mrp": productMrp,
        "selling_price": sellingPrice,
        "tax_status": taxStatus,
        "tax_percent": taxPercent,
        "status": status,
        "tags": tags,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "isactive": isactive,
        "total_sold": totalSold,
        "total_left": totalLeft,
        "productname": productname,
        "productdescription": productdescription,
        "avg_price": avgPrice,
        "mrp": mrp,
        "manufacture": manufacture,
        "colorcode": colorcode,
        "icon": icon,
        "measure": measure,
        "producttype": producttype,
        "manufacturerID": manufacturerId,
        "system_status": systemStatus,
        "cat_id": catId,
        "sku": sku,
        "added_by": addedBy,
        "added_id": addedId,
        "approved_at": approvedAt,
        "approved_by": approvedBy,
        "remark": remark,
        "barcode": barcode,
        "merchant_list_id": merchantListId,
      };
}
