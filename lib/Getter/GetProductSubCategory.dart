// To parse this JSON data, do
//
//     final productSubCategory = productSubCategoryFromJson(jsonString);

import 'dart:convert';

ProductSubCategory productSubCategoryFromJson(String str) =>
    ProductSubCategory.fromJson(json.decode(str));

String productSubCategoryToJson(ProductSubCategory data) =>
    json.encode(data.toJson());

class ProductSubCategory {
  ProductSubCategory({
    this.info,
    this.status,
  });

  List<SubCategoryInfo> info;
  String status;

  factory ProductSubCategory.fromJson(Map<String, dynamic> json) =>
      ProductSubCategory(
        info: List<SubCategoryInfo>.from(
            json["info"].map((x) => SubCategoryInfo.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": List<dynamic>.from(info.map((x) => x.toJson())),
        "status": status,
      };
}

class SubCategoryInfo {
  SubCategoryInfo({
    this.subcatId,
    this.name,
    this.description,
    this.categoryId,
    this.productCount,
  });

  int subcatId;
  String name;
  String description;
  int categoryId;
  int productCount;

  factory SubCategoryInfo.fromJson(Map<String, dynamic> json) =>
      SubCategoryInfo(
        subcatId: json["subcat_id"],
        name: json["name"].toString(),
        description: json["description"].toString(),
        categoryId: json["category_id"],
        productCount: json["product_count"],
      );

  Map<String, dynamic> toJson() => {
        "subcat_id": subcatId,
        "name": name,
        "description": description,
        "category_id": categoryId,
        "product_count": productCount,
      };
}
