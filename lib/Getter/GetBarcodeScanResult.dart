// To parse this JSON data, do
//
//     final barcodeScanResult = barcodeScanResultFromJson(jsonString);

import 'dart:convert';

BarcodeScanResult barcodeScanResultFromJson(String str) =>
    BarcodeScanResult.fromJson(json.decode(str));

String barcodeScanResultToJson(BarcodeScanResult data) =>
    json.encode(data.toJson());

class BarcodeScanResult {
  BarcodeScanResult({
    this.product,
    this.suppliers,
    this.status,
  });

  List<Product> product;
  List<Supplier> suppliers;
  String status;

  factory BarcodeScanResult.fromJson(Map<String, dynamic> json) =>
      BarcodeScanResult(
        product:
            List<Product>.from(json["product"].map((x) => Product.fromJson(x))),
        suppliers: List<Supplier>.from(
            json["suppliers"].map((x) => Supplier.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "product": List<dynamic>.from(product.map((x) => x.toJson())),
        "suppliers": List<dynamic>.from(suppliers.map((x) => x.toJson())),
        "status": status,
      };
}

class Product {
  Product({
    this.id,
    this.merchantId,
    this.storeId,
    this.productId,
    this.subcatId,
    this.masterProductId,
    this.productMrp,
    this.sellingPrice,
    this.discount,
    this.taxStatus,
    this.taxPercent,
    this.status,
    this.tags,
    this.createdAt,
    this.updatedAt,
    this.isactive,
    this.totalSold,
    this.totalLeft,
    this.barcodeNo,
    this.expireyDate,
    this.productname,
    this.productdescription,
    this.avgPrice,
    this.mrp,
    this.manufacture,
    this.colorcode,
    this.icon,
    this.measure,
    this.producttype,
    this.manufacturerId,
    this.systemStatus,
    this.catId,
    this.sku,
    this.addedBy,
    this.addedId,
    this.approvedAt,
    this.approvedBy,
    this.remark,
    this.barcode,
    this.hsnNumber,
    this.skuNumber,
    this.isDefault,
    this.productType,
    this.imageName,
    this.imageType,
    this.imagePath,
    this.mproductId,
    this.imageFor,
    this.size,
    this.isFeatured,
    this.merchantlistId,
  });

  int id;
  int merchantId;
  String storeId;
  int productId;
  int subcatId;
  int masterProductId;
  String productMrp;
  String sellingPrice;
  String discount;
  String taxStatus;
  String taxPercent;
  int status;
  String tags;
  String createdAt;
  String updatedAt;
  int isactive;
  int totalSold;
  int totalLeft;
  String barcodeNo;
  String expireyDate;
  String productname;
  String productdescription;
  String avgPrice;
  String mrp;
  String manufacture;
  String colorcode;
  String icon;
  String measure;
  String producttype;
  int manufacturerId;
  String systemStatus;
  int catId;
  String sku;
  String addedBy;
  int addedId;
  String approvedAt;
  String approvedBy;
  String remark;
  String barcode;
  String hsnNumber;
  String skuNumber;
  String isDefault;
  String productType;
  String imageName;
  String imageType;
  String imagePath;
  int mproductId;
  String imageFor;
  String size;
  String isFeatured;
  int merchantlistId;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json["id"],
        merchantId: json["merchant_id"],
        storeId: json["store_id"].toString(),
        productId: json["product_id"],
        subcatId: json["subcat_id"],
        masterProductId: json["master_product_id"],
        productMrp: json["product_mrp"].toString().replaceAll(',', ""),
        sellingPrice: json["selling_price"].toString().replaceAll(',', ""),
        discount: json["discount"].toString().replaceAll(',', ""),
        taxStatus: json["tax_status"],
        taxPercent: json["tax_percent"].toString().replaceAll(',', ""),
        status: json["status"],
        tags: json["tags"].toString(),
        createdAt: json["created_at"].toString(),
        updatedAt: json["updated_at"].toString(),
        isactive: json["isactive"],
        totalSold: json["total_sold"],
        totalLeft: json["total_left"],
        barcodeNo: json["barcode_no"].toString(),
        expireyDate: json["expirey_date"].toString(),
        productname: json["productname"].toString(),
        productdescription: json["productdescription"].toString(),
        avgPrice: json["avg_price"].toString().replaceAll(',', ""),
        mrp: json["mrp"].toString().replaceAll(',', ""),
        manufacture: json["manufacture"].toString(),
        colorcode: json["colorcode"].toString(),
        icon: json["icon"].toString(),
        measure: json["measure"].toString(),
        producttype: json["producttype"].toString(),
        manufacturerId: json["manufacturerID"],
        systemStatus: json["system_status"].toString(),
        catId: json["cat_id"],
        sku: json["sku"].toString(),
        addedBy: json["added_by"].toString(),
        addedId: json["added_id"],
        approvedAt: json["approved_at"].toString(),
        approvedBy: json["approved_by"].toString(),
        remark: json["remark"].toString(),
        barcode: json["barcode"].toString(),
        hsnNumber: json["hsn_number"].toString(),
        skuNumber: json["sku_number"].toString(),
        isDefault: json["is_default"],
        productType: json["product_type"].toString(),
        imageName: json["image_name"].toString(),
        imageType: json["image_type"].toString(),
        imagePath: json["image_path"].toString(),
        mproductId: json["mproduct_id"],
        imageFor: json["image_for"].toString(),
        size: json["size"].toString(),
        isFeatured: json["is_featured"].toString(),
        merchantlistId: json["merchantlist_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "merchant_id": merchantId,
        "store_id": storeId,
        "product_id": productId,
        "subcat_id": subcatId,
        "master_product_id": masterProductId,
        "product_mrp": productMrp,
        "selling_price": sellingPrice,
        "discount": discount,
        "tax_status": taxStatus,
        "tax_percent": taxPercent,
        "status": status,
        "tags": tags,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "isactive": isactive,
        "total_sold": totalSold,
        "total_left": totalLeft,
        "barcode_no": barcodeNo,
        "expirey_date": expireyDate,
        "productname": productname,
        "productdescription": productdescription,
        "avg_price": avgPrice,
        "mrp": mrp,
        "manufacture": manufacture,
        "colorcode": colorcode,
        "icon": icon,
        "measure": measure,
        "producttype": producttype,
        "manufacturerID": manufacturerId,
        "system_status": systemStatus,
        "cat_id": catId,
        "sku": sku,
        "added_by": addedBy,
        "added_id": addedId,
        "approved_at": approvedAt,
        "approved_by": approvedBy,
        "remark": remark,
        "barcode": barcode,
        "hsn_number": hsnNumber,
        "sku_number": skuNumber,
        "is_default": isDefault,
        "product_type": productType,
        "image_name": imageName,
        "image_type": imageType,
        "image_path": imagePath,
        "mproduct_id": mproductId,
        "image_for": imageFor,
        "size": size,
        "is_featured": isFeatured,
        "merchantlist_id": merchantlistId,
      };
}

class Supplier {
  Supplier({
    this.id,
    this.supplierId,
    this.masterProductId,
    this.merchantProductId,
    this.createdAt,
    this.updatedAt,
    this.isactive,
    this.supplierName,
    this.supplierAddress,
    this.phoneNo,
    this.contactPerson,
    this.gstnNo,
    this.profileImage,
    this.storeId,
    this.merchantId,
    this.productId,
    this.subcatId,
    this.productMrp,
    this.sellingPrice,
    this.discount,
    this.taxStatus,
    this.taxPercent,
    this.status,
    this.tags,
    this.totalSold,
    this.totalLeft,
    this.barcodeNo,
    this.expireyDate,
  });

  int id;
  int supplierId;
  int masterProductId;
  int merchantProductId;
  String createdAt;
  String updatedAt;
  int isactive;
  String supplierName;
  String supplierAddress;
  String phoneNo;
  String contactPerson;
  String gstnNo;
  String profileImage;
  int storeId;
  int merchantId;
  int productId;
  int subcatId;
  String productMrp;
  String sellingPrice;
  String discount;
  String taxStatus;
  String taxPercent;
  int status;
  String tags;
  int totalSold;
  int totalLeft;
  String barcodeNo;
  String expireyDate;

  factory Supplier.fromJson(Map<String, dynamic> json) => Supplier(
        id: json["id"],
        supplierId: json["supplier_id"],
        masterProductId: json["master_product_id"],
        merchantProductId: json["merchant_product_id"],
        createdAt: json["created_at"].toString(),
        updatedAt: json["updated_at"].toString(),
        isactive: json["isactive"],
        supplierName: json["supplier_name"].toString(),
        supplierAddress: json["supplier_address"].toString(),
        phoneNo: json["phone_no"].toString(),
        contactPerson: json["contact_person"].toString(),
        gstnNo: json["gstn_no"].toString(),
        profileImage: json["profile_image"].toString(),
        storeId: json["store_id"],
        merchantId: json["merchant_id"],
        productId: json["product_id"],
        subcatId: json["subcat_id"],
        productMrp: json["product_mrp"].toString().replaceAll(',', ""),
        sellingPrice: json["selling_price"].toString().replaceAll(',', ""),
        discount: json["discount"].toString().replaceAll(',', ""),
        taxStatus: json["tax_status"],
        taxPercent: json["tax_percent"].toString(),
        status: json["status"],
        tags: json["tags"].toString(),
        totalSold: json["total_sold"],
        totalLeft: json["total_left"],
        barcodeNo: json["barcode_no"].toString(),
        expireyDate: json["expirey_date"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "supplier_id": supplierId,
        "master_product_id": masterProductId,
        "merchant_product_id": merchantProductId,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "isactive": isactive,
        "supplier_name": supplierName,
        "supplier_address": supplierAddress,
        "phone_no": phoneNo,
        "contact_person": contactPerson,
        "gstn_no": gstnNo,
        "profile_image": profileImage,
        "store_id": storeId,
        "merchant_id": merchantId,
        "product_id": productId,
        "subcat_id": subcatId,
        "product_mrp": productMrp,
        "selling_price": sellingPrice,
        "discount": discount,
        "tax_status": taxStatus,
        "tax_percent": taxPercent,
        "status": status,
        "tags": tags,
        "total_sold": totalSold,
        "total_left": totalLeft,
        "barcode_no": barcodeNo,
        "expirey_date": expireyDate,
      };
}
