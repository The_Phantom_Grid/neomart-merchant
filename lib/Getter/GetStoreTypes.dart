// To parse this JSON data, do
//
//     final storeTypes = storeTypesFromJson(jsonString);

import 'dart:convert';

StoreTypes storeTypesFromJson(String str) =>
    StoreTypes.fromJson(json.decode(str));

String storeTypesToJson(StoreTypes data) => json.encode(data.toJson());

class StoreTypes {
  StoreTypes({
    this.data,
    this.status,
  });

  List<Datum> data;
  String status;

  factory StoreTypes.fromJson(Map<String, dynamic> json) => StoreTypes(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status,
      };
}

class Datum {
  Datum({
    this.id,
    this.catName,
    this.catDescr,
    this.systemCatConfigId,
    this.systemCatConfigName,
    this.createdAt,
    this.updatedAt,
    this.isactive,
    this.icons,
  });

  int id;
  String catName;
  String catDescr;
  int systemCatConfigId;
  SystemCatConfigName systemCatConfigName;
  DateTime createdAt;
  DateTime updatedAt;
  int isactive;
  String icons;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        catName: json["cat_name"],
        catDescr: json["cat_descr"],
        systemCatConfigId: json["system_cat_config_id"],
        systemCatConfigName:
            systemCatConfigNameValues.map[json["system_cat_config_name"]],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        isactive: json["isactive"],
        icons: json["icons"] == null ? null : json["icons"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "cat_name": catName,
        "cat_descr": catDescr,
        "system_cat_config_id": systemCatConfigId,
        "system_cat_config_name":
            systemCatConfigNameValues.reverse[systemCatConfigName],
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "isactive": isactive,
        "icons": icons == null ? null : icons,
      };
}

enum SystemCatConfigName { SHOPPING }

final systemCatConfigNameValues =
    EnumValues({"SHOPPING": SystemCatConfigName.SHOPPING});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
