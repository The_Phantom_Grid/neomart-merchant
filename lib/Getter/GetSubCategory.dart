// To parse this JSON data, do
//
//     final subCategory = subCategoryFromJson(jsonString);

import 'dart:convert';

SubCategory subCategoryFromJson(String str) =>
    SubCategory.fromJson(json.decode(str));

String subCategoryToJson(SubCategory data) => json.encode(data.toJson());

class SubCategory {
  SubCategory({
    this.info,
    this.status,
  });

  List<Info> info;
  String status;

  factory SubCategory.fromJson(Map<String, dynamic> json) => SubCategory(
        info: List<Info>.from(json["info"].map((x) => Info.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": List<dynamic>.from(info.map((x) => x.toJson())),
        "status": status,
      };
}

class Info {
  Info({
    this.subcategoryId,
    this.subCategoryName,
    this.categoryId,
    this.categoryImage,
    this.checkStatus,
  });

  int subcategoryId;
  String subCategoryName;
  int categoryId;
  String categoryImage;
  bool checkStatus;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        subcategoryId: json["subcategory_id"],
        subCategoryName: json["sub_category_name"],
        categoryId: json["category_id"],
        categoryImage: json["category_image"],
        checkStatus: json["check_status"],
      );

  Map<String, dynamic> toJson() => {
        "subcategory_id": subcategoryId,
        "sub_category_name": subCategoryName,
        "category_id": categoryId,
        "category_image": categoryImage,
        "check_status": checkStatus,
      };
}
