// To parse this JSON data, do
//
//     final revenue = revenueFromJson(jsonString);

import 'dart:convert';

StoreOrders revenueFromJson(String str) =>
    StoreOrders.fromJson(json.decode(str));

String revenueToJson(StoreOrders data) => json.encode(data.toJson());

class StoreOrders {
  StoreOrders({
    this.data,
    this.status,
    this.message,
  });

  Data data;
  String status;
  String message;

  factory StoreOrders.fromJson(Map<String, dynamic> json) => StoreOrders(
        data: Data.fromJson(json["data"]),
        status: json["status"].toString(),
        message: json["message"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "data": data.toJson(),
        "status": status,
        "message": message,
      };
}

class Data {
  Data({
    this.totalOrders,
  });

  String totalOrders;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        totalOrders: json["total_orders"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "total_orders": totalOrders,
      };
}
