// To parse this JSON data, do
//
//     final getCustomerData = getCustomerDataFromJson(jsonString);

import 'dart:convert';

GetCustomerData getCustomerDataFromJson(String str) =>
    GetCustomerData.fromJson(json.decode(str));

String getCustomerDataToJson(GetCustomerData data) =>
    json.encode(data.toJson());

class GetCustomerData {
  GetCustomerData({
    this.data,
    this.status,
  });

  List<Datum> data;
  String status;

  factory GetCustomerData.fromJson(Map<String, dynamic> json) =>
      GetCustomerData(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "status": status,
      };
}

class Datum {
  Datum({
    this.userid,
    this.userNumber,
    this.userEmail,
    this.firstname,
    this.middlename,
    this.lastname,
    this.avatarPath,
    this.avatarBaseUrl,
    this.gender,
//    this.customerAddress,
  });

  int userid;
  int userNumber;
  String userEmail;
  String firstname;
  String middlename;
  String lastname;
  String avatarPath;
  String avatarBaseUrl;
  String gender;

//  List<CustomerAddressElement> customerAddress;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        userid: json["userid"],
        userNumber: json["user_number"],
        userEmail:
            json["user_email"] == null ? null : json["user_email"].toString(),
        firstname: json["firstname"].toString(),
        middlename: json["middlename"].toString(),
        lastname: json["lastname"].toString(),
        avatarPath:
            json["avatar_path"] == null ? null : json["avatar_path"].toString(),
        avatarBaseUrl: json["avatar_base_url"] == null
            ? null
            : json["avatar_base_url"].toString(),
        gender: json["gender"].toString(),
//    customerAddress: json["customer_address"],
      );

  Map<String, dynamic> toJson() => {
        "userid": userid,
        "user_number": userNumber,
        "user_email": userEmail == null ? null : userEmail,
        "firstname": firstname,
        "middlename": middlename,
        "lastname": lastname,
        "avatar_path": avatarPath == null ? null : avatarPath,
        "avatar_base_url": avatarBaseUrl == null ? null : avatarBaseUrl,
        "gender": gender,
//    "customer_address": customerAddress,
      };
}

class CustomerAddressElement {
  CustomerAddressElement({
    this.id,
    this.userId,
    this.address,
    this.pincode,
    this.city,
    this.state,
    this.lat,
    this.lng,
    this.isDefault,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.fullName,
    this.flatHouseno,
    this.colonyStreet,
    this.landmark,
  });

  int id;
  int userId;
  String address;
  int pincode;
  String city;
  String state;
  double lat;
  double lng;
  int isDefault;
  int isActive;
  String createdAt;
  String updatedAt;
  String fullName;
  String flatHouseno;
  String colonyStreet;
  String landmark;

  factory CustomerAddressElement.fromJson(Map<String, dynamic> json) =>
      CustomerAddressElement(
        id: json["id"],
        userId: json["user_id"],
        address: json["address"],
        pincode: json["pincode"] == null ? null : json["pincode"],
        city: json["city"],
        state: json["state"],
        lat: json["lat"] == null ? null : json["lat"].toDouble(),
        lng: json["lng"] == null ? null : json["lng"].toDouble(),
        isDefault: json["is_default"],
        isActive: json["is_active"],
        createdAt: json["created_at"].toString(),
        updatedAt: json["updated_at"].toString(),
        fullName:
            json["full_name"] == null ? null : json["full_name"].toString(),
        flatHouseno: json["flat_houseno"] == null
            ? null
            : json["flat_houseno"].toString(),
        colonyStreet: json["colony_street"] == null
            ? null
            : json["colony_street"].toString(),
        landmark: json["landmark"] == null ? null : json["landmark"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "address": address,
        "pincode": pincode == null ? null : pincode,
        "city": city,
        "state": state,
        "lat": lat == null ? null : lat,
        "lng": lng == null ? null : lng,
        "is_default": isDefault,
        "is_active": isActive,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "full_name": fullName == null ? null : fullName,
        "flat_houseno": flatHouseno == null ? null : flatHouseno,
        "colony_street": colonyStreet == null ? null : colonyStreet,
        "landmark": landmark == null ? null : landmark,
      };
}

class PurpleCustomerAddress {
  PurpleCustomerAddress({
    this.message,
  });

  String message;

  factory PurpleCustomerAddress.fromJson(Map<String, dynamic> json) =>
      PurpleCustomerAddress(
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "message": message,
      };
}
