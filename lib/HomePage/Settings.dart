import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:merchant/ChooseStore.dart';
import 'package:merchant/Components/VariableControllers.dart';
import 'package:merchant/Getter/GetUserProfile.dart';
import 'package:merchant/LoginScreenComponents/Login.dart';
import 'package:merchant/MyStoreInventory.dart';
import 'package:merchant/MyStoreSetup.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/Settings/IntegratePaymentMethod.dart';
import 'package:merchant/Settings/ReferralPage.dart';
import 'package:merchant/Settings/SalesSummary.dart';
import 'package:merchant/Webview.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import '../Settings/MyProfile.dart';
import '../constants.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  HttpRequests requests = HttpRequests();
  bool loading = true;
  String appVersion;
  String SERVICE_CALL = "9319392255";

  getProfile() async {
    await getMerchantProfile();
    setState(() {
      loading = false;
    });
  }

  getDeviceInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    setState(() {
      appVersion = packageInfo.version;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    if (userProfile == null)
      getProfile();
    else
      loading = false;
    getDeviceInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    VarController v = Get.put(VarController());

    return loading
        ? loadingCircular()
        : SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).padding.bottom),
              child: Container(
                color: Colors.white38,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  "Settings",
                  style: subHeading,
                ),
              ),
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0)),
                  elevation: 0.0,
                  child: Column(
                    children: <Widget>[
                      ListTile(
                        onTap: () async {
                          await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MyProfile()));
                          setState(() {});
                        },
                        leading: ClipRRect(
                            borderRadius: BorderRadius.circular(40),
                            child: Container(
                                height: 50, width: 50, child: FadeInImage
                                .assetNetwork(placeholder: 'assets/tsp.png',
                              image: "${userProfile.profileData[0]
                                  .avatarBaseUrl}${userProfile.profileData[0]
                                  .avatarPath}", fit: BoxFit.cover,))),
                        title: Text(userProfile.profileData[0].firstname,
                          style: subHeading,),
                        subtitle: Text(
                          userProfile.profileData[0].city, style: regularText,),
                      ),
//                      Divider(
//                        height: 0,
//                      ),
//                      ListTile(
//                        onTap: () {
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (context) => ChooseStore()));
//                        },
//                        leading: Icon(
//                          Icons.store_mall_directory,
//                          color: Colors.black,
//                        ),
//                        title: Text("Store List", style: regularText,),
//                      ),
                      Divider(
                        height: 0,
                      ),
                      ListTile(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => IntegratePaymentMethod()));
                        },
                        leading: SvgPicture.asset('assets/paytm.svg',
                          color: Colors.black,
                          height: 15,
                        ),
                        title: Text("Integrate Paytm", style: regularText,),
                      ),
                    ],
                  )),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  "Store Settings",
                  style: subHeading,
                ),
              ),
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0)),
                  elevation: 0.0,
                  child: Column(
                    children: <Widget>[
                      ListTile(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MyStoreSetup()));
                        },
                        leading: Icon(
                          Icons.store,
                          color: Colors.black,
                        ),
                        title: Text("My Store Setup", style: regularText,),
                      ),
                      ListTile(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MyStoreInventory()));
                        },
                        leading: Icon(
                          Icons.store,
                          color: Colors.black,
                        ),
                        title: Text("My Store Inventory", style: regularText,),
                      ),
                    ],
                  )),
              // Padding(
              //   padding: const EdgeInsets.all(4.0),
              //   child: Text(
              //     "Store Accounts",
              //     style: subHeading,
              //   ),
              // ),
              // Card(
              //     shape: RoundedRectangleBorder(
              //         borderRadius: BorderRadius.circular(0)),
              //     elevation: 0.0,
              //     child: Column(
              //       children: <Widget>[
              //         ListTile(
              //           onTap: () {
              //             Get.to(SaleSummary());
              //           },
              //           leading: Icon(
              //             Icons.shopping_cart,
              //             color: Colors.black,
              //           ),
              //           title: Text("Sales Summary", style: regularText,),
              //         ),
              //       ],
              //     )),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  "Help",
                  style: subHeading,
                ),
              ),
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0)),
                  elevation: 0.0,
                  child: Column(
                    children: <Widget>[
                      ListTile(
                              onTap: () {
                                Get.to(FAQ());
                              },
                              leading: Icon(
                                Icons.question_answer,
                                color: Colors.black,
                              ),
                              title: Text(
                                "FAQ",
                                style: regularText,
                              ),
                            ),
                      Divider(
                        height: 0,
                      ),
                      ListTile(
                        onTap: () {
                          Get.to(TnC());
                        },
                        leading: Icon(
                          Icons.content_paste,
                          color: Colors.black,
                        ),
                        title: Text("Terms & Conditions", style: regularText,),
                      ),
                      Divider(
                        height: 0,
                      ),
                      ListTile(
                        onTap: () {
                          Get.to(PrivacyPolicy());
                        },
                        leading: Icon(
                          Icons.lock,
                          color: Colors.black,
                        ),
                        title: Text("Privacy Policy", style: regularText,),
                      ),
                      Divider(
                        height: 0,
                      ),
                      ListTile(
                        onTap: () {
                          Get.to(ContactUs());
                        },
                        leading: Icon(
                          Icons.local_phone,
                          color: Colors.black,
                        ),
                        title: Text("Contact Us", style: regularText,),
                      ),
                    ],
                  )),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text(
                    "Other",
                    style: subHeading,
                  ),
                ),
                Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0)),
                    elevation: 0.0,
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ReferralPage()));
                          },
                          leading: Icon(
                            Icons.share,
                            color: Colors.black,
                          ),
                          title: Text("Refer & Earn", style: regularText,),
                        ),
                        Divider(
                          height: 0,
                        ),
                        ListTile(
                                onTap: () {
                                  Get.to(CancellationAndRefund());
                                },
                                leading: Icon(
                                  Icons.cancel,
                                  color: Colors.black,
                                ),
                                title: Text(
                                  "Cancellation & Refund",
                                  style: regularText,
                                ),
                              ),
                        Divider(
                          height: 0,
                        ),
                        ListTile(
                          onTap: () {
                            UrlLauncher.launch("tel:0$SERVICE_CALL");
                          },
                          leading: Icon(
                            Icons.phone,
                            color: Colors.black,
                          ),
                          title: Text("Call Us", style: regularText,),
                        ),
                        Divider(
                          height: 0,
                        ),
                        ListTile(
                          onTap: () {
                            if(Platform.isIOS)
                              UrlLauncher.launch("https://apps.apple.com/us/app/neomart-seller-app/id1538698493");
                            else if (Platform.isAndroid)
                              UrlLauncher.launch("https://play.google.com/store/apps/details?id=com.nukkadse.merchantapp&hl=en_IN&gl=US");
                          },
                          leading: Icon(
                            Icons.star,
                            color: Colors.black,
                          ),
                          title: Text("Review & Rating", style: regularText,),
                        ),
                        Divider(
                          height: 0,
                        ),
                        ListTile(
                          onTap: () {},
                          leading: Icon(
                            Icons.update,
                            color: Colors.black,
                          ),
                          title: Text("Check For Updates", style: regularText,),
                        ),
                      ],
                    )),
                SizedBox(
                  height: 30,
                ),

                Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0)),
                  elevation: 0.0,
                  child: ListTile(
                    onTap: () {
                      requests.switchServer();
                      clearData();
                      clearSharedPreference();
                      Get.offAll(Login());
                    },
                    leading: Icon(FlutterIcons.server_faw, color: Colors.black,),
                    title: Text("Switch to ${v.devServer.value
                        ? "Production"
                        : "Dev"} Server", style: regularText,),
                  ),
                ),
                Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0)),
                  elevation: 0.0,
                  child: ListTile(
                    onTap: () {
                      clearData();
                      clearSharedPreference();
                      Get.offAll(Login());
                    },
                    leading: Icon(
                      Icons.input,
                      color: Colors.black,
                    ),
                    title: Text("Logout", style: regularText,),
                    trailing: appVersion == null ? SizedBox() : Text(
                      "Ver $appVersion", style: subHeading,),
                  ),
                ),
              ],
            ),
          ),
       ),
    );
  }
}
