import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:merchant/Getter/GetOffers.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/constants.dart';

class CreateCollection extends StatefulWidget {
  @override
  _CreateCollectionState createState() => _CreateCollectionState();
}

class _CreateCollectionState extends State<CreateCollection> {
  HttpRequests requests = HttpRequests();
  Offer offer;
  bool loading = true, noOffers = false;
  var srpController = TextEditingController();
  var qtyController = TextEditingController();
  double srp;
  int qty;
  String infoString =
      "Handpick your best products into collections. Collection can promote a similar look, Showcase products for Festival or Represents similar category prodcuts";

  getOffers() async {
    var res = await requests.getAllOffers();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        offer = Offer.fromJson(res);
        if (offer.info.length != 0) {
          loading = false;
          noOffers = false;
        } else {
          loading = false;
          noOffers = true;
        }
      });
    }
  }

  removeOffer(Info info) async {
    var res = await requests.removeOffer(info.collectionId.toString());
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      getOffers();
    }
  }

  changeOfferStatus(Info info, bool pause) async {
    print("COLLECTION ID: ${info.collectionId}");
    print("MERCHANT ID: $merchantid");
    print("TOKEN: $token");
    if (pause) {
      print("PAUSE");
      var res = await requests.pauseOffer(info.collectionId.toString());
      if (res != null && res['status'].toString().toLowerCase() == "success") {
        getOffers();
      }
    } else {
      print("RESUME");
      var res = await requests.resumeOffer(info.collectionId.toString(),
          info.validFrom.toString(), info.validTo.toString());
      if (res != null && res['status'].toString().toLowerCase() == "success") {
        getOffers();
      }
    }
  }

  showOfferInfo(Info info) {
    srpController.text = info.comboPrice.toString();
    qtyController.text = info.inventory.toString();
    bool sellingStatus = info.isPublished == 1 ? true : false;
    Get.bottomSheet(
      StatefulBuilder(
        builder: (context, setState) {
          return Container(
            padding: EdgeInsets.all(16),
            width: double.infinity,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(8), topLeft: Radius.circular(8))),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(8),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 2,
                            color: textBoxFillColor,
                            spreadRadius: 2,
                          )
                        ]),
                    child: FadeInImage.assetNetwork(
                      placeholder: 'assets/tsp.png',
                      image: info.collectionImages[0].imageUrl,
                      height: 200,
                      width: double.infinity,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    info.collectionName,
                    style: subHeading,
                  ),
                  Text(
                    info.collectionDescr,
                    style: TextStyle(fontSize: 11, color: Colors.grey),
                  ),
                  Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Expanded(
                              child: TextField(
                                keyboardType: TextInputType.numberWithOptions(),
                                scrollPadding: EdgeInsets.all(8.0),
                                maxLength: 10,
                                controller: srpController,
                                maxLengthEnforced: true,
                                enabled: false,
                                inputFormatters: [
                                  WhitelistingTextInputFormatter(
                                      RegExp("[0-9]"))
                                ],
                                onChanged: (value) {
                                  setState(() {
                                    srp = double.tryParse(double.tryParse(value)
                                        .toStringAsFixed(2));
                                  });
                                },
                                decoration: InputDecoration(
                                    prefixIcon: Icon(FlutterIcons.rupee_faw,
                                        size: 15, color: Colors.black),
                                    contentPadding: EdgeInsets.symmetric(
                                        horizontal: 8, vertical: 8),
                                    labelStyle: TextStyle(color: Colors.black),
                                    filled: true,
                                    fillColor: textBoxFillColor,
                                    focusColor: Colors.white38,
                                    focusedBorder: OutlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.white),
                                        borderRadius: BorderRadius.circular(4)),
                                    border: OutlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.white),
                                        borderRadius: BorderRadius.circular(4)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide:
                                            BorderSide(color: Colors.white),
                                        borderRadius: BorderRadius.circular(4)),
                                    counterText: "",
                                    labelText: "SRP"),
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Expanded(
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    "Quantity: ",
                                    style: regularText,
                                  ),
//                          IconButton(
//                            onPressed: qty <= 0? null: (){
//                              setState(() {
//                                qty--;
//                                qtyController.text = qty.toString();
//                              });
//                            },
//                            icon: Icon(Icons.remove, size: 15, color: Colors.black,),
//                          ),
                                  SizedBox(
                                    width: 60,
                                    child: TextField(
                                      keyboardType:
                                          TextInputType.numberWithOptions(),
                                      scrollPadding: EdgeInsets.all(8.0),
                                      maxLength: 10,
                                      enabled: false,
                                      maxLengthEnforced: true,
                                      controller: qtyController,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter(
                                            RegExp("[0-9]"))
                                      ],
                                      onChanged: (value) {
                                        setState(() {
                                          qty = int.tryParse(value);
                                        });
                                      },
                                      decoration: InputDecoration(
                                        isDense: true,
//                              prefixIcon: Icon(Icons.remove, size: 15, color: Colors.black,),
//                              suffixIcon: Icon(Icons.add, size: 15, color: Colors.black,),
                                        contentPadding: EdgeInsets.symmetric(
                                            horizontal: 8, vertical: 8),
                                        labelStyle: TextStyle(
                                            color:
                                                Colors.black.withOpacity(.4)),
                                        filled: true,
                                        fillColor: textBoxFillColor,
                                        focusColor: Colors.white38,
                                        focusedBorder: OutlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.white),
                                            borderRadius:
                                                BorderRadius.circular(4)),
                                        border: OutlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.white),
                                            borderRadius:
                                                BorderRadius.circular(4)),
                                        enabledBorder: OutlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.white),
                                            borderRadius:
                                                BorderRadius.circular(4)),
                                        counterText: "",
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                                child: Row(
                              children: <Widget>[
                                Text("Selling Status: "),
                                SizedBox(
                                  width: 5,
                                ),
                                SizedBox(
                                  width: 40,
                                  child: Switch(
                                    activeColor: Colors.green,
                                    inactiveThumbColor: Colors.orangeAccent,
                                    onChanged: (val) {},
                                    value: sellingStatus,
                                  ),
                                ),
                                sellingStatus
                                    ? Text("Published",
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: Colors.green,
                                            fontWeight: FontWeight.bold))
                                    : Text(
                                        "Paused",
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: Colors.orangeAccent,
                                            fontWeight: FontWeight.bold),
                                      )
                              ],
                            )),

//                          IconButton(
//                            onPressed: (){
//                              print("ADD");
//                              setState(() {
//                                qty++;
//                                qtyController.text = qty.toString();
//                              });
//                            },
//                            icon: Icon(Icons.add, size: 15, color: Colors.black,),
//                          ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text("Offer Valid till: "),
                            SizedBox(
                              width: 5,
                            ),
                            Text(info.validTo,
                                style: TextStyle(
                                    fontSize: 13, color: Colors.redAccent)),
                          ],
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: ButtonTheme(
                                height: 35,
                                child: FlatButton(
                                  onPressed: () {
                                    Get.back();
                                    removeOffer(info);
                                  },
                                  color: textBoxFillColor,
                                  textColor: Colors.black,
                                  child: Text("Remove"),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Expanded(
                              child: ButtonTheme(
                                height: 35,
                                child: FlatButton(
                                  onPressed: () {
//                                  validateDetails();
                                    Get.back();
                                    changeOfferStatus(info, sellingStatus);
                                  },
                                  color: Colors.black,
                                  textColor: textBoxFillColor,
                                  child:
                                      Text(sellingStatus ? "Pause" : "Resume"),
                                ),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
      isDismissible: true,
      enableDrag: true,
      isScrollControlled: true,
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    getOffers();
    print("TOKEN: $token");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? loadingCircular()
        : noOffers
            ? Align(alignment: Alignment.topCenter, child: noCollection())
            : Center(
                child: ListView.builder(
                padding: EdgeInsets.only(top: 8, right: 4, bottom: 120),
                itemCount: offer.info.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      showOfferInfo(offer.info[index]);
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                      child: Stack(
                        overflow: Overflow.visible,
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            child: Card(
                              elevation: 0.0,
                              margin: new EdgeInsets.only(left: 36.0, right: 0),
                              child: OfferContent(offer.info[index]),
                            ),
                          ),
                          Container(
                            height: 70,
                            width: 70,
                            alignment: FractionalOffset.centerLeft,
                            margin: EdgeInsets.symmetric(vertical: 24),
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: textBoxFillColor, width: 3),
                              shape: BoxShape.circle,
                            ),
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(35),
                                child: FadeInImage.assetNetwork(
                                    placeholder: 'assets/tsp.png',
                                    image: offer.info[index].collectionImages
                                        .isNotEmpty
                                        ? offer.info[index].collectionImages[0]
                                        .imageUrl
                                        .toString()
                                        : "")),
                          ),
                        ],
                      ),
                    ),
                  );
                },
        )
    );
  }

  Widget noCollection() {
    return Container(
      margin: EdgeInsets.all(20),
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          border: Border.all(width: 1, color: Colors.black), color: Colors.white
      ),
      child: Column(crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Image.asset('assets/no_collection.png', height: 100,),
          SizedBox(height: 8,),
          Text(infoString, textAlign: TextAlign.justify,)
        ],
      ),
    );
  }

  Widget OfferContent(Info info) {
    return Container(
      margin: EdgeInsets.only(left: 36),
      padding: EdgeInsets.only(top: 16),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(info.collectionName, style: subHeading,),
          SizedBox(height: 15,),
          Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Text(
                    "Selling Price: ₹ ${info.comboPrice}", style: regularText),
              ),
              Expanded(
                child: Text(
                  "Qty/Packets: ${info.inventory}", style: regularText,),
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Expanded(
                child: FlatButton(
                  onPressed: () {
                    removeOffer(info);
                  },
                  textColor: Colors.red,
                  child: Text("Remove"),
                ),
              ),
              Expanded(
                child: FlatButton(
                  onPressed: () {
                    changeOfferStatus(
                        info, info.isPublished == 1 ? true : false);
                  },
                  child: Text(
                    info.isPublished == 1 ? "Pause Offer" : "Resume Offer",
                    style: TextStyle(fontWeight: FontWeight.bold),),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
