import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:merchant/Dashboard.dart';
import 'package:merchant/HomePage/CreateCollection.dart';
import 'package:merchant/CreateSingleCollection.dart';
import 'package:merchant/HomePage/HomeScreen.dart';
import 'package:merchant/NotificationCenter.dart';
import 'package:merchant/Orders.dart';
import 'package:merchant/HomePage/Settings.dart';

import '../constants.dart';
import 'POS.dart';

class HomePage extends StatefulWidget {
//  String storeid, merchantid;
//  HomePage(this.storeid, this.merchantid);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var mColor = Color(0xFFfc0c5b);
  var pageController = PageController();
  int _page = 0, show = 1;

  Widget customAppbar() {
    if (_page == 2) {
      return AppBar(
        automaticallyImplyLeading: false,
        title: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Create Collection",
            ),
            Text(
              storeName,
              style: storeNameTextStyle,
            ),
          ],
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CreateSingleCollection()));
            },
            icon: Icon(Icons.add_circle_outline),
          )
        ],
        centerTitle: true,
      );
    } else if (_page == 0) {
      return AppBar(
        centerTitle: true,
//        leading: IconButton(
//          onPressed: () {
//            Navigator.push(context,
//                MaterialPageRoute(builder: (context) => NotificationCenter()));
//          },
//          icon: Icon(Icons.notifications),
//        ),
        title: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "My Store",
            ),
            Text(
              storeName,
              style: storeNameTextStyle,
            )
          ],
        ),
        automaticallyImplyLeading: false,
      );
    } else if (_page == 3) {
      return AppBar(
        centerTitle: true,
        title: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Settings",
            ),
            Text(
              storeName,
              style: storeNameTextStyle,
            ),
          ],
        ),
        automaticallyImplyLeading: false,
      );
    } else if (_page == 1) {
      return AppBar(
        centerTitle: true,
        title: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "POS",
            ),
            Text(
              storeName,
              style: storeNameTextStyle,
            ),
          ],
        ),
        automaticallyImplyLeading: false,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: textBoxFillColor,
      extendBody: true,
      appBar: customAppbar(),
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: pageController,
        children: <Widget>[
//          HomeScreen(),
          Dashboard(),
          POS(),
          CreateCollection(),
          Settings(),
        ],
      ),

      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        heroTag: 'getStarted',
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => Orders()));
        },
        backgroundColor: Colors.black,
        child: Image.asset('assets/icons/myorders.png', height: 20,),
      ),

//      bottomNavigationBar: BottomAppBar(
//        elevation: 18,
//        shape: CircularNotchedRectangle(),
//        notchMargin: 6,
//        child: Container(
//          padding: EdgeInsets.all(4),
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[
//              InkWell(
//                onTap: () {
//                  pageController.jumpToPage(0);
//                  setState(() {
//                    _page = 0;
//                  });
//                },
//                child: Container(
//                  padding: EdgeInsets.all(4),
//                  child: Column(
//                    mainAxisSize: MainAxisSize.min,
//                    children: <Widget>[
//                      Icon(
//                        Icons.home,
//                        color: _page == 0 ? Colors.black : Colors.grey,
//                      ),
//                      Text(
//                        "My Store",
//                        style: TextStyle(fontSize: 12),
//                        textScaleFactor: .9,
//                      )
//                    ],
//                  ),
//                ),
//              ),
//              InkWell(
//                onTap: () {
//                  pageController.jumpToPage(1);
////                    pageController.animateToPage(2,
////                      duration: Duration(milliseconds: 800),
////                      curve: Curves.decelerate,
////                    );
//                  setState(() {
//                    _page = 1;
//                  });
//                },
//                child: Container(
//                  padding: EdgeInsets.all(4),
//                  child: Column(
//                    mainAxisSize: MainAxisSize.min,
//                    children: <Widget>[
//                      Icon(Icons.content_paste,
//                          color: _page == 1 ? Colors.black : Colors.grey),
//                      Text(
//                        "POS",
//                        style: TextStyle(fontSize: 12),
//                        textScaleFactor: .9,
//                      )
//                    ],
//                  ),
//                ),
//              ),
//              SizedBox(
//                width: 50,
//              ),
//              InkWell(
//                onTap: () {
//                  pageController.jumpToPage(2);
//                  setState(() {
//                    _page = 2;
//                  });
//                },
//                child: Container(
//                  padding: EdgeInsets.all(4),
//                  child: Column(
//                    mainAxisSize: MainAxisSize.min,
//                    children: <Widget>[
//                      Icon(Icons.create,
//                          color: _page == 2 ? Colors.black : Colors.grey),
//                      Text(
//                        "Create",
//                        style: TextStyle(fontSize: 12),
//                        textScaleFactor: .9,
//                      )
//                    ],
//                  ),
//                ),
//              ),
//              InkWell(
//                onTap: () {
//                  pageController.jumpToPage(3);
////                    pageController.animateToPage(2,
////                      duration: Duration(milliseconds: 800),
////                      curve: Curves.decelerate,
////                    );
//                  setState(() {
//                    _page = 3;
//                  });
//                },
//                child: Container(
//                  padding: EdgeInsets.all(4),
//                  child: Column(
//                    mainAxisSize: MainAxisSize.min,
//                    children: <Widget>[
//                      Icon(Icons.settings,
//                          color: _page == 3 ? Colors.black : Colors.grey),
//                      Text(
//                        "Settings",
//                        style: TextStyle(fontSize: 12),
//                        textScaleFactor: .9,
//                      )
//                    ],
//                  ),
//                ),
//              ),
//            ],
//          ),
//        ),
//      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          color: Colors.white,
//          padding: EdgeInsets.symmetric(horizontal: ),
          height: 56,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          pageController.jumpToPage(0);
                          setState(() {
                            _page = 0;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Image.asset(
                              'assets/icons/mystore.png',
                              height: 18,
                              color: _page == 0 ? Colors.black : Colors.grey,
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              "MY STORE",
                              style: bottomAppbarText,
                            ),
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          pageController.jumpToPage(1);
                          setState(() {
                            _page = 1;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Image.asset('assets/icons/POS.png', height: 18,
                              color: _page == 1 ? Colors.black : Colors.grey,),
                            SizedBox(height: 5,),
                            Text("POS", style: bottomAppbarText,),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    SizedBox(height: 16,),
                    Text("ORDERS", style: bottomAppbarText,),
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          pageController.jumpToPage(2);
                          setState(() {
                            _page = 2;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Image.asset(
                              'assets/icons/collection.png', height: 18,
                              color: _page == 2 ? Colors.black : Colors.grey,),
                            SizedBox(height: 5,),
                            Text("ADD PRODUCT", style: bottomAppbarText,),
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          pageController.jumpToPage(3);
                          setState(() {
                            _page = 3;
                          });
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Image.asset('assets/icons/settings.png', height: 18,
                              color: _page == 3 ? Colors.black : Colors.grey,),
                            SizedBox(height: 5,),
                            Text("SETTINGS", style: bottomAppbarText,),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
