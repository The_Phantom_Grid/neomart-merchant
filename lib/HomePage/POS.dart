import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:merchant/Getter/GetSearchNumber.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/PosCreateOrder.dart';
import 'package:merchant/constants.dart';

class POS extends StatefulWidget {
  @override
  _POSState createState() => _POSState();
}

class _POSState extends State<POS> {
  var mColor = Color(0xFFfc0c5b);
  var phoneController = TextEditingController();
  String numError;
  HttpRequests requests = HttpRequests();
  SearchNumber searchNumbers;

  validateNumber() {
    if (phoneController.text.length < 10 ||
        int.parse(phoneController.text.toString()) < 6000000000 ||
        int.parse(phoneController.text.toString()) > 9999999999) {
      numError = "Please enter valid mobile number";
    } else
      Get.to(PosCreateOrder(phoneController.text.toString()));
  }

//  searchNumber() async {
//    var res = await requests.searchNumber(
//        phoneController.text.toString(), storeid, merchantid, token);
//    if (res != null && res['status'].toString().toLowerCase() == "success") {
//      searchNumbers = SearchNumber.fromJson(res);
//      if (searchNumbers.data.length != 0) {
//        Get.to(PosCreateOrder(phoneController.text.toString()));
//      }
//    }
//  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 30,
            ),
            Image.asset(
              'assets/neomart_splash_image.png',
              height: 60,
            ),
            SizedBox(
              height: 40,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Enter Customer Phone Number", style: h1,),
                  TextField(
                    keyboardType: TextInputType.numberWithOptions(),
                    controller: phoneController,
                    scrollPadding: EdgeInsets.all(8.0),
                    maxLength: 10,
                    maxLengthEnforced: true,
                    inputFormatters: [
                      WhitelistingTextInputFormatter(RegExp("[0-9]"))
                    ],
                    onChanged: (value) {
                      setState(() {
                        numError = null;
                      });
                      if (value.length == 10) {
                        FocusScope.of(context).requestFocus(FocusNode());
                      }
                    },
                    decoration: InputDecoration(
                      errorText: numError,
                      contentPadding: EdgeInsets.symmetric(
                          horizontal: 16, vertical: 16),
                      prefixIcon: Icon(Icons.call, color: Colors.black,),
                      labelStyle: TextStyle(
                          color: Colors.black.withOpacity(.4)),
                      filled: true,
                      fillColor: Colors.white,
                      focusColor: Colors.white38,
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(4)),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(4)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(4)),
                      counterText: "",
                      hintText: "00 0000 0000",
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: ButtonTheme(
                        minWidth: double.infinity,
                        height: 50.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6)),
                        child: RaisedButton(
                          child: Text("NEXT"),
                          onPressed: () {
                            validateNumber();
//                            Navigator.push(
//                                context,
//                                MaterialPageRoute(
//                                    builder: (context) => PosCreateOrder()));
                          },
                          textColor: Colors.white,
                          color: Colors.black,
                        )),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
