import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';

import 'Components/VariableControllers.dart';
import 'Network/httpRequests.dart';
import 'constants.dart';

class DeliverySettings extends StatefulWidget {
  @override
  _DeliverySettingsState createState() => _DeliverySettingsState();
}

class _DeliverySettingsState extends State<DeliverySettings> {
  bool homeDelivery = false, loading = true;
  var deliveryTime = TextEditingController(),
      deliveryRadius = TextEditingController(),
      deliveryCharge = TextEditingController(),
      minOrder = TextEditingController(),
      openTime = TextEditingController(),
      closeTime = TextEditingController(),
      deliveryCloseTime = TextEditingController(),
      deliveryOpenTime = TextEditingController();
  HttpRequests requests = HttpRequests();
  String cTime = "", oTime = "", deliveryOtime = "", deliveryCtime = "";

  List<String> offDays = [];
  Map<int, String> wDays = {
    1: "Mon",
    2: "Tue",
    3: "Wed",
    4: "Thu",
    5: "Fri",
    6: "Sat",
    7: "Sun",
  };

  selectDay(int key) {
    if(offDays.contains(key.toString())){
      setState(() {
        offDays.remove(key.toString());
      });
      print(
          offDays.toString().replaceAll('[', "").replaceAll(']', "").replaceAll(
              " ", ""));
    }else{
      setState(() {
        offDays.add(key.toString());
        print(offDays.toString().replaceAll('[', "")
            .replaceAll(']', "")
            .replaceAll(" ", ""));
      });
    }
  }

  Widget weekDays() {
    return Container(
        height: 50,
        child: ListView(
          itemExtent: 55,
          scrollDirection: Axis.horizontal,
          children: wDays.entries.map((e) {
            return GestureDetector(
              onTap: () {
                selectDay(e.key);
              },
              child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 2),
                  decoration: BoxDecoration(
                    color: offDays.contains(e.key.toString())
                        ? Colors.red
                        : Colors.green,
                  ),
                  child: Center(child: Text(e.value.toString(),
                    style: TextStyle(color: Colors.white),),)
              ),
            );
          }).toList(),
        )
    );
  }

  toggleHomeDelivery() {
    setState(() {
      if (homeDelivery) {
        homeDelivery = false;
        deliveryTime.text = "0";
        deliveryRadius.text = "0";
        deliveryOpenTime.text = "0";
        deliveryCloseTime.text = "0";
        deliveryOtime = deliveryOpenTime.text;
        deliveryCtime = deliveryCloseTime.text;
      } else {
        homeDelivery = true;
        deliveryTime.text = "";
        deliveryRadius.text = "";
        deliveryOpenTime.text = "";
        deliveryCloseTime.text = "";
        deliveryOtime = deliveryOpenTime.text;
        deliveryCtime = deliveryCloseTime.text;
      }
    });
    print(homeDelivery);
    print(deliveryTime.text);
  }

  validate() {
    if (deliveryTime.text == "" || deliveryTime.text.length < 0) {
      Fluttertoast.showToast(msg: "Invalid detail: Min. Delivery Time");
    } else if (deliveryRadius.text == "" || deliveryRadius.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Set Delivery Radius");
    } else if (deliveryCharge.text == "" || deliveryCharge.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Delivery Charge");
    } else if (minOrder.text == "" || minOrder.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Minimum Order");
    } else if (oTime == "" || oTime.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Set Store Opening Time");
    } else if (cTime == "" || cTime.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Set Store Closing Time");
    } else
    if (deliveryCtime == "" || deliveryCtime.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Set Delivery Closing Time");
    } else
    if (deliveryOtime == "" || deliveryOtime.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Set Delivery Opening Time");
    } else {
      saveDeliverySettings();
    }
  }

  saveDeliverySettings() async {
    var res = await requests.saveShippingSettings(
        deliveryCharge.text.toString(),
        deliveryCtime,
        deliveryRadius.text.toString(),
        deliveryOtime,
        merchantid,
        deliveryTime.text.toString(),
        minOrder.text.toString(),
        cTime,
        offDays.toString().replaceAll('[', "").replaceAll(']', "").replaceAll(
            " ", ""),
        storeid,
        oTime,
        token);

    if (res != null && res['status'].toString().toLowerCase() == "success") {
      detailsUpdated = true;
      Fluttertoast.showToast(msg: "Settings Saved",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    } else {
      Fluttertoast.showToast(msg: "Something went wrong!",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    }
  }

//  setHomeDelivery() {
//    toggleHomeDelivery();
//  }


  getDetails() async {
    print(detailsUpdated);
    if (detailsUpdated) {
      await getStoreDetails();
      setState(() {
        loading = false;
        detailsUpdated = false;
      });
      initControllers();
    } else {
      setState(() {
        loading = false;
      });
      initControllers();
    }
  }

  initControllers() {
    print("DATETIME NOW: ${DateTime.now()}");
    print("${DateFormat('hh:mm a').format(DateTime.parse("${DateFormat('yyyy-MM-DD').format(DateTime.now())} ${storeDetails.info.storeDetail.storeOpenTime}"))}");
    setState(() {
      deliveryTime.text = storeDetails.info.storeDetail.deliveryTime;
      deliveryRadius.text = storeDetails.info.storeDetail.deliveryRadius;
      deliveryOpenTime.text = DateFormat('hh:mm a').format(DateTime.parse("${DateFormat('yyyy-MM-DD').format(DateTime.now())} ${storeDetails.info.storeDetail.storeOpenTime}"));
      deliveryCloseTime.text = DateFormat('hh:mm a').format(DateTime.parse("${DateFormat('yyyy-MM-DD').format(DateTime.now())} ${storeDetails.info.storeDetail.storeCloseTime}"));
      deliveryCharge.text = storeDetails.info.storeDetail.deliveryCharge;
      minOrder.text = storeDetails.info.storeDetail.minOrder;
      openTime.text = DateFormat('hh:mm a').format(DateTime.parse("${DateFormat('yyyy-MM-DD').format(DateTime.now())} ${storeDetails.info.storeDetail.storeOpenTime}"));
      closeTime.text = DateFormat('hh:mm a').format(DateTime.parse("${DateFormat('yyyy-MM-DD').format(DateTime.now())} ${storeDetails.info.storeDetail.storeCloseTime}"));
      oTime = storeDetails.info.storeDetail.storeOpenTime;
      cTime = storeDetails.info.storeDetail.storeCloseTime;
      deliveryCtime = storeDetails.info.storeDetail.storeCloseTime;
      deliveryOtime = storeDetails.info.storeDetail.storeOpenTime;
      if(deliveryRadius.text == "0" && deliveryTime.text == "0")
        homeDelivery = false;
      else homeDelivery = true;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    getDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Delivery Settings"),
      ),
      extendBody: true,
      body: loading ? loadingCircular() : Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              padding: EdgeInsets.all(8),
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text("Home Delivery"),
                        Switch.adaptive(
//                  title: Text("Home Delivery"),
                          value: homeDelivery,
                          onChanged: (val) {
                            toggleHomeDelivery();
                          },
                        ),
                      ],
                    ),
                    !homeDelivery ? SizedBox() : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 8,),
                        Text(
                          "Set Minimum Delivery Time (In Minutes)",
                          textAlign: TextAlign.left,
                          style: smallHeading,
                        ),
                        TextFormField(
                          enabled: true,
                          controller: deliveryTime,
                          inputFormatters: [
                            WhitelistingTextInputFormatter(RegExp("[0-9]"))
                          ],
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 8),
                              isDense: true,
                              hasFloatingPlaceholder: true,
                              hintText: "15 minutes",
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black))),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          "Set Delivery Radius (In Kilometers)",
                          textAlign: TextAlign.left,
                          textScaleFactor: 1,
                          style: smallHeading,
                        ),
                        TextFormField(
                          enabled: true,
                          controller: deliveryRadius,
                          inputFormatters: [
                            WhitelistingTextInputFormatter(RegExp("[0-9]"))
                          ],
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 8),
                              isDense: true,
                              hasFloatingPlaceholder: true,
                              hintText: "4 km",
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black))),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Delivery Opening Time",
                                    textAlign: TextAlign.left,
                                    textScaleFactor: 1,
                                    style: smallHeading,
                                  ),
                                  TextFormField(
                                    onTap: () {
                                      DatePicker.showTime12hPicker(
                                          context, onConfirm: (time) {
                                        setState(() {
                                          deliveryOpenTime.text = DateFormat('hh:mm a').format(time);
                                          deliveryOtime = "${time.hour.toString()} : ${time
                                              .minute.toString()}";
                                        });
                                      });
                                    },
                                    controller: deliveryOpenTime,
                                    enabled: true,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter(
                                          RegExp("[0-9]"))
                                    ],
                                    decoration: InputDecoration(
                                        contentPadding: EdgeInsets.symmetric(
                                            vertical: 12, horizontal: 8),
                                        isDense: true,
                                        hasFloatingPlaceholder: true,
                                        hintText: "Tap to Set",
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.black))),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 4,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Delivery Closing Time",
                                    textAlign: TextAlign.left,
                                    textScaleFactor: 1,
                                    style: smallHeading,
                                  ),
                                  TextFormField(
                                    onTap: () {
                                      DatePicker.showTime12hPicker(
                                          context,
                                          onConfirm: (time) {
                                            setState(() {
                                              deliveryCtime = "${time.hour.toString()} : ${time
                                                  .minute.toString()}";
                                              deliveryCloseTime.text = DateFormat('hh:mm a').format(time);
                                            });
                                      });
                                    },
                                    controller: deliveryCloseTime,
                                    enabled: true,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter(
                                          RegExp("[0-9]"))
                                    ],
                                    decoration: InputDecoration(
                                        contentPadding: EdgeInsets.symmetric(
                                            vertical: 12, horizontal: 8),
                                        isDense: true,
                                        hasFloatingPlaceholder: true,
                                        hintText: "Tap to Set",
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.black))),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                    SizedBox(height: 8,),
                    Text(
                      "Select Store Close Days",
                      textAlign: TextAlign.left,
                      style: smallHeading,
                    ),
                    weekDays(),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Delivery Charge(In Rs.)",
                                textAlign: TextAlign.left,
                                textScaleFactor: 1,
                                style: smallHeading,
                              ),
                              TextFormField(
                                enabled: true,
                                controller: deliveryCharge,
                                inputFormatters: [
                                  WhitelistingTextInputFormatter(
                                      RegExp("[0-9]"))
                                ],
                                decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      FlutterIcons.rupee_faw, size: 18,
                                      color: Colors.black,),
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 12, horizontal: 8),
                                    isDense: true,
                                    hasFloatingPlaceholder: true,
                                    hintText: "99",
                                    border: OutlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Colors.black))),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: 5,),
                        Expanded(
                          flex: 1,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Minimum Order(In Rs.)",
                                textAlign: TextAlign.left,
                                textScaleFactor: 1,
                                style: smallHeading,
                              ),
                              TextFormField(
                                enabled: true,
                                controller: minOrder,
                                inputFormatters: [
                                  WhitelistingTextInputFormatter(
                                      RegExp("[0-9]"))
                                ],
                                decoration: InputDecoration(
                                    prefixIcon: Icon(
                                      FlutterIcons.rupee_faw, size: 18,
                                      color: Colors.black,),
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 12, horizontal: 8),
                                    isDense: true,
                                    hasFloatingPlaceholder: true,
                                    hintText: "399",
                                    border: OutlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Colors.black))),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      "Store Opening Time",
                      textAlign: TextAlign.left,
                      textScaleFactor: 1,
                      style: smallHeading,
                    ),
                    TextFormField(
                      onTap: () {
                        DatePicker.showTime12hPicker(
                            context, onConfirm: (time) {
                          setState(() {
                            oTime = "${time.hour.toString()} : ${time.minute
                                .toString()}";
                            openTime.text = DateFormat('hh:mm a').format(time);
                          });
                        });
                      },
                      controller: openTime,
                      enabled: true,
                      inputFormatters: [
                        WhitelistingTextInputFormatter(RegExp("[0-9]"))
                      ],
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 12, horizontal: 8),
                          isDense: true,
                          hasFloatingPlaceholder: true,
                          hintText: "Tap to Set",
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black))),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text("Store Closing Time",
                      textAlign: TextAlign.left,
                      textScaleFactor: 1,
                      style: smallHeading,
                    ),
                    TextFormField(
                      onTap: () {
                        DatePicker.showTime12hPicker(
                            context, onConfirm: (time) {
                          setState(() {
                            cTime = "${time.hour.toString()} : ${time.minute
                                .toString()}";
                            closeTime.text = DateFormat('hh:mm a').format(time);
                          });
                        });
                      },
                      controller: closeTime,
                      enabled: true,
                      inputFormatters: [
                        WhitelistingTextInputFormatter(RegExp("[0-9]"))
                      ],
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 12, horizontal: 8),
                          isDense: true,
                          hasFloatingPlaceholder: true,
                          hintText: "Tap to Set",
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black))),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ButtonTheme(
          height: 50,
          splashColor: Colors.white38,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: RaisedButton(
            onPressed: () {
              validate();
            },
            child: Text(
              "UPDATE",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
