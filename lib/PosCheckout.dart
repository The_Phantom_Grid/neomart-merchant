import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:merchant/Components.dart';
import 'package:merchant/Components/ProductCard.dart';
import 'package:merchant/Getter/GetPosCart.dart';
import 'package:merchant/HomePage/HomePage.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/constants.dart';

_PosCheckoutState posCheckoutState;

class PosCheckout extends StatefulWidget {
  String deliveryType, customerUserid, number;

  PosCheckout(this.deliveryType, this.customerUserid, this.number);

  @override
  _PosCheckoutState createState() {
    posCheckoutState = _PosCheckoutState();
    return posCheckoutState;
  }
}

class _PosCheckoutState extends State<PosCheckout> {
  HttpRequests requests = HttpRequests();
  PosCart posCart;
  TextEditingController discountController = TextEditingController();
  bool loading = true, showDiscountError = false, edit = false;
  int paymentMethod = 0, discountOption = 0;
  double discount = 0, totalDiscount = 0, grandtotal, subTotal, shipping, totalDue;
  String discountText = "Add discount?",
      discountError,
      paymentMode,
      discountFlag = "0";

  getCart() async {
    setState(() {
      loading = true;
    });
    print(widget.deliveryType);
    print(widget.customerUserid);
    var res = await requests.posViewCart(
        widget.deliveryType, widget.customerUserid, token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      posCart = null;
      setState(() {
        posCart = PosCart.fromJson(res);
        if(posCart.product.isEmpty)
          Get.back();
        else{
          totalDiscount = double.tryParse(double.tryParse(posCart.total.discount).toStringAsFixed(2));
          grandtotal = double.tryParse(double.tryParse(posCart.total.grandtotal).toStringAsFixed(2));
          subTotal = double.tryParse(double.tryParse(posCart.total.subtotal).toStringAsFixed(2));
          shipping = double.tryParse(double.tryParse(posCart.total.shipping).toStringAsFixed(2));
          print("TOTAL DIS: $totalDiscount");
          if (totalDiscount > 0)
            discountText = "Change discount amount?";
          loading = false;
        }
      });
      print("TAX: ${posCart.total.totalTax}");
    }
  }

  checkout() async {
    var res = await requests.posCheckout(widget.deliveryType, paymentMode,
        widget.number, widget.customerUserid, token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Fluttertoast.showToast(msg: "Order Created");
      Get.offAll(HomePage());
    }
  }

  addDiscount() async {
    print("DISCOUNT: $discount");
    var res = await requests.addDiscount(
        "",
        widget.deliveryType,
        discount.toStringAsFixed(2),
        "0",
        posCart.total.grandtotal.toString(),
        widget.customerUserid);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Fluttertoast.showToast(msg: "Discount updated");
      getCart();
    }
  }

  togglePayment(int option) {
    setState(() {
      paymentMethod = option;
      paymentMode = "CASH";
    });
  }

  toggleDiscount(int option) {
    setState(() {
      discountOption = option;
      if (discountOption == 0) {
        discountFlag = "0";
      } else if (discountOption == 1) {
        discountFlag = "1";
      }
    });
    print("DSCOUNT FLAG: $discountFlag");
  }

    toggleEdit(){
    if(edit)
      edit = false;
    else edit = true;
    setPageState();
  }

  setPageState(){
    setState(() {});
  }

  showAddDiscount() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return AlertDialog(
              title: Text(
                "Add discount",
                style: subHeading,
              ),
              content: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          toggleDiscount(0);
                        });
                      },
                      child: Row(
//                          mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.fiber_manual_record,
                            color: discountOption == 0 ? mColor : Colors.grey,
                            size: 18,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Flat Discount",
                            style: subHeading,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          toggleDiscount(1);
                        });
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.fiber_manual_record,
                            color: discountOption == 1 ? mColor : Colors.grey,
                            size: 18,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Discount %",
                            style: subHeading,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      height: 40,
                      width: 180,
                      child: TextFormField(
                        keyboardType: TextInputType.numberWithOptions(
                            decimal: true),
                        style: TextStyle(fontSize: 14),
                        controller: discountController,
                        inputFormatters: [amountInputFormatter],
                        onChanged: (val) {
                          setState(() {
                            discount = double.tryParse(val);
                            print(discount);
                            showDiscountError = false;
                          });
                        },
                        decoration: InputDecoration(
                            fillColor: textBoxFillColor,
                            hintStyle: TextStyle(fontSize: 14),
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 8, vertical: 4),
//                          focusColor: Colors.white38,
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black12),
                                borderRadius: BorderRadius.circular(4)),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black12),
                                borderRadius: BorderRadius.circular(4)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black12),
                                borderRadius: BorderRadius.circular(4)),
                            counterText: "",
//                              errorText: discountError,
                            hintText: discountOption == 0
                                ? "Enter amount"
                                : "Enter %",
                            labelStyle: TextStyle(color: Colors.grey)),
                      ),
                    ),
                    showDiscountError
                        ? Text(
                            discountError,
                            style: TextStyle(fontSize: 11, color: Colors.red),
                          )
                        : SizedBox()
                  ],
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18)),
              actions: <Widget>[
                ButtonTheme(
                  height: 40,
                  minWidth: 100,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  child: RaisedButton(
                    onPressed: () {
                      if (discountController.text == "" ||
                          discountController.text.length < 1) {
                        setState(() {
                          discountError = "Please enter value";
                          showDiscountError = true;
                        });
                      } else if (discount > subTotal &&
                          discountFlag == "0") {
                        setState(() {
                          discountError =
                              "Discount cannot be greater than bill amount";
                          showDiscountError = true;
                        });
                      } else if (discount > 100 && discountFlag == "1") {
                        setState(() {
                          discountError = "Discount cannot be more than 100%";
                          showDiscountError = true;
                        });
                      } else {
                        setState(() {
                          discountController.text = "";
                        });
                        if (discountOption == 0)
                          addDiscount();
                        else if (discountOption == 1) {
                          discount = (subTotal * discount / 100);
                          print("DISCOUNT $discount ");
                          addDiscount();
                        }
                        print(discount);
                        Navigator.pop(context);
                      }
                    },
                    child: Text(
                      "Submit",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ],
            );
          });
        });
  }

  @override
  void initState() {
    // TODO: implement initState
    getCart();
//  print(token)
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: textBoxFillColor,
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: true,
        title: Text("Checkout"),
        actions: <Widget>[
          IconButton(
            onPressed: (){
              toggleEdit();
            },
            icon: edit? Icon(FlutterIcons.save_mdi): Icon(FlutterIcons.square_edit_outline_mco),
          )
        ],
      ),

      body: loading ? loadingCircular() : SingleChildScrollView(
        padding: EdgeInsets.only(left: 16, right: 16, bottom: 70, top: 14),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Card(
              elevation: 0.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6)),
              child: Container(
                padding: EdgeInsets.all(16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        ClipRRect(
                            borderRadius: BorderRadius.circular(4),
                            child: FadeInImage.assetNetwork(
                              placeholder: 'assets/tsp.png', image: posCart
                                .product[0].storeIcon, height: 70, width: 70,)),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(posCart.product[0].storeName, maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  style: subHeading,),
                                Text(
                                        storeLocation,
                                        style: productName,
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                Row(
                                  children: <Widget>[
                                    Text("Total cart value: ",
                                      style: productName,),
                                    Text("₹ ${subTotal}",
                                      style: TextStyle(
                                          color: mColor, fontSize: 12),),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(width: 10,),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.shopping_cart, size: 25,),
                            Text("${posCart.total.totalItems} items",
                              style: smallHeading,)
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),

            Container(
              margin: EdgeInsets.symmetric(horizontal: 4),
              decoration: BoxDecoration(
                  color: darkBackground,
                  borderRadius: BorderRadius.circular(12)
              ),
              child: Column(
                children: <Widget>[
                  Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)
                    ),
                    margin: EdgeInsets.all(0),
                    elevation: 0.0,
                    child: ListView.separated(
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 14),
                        physics: NeverScrollableScrollPhysics(),
                        separatorBuilder: (context, index){
                          return Divider();
                        },
                        itemBuilder: (context, index){
                          return ProductCard(posCart.product[index], edit , widget.deliveryType, widget.customerUserid);
                        },
                        itemCount: posCart.product.length
                    ),
                  ),
                  SizedBox(height: 8,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Total", style: TextStyle(
                                fontSize: 13, color: Colors.white),),
                            Text("₹ $subTotal", style: TextStyle(
                                fontSize: 14, color: Colors.white)),
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Tax", style: TextStyle(
                                fontSize: 13, color: Colors.white),),
                            Text("₹ ${double.tryParse(
                                posCart.total.totalTax.toString())}",
                                style: TextStyle(
                                    fontSize: 14, color: Colors.white)),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Shipping", style: TextStyle(
                                fontSize: 13, color: Colors.white),),
                            Text("₹ $shipping",
                                style: TextStyle(
                                    fontSize: 14, color: Colors.white)),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Discount", style: TextStyle(
                                fontSize: 13, color: Colors.white),),
                            Text("₹ ${double.tryParse(
                                totalDiscount.toString())}",
                                style: TextStyle(
                                    fontSize: 14, color: Colors.white)),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Divider(color: Colors.grey,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Bill amount",
                          style: TextStyle(fontSize: 13, color: Colors.white),),
                        Text("₹ $grandtotal",
                            style: TextStyle(fontSize: 14, color: Colors
                                .white)),
                      ],
                    ),
                  ),
                  SizedBox(height: 14,),
                ],
              ),
            ),
            SizedBox(height: 14,),
            GestureDetector(
                onTap: () {
                  showAddDiscount();
                },
                child: Text(
                  discountText, style: TextStyle(fontSize: 13, color: mColor),
                  textAlign: TextAlign.start,)
            ),
            SizedBox(height: 14,),

            Container(
              height: 45,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(6)
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Expanded(
                    flex: 3,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 16),
                      child: Text("Apply Coupon Code", style: subHeading,),
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      height: 45,
                      decoration: BoxDecoration(
                          color: darkBackground,
                          borderRadius: BorderRadius.circular(6)
                      ),
                      child: Center(child: Text("REDEEM", style: TextStyle(fontSize: 13, color: Colors.white),)),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 14,),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () {
                      togglePayment(0);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(Icons.fiber_manual_record,
                          color: paymentMethod == 0 ? mColor : Colors.grey,),
                        SizedBox(width: 5,),
                        Text("CASH ON DELIVERY", style: subHeading,)
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 8,),
                posCart.total.paytmAvail == 0 ? SizedBox() : Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () {
                      togglePayment(1);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Icon(Icons.fiber_manual_record,
                          color: paymentMethod == 1 ? mColor : Colors.grey,),
                        SizedBox(width: 5,),
                        Text("PAYTM", style: subHeading,)
                      ],
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),

      floatingActionButton: FloatingActionButton.extended(
          onPressed: loading ? null : () {
            checkout();
          },
          label: Text("Proceed to checkout")
      ),
    );
  }
}
