import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:merchant/Getter/GetOrderByStatus.dart';
import 'package:merchant/Getter/GetOrderCountByStatus.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/OrderDetail.dart';

import '../constants.dart';

class DeliveredOrders extends StatefulWidget {
  @override
  _DeliveredOrdersState createState() => _DeliveredOrdersState();
}

class _DeliveredOrdersState extends State<DeliveredOrders> {
  var scrollController = ScrollController();
  int selectedChip = 0;
  HttpRequests requests = HttpRequests();
  OrderCountByStatus orderCountByStatus;
  OrderByStatus orderByStatus;
  String orderStatus = "Order Delivered";
  bool loading = true, filter = false, sort = false, noOrder = false;
  List<OrderByStatusDetail> filterList = [];
  int filterValue = 0, sortVal = 0, sortTypeVal;
  Map<String, String> options = {
    "Order Delivered": "Review Pending",
    "Store pickup": "Reviewed",
    "Order Returned": "Order Returned",
  };

  selectedOption(int index) {
    setState(() {
      selectedChip = index;
      orderStatus = options.keys.toList()[index];
      getOrderCountByStatus();
    });
  }

  getOrderCountByStatus() async {
    var res = await requests.getOrderCountByStatus();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        orderCountByStatus = OrderCountByStatus.fromJson(res);
      });
      getOrderByStatus();
    }
  }

  getOrderByStatus() async {
    print("ORDER STATUS: $orderStatus");
    var res = await requests.getOrdersByStatus(orderStatus);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        orderByStatus = OrderByStatus.fromJson(res);
        loading = false;
      });
    } else {
      setState(() {
        loading = false;
      });
    }
  }


  filterByDate(int days){
    filterList.clear();
    orderByStatus.meta.orderDetails.forEach((element) {
      var d = DateTime.parse(element.orderDate);
      var dn = DateTime.now().subtract(Duration(days: days)) ;
      if(d.isAfter(dn)) {
        print("TRUE DATE TIME 2 : $d");
        print("TRUE DATE TIME 2 : $dn");
        filterList.add(element);
      }
    });
    setState(() {
      if(filterList.isEmpty)
        noOrder = true;
      else noOrder = false;
    });

    if(filterList.isNotEmpty && sort) {
      print("LENGTH1: ${filterList.length} || TYPE: $sortTypeVal");
      selectSort(sortVal);
    }
  }

  selectSort(int sortType){
    switch(sortType){
      case 1: sortListByHomedelivery_POS(); break;
      case 2: sortListByStorePickup_POS(); break;
      case 3: sortListByHomedelivery_App(); break;
      case 4: sortListByStorePickup_App(); break;
    }
  }

  sortListByHomedelivery_POS(){
//    filterList.forEach((element) {
//      print("DEL TYPE: ${element.delivertype} || FROM: ${element.orderMadeThrough}");
//      if(element.delivertype.toLowerCase() != "homedelivery" && element.orderMadeThrough.toLowerCase() != "pos") {
//        print(element.delivertype);
//        filterList.remove(element);
//      }
//    });
    filterList.removeWhere((element) => element.delivertype.toLowerCase() != "homedelivery" || element.orderMadeThrough.toLowerCase() != "pos");
    print("LENGTH: ${filterList.length}");

    setState(() {
      if(filterList.isEmpty)
        noOrder = true;
      else noOrder = false;
    });
  }

  sortListByStorePickup_POS(){
//    filterList.forEach((element) {
//      if(element.delivertype.toLowerCase() != "pickup" && element.orderMadeThrough.toLowerCase() != "pos")
//        filterList.remove(element);
//    });
    filterList.removeWhere((element) => element.delivertype.toLowerCase() != "pickup" || element.orderMadeThrough.toLowerCase() != "pos");

    setState(() {
      if(filterList.isEmpty)
        noOrder = true;
      else noOrder = false;
    });
  }

  sortListByHomedelivery_App(){
//    filterList.forEach((element) {
//      if(element.delivertype.toLowerCase() != "homedelivery" && element.orderMadeThrough.toLowerCase() != "")
//        filterList.remove(element);
//    });

    filterList.removeWhere((element) => element.delivertype.toLowerCase() != "homedelivery" || element.orderMadeThrough.toLowerCase() != "customer_itself");

    setState(() {
      if(filterList.isEmpty)
        noOrder = true;
      else noOrder = false;
    });
  }

  sortListByStorePickup_App(){
//    filterList.forEach((element) {
//      if(element.delivertype.toLowerCase() != "pickup" && element.orderMadeThrough.toLowerCase() != "")
//        filterList.remove(element);
//    });
    filterList.removeWhere((element) => element.delivertype.toLowerCase() != "pickup" || element.orderMadeThrough.toLowerCase() != "customer_itself");

    setState(() {
      if(filterList.isEmpty)
        noOrder = true;
      else noOrder = false;
    });
  }

  selectFilterValue(int val) {
    setState(() {
      if(val != 0){
        filterValue = val;
        filter = true;
      }
    });
    switch(filterValue){
      case 1: filterByDate(1); break;
      case 2: filterByDate(7); break;
      case 3: filterByDate(28); break;
      case 4: filterByDate(84); break;
      default: filterByDate(365);
    }
  }

  clearFilter(){
    setState(() {
      filterValue = 0;
      filter = false;
      noOrder = false;
    });
  }

  clearSort(){
    setState(() {
      noOrder = false;
      sort = false;
      sortVal = 0;
    });
  }

  setPageState(){
    setState(() {});
  }

  filterMenu(){
    Get.bottomSheet(
      Container(
          color: Colors.white,
          child: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState){
                return SafeArea(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(Icons.filter_list),
                                  SizedBox(width: 5,),
                                  Text("FILTER", style: bigTextStyle,),
                                ],
                              ),
                              GestureDetector(
                                  onTap: (){
                                    clearFilter();
                                    Get.back();
                                  },
                                  child: Text("Clear", style: TextStyle(fontSize: 12, color: Colors.blue),)
                              ),
                            ],
                          ),
                        ),
                        Divider(height: 0,),
                        RadioListTile(
                          dense: true,
                          onChanged: (val){
                            selectFilterValue(val);
                            Get.back();
                          },
                          value: 1,
                          groupValue: filterValue,
                          title: Text("Today", style: regularText,),
                        ),
                        RadioListTile(
                          dense: true,
                          onChanged: (val){
                            selectFilterValue(val);
                            Get.back();
                          },
                          value: 2,
                          groupValue: filterValue,
                          title: Text("Last 1 week",  style: regularText),
                        ),
                        RadioListTile(
                          dense: true,
                          onChanged: (val){
                            selectFilterValue(val);
                            Get.back();
                          },
                          value: 3,
                          groupValue: filterValue,
                          title: Text("Last 1 month",  style: regularText),
                        ),
                        RadioListTile(
                          dense: true,
                          onChanged: (val){
                            selectFilterValue(val);
                            Get.back();
                          },
                          value: 4,
                          groupValue: filterValue,
                          title: Text("Last 3 months",  style: regularText),
                        ),
                      ],
                    ),
                  ),
                );
              }
          )
      ),
      isDismissible: true,
      isScrollControlled: true,
    );
  }

  sortMenu(){
    Get.bottomSheet(
      Container(
          color: Colors.white,
          child: StatefulBuilder(
              builder: (BuildContext context, StateSetter setState){
                return SafeArea(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(Icons.sort),
                                  SizedBox(width: 5,),
                                  Text("SORT", style: bigTextStyle,),
                                ],
                              ),
                              GestureDetector(
                                  onTap: (){
                                    clearSort();
                                    Get.back();
                                  },
                                  child: Text("Clear", style: TextStyle(fontSize: 12, color: Colors.blue),)
                              ),
                            ],
                          ),
                        ),
                        Divider(height: 0,),
                        RadioListTile(
                          dense: true,
                          onChanged: (val){
                            setState(() {
                              sort = true;
                              sortVal = val;
                            });
                            selectFilterValue(0);
                            Get.back();
                          },
                          value: 1,
                          groupValue: sortVal,
                          title: Text("Home Delivery - POS", style: regularText,),
                        ),
                        RadioListTile(
                          dense: true,
                          onChanged: (val){
                            setState(() {
                              sort = true;
                              sortVal = val;
                            });
                            selectFilterValue(0);
                            Get.back();
                          },
                          value: 2,
                          groupValue: sortVal,
                          title: Text("Store Pickup - POS",  style: regularText),
                        ),
                        RadioListTile(
                          dense: true,
                          onChanged: (val){
                            setState(() {
                              sort = true;
                              sortVal = val;
                            });
                            selectFilterValue(0);
                            Get.back();
                          },
                          value: 3,
                          groupValue: sortVal,
                          title: Text("Home Delivery - App",  style: regularText),
                        ),
                        RadioListTile(
                          dense: true,
                          onChanged: (val){
                            setState(() {
                              sort = true;
                              sortVal = val;
                            });
                            selectFilterValue(0);
                            Get.back();
                          },
                          value: 4,
                          groupValue: sortVal,
                          title: Text("Store Pickup - App",  style: regularText),
                        ),
                      ],
                    ),
                  ),
                );
              }
          )
      ),
      isDismissible: true,
      isScrollControlled: true,
    );
  }

  int _getCount(String status){
    print("CALLLL");
    int count = 0;
    for(int x = 0; x < orderCountByStatus.meta.orderCount.length; x++){
      if(orderCountByStatus.meta.orderCount[x].orderStatus.toLowerCase() == status.toLowerCase()) {
        count = orderCountByStatus.meta.orderCount[x].orderCount;
        break;
      }
    }
    return count;
  }

  @override
  void initState() {
    // TODO: implement initState
    getOrderCountByStatus();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? loadingCircular()
        : Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12),
                  child: Container(
                    height: 25,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: options.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            print("OPTION KEY: ${options.keys.toList()[index]}");
                            selectedOption(index);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: selectedChip == index ? Colors.black : Colors
                                    .grey,
                                borderRadius: BorderRadius.circular(8)
                            ),
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            margin: EdgeInsets.symmetric(horizontal: 4),
                            child: Center(child: Text("${options.values
                                .toList()[index]} (${_getCount(options.keys.toList()[index].toString())})", style: TextStyle(fontSize: 11,
                                color: selectedChip == index ? Colors.white : Colors
                                    .black),)),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                orderByStatus.meta.orderDetails.isEmpty
                    ? Center(
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: 200,
                            ),
                            Text(
                              "No Orders.",
                              style: h1,
                            ),
                          ],
                        ),
                      )
                    : Expanded(
                        flex: 9,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: (){
                                      sortMenu();
                                    },
                                    child: Row(
                                      children: <Widget>[
                                        Text("Sort", style: regularText),
                                        Stack(
                                          children: <Widget>[
                                            Icon(Icons.sort),
                                            !sort? SizedBox(): Positioned(right: 0, top: 0,child: Icon(Icons.fiber_manual_record, size: 12, color: mColor,))
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: (){
                                      filterMenu();
                                    },
                                    child: Row(
                                      children: <Widget>[
                                        Text("Filter", style: regularText,),
                                        Stack(
                                          children: <Widget>[
                                            Icon(Icons.filter_list),
                                            !filter? SizedBox(): Positioned(right: 0, top: 0,child: Icon(Icons.fiber_manual_record, size: 12, color: mColor,))
                                          ],
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                ),
                noOrder? Center(child: Column(
                  children: <Widget>[
                    SizedBox(height: 200,),
                    Text("No Orders.", style: h1,),
                  ],
                ),
                ): Expanded(
                  child: ListView.builder(
                    padding: EdgeInsets.symmetric(horizontal: 8),
//                      controller: scrollController,
                    shrinkWrap: true,
                    itemCount: filter || sort? filterList.length: orderByStatus.meta.orderDetails.length,
                    itemBuilder: (BuildContext context, int index) {
                      return filter || sort? deliveredOrderCard(filterList[index]): deliveredOrderCard(
                          orderByStatus.meta.orderDetails[index]);
                    },
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class deliveredOrderCard extends StatefulWidget {
  OrderByStatusDetail orderDetail;

  deliveredOrderCard(this.orderDetail);

  @override
  _deliveredOrderCardState createState() => _deliveredOrderCardState();
}

class _deliveredOrderCardState extends State<deliveredOrderCard> {

  bool storeDelivery = false;

  checkOrder() {
    if (widget.orderDetail.delivertype.toLowerCase() == "pickup" ||
        widget.orderDetail.delivertype.toLowerCase() == "counter purchase")
      storeDelivery = true;
  }

  @override
  void initState() {
    // TODO: implement initState
    checkOrder();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0.0,
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => OrderDetail(widget.orderDetail)));
        },
        child: Container(

          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(widget.orderDetail.firstname,
                  style: subHeading,
                ),
                SizedBox(height: 5,),
                  Row(
                    children: <Widget>[
                      Icon(Icons.location_on),
                      SizedBox(width: 5,),
                      Expanded(child: storeDelivery ? Text(storeLocation,
                        style: defaultTextStyle,) : Text(widget.orderDetail
                          .deliveryAddress,
                        style: defaultTextStyle,)),
                    ],
                  ),
                ListTile(
                  title: RichText(
                    text: TextSpan(children: <TextSpan>[
                      TextSpan(
                          text: "₹ ${widget.orderDetail.grandtotal}",
                          style: TextStyle(
                              fontSize: 12,
                              color: mColor,
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: ", ${widget.orderDetail.paymentMode
                              .toUpperCase()}",
                          style: TextStyle(fontSize: 11, color: Colors.black))
                    ]),
                  ),
                  trailing: Text(
                    widget.orderDetail.delivertype.toUpperCase(), style: smallHeading,),
                ),
                  Row(
                    children: <Widget>[
                      Icon(Icons.date_range),
                      SizedBox(width: 5,),
                      Expanded(child:Text("${DateFormat('dd MMM yyy | hh:mm a').format(DateTime.parse(widget.orderDetail.orderDate))}", style: regularText)),
                    ],
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

