import 'package:flutter/material.dart';
import 'package:merchant/Getter/GetOrderByStatus.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/constants.dart';

import '../OrderDetail.dart';

class CancelledOrders extends StatefulWidget {
  @override
  _CancelledOrdersState createState() => _CancelledOrdersState();
}

class _CancelledOrdersState extends State<CancelledOrders> {
  var mColor = Color(0xFFfc0c5b);
  var scrollController = ScrollController();
  HttpRequests requests = HttpRequests();
  OrderByStatus orderByStatus;
  bool loading = true;

  getOrderByStatus() async {
    var res = await requests.getOrdersByStatus("Order Canceled");
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        orderByStatus = OrderByStatus.fromJson(res);
        loading = false;
      });
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    getOrderByStatus();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? loadingCircular()
        : orderByStatus.meta.orderDetails.isEmpty
            ? Center(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 200,
                    ),
                    Text(
                      "No Orders.",
                      style: h1,
                    ),
                  ],
                ),
              )
            : Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      flex: 9,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          SizedBox(
                            height: 10,
                ),
                Expanded(
                  child: ListView.builder(
                    padding: EdgeInsets.symmetric(horizontal: 8),
//                      controller: scrollController,
                    shrinkWrap: true,
                    itemCount: orderByStatus.meta.orderDetails.length,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      OrderDetail(orderByStatus.meta
                                          .orderDetails[index])));
                        },
                        child: Card(
                          elevation: 0.0,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  orderByStatus.meta.orderDetails[index]
                                      .firstname,
                                  style: subHeading,
                                ),
                                SizedBox(height: 8,),
                                  Row(
                                    children: <Widget>[
                                      Icon(Icons.location_on),
                                      SizedBox(width: 5,),
                                      Expanded(child: Text(
                                        orderByStatus.meta.orderDetails[index]
                                            .deliveryAddress,
                                        style: defaultTextStyle,)),
                                    ],
                                  ),
                                SizedBox(height: 8,),
                                Center(
                                  child: Text(
                                    orderByStatus.meta.orderDetails[index]
                                        .orderCancelledOrRejectedBy,
                                    style: subHeading,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
