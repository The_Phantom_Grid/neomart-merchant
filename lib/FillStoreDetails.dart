import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:merchant/Components/ImagePick.dart';
import 'package:merchant/Getter/GetStoreTypes.dart';
import 'package:merchant/Network/httpRequests.dart';

import 'Components/VariableControllers.dart';
import 'constants.dart';

class FillStoreDetails extends StatefulWidget {
  @override
  _FillStoreDetailsState createState() => _FillStoreDetailsState();
}

class _FillStoreDetailsState extends State<FillStoreDetails> {
  var cNameController = TextEditingController();
  var panController = TextEditingController();
  var numberController = TextEditingController();
  var cDescription = TextEditingController();
  var gstinController = TextEditingController();
  var aadharController = TextEditingController();
  var fssaiController = TextEditingController();
  HttpRequests requests = HttpRequests();
  bool loading = true;
  StoreTypes storeTypes;
  String panCardImage,
      gstImage,
      logo,
      aadharImage,
      storeImage1,
      storeImage2,
      storeImage3;
  String cNameError,
      cDescError,
      storeType = "Choose store type",
      storeSubCat = "Choose store sub category";
  String categoryId,
      subCatId,
      aadharCard,
      description,
      documentsAvail,
      fssai,
      gstNo,
      panCard,
      shopName,
      storeImageAvail,
      storeNumber;
  var ratio = [
    CropAspectRatioPreset.square,
    CropAspectRatioPreset.ratio3x2,
    CropAspectRatioPreset.original,
    CropAspectRatioPreset.ratio4x3,
    CropAspectRatioPreset.ratio16x9
  ];

  validate() {
    if (cNameController.text == "" || cNameController.text.length < 3)
      Fluttertoast.showToast(msg: "Invalid detail: Store Name");
    else if (cDescription.text == "" || cNameController.text.length < 5)
      Fluttertoast.showToast(msg: "Invalid detail: Store Description");
    else if (numberController.text.length < 10)
      Fluttertoast.showToast(msg: "Invalid detail: Store Contact Number");
    else if (categoryId == null)
      Fluttertoast.showToast(msg: "Invalid detail: Select Store Type");
    else if (logo == null)
      Fluttertoast.showToast(msg: "Invalid detail: Upload Store Image");
    else if (gstinController.text != "" && gstinController.text.length < 15)
      Fluttertoast.showToast(msg: "Invalid detail: GST Number");
    else if (panController.text == "" || panController.text.length < 10)
      Fluttertoast.showToast(msg: "Invalid detail: Pan Card Number");
    else if (aadharController.text == "" || aadharController.text.length < 12)
      Fluttertoast.showToast(msg: "Invalid detail: Aadhar Card Number");
    else if (fssaiController.text != "" && fssaiController.text.length < 15)
      Fluttertoast.showToast(msg: "Invalid detail: FSSAI Number");
    else if (aadharImage == null)
      Fluttertoast.showToast(msg: "Invalid detail: Upload Aadhar Card Image");
    else if (panCardImage == null)
      Fluttertoast.showToast(msg: "Invalid detail: Upload Pan Card Image");
    else
      saveStoreDetails();
  }

  saveStoreDetails() async {
    var res = await requests.saveStoreInfo(
        aadharCard,
        aadharImage,
        categoryId,
        description,
        "0",
        fssai,
        gstImage,
        gstNo,
        logo,
        merchantid,
        panCard,
        panCardImage,
        shopName,
        storeid,
        "1",
        storeNumber,
        storeType,
        "",
        "",
        "0",
        storeImage1,
        storeImage2,
        storeImage3,
        token
    );
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      detailsUpdated = true;
//      storeId = res['data']['store_id'].toString();
//      getSubCategory();
//      changeStep();
      storeName = shopName;
      Fluttertoast.showToast(
          msg: "Store details saved",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    } else {
      Fluttertoast.showToast(
          msg: "Something went wrong",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    }
  }

  getStoreTypes() async {
    print("TOKEN: ${token}| MERCHANT: ${merchantid}");
    var res = await requests.getStoreTypes(token);
    if (res != null) {
      if (res['status'].toString().toLowerCase() == "success") {
        setState(() {
          storeTypes = StoreTypes.fromJson(res);
        });
        setStoreType();
      }
    }
  }

  setStoreType() {
    setState(() {
      storeTypes.data.forEach((e) {
        if (e.id == storeDetails.info.storeDetail.storeTypeId)
          storeType = e.catName.toString();
      });
    });
  }

  showStoreTypes() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (_) {
          return AlertDialog(
            title: Text(
              "Select store type",
              style: smallHeading,
            ),
            content: Container(
              height: MediaQuery.of(context).size.height - 60,
              width: MediaQuery.of(context).size.width,
              child: GridView.count(
                shrinkWrap: true,
                childAspectRatio: 4 / 2,
                crossAxisCount: 2,
                crossAxisSpacing: 4,
                mainAxisSpacing: 12,
                children: storeTypes.data.map((e) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      setState(() {
                        storeType = e.catName.toString();
                        categoryId = e.id.toString();
//                        storeSubCategories = null;
//                        storeSubCat = "Choose store sub category";
//                        subCatId = null;
                        print("CAT ID: $categoryId");
                      });
//                      getStoreSubCat();
                    },
                    child: Container(
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            color:
                                storeType == e.catName ? mColor : Colors.white,
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(color: mColor)),
                        child: Center(
                            child: Text(
                          e.catName.toString(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 12,
                              color: storeType == e.catName
                                  ? Colors.white
                                  : mColor),
                        ))),
                  );
                }).toList(),
                padding: EdgeInsets.symmetric(vertical: 4),
              ),
            ),
          );
        });
  }

  setImage(int imageType, String imageString) {
    switch (imageType) {
      case 0:
        logo = imageString;
        break;
      case 1:
        storeImage1 = imageString;
        break;
      case 2:
        storeImage2 = imageString;
        break;
      case 3:
        storeImage3 = imageString;
        break;
      case 4:
        panCardImage = imageString;
        break;
      case 5:
        aadharImage = imageString;
        break;
    }
  }

  openBottomSheet(int imageType) {
    String base64ImageString;
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (context) {
          return Container(
              height: MediaQuery.of(context).size.height * .2,
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      image = await getImage(ImageSource.camera, ratio);
                      if (image != null) {
                        base64ImageString = convertImageToBase64(image);
                        setImage(imageType, base64ImageString);
                      }
                      setState(() {});
                    },
                    leading: Icon(Icons.camera_alt),
                    title: Text("Camera"),
                  ),
                  ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      image = await getImage(ImageSource.gallery, ratio);
                      if (image != null) {
                        base64ImageString = convertImageToBase64(image);
                        setImage(imageType, base64ImageString);
                      }
                      setState(() {});
                    },
                    leading: Icon(Icons.photo_library),
                    title: Text("Gallery"),
                  ),
                ],
              ));
        });
  }

  getDetails() async {
    print(detailsUpdated);
    if (detailsUpdated) {
      await getStoreDetails();
      setState(() {
        loading = false;
        detailsUpdated = false;
      });
      initControllers();
    } else {
      setState(() {
        loading = false;
      });
      initControllers();
    }
  }

  getImages() async {
    print("STORE: ${storeDetails.info.storeImages}");
    logo = await getImageFromNetwork(storeDetails.info.storeDetail.logo);
//    storeImage1 = await getImageFromNetwork(storeDetails.info.storeImages[0]);
//    storeImage2 = await getImageFromNetwork(storeDetails.info.storeImages[1]);
//    storeImage3 = await getImageFromNetwork(storeDetails.info.storeImages[2]);
    aadharImage =
        await getImageFromNetwork(storeDetails.info.storeDetail.aadharCardImg);
    panCardImage =
        await getImageFromNetwork(storeDetails.info.storeDetail.panCardImg);
    print("PANcARD: ${storeDetails.info.storeDetail.panCardImg}");
    setState(() {});
  }

  initControllers() {
    setState(() {
      cNameController.text = storeDetails.info.storeDetail.storeName;
      cDescription.text = storeDetails.info.storeDetail.storeDescr;
      numberController.text = storeDetails.info.storeDetail.phoneNo.toString();
      gstinController.text = storeDetails.info.storeDetail.gst;
      panController.text = storeDetails.info.storeDetail.panCard;
      aadharController.text = storeDetails.info.storeDetail.aadharCard;
      fssaiController.text = storeDetails.info.storeDetail.fssai;
      shopName = cNameController.text;
      description = cDescription.text;
      storeNumber = numberController.text;
      gstNo = gstinController.text;
      panCard = panController.text;
      aadharCard = aadharController.text;
      fssai = fssaiController.text;
      categoryId = storeDetails.info.storeDetail.storeTypeId.toString();
      print("CAT ID: $categoryId");
    });
    getImages();
    getStoreTypes();
  }

  @override
  void initState() {
    // TODO: implement initState
    getDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Fill Your Store Details"),
      ),
      extendBody: true,
      body: loading
          ? loadingCircular()
          : Column(
              children: <Widget>[
                Expanded(
                  child: ListView(
                      padding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).padding.bottom + 60,
                          left: 8,
                          right: 8,
                          top: 8),
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  border: Border.all(color: Colors.black)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Store name",
                                    textAlign: TextAlign.left,
                                    style: smallHeading,
                                  ),
                                  TextFormField(
                                    controller: cNameController,
                                    enabled: true,
                                    decoration: InputDecoration(
                                        contentPadding:
                                            EdgeInsets.symmetric(vertical: 12),
                                        errorText: cNameError,
                                        isDense: true,
                                        hasFloatingPlaceholder: true,
                                        hintText: "Store Name",
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.transparent)),
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.transparent)),
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.transparent))),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Container(
                              padding: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  border: Border.all(color: Colors.black)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "About Store",
                                    textAlign: TextAlign.left,
                                    style: smallHeading,
                                  ),
                                  TextFormField(
                                    controller: cDescription,
                                    enabled: true,
                                    maxLines: 2,
                                    maxLength: 150,
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 12),
                                      errorText: cDescError,
                                      isDense: true,
                                      hasFloatingPlaceholder: true,
                                      hintText:
                                          "Write short description about the store",
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.transparent)),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.transparent)),
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.transparent)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Container(
                              padding: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  border: Border.all(color: Colors.black)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Store Contact Number",
                                    textAlign: TextAlign.left,
                                    style: smallHeading,
                                  ),
                                  TextFormField(
                                    keyboardType: TextInputType.number,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter(
                                          RegExp("[0-9]"))
                                    ],
                                    maxLengthEnforced: true,
                                    maxLength: 10,
                                    decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.transparent)),
                                        focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.transparent)),
                                        border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.transparent)),
                                        contentPadding:
                                            EdgeInsets.symmetric(vertical: 12),
                                        counterText: "",
//                              prefixIcon: Icon(Icons.phone_iphone),
                                        hintText: "000 000 0000",
//                            labelText: "Enter Mobile Number",
                                        hasFloatingPlaceholder: true,
                                        labelStyle:
                                            TextStyle(color: Colors.grey)),
                                    controller: numberController,
                                    onChanged: (val) {
                                      storeNumber = numberController.text;
                                    },
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Container(
                              padding: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  border: Border.all(color: Colors.black)),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Store Type",
                                    textAlign: TextAlign.left,
                                    style: smallHeading,
                                  ),
                                  SizedBox(
                                    height: 4,
                                  ),
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          storeTypes == null
                                              ? "--"
                                              : storeType.toString(),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        SizedBox(
                                          width: 8,
                                        ),
                                        IconButton(
                                          icon: Icon(Icons.arrow_drop_down),
                                          onPressed: storeTypes == null
                                              ? null
                                              : () {
                                                  showStoreTypes();
                                                },
                                        ),
                                      ]),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: GestureDetector(
                                    onTap: () {
                                      openBottomSheet(0);
                                    },
                                    child: Container(
                                        height: 65,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            border: Border.all(
                                                color: Colors.black)),
                                        child: logo == null
                                            ? Center(
                                                child: Icon(FlutterIcons
                                                    .add_a_photo_mdi))
                                            : Image.memory(
                                                Base64Decoder().convert(logo))
//                              FadeInImage(
//                                placeholder: AssetImage('assets/tsp.png'),
//                                image: NetworkImage(storeDetails.info.storeDetail.logo),
//                                fit: BoxFit.cover,
//                              )
                                        ),
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  flex: 1,
                                  child: GestureDetector(
                                    onTap: () {
                                      openBottomSheet(1);
                                    },
                                    child: Container(
                                        height: 65,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            border: Border.all(
                                                color: Colors.black)),
                                        child: storeImage1 == null
                                            ? Center(
                                                child: Icon(FlutterIcons
                                                    .add_a_photo_mdi))
                                            : Image.memory(Base64Decoder()
                                                .convert(storeImage1))
//                              FadeInImage(
//                                placeholder: AssetImage('assets/tsp.png'),
//                                image: NetworkImage(storeDetails.info.storeDetail.aadharCardImg),
//                                fit: BoxFit.fitWidth,
//                              )
                                        ),
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  flex: 1,
                                  child: GestureDetector(
                                    onTap: () {
                                      openBottomSheet(2);
                                    },
                                    child: Container(
                                        height: 65,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            border: Border.all(
                                                color: Colors.black)),
                                        child: storeImage2 == null
                                            ? Center(
                                                child: Icon(FlutterIcons
                                                    .add_a_photo_mdi))
                                            : Image.memory(Base64Decoder()
                                                .convert(storeImage2))
//                              FadeInImage(
//                                placeholder: AssetImage('assets/tsp.png'),
//                                image: NetworkImage(storeDetails.info.storeDetail.aadharCardImg),
//                                fit: BoxFit.fitWidth,
//                              )
                                        ),
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                  flex: 1,
                                  child: GestureDetector(
                                    onTap: () {
                                      openBottomSheet(3);
                                    },
                                    child: Container(
                                        height: 65,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(8),
                                            border: Border.all(
                                                color: Colors.black)),
                                        child: storeImage3 == null
                                            ? Center(
                                                child: Icon(FlutterIcons
                                                    .add_a_photo_mdi))
                                            : Image.memory(Base64Decoder()
                                                .convert(storeImage3))
//                              FadeInImage(
//                                placeholder: AssetImage('assets/tsp.png'),
//                                image: NetworkImage(storeDetails.info.storeDetail.aadharCardImg),
//                                fit: BoxFit.fitWidth,
//                              )
                                        ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Text(
                              "Update Store Validation Details",
                              textAlign: TextAlign.left,
                              style: subHeading,
                            ),
                            Text(
                              "(Minimum 1 required)",
                              style: subHeading,
                            ),
                            Divider(),
                            SizedBox(
                              height: 5,
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        border:
                                            Border.all(color: Colors.black)),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "Enter GSTIN Number",
                                          textAlign: TextAlign.left,
                                          style: smallHeading,
                                        ),
                                        TextFormField(
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter(
                                                RegExp("[0-9, A-Z, a-z]"))
                                          ],
                                          maxLengthEnforced: true,
                                          maxLength: 15,
                                          decoration: InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.symmetric(
                                                      vertical: 12),
                                              counterText: "",
                                              hintText:
                                                  "enter gstin number here...",
                                              enabledBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.transparent)),
                                              focusedBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.transparent)),
                                              border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.transparent)),
                                              hasFloatingPlaceholder: true,
                                              labelStyle: TextStyle(
                                                  color: Colors.grey)),
                                          controller: gstinController,
                                          onChanged: (text) {
                                            gstNo = gstinController.text
                                                .toUpperCase();
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: GestureDetector(
                                          onTap: () {
                                            openBottomSheet(4);
                                          },
                                          child: Container(
                                              height: 65,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                  border: Border.all(
                                                      color: Colors.black)),
                                              child: panCardImage == null
                                                  ? Center(
                                                      child: Icon(FlutterIcons
                                                          .add_a_photo_mdi))
                                                  : Image.memory(Base64Decoder()
                                                      .convert(panCardImage))
//                                      FadeInImage(
//                                        placeholder: AssetImage('assets/tsp.png'),
//                                        image: NetworkImage(storeDetails.info.storeDetail.panCardImg),
//                                        fit: BoxFit.fitWidth,
//                                      )
                                              ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: Container(
                                          padding: EdgeInsets.all(8),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              border: Border.all(
                                                  color: Colors.black)),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                "Enter Pan Number",
                                                textAlign: TextAlign.left,
                                                style: smallHeading,
                                              ),
                                              TextFormField(
                                                inputFormatters: [
                                                  WhitelistingTextInputFormatter(
                                                      RegExp("[0-9, A-Z, a-z]"))
                                                ],
                                                maxLengthEnforced: true,
                                                maxLength: 10,
                                                decoration: InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.symmetric(
                                                            vertical: 12),
                                                    counterText: "",
                                                    hintText: "AAAPL1234C",
                                                    enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Colors
                                                                .transparent)),
                                                    focusedBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Colors
                                                                .transparent)),
                                                    border: OutlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Colors
                                                                .transparent)),
                                                    hasFloatingPlaceholder:
                                                        true,
                                                    labelStyle: TextStyle(
                                                        color: Colors.grey)),
                                                controller: panController,
                                                onChanged: (text) {
                                                  panCard = panController.text
                                                      .toUpperCase();
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: GestureDetector(
                                          onTap: () {
                                            openBottomSheet(5);
                                          },
                                          child: Container(
                                              height: 65,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                  border: Border.all(
                                                      color: Colors.black)),
                                              child: aadharImage == null
                                                  ? Center(
                                                      child: Icon(FlutterIcons
                                                          .add_a_photo_mdi))
                                                  : Image.memory(Base64Decoder()
                                                      .convert(aadharImage))
//                                    FadeInImage(
//                                      placeholder: AssetImage('assets/tsp.png'),
//                                      image: NetworkImage(storeDetails.info.storeDetail.aadharCardImg),
//                                      fit: BoxFit.fitWidth,
//                                    )
                                              ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Expanded(
                                        flex: 4,
                                        child: Container(
                                          padding: EdgeInsets.all(8),
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(8),
                                              border: Border.all(
                                                  color: Colors.black)),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Text(
                                                "Enter Aadhaar Card Number",
                                                textAlign: TextAlign.left,
                                                style: smallHeading,
                                              ),
                                              TextFormField(
                                                keyboardType:
                                                    TextInputType.number,
                                                inputFormatters: [
                                                  WhitelistingTextInputFormatter(
                                                      RegExp("[0-9]"))
                                                ],
                                                maxLengthEnforced: true,
                                                maxLength: 12,
                                                decoration: InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.symmetric(
                                                      vertical: 12,
                                                    ),
                                                    counterText: "",
                                                    hintText: "0000-0000-0000",
                                                    enabledBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Colors
                                                                .transparent)),
                                                    focusedBorder: OutlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Colors
                                                                .transparent)),
                                                    border: OutlineInputBorder(
                                                        borderSide: BorderSide(
                                                            color: Colors
                                                                .transparent)),
                                                    hasFloatingPlaceholder:
                                                        true,
                                                    labelStyle: TextStyle(
                                                        color: Colors.grey)),
                                                controller: aadharController,
                                                onChanged: (text) {
                                                  aadharCard = aadharController
                                                      .text
                                                      .toUpperCase();
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(8),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(8),
                                        border:
                                            Border.all(color: Colors.black)),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "Enter FSSAI Number",
                                          textAlign: TextAlign.left,
                                          style: smallHeading,
                                        ),
                                        TextFormField(
                                          keyboardType: TextInputType.number,
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter(
                                                RegExp("[0-9, A-Z, a-z]"))
                                          ],
                                          maxLengthEnforced: true,
                                          maxLength: 15,
                                          decoration: InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.symmetric(
                                                      vertical: 12),
                                              counterText: "",
                                              hintText: "0000-0000-0000-00",
                                              enabledBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.transparent)),
                                              focusedBorder: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.transparent)),
                                              border: OutlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color:
                                                          Colors.transparent)),
                                              hasFloatingPlaceholder: true,
                                              labelStyle: TextStyle(
                                                  color: Colors.grey)),
                                          controller: fssaiController,
                                          onChanged: (text) {
                                            fssai = fssaiController.text
                                                .toUpperCase();
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ].toList(),
                        ),
                      ]),
                ),
              ],
            ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ButtonTheme(
          height: 50,
          splashColor: Colors.white38,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: RaisedButton(
            onPressed: () {
              validate();
            },
            child: Text(
              "UPDATE",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
