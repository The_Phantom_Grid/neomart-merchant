import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:merchant/Getter/GetProductCategory.dart';
import 'package:merchant/Getter/GetProductSubCategory.dart';
import 'package:merchant/MyStoreInventory.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/QrScan.dart';
import 'package:merchant/constants.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:permission_handler/permission_handler.dart';

import 'Components/ImagePick.dart';
import 'Components/VariableControllers.dart';

class AddOwnBrand extends StatefulWidget {
  @override
  _AddOwnBrandState createState() => _AddOwnBrandState();
}

class _AddOwnBrandState extends State<AddOwnBrand> {
  TextEditingController barcodeController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController brandController = TextEditingController();
  TextEditingController companyController = TextEditingController();
  TextEditingController catController = TextEditingController();
  TextEditingController subCatController = TextEditingController();
  TextEditingController qtyController = TextEditingController();
  TextEditingController mrpController = TextEditingController();
  TextEditingController srpController = TextEditingController();
  TextEditingController gstController = TextEditingController();
  TextEditingController descController = TextEditingController();
  TextEditingController packSizeController = TextEditingController();

  String productImage, categoryId, subCatId, catName, subCatName;
  int qty = 0, taxVal = 0;
  double mrp = 0, srp = 0, gst = 0;
  HttpRequests requests = HttpRequests();
  bool loadCats = false, loadSubCats = false, loading = false;
  var ratio = [
    CropAspectRatioPreset.square,
    CropAspectRatioPreset.ratio3x2,
    CropAspectRatioPreset.original,
    CropAspectRatioPreset.ratio4x3,
    CropAspectRatioPreset.ratio16x9
  ];

  Future _scan() async {
    PermissionStatus status = await Permission.camera.status;
    if (!status.isGranted)
      await Permission.camera.request();
    else {
      String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', "Cancel", true, ScanMode.BARCODE);
      if (barcodeScanRes == "-1") barcodeScanRes = "";
      // HapticFeedback.mediumImpact();
      setState(() {
        barcodeController.text = barcodeScanRes.toString();
      });
    }
  }

  saveProduct() async {
    var res = await requests.addProductUnderOwnBrand(
        barcodeController.text.toString(),
        brandController.text.toString(),
        companyController.text.toString(),
        categoryId,
        descController.text.toString(),
        gst.toString(),
        mrp.toString(),
        nameController.text.toString(),
        productImage,
        "",
        "",
        qty.toString(),
        srp.toString(),
        subCatId,
        taxVal == 1 ? "Inclusive" : "Exclusive",
        packSizeController.text.toString());
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Fluttertoast.showToast(msg: "Product added to your inventory");
      Get.off(MyStoreInventory());
      setState(() {
        loading = false;
      });
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  openBottomSheet() {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (context) {
          return Container(
              height: MediaQuery.of(context).size.height * .2,
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      image = await getImage(ImageSource.camera, ratio);
                      if (image != null) {
                        productImage = convertImageToBase64(image);
                      }
                      setState(() {});
                    },
                    leading: Icon(Icons.camera_alt),
                    title: Text("Camera"),
                  ),
                  ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      image = await getImage(ImageSource.gallery, ratio);
                      if (image != null) {
                        productImage = convertImageToBase64(image);
                      }
                      setState(() {});
                    },
                    leading: Icon(Icons.photo_library),
                    title: Text("Gallery"),
                  ),
                ],
              ));
        });
  }

  validateDetails() {
    if (nameController.text == "" || nameController.text.length < 3)
      Fluttertoast.showToast(msg: "Invalid detail: Product Name");
    else if (barcodeController.text == "" || barcodeController.text.length < 2)
      Fluttertoast.showToast(msg: "Invalid detail: Barcode Number");
    else if (packSizeController.text == "" ||
        packSizeController.text.length < 1)
      Fluttertoast.showToast(msg: "Invalid detail: Pack Size & Unit");
    else if (brandController.text == "" || brandController.text.length < 1)
      Fluttertoast.showToast(msg: "Invalid detail: Brand Name");
    else if (categoryId == null)
      Fluttertoast.showToast(msg: "Invalid detail: Select Main Category");
    else if (subCatId == null)
      Fluttertoast.showToast(msg: "Invalid detail: Select Sub-Category");
    else if (productImage == null)
      Fluttertoast.showToast(msg: "Invalid detail: Upload Product Image");
    else if (companyController.text == "" || companyController.text.length < 1)
      Fluttertoast.showToast(msg: "Invalid detail: Company Name");
    else if (descController.text == "" || descController.text.length < 3)
      Fluttertoast.showToast(msg: "Invalid detail: Product Description");
    else if (qty <= 0)
      Fluttertoast.showToast(msg: "Invalid detail: Quantity cannot be 0");
    else if (mrp <= 0)
      Fluttertoast.showToast(msg: "Invalid detail: MRP cannot be 0");
    else if (srp <= 0)
      Fluttertoast.showToast(msg: "Invalid detail: SRP cannot be 0");
    else if (taxVal == 0)
      Fluttertoast.showToast(msg: "Invalid detail: Select GST");
    else if (mrp < srp)
      Fluttertoast.showToast(
          msg: "Invalid detail: SRP cannot be greater than MRP");
    else if (gst > (mrp * 0.50))
      Fluttertoast.showToast(
          msg: "Invalid detail: GST cannot be greater than 50% of MRP");
    else {
      setState(() {
        loading = true;
      });
      saveProduct();
    }
  }

  toggleTax(int val) {
    print(val);
    setState(() {
      taxVal = val;
    });
  }

  getProductCat() async {
    var res = await requests.getProductCatFordp();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        addProdCats = ProductCategory.fromJson(res);
        loadCats = false;
      });
      showCats();
    }
  }

  getProductSubCat() async {
    var res = await requests.getProductSubCatFordp(categoryId);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        addProdSubCats = ProductSubCategory.fromJson(res);
        loadSubCats = false;
      });
      showSubCats();
    }
  }

  showCats() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (_) {
          return AlertDialog(
            title: Text(
              "Select Product Category",
              style: smallHeading,
            ),
            content: Container(
              height: MediaQuery.of(context).size.height - 60,
              width: MediaQuery.of(context).size.width,
              child: GridView.count(
                shrinkWrap: true,
                childAspectRatio: 4 / 2,
                crossAxisCount: 2,
                crossAxisSpacing: 4,
                mainAxisSpacing: 12,
                children: addProdCats.info.map((e) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      setState(() {
                        catName = e.prodCategoryName.toString();
                        catController.text = catName;
                        categoryId = e.prodCategoryId.toString();
                        subCatController.text = "";
                        subCatName = null;
                        addProdSubCats = null;
                        subCatId = null;
                      });
                      getProductSubCat();
                    },
                    child: Container(
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            color: catName == e.prodCategoryName
                                ? mColor
                                : Colors.white,
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(color: mColor)),
                        child: Center(
                            child: Text(
                          e.prodCategoryName.toString(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 12,
                              color: catName == e.prodCategoryName
                                  ? Colors.white
                                  : mColor),
                        ))),
                  );
                }).toList(),
                padding: EdgeInsets.symmetric(vertical: 4),
              ),
            ),
          );
        });
  }

  showSubCats() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (_) {
          return AlertDialog(
            title: Text(
              "Select Product Sub-Category",
              style: smallHeading,
            ),
            content: Container(
              height: MediaQuery.of(context).size.height - 60,
              width: MediaQuery.of(context).size.width,
              child: GridView.count(
                shrinkWrap: true,
                childAspectRatio: 4 / 2,
                crossAxisCount: 2,
                crossAxisSpacing: 4,
                mainAxisSpacing: 12,
                children: addProdSubCats.info.map((e) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      setState(() {
                        subCatName = e.name.toString();
                        subCatController.text = subCatName;
                        subCatId = e.subcatId.toString();
                      });
                    },
                    child: Container(
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            color: subCatName == e.name ? mColor : Colors.white,
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(color: mColor)),
                        child: Center(
                            child: Text(
                          e.name.toString(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 12,
                              color:
                                  subCatName == e.name ? Colors.white : mColor),
                        ))),
                  );
                }).toList(),
                padding: EdgeInsets.symmetric(vertical: 4),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: loading,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Add Your Own Brand"),
        ),
        extendBody: true,
        body: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: MediaQuery
              .of(context)
              .padding
              .bottom + 60),
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(7),
                      child: GestureDetector(
                        onTap: () {
                          openBottomSheet();
                        },
                        child: Container(
                          height: 130,
                          width: 130,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.blueGrey.withOpacity(.4),
                          ),
                          child: productImage == null
                              ?
                          Center(child: Icon(
                              FlutterIcons.add_a_photo_mdi,
                              color: Colors.white))
                              :
                          Image.memory(Base64Decoder().convert(productImage)),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "Product Name",
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: subHeading,
                  ),
                  TextFormField(
                    enabled: true,
                    controller: nameController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 16, vertical: 16),
                        isDense: true,
                        hasFloatingPlaceholder: true,
                        hintText: "Enter Product Name",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "Brand Name",
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: subHeading,
                  ),
                  TextFormField(
                    enabled: true,
                    controller: brandController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 16, vertical: 16),
                        isDense: true,
                        hasFloatingPlaceholder: true,
                        hintText: "Enter Brand",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "Company Name",
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: subHeading,
                  ),
                  TextFormField(
                    enabled: true,
                    controller: companyController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 16, vertical: 16),
                        isDense: true,
                        hasFloatingPlaceholder: true,
                        hintText: "Enter Company Name",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "Choose Main Category",
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: subHeading,
                  ),
                  TextFormField(
                    enabled: loadCats ? false : true,
                    controller: catController,
                    onTap: () {
                      setState(() {
                        loadCats = true;
                        getProductCat();
//                        if (addProdCats == null) {
//                          print("GET PROs");
//                          loadCats = true;
//                          getProductCat();
//                        } else {
//                          showCats();
//                        }
                      });
                    },

                    onChanged: (val) {
                      setState(() {
                        catController.text = catName;
                      });
                    },
                    decoration: InputDecoration(
                        suffixIconConstraints: BoxConstraints(
                          minWidth: 20, minHeight: 20,),
                        suffixIcon: loadCats ? Padding(
                          padding: const EdgeInsets.all(8),
                          child: CircularProgressIndicator(strokeWidth: 1.5,),
                        ) : SizedBox(),
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 16, vertical: 16),
                        isDense: true,
                        hasFloatingPlaceholder: true,
                        hintText: "Tap to Select",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "Choose Sub Category",
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: subHeading,
                  ),
                  TextFormField(
                    enabled: loadSubCats || categoryId == null ? false : true,
                    controller: subCatController,
                    onTap: () {
                      setState(() {

                        loadSubCats = true;
                        getProductSubCat();
//                        if (addProdSubCats == null) {
//                          print("GET Sub PROs");
//                          loadSubCats = true;
//                          getProductSubCat();
//                        } else {
//                          showSubCats();
//                        }
                      });
                    },
                    onChanged: (val) {
                      setState(() {
                        if (subCatName == null)
                          showSubCats();
                        else
                          subCatController.text = subCatName;
                      });
                    },
                    decoration: InputDecoration(
                        suffixIconConstraints: BoxConstraints(
                          minWidth: 20, minHeight: 20,),
                        suffixIcon: loadSubCats ? Padding(
                          padding: const EdgeInsets.all(8),
                          child: CircularProgressIndicator(strokeWidth: 1.5,),
                        ) : SizedBox(),
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 16, vertical: 16),
                        isDense: true,
                        hasFloatingPlaceholder: true,
                        hintText: "Tap to Select",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 15),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Quantity",
                                textAlign: TextAlign.left,
                                textScaleFactor: 1,
                                style: subHeading,
                              ),
                              TextFormField(
                                enabled: true,
                                controller: qtyController,
                                onChanged: (val) {
                                  setState(() {
                                    if (qtyController.text == "")
                                      qty = 0;
                                    else
                                      qty = int.tryParse(qtyController.text);
                                  });
                                },
                                inputFormatters: [
                                  WhitelistingTextInputFormatter(
                                      RegExp("[0-9]"))
                                ],
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(
                                        horizontal: 16, vertical: 16),
                                    isDense: true,
                                    hasFloatingPlaceholder: true,
                                    hintText: "Quantity",
                                    border: OutlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Colors.black))),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          flex: 1,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "MRP",
                                textAlign: TextAlign.left,
                                textScaleFactor: 1,
                                style: subHeading,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: true),
                                enabled: true,
                                controller: mrpController,
                                onChanged: (val) {
                                  setState(() {
                                    if (mrpController.text == "")
                                      mrp = 0;
                                    else
                                      mrp = double.tryParse(mrpController.text);
                                  });
                                },
                                inputFormatters: [
//                                WhitelistingTextInputFormatter(RegExp("[0-9]")),
                                  amountInputFormatter
                                ],
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(
                                        horizontal: 16, vertical: 16),
                                    isDense: true,
                                    hasFloatingPlaceholder: true,
                                    hintText: "MRP",
                                    border: OutlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Colors.black))),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "SRP",
                                textAlign: TextAlign.left,
                                textScaleFactor: 1,
                                style: subHeading,
                              ),
                              TextFormField(
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: true),
                                enabled: true,
                                controller: srpController,
                                onChanged: (val) {
                                  setState(() {
                                    if (srpController.text == "")
                                      srp = 0;
                                    else
                                      srp = double.tryParse(srpController.text);
                                  });
                                },
                                inputFormatters: [
                                  amountInputFormatter
                                ],
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(
                                        horizontal: 16, vertical: 16),
                                    isDense: true,
                                    hasFloatingPlaceholder: true,
                                    hintText: "SRP",
                                    border: OutlineInputBorder(
                                        borderSide:
                                        BorderSide(color: Colors.black))),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Text(
                    "Pack Size & Unit",
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: subHeading,
                  ),
                  TextFormField(
                    enabled: true,
                    controller: packSizeController,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 16, vertical: 16),
                        isDense: true,
                        hasFloatingPlaceholder: true,
                        hintText: "Eg. 1 kg",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "GST",
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: subHeading,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.numberWithOptions(
                        decimal: true),
                    enabled: true,
                    controller: gstController,
                    onChanged: (val) {
                      setState(() {
                        if (gstController.text == "")
                          gst = 0;
                        else
                          gst = double.tryParse(gstController.text);
                      });
                    },
                    inputFormatters: [
                      amountInputFormatter
                    ],
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 16, vertical: 16),
                        isDense: true,
                        hasFloatingPlaceholder: true,
                        hintText: "Enter GST tax",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: RadioListTile(
                            title: Text("Inclusive"),
                            value: 1,
                            groupValue: taxVal,
                            onChanged: (val) {
                              toggleTax(val);
                            },
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: RadioListTile(
                            title: Text("Exclusive"),
                            value: 2,
                            groupValue: taxVal,
                            onChanged: (val) {
                              toggleTax(val);
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Text(
                    "Description",
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: subHeading,
                  ),
                  TextFormField(
                    enabled: true,
                    maxLines: 3,
                    controller: descController,
                    maxLength: 150,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 16, vertical: 16),
                        isDense: true,
                        hasFloatingPlaceholder: true,
                        hintText: "Write short description about the product",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    "Barcode Number",
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: subHeading,
                  ),
                  TextFormField(
                    controller: barcodeController,
                    enabled: true,
                    onChanged: (text) {
                      barcodeController.text = text;
                    },
                    inputFormatters: [
                      WhitelistingTextInputFormatter(RegExp("[0-9]"))
                    ],
                    decoration: InputDecoration(
                        contentPadding:
                        EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                        isDense: true,
                        suffixIcon: IconButton(
                          onPressed: () {
                            _scan();
//                          Navigator.push(context, MaterialPageRoute(builder: (context) => QrBarcodeScanner()));
                          },
                          icon: Icon(
                            FlutterIcons.qrcode_scan_mco,
                            size: 30,
                            color: Colors.black,
                          ),
                        ),
                        hintText: "Barcode Number",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ButtonTheme(
            height: 50,
            splashColor: Colors.white38,
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
            child: RaisedButton(
              onPressed: () {
                validateDetails();
              },
              child: loading ? SizedBox(height: 25,
                  width: 25,
                  child: SpinKitCircle(color: Colors.white, size: 25,)) : Text(
                "Add to Inventory",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
