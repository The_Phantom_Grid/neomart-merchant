//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter/rendering.dart';
//
//import 'HomePage/HomePage.dart';
//
//class ChooseStore extends StatefulWidget {
//  @override
//  _ChooseStoreState createState() => _ChooseStoreState();
//}
//
//class _ChooseStoreState extends State<ChooseStore> {
//  var mColor = Color(0xFFfc0c5b);
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        backgroundColor: Colors.black,
//        centerTitle: true,
//        title: Text(
//          "Choose Store",
//          style: TextStyle(color: mColor),
//        ),
//      ),
//      body: Container(
//        padding: EdgeInsets.all(2),
//        child: ListView.builder(
//          itemCount: 2,
//          itemExtent: 120,
//          itemBuilder: (BuildContext context, int index) {
//            return GestureDetector(
//              onTap: () {
//                Navigator.push(context,
//                    MaterialPageRoute(builder: (context) => HomePage()));
//              },
//              child: Card(
//                elevation: 2.0,
//                shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(5),
//                    side: BorderSide(color: mColor)),
//                child: Center(
//                  child: ListTile(
//                    leading: ClipRRect(
//                      borderRadius: BorderRadius.circular(30),
//                      child: Container(
//                        height: 60,
//                        width: 60,
//                        color: Colors.grey,
//                      ),
//                    ),
//                    title: Text(
//                      "Store Name",
//                      maxLines: 2,
//                      overflow: TextOverflow.ellipsis,
//                    ),
//                    subtitle: Text(
//                      "Store Address max 2 lines",
//                      maxLines: 2,
//                      overflow: TextOverflow.ellipsis,
//                    ),
//                    trailing: Column(
//                      children: <Widget>[
//                        Icon(
//                          Icons.check_circle,
//                          color: Colors.green,
//                        ),
//                        Text(
//                          "Approved",
//                          style: TextStyle(color: Colors.green),
//                        )
//                      ],
//                    ),
//                  ),
//                ),
//              ),
//            );
//          },
//        ),
//      ),
//    );
//  }
//}
