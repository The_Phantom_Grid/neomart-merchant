import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/model.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:merchant/CreateStore.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/SearchLocation.dart';

import 'Components/ImagePick.dart';
import 'constants.dart';

class CreateProfile extends StatefulWidget {
  String phone, userid, token;

  CreateProfile(this.phone, this.userid, this.token);

  @override
  _CreateProfileState createState() => _CreateProfileState();
}

class _CreateProfileState extends State<CreateProfile> {
  File image;
  String profileImage, fullName, eMail, locality;
  var nameController = TextEditingController();
  var localityController = TextEditingController();
  var numberController = TextEditingController();
  String nameError,
      emailError,
      addressError,
      numberError,
      base64ImageString,
      merchantid;
  var status;
  bool loading = false;
  HttpRequests requests = HttpRequests();
  var ratio = [
    CropAspectRatioPreset.square,
    CropAspectRatioPreset.ratio3x2,
    CropAspectRatioPreset.original,
    CropAspectRatioPreset.ratio4x3,
    CropAspectRatioPreset.ratio16x9
  ];

  List<Address> address;

  validateInfo() {
    setState(() {
      print("Validating...");
      if (nameController.text.isEmpty)
        nameError = "Name field cannot be empty!";
      else if (numberController.text.isEmpty)
        numberError = "Invalid phone number";
      else if (localityController.text.isEmpty)
        addressError = "Enter your locality";
      else if (image == null) {
        Fluttertoast.showToast(
            msg: "Select Profile Image",
            toastLength: Toast.LENGTH_LONG,
            timeInSecForIosWeb: 2);
      } else
        updateProfile();
    });
  }

  openBottomSheet() {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (context) {
          return Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      image = await getImage(ImageSource.camera, ratio);
                      if (image != null) {
                        base64ImageString = convertImageToBase64(image);
                      }
                      setState(() {});
                    },
                    leading: Icon(Icons.camera_alt),
                    title: Text("Camera"),
                  ),
                  ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      image = await getImage(ImageSource.gallery, ratio);
                      if (image != null) {
                        base64ImageString = convertImageToBase64(image);
                      }
                      setState(() {});
                    },
                    leading: Icon(Icons.photo_library),
                    title: Text("Gallery"),
                  ),
                ],
              ));
        });
  }

  updateProfile() async {
    var res = await requests.updateProfile(
        localityController.text.toString(),
        address.first.coordinates.latitude.toString(),
        address.first.coordinates.longitude.toString(),
        localityController.text.toString(),
        nameController.text.toString(),
        widget.phone,
        base64ImageString,
        widget.userid
    );
    if (res != null) {
      print("DONE");
      merchantid = res['merchant_id'].toString();
      if (res['status'].toString().toLowerCase() == "success") {
        Get.off(CreateStore(merchantid, widget.token));
      }
    }
  }

  updateDetail(){
    setState(() {
      localityController.text = "${address.first.featureName}, ${address.first.locality}";
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    numberController.text = widget.phone;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text(
            "Create Profile",
          ),
          centerTitle: true,
//          actions: <Widget>[
//            Padding(
//              padding: const EdgeInsets.symmetric(horizontal: 16),
//              child: Row(
//                children: <Widget>[
//                  Icon(Icons.check, color: Colors.white),
//                  Text("Save")
//                ],
//              ),
//            )
//          ],
        ),
        body: SingleChildScrollView(
          child: Center(
              child: Container(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Column(
                          children: <Widget>[
                            Stack(
                              children: <Widget>[
                                ClipOval(
                                  clipBehavior: Clip.antiAlias,
                                  child: Container(
                                    width: 200,
                                    height: 200,
                                    child: Hero(
                                        tag: 'neomart_logo',
                                        child: image != null
                                            ? Image.file(
                                          image,
                                          fit: BoxFit.cover,
                                        )
                                            : Icon(
                                          Icons.account_circle,
                                          size: 200,
                                          color: Colors.grey,
                                        )),
                                  ),
                                ),
                                Positioned(
                                  bottom: 10,
                                  right: 20,
                                  child: GestureDetector(
                                    onTap: openBottomSheet,
                                    child: CircleAvatar(
                                      radius: 25,
                                      child: Icon(Icons.camera_alt),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            customTextField(
                                nameController, (val) {
                              setState(() {
                                nameError = null;
                              });
                            }, () {},
                                nameError,
                                textBoxFillColor,
                                "Full Name",
                                ""),
//                            TextFormField(
//                              controller: nameController,
////                                    "${profile.prifiledata[0].firstname}",
//                              enabled: true,
//                              decoration: InputDecoration(
//                                errorText: nameError,
//                                isDense: true,
//                                hasFloatingPlaceholder: true,
//                                border: OutlineInputBorder(
//                                    borderSide:
//                                    BorderSide(color: mColor, width: 5.0)),
//                                labelText: "Full Name",
//                              ),
//                            ),
                            SizedBox(
                              height: 25,
                            ),
                            TextField(
                              keyboardType: TextInputType.numberWithOptions(),
                              controller: numberController,
                              enabled: false,
                              scrollPadding: EdgeInsets.all(8.0),
                              decoration: InputDecoration(
                                errorText: numberError,
                                contentPadding:
                                EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 16),
                                labelStyle: TextStyle(
                                    color: Colors.black.withOpacity(.4)),
                                filled: true,
                                labelText: "Permanent Phone Number",
                                fillColor: textBoxFillColor,
                                focusColor: Colors.white38,
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                    borderRadius: BorderRadius.circular(4)),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                    borderRadius: BorderRadius.circular(4)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                    borderRadius: BorderRadius.circular(4)),
                                counterText: "",
                              ),
                            ),
//                            TextFormField(
////                                initialValue: "${profile.prifiledata[0].email}",
//                              enabled: true,
//                              controller: numberController,
//                              decoration: InputDecoration(
//                                errorText: numberError,
//                                isDense: true,
//                                hasFloatingPlaceholder: true,
//                                border: OutlineInputBorder(
//                                    borderSide:
//                                    BorderSide(color: mColor, width: 5.0)),
//                                labelText: "Permanent Phone Number",
//                              ),
//                            ),
                            SizedBox(
                              height: 25,
                            ),
                            TextField(
                              controller: localityController,
                              scrollPadding: EdgeInsets.all(8.0),
                              onChanged: (val) {
                                setState(() {
                                  addressError = null;
                                });
                              },
                              decoration: InputDecoration(
                                suffixIcon: IconButton(
                                  onPressed: () async {
                                    address = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => SearchLocation()));
                                    if (address != null) updateDetail();
                                  },
                                  icon: Icon(Icons.my_location), color: Colors.black,),
                                errorText: addressError,
                                contentPadding:
                                EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 16),
                                labelStyle: TextStyle(
                                    color: Colors.black.withOpacity(.4)),
                                filled: true,
                                labelText: "City",
                                fillColor: textBoxFillColor,
                                focusColor: Colors.white38,
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                    borderRadius: BorderRadius.circular(4)),
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                    borderRadius: BorderRadius.circular(4)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.white),
                                    borderRadius: BorderRadius.circular(4)),
                                counterText: "",
                              ),
                            ),
//                            TextFormField(
//                              controller: localityController,
////                                initialValue: "${profile.prifiledata[0].city}",
//                              enabled: true,
//                              decoration: InputDecoration(
//                                  errorText: addressError,
//                                  isDense: true,
//                                  hasFloatingPlaceholder: true,
//                                  border: OutlineInputBorder(
//                                      borderSide:
//                                      BorderSide(color: mColor, width: 5.0)),
//                                  labelText: "City",
//                                  enabled: true),
//                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: ButtonTheme(
                            minWidth: 180,
                            height: 45.0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(27)),
                            child: RaisedButton(
                              child: loading
                                  ? Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
                                    height: 15,
                                    width: 15,
                                    child: CircularProgressIndicator(
                                      strokeWidth: 2,
                                      backgroundColor: Colors.white,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Text("Updating Profile..."),
                                ],
                              )
                                  : Text("SAVE"),
                              onPressed: () {
                                validateInfo();
//                                Navigator.push(context, MaterialPageRoute(builder: (context) => CreateStore()));
                              },
                              textColor: Colors.white,
                              color: Colors.black,
                            )),
                      )
                    ],
                  ),
                ),
              )),
        ));
  }
}
