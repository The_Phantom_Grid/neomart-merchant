import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:merchant/Getter/GetSubCategory.dart';

import 'StepsCreateStore.dart';
import 'constants.dart';

class ChooseCategory extends StatefulWidget {
  @override
  _ChooseCategoryState createState() => _ChooseCategoryState();
}

class _ChooseCategoryState extends State<ChooseCategory> {

  TextEditingController searchController = TextEditingController();

  addProductPrompt() {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context){
        return AlertDialog(
          title: Text("Add a product category", style: subHeading,),
          content: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                TextFormField(
                  style: TextStyle(fontSize: 14),
                  decoration: InputDecoration(
                    hintStyle: TextStyle(fontSize: 14),
                    contentPadding: EdgeInsets.all(8),
                    focusColor: Colors.white38,
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12), borderRadius: BorderRadius.circular(4)),
                    border: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12), borderRadius: BorderRadius.circular(4)),
                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12), borderRadius: BorderRadius.circular(4)),
                    counterText: "",
                    hintText: "Category name",
                    labelStyle: TextStyle(color: Colors.grey)),
                ),
              ],
            ),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18)
          ),
          actions: <Widget>[
            ButtonTheme(
              height: 40,
              minWidth: 100,
              shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
              child: RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Row(
                  children: <Widget>[
                    Icon(Icons.add, color: Colors.white,),
                    Text("Add",
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Choose Category"),
        automaticallyImplyLeading: true,
      ),

      body: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[

            TextFormField(
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(16),
                  filled: true,
                  fillColor: textBoxFillColor,
                  focusColor: Colors.white38,
                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
                  border: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
                  counterText: "",
                  suffixIcon: Icon(Icons.search, color: Colors.black),
                  hintText: "Search Category",
                  labelStyle: TextStyle(color: Colors.grey)),
              controller: searchController,
            ),
            SizedBox(
              height: 20,
            ),
            Wrap(
              spacing: 12,
              runSpacing: 12,
              children: List.generate(1, (index) {
                return addProductContainer();
              }),
            )
          ],
        ),
      ),

      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ButtonTheme(
          height: 50,
          splashColor: Colors.white38,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: RaisedButton(
            onPressed: () {},
            child: Text(
              "Update",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }

  addProductContainer() {
    return GestureDetector(
      onTap: (){
        addProductPrompt();
      },
      child: Container(
        height: 100,
        width: 100,
        decoration: BoxDecoration(
            boxShadow: [BoxShadow(color: Colors.black12, offset: Offset(0, 0), blurRadius: 6, spreadRadius: 2
            )],
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: Colors.grey)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.add),
            SizedBox(height: 5,),
            Text("Add a product category", style: smallHeading, textAlign: TextAlign.center,)
          ],
        ),
      ),
    );
  }
}

class buildContainer extends StatefulWidget {
  Info info;

  buildContainer(this.info);

  @override
  _buildContainerState createState() => _buildContainerState();
}

class _buildContainerState extends State<buildContainer> {
  bool selected = false;

  selectedOption() {
    setState(() {
      if (selected) {
        selected = false;
        stepsCreateStoreState.selectedCat
            .removeWhere((key, value) => key == widget.info.subcategoryId);
      } else {
        selected = true;
        stepsCreateStoreState.selectedCat[widget.info.subcategoryId] =
            widget.info.subCategoryName.toString();
      }
    });
    print(stepsCreateStoreState.selectedCat
        .toString()
        .replaceAll('[', "")
        .replaceAll(']', "")
        .replaceAll(" ", ""));
//    checkSelected();
  }

  checkSelected() {
    print(widget.info.subCategoryName);
    setState(() {
      if (stepsCreateStoreState.selectedCat
          .toString()
          .replaceAll('[', "")
          .replaceAll(']', "")
          .replaceAll(" ", "")
          .contains(widget.info.subcategoryId.toString())){ selected = true;}
      else selected = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    checkSelected();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        selectedOption();
      },
      child: Container(
        height: 100,
        width: 100,
        decoration: BoxDecoration(
            boxShadow: [BoxShadow(color: Colors.black12, offset: Offset(0, 0), blurRadius: 6, spreadRadius: 2
            )],
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: selected? mColor: Colors.grey)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(),
                Icon(Icons.fiber_manual_record,
                  color: selected ? mColor : Colors.grey, size: 15,),
              ],
            ),
            Image.network(widget.info.categoryImage.toString(), height: 34,),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: Text(
                widget.info.subCategoryName.toString(), style: smallHeading,
                textAlign: TextAlign.center,),
            ),

          ],
        ),
      ),
    );
  }
}

