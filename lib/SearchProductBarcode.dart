import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:merchant/Getter/GetBarcodeScanResult.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/constants.dart';

class SearchProductBarcode extends StatefulWidget {
  String barcode;

  SearchProductBarcode(this.barcode);

  @override
  _SearchProductBarcodeState createState() => _SearchProductBarcodeState();
}

class _SearchProductBarcodeState extends State<SearchProductBarcode> {
  HttpRequests requests = HttpRequests();
  bool loading = true, found = false;
  BarcodeScanResult barcodeScanResult;

  scanFromBarcode() async {
    var res = await requests.searchProductFromBarcode(widget.barcode);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        barcodeScanResult = BarcodeScanResult.fromJson(res);
        loading = false;
        if (barcodeScanResult.product.isNotEmpty) found = true;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    scanFromBarcode();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Scan Result",
          style: regularText,
        ),
        centerTitle: true,
        automaticallyImplyLeading: true,
      ),
      body: loading
          ? loadingCircular()
          : Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("Scan result: $found"),
                Text("Found (${barcodeScanResult.product.length}) product"),
              ],
            ),
    );
  }
}
