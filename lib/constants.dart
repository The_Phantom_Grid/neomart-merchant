import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:merchant/Components/VariableControllers.dart';
import 'package:shared_preferences/shared_preferences.dart';

Color mColor = Color(0xFFfc0c5b);
Color darkBackground = Color(0xFF434d53);
Color textBoxFillColor = Color(0xFFEEF1F4);

TextStyle defaultTextStyle = TextStyle(fontSize: 13);
TextStyle bigTextStyle = TextStyle(fontSize: 16, fontWeight: FontWeight.bold);
TextStyle percentDiffUp = TextStyle(fontSize: 13, color: Colors.lightGreen, fontWeight: FontWeight.w600);
TextStyle percentDiffDown = TextStyle(fontSize: 13, color: Colors.redAccent, fontWeight: FontWeight.w600);
TextStyle smallHeading = TextStyle(fontSize: 11, fontWeight: FontWeight.bold);
TextStyle regularText = TextStyle(fontSize: 13);
TextStyle productName = TextStyle(fontSize: 11);
TextStyle mrpTextStyle = TextStyle(fontSize: 14, color: mColor);
TextStyle bottomAppbarText =
    TextStyle(fontSize: 9, fontWeight: FontWeight.bold);
TextStyle storeNameTextStyle = TextStyle(color: mColor);
TextStyle h1 =
    TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.black);
TextStyle h2 =
    TextStyle(fontSize: 13, fontWeight: FontWeight.bold, color: Colors.black);
TextStyle welcomeHeading = TextStyle(fontSize: 22, fontWeight: FontWeight.bold);
TextStyle subHeading = TextStyle(fontSize: 13, fontWeight: FontWeight.bold);

List<WhitelistingTextInputFormatter> onlyNumberInputFormatter = [
  WhitelistingTextInputFormatter(RegExp("[0-9]"))
];

var amountInputFormatter =
    WhitelistingTextInputFormatter(RegExp(r'^\d+\.?\d*'));

Widget loadingCircular() {
  return Center(
    child: CircularProgressIndicator(),
  );
}

Widget customTextField(
    @required TextEditingController controller,
    Function onChange,
    Function onTap,
    @required String errorText,
    @required Color fillColor,
    @required labelText,
    @required String hintText) {
  return TextField(
    controller: controller,
    scrollPadding: EdgeInsets.all(8.0),
    onChanged: onChange,
    onTap: onTap,
    decoration: InputDecoration(
      errorText: errorText,
      contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
//      suffixIcon: Icon(Icons.remove_red_eye,
//          color: Colors.black.withOpacity(.4)),
//      prefixIcon: Icon(Icons.vpn_key, color: Colors.black),
      labelStyle: TextStyle(color: Colors.black.withOpacity(.4)),
      filled: true,
      labelText: labelText,
      fillColor: fillColor,
      focusColor: Colors.white38,
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black),
          borderRadius: BorderRadius.circular(4)),
      border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
          borderRadius: BorderRadius.circular(4)),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
          borderRadius: BorderRadius.circular(4)),
      counterText: "",
      hintText: hintText,
    ),
  );
}

String storeid,
    merchantid,
    token,
    userid,
    storeName,
    mobile,
    password,
    storeLocation,
    storeLat,
    storeLng;

clearData() async {
  storeid = null;
  merchantid = null;
  storeLocation = null;
  storeLat = null;
  storeLng = null;
  token = null;
  userid = null;
  storeName = null;
  userProfile = null;
  addProdSubCats = null;
  addProdCats = null;
  mobile = null;
  password = null;
  storeDetails = null;
  detailsUpdated = true;
}

clearSharedPreference() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.setString('showIntro', "NO");
  preferences.setString('mobile', null);
  preferences.setString('password', null);
}

setSharedPreference() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.setString('showIntro', "NO");
  preferences.setString('mobile', mobile);
  preferences.setString('password', password);
}

showToast(String msg, Color color) {
  Fluttertoast.showToast(
    msg: msg,
    timeInSecForIosWeb: 2,
    toastLength: Toast.LENGTH_LONG,
    backgroundColor: color,
    textColor: Colors.white,
    fontSize: 13,
  );
}

//Widget myButton(@required double minHeight, @required double minWidth, @required double radius, @required Function onPressed, @required Widget child){
//  return ButtonTheme(
//    height: minHeight,
//    minWidth: minWidth,
//    shape: RoundedRectangleBorder(
//      borderRadius: BorderRadius.circular(radius),
//    ),
//    child: RaisedButton(
//      elevation: 0,
//      textColor: Colors.white,
//      onPressed: onPressed,
//      child: child,
//    ),
//  );
//}