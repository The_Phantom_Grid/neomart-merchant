import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:merchant/Getter/GetNotifications.dart';
import 'package:merchant/Getter/GetRevenue.dart';
import 'package:merchant/Getter/GetStock.dart';
import 'package:merchant/Getter/GetTotalOrders.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/Orders.dart';

import 'NotificationCenter.dart';
import 'StepsCreateStore.dart';
import 'constants.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  HttpRequests requests = HttpRequests();
  Revenue revenue;
  StoreOrders storeOrders;
  Stock stock;
  UserNotifications notifications;
  bool noNotification = false,
      loadRevenue = true,
      loadStock = true,
      loadOrders = true,
      loadNotification = true;

  getNotifications() async {
    var res = await requests.getNotifications();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        notifications = UserNotifications.fromJson(res);
        loadNotification = false;
        if (notifications.data.isEmpty)
          noNotification = true;
        else
          noNotification = false;
      });
    } else {
      setState(() {
        loadNotification = false;
      });
    }
    print(loadNotification);
  }

  getOrders() async {
    var res = await requests.getOrders();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        storeOrders = StoreOrders.fromJson(res);
        loadOrders = false;
      });
    } else {
      setState(() {
        loadOrders = false;
      });
    }
  }

  getRevenue() async {
    var res = await requests.getRevenue();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        revenue = Revenue.fromJson(res);
        loadRevenue = false;
      });
    } else {
      setState(() {
        loadRevenue = false;
      });
    }
  }

  getStock() async {
    var res = await requests.getStock();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        stock = Stock.fromJson(res);
        loadStock = false;
      });
    } else {
      setState(() {
        loadStock = false;
      });
    }
  }

  Widget loadingWidget() {
    return SpinKitThreeBounce(
      size: 20,
      color: Colors.black,
    );
  }

//  getReviews() async {
//    var res = await requests.getOrders();
//    if(res != null && res['status'].toString().toLowerCase() == "success"){
//
//    }
//  }

  initCalls() {
    getOrders();
    getNotifications();
    getRevenue();
    getStock();
//    getReviews();
//    getTopSellings();
  }

  @override
  void initState() {
    // TODO: implement initState
    initCalls();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: textBoxFillColor,
//      appBar: AppBar(
//        centerTitle: true,
//        title: Column(
//          children: <Widget>[
//            Text("My Store"),
//            Text("Chetna Store", style: storeNameTextStyle,),
//          ],
//        ),
//      ),

      body: Padding(
        padding: EdgeInsets.only(left: 8.0, right: 8.0, bottom: 60.0, top: 4.0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Image.asset(
                'assets/clip_group.png',
                height: 122,
                width: double.infinity,
                fit: BoxFit.fitWidth,
              ),
              SizedBox(height: 12,),
              Container(
                height: 130,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(6)
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(4.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("Total Revenue", style: smallHeading,),
                              Text("Today", style: smallHeading,),
                              Image.asset(
                                'assets/revenue.png',
                                height: 30,
                              ),
                              loadRevenue ? loadingWidget() : Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  AutoSizeText("₹ ${revenue.data.totalRevenue}",
                                    style: bigTextStyle,
                                    minFontSize: 9,
                                    maxFontSize: 16,),
                                  Row(
                                    children: <Widget>[
                                      Transform.rotate(
                                          angle: 3.14 / 2 * .4,
                                          child: Icon(
                                            FlutterIcons.arrow_circle_o_up_faw,
                                            size: 18,
                                            color: Colors.lightGreen,)),
                                      SizedBox(width: 5,),
                                      Text("+33%", style: percentDiffUp,)
                                    ],
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 8,),
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(6)
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(4.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("Total Orders", style: smallHeading,),
                              Text("Today", style: smallHeading,),
                              Image.asset(
                                'assets/orders.png',
                                height: 30,
                              ),
                              loadOrders ? loadingWidget() : Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  AutoSizeText(storeOrders.data.totalOrders,
                                      style: bigTextStyle,
                                      minFontSize: 9,
                                      maxFontSize: 16),
                                  Row(
                                    children: <Widget>[
                                      Transform.rotate(
                                          angle: -3.14 / 2 * .4,
                                          child: Icon(FlutterIcons
                                              .arrow_circle_o_down_faw,
                                            size: 18,
                                            color: Colors.redAccent,)),
                                      SizedBox(width: 5,),
                                      Text("-33%", style: percentDiffDown,)
                                    ],
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 8,),
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(6)
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(4.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("Products", style: smallHeading,),
                              Text("Today", style: smallHeading,),
                              Image.asset(
                                'assets/products.png',
                                height: 30,
                              ),
                              loadStock ? loadingWidget() : Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  AutoSizeText(
                                      stock.data.inStock, style: bigTextStyle,
                                      minFontSize: 9,
                                      maxFontSize: 16),
                                  AutoSizeText(
                                      "${stock.data.outOfStock} Out of stock",
                                      style: TextStyle(fontSize: 11,
                                          color: Colors.redAccent,
                                          fontWeight: FontWeight.w600),
                                      minFontSize: 9,
                                      maxFontSize: 11),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 12,),
              Container(
                height: 150,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(6)
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Store Profile", style: smallHeading,),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Text("Please complete your store profile.", style: TextStyle(fontSize: 10), textAlign: TextAlign.center,),
                                  Stack(
                                    children: <Widget>[
                                      Container(
                                        height: 57,
                                        width: 57,
                                        child: PieChart(
                                            PieChartData(
                                                borderData: FlBorderData(
                                                    show: false
                                                ),
                                                centerSpaceRadius: 20,
                                                startDegreeOffset: 320,
                                                centerSpaceColor: Colors.transparent,
                                                sections: [
                                                  PieChartSectionData(value: 50.0, color: Colors.black, showTitle: false, radius: 5),
                                                  PieChartSectionData(value: 50.0, color: Colors.grey, showTitle: false, radius: 5)
                                                ]
                                            )
                                        ),
                                      ),
                                      Positioned(
                                        top: 0,
                                        left: 0,
                                        bottom: 0,
                                        right: 0,
                                        child: Center(child: Text("50%", style: smallHeading,)),
                                      )
                                    ],
                                  ),
                                  SizedBox(height: 15,),
                                  Container(
                                    height: 20,
                                    child: ButtonTheme(
                                      height: 5,
                                      minWidth: double.infinity,
                                      child: RaisedButton(
                                        elevation: 0.0,
                                        onPressed: (){},
                                        textColor: Colors.white,
                                        color: Colors.black,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text("Complete", style: TextStyle(fontSize: 10),),
                                            SizedBox(width: 5,),
                                            Icon(Icons.arrow_forward, color: Colors.white, size: 10)
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 8,),
                    Expanded(
                      flex: 2,
                      child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(6)
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: GestureDetector(
                              onTap: (){
//                                Get.to(StepsCreateStore(merchantid, "0R3BSl6nVNIdouGswF6FMfuDL-Slc12o"));
                                Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationCenter()));
                              },
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Text("New Messages/Notifications",
                                        style: smallHeading,),
                                      SizedBox(width: 5,),
                                      Icon(Icons.arrow_forward,
                                        color: Colors.black, size: 15,)
                                    ],
                                  ),
                                  loadNotification ? Expanded(
                                      child: SpinKitCircle(
                                        color: Colors.black,
                                        size: 25,
                                      )) : noNotification ? Expanded(
                                    child: Center(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment: MainAxisAlignment
                                            .center,
                                        crossAxisAlignment: CrossAxisAlignment
                                            .center,
                                        children: <Widget>[
                                          Icon(Icons.notification_important,
                                            size: 20,),
                                          Text("No Notifications",
                                            style: regularText,)
                                        ],
                                      ),
                                    ),
                                  ) : Expanded(
                                    child: ListView.separated(
                                        shrinkWrap: true,
                                        physics: ScrollPhysics(),
                                        separatorBuilder: (context, index) {
                                          return Divider(height: 0,);
                                        },
                                        itemBuilder: (context, index) {
                                          return notificationCard(notifications.data[index]);
                                        },
                                        itemCount: notifications.data.length
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 12,),
              Container(
                height: 150,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(6)
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Text("Order Reviews", style: smallHeading,),
                                    SizedBox(width: 5,),
                                    Icon(Icons.arrow_forward, color: Colors.black, size: 15,)
                                  ],
                                ),
                                Text("Today", style: smallHeading,),
                                Expanded(
                                  child: ListView.separated(
                                      shrinkWrap: true,
                                      physics: ScrollPhysics(),
                                      separatorBuilder: (context, index){
                                        return Divider(height: 0,);
                                      },
                                      itemBuilder: (context, index){
                                        return Container(
                                          padding: EdgeInsets.symmetric(vertical: 4),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              Container(
                                                decoration: BoxDecoration(
                                                    color: Colors.redAccent,
                                                    shape: BoxShape.circle
                                                ),
                                                child: Center(child: Text("R", style: TextStyle(color: Colors.white),)),
                                                height: 30,
                                                width: 30,
                                              ),
                                              Expanded(
                                                flex: 4,
                                                child: Padding(
                                                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                      Text("Rahul Chauhan", style: TextStyle(fontSize: 10, fontWeight: FontWeight.w600),),
                                                      Text("H.No. 27, Chhatarpur extension", style: TextStyle(fontSize: 10, fontWeight: FontWeight.w500),),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Text("₹ 1200", style: smallHeading,),
                                              )
                                            ],
                                          ),
                                        );
                                      },
                                      itemCount: 3
                                  ),
                                )
                              ],
                            ),
                          )
                      ),
                    ),
                    SizedBox(width: 8,),
                    Expanded(
                      flex: 1,
                      child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(6)
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Flexible(child: Text("Top Selling", style: smallHeading,)),
                                    SizedBox(width: 5,),
                                    Icon(Icons.arrow_forward, color: Colors.black, size: 15,)
                                  ],
                                ),
                                Expanded(
                                  child: ListView.separated(
                                      shrinkWrap: true,
                                      physics: ScrollPhysics(),
                                      separatorBuilder: (context, index){
                                        return Divider(height: 0,);
                                      },
                                      itemBuilder: (context, index){
                                        return Container(
                                          padding: EdgeInsets.symmetric(vertical: 4),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Image.asset(
                                                'assets/tsp.png',
                                                height: 20,
                                                width: 20,
                                              ),
                                              Expanded(
                                                flex: 4,
                                                child: Padding(
                                                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                                                  child: Text("Nescafe Cold Cappuccino", style: TextStyle(fontSize: 8, fontWeight: FontWeight.w600),),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child: Container(
                                                    decoration: BoxDecoration(
                                                        color: Colors.green,
                                                        borderRadius: BorderRadius.circular(10)
                                                    ),
                                                    child: Center(child: Padding(
                                                      padding: const EdgeInsets.symmetric(horizontal: 2.0),
                                                      child: Text("15%", style: TextStyle(fontSize: 7, color: Colors.white),),
                                                    ))),
                                              )
                                            ],
                                          ),
                                        );
                                      },
                                      itemCount: 3
                                  ),
                                )
                              ],
                            ),
                          )
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),

//      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
//      floatingActionButton: FloatingActionButton(
//        onPressed: (){
//          Navigator.push(context, MaterialPageRoute(builder: (context) => Orders()));
//          },
//        backgroundColor: Colors.black,
//        child: Image.asset('assets/icons/myorders.png', height: 20,),
//      ),

//      bottomNavigationBar: BottomAppBar(
//        child: Container(
//          color: Colors.white,
////          padding: EdgeInsets.symmetric(horizontal: ),
//          height: 56,
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[
//              Expanded(
//                child: Container(
//                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceAround,
//                    children: <Widget>[
//                      Column(
//                        mainAxisAlignment: MainAxisAlignment.end,
//                        children: <Widget>[
//                          Image.asset('assets/icons/mystore.png', height: 18,),
//                          SizedBox(height: 5,),
//                          Text("MY STORE", style: bottomAppbarText,),
//                        ],
//                      ),
//                      Column(
//                        mainAxisAlignment: MainAxisAlignment.end,
//                        children: <Widget>[
//                          Image.asset('assets/icons/POS.png', height: 18,),
//                          SizedBox(height: 5,),
//                          Text("POS", style: bottomAppbarText,),
//                        ],
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//              Padding(
//                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
//                child: Column(
//                  mainAxisAlignment: MainAxisAlignment.end,
//                  children: <Widget>[
//                    SizedBox(height: 16,),
//                    Text("ORDERS", style: bottomAppbarText,),
//                  ],
//                ),
//              ),
//              Expanded(
//                child: Container(
//                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceAround,
//                    children: <Widget>[
//                      Column(
//                        mainAxisAlignment: MainAxisAlignment.end,
//                        children: <Widget>[
//                          Image.asset('assets/icons/collection.png', height: 18,),
//                          SizedBox(height: 5,),
//                          Text("ADD PRODUCT", style: bottomAppbarText,),
//                        ],
//                      ),
//                      Column(
//                        mainAxisAlignment: MainAxisAlignment.end,
//                        children: <Widget>[
//                          Image.asset('assets/icons/settings.png', height: 18,),
//                          SizedBox(height: 5,),
//                          Text("SETTINGS", style: bottomAppbarText,),
//                        ],
//                      ),
//                    ],
//                  ),
//                ),
//              ),
//            ],
//          ),
//        ),
//      ),
    );
  }
}

class notificationCard extends StatelessWidget {
  Datum data;
  notificationCard(this.data, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: Colors.redAccent,
                shape: BoxShape.circle
            ),
            child: Center(
                child: Text(data.firstname[0], style: TextStyle(color: Colors.white),)),
            height: 30,
            width: 30,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(data.firstname, style: TextStyle(
                      fontSize: 10, fontWeight: FontWeight.w600),),
                  Text(data.messageContent,
                    style: TextStyle(
                        fontSize: 10, fontWeight: FontWeight.w500),),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
