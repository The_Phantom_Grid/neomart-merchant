import 'dart:convert';
import 'dart:io';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:dio/dio.dart';

import 'package:http/http.dart' as http;
import 'package:merchant/Components/VariableControllers.dart';
import 'package:merchant/LoginScreenComponents/Login.dart';
import 'package:merchant/constants.dart';
import 'package:flutter/material.dart';

class HttpRequests {
  loginExpired() {
    Get.offAll(Login());
    Get.snackbar(
      "SESSION EXPIRED",
      "Token expired, Login again.",
      isDismissible: true,
      duration: Duration(seconds: 2),
      snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.white54);
  }

  showError(String message) {
    print("ERROR");
    Get.snackbar(
      "ERROR",
        message,
        isDismissible: true,
        duration: Duration(seconds: 2),
        snackPosition: SnackPosition.TOP,
        backgroundColor: Colors.white54
    );
  }


  var dio = Dio();
  VarController STATUS = Get.put(VarController());

  switchServer() {
    if (STATUS.devServer.value)
      STATUS.devServer.value = false;
    else
      STATUS.devServer.value = true;

    if (STATUS.devServer.value) {
      STATUS.baseUrl.value = "https://www.neomart.com/rest/web/index.php/v2";
    } else
      STATUS.baseUrl.value = "https://www.neomart.com/rest/web/index.php/v2";
    Phoenix.rebirth(Get.context);
    print("SERVER SWITCHED TO ${STATUS.baseUrl.value}");
    Fluttertoast.showToast(msg: "SERVER SWITCHED TO ${STATUS.devServer.value
        ? "DEVELOPMENT"
        : "PRODUCTION"}");
  }

//  String ${status.baseUrl.value} = "http://staging.neomart.com/rest/web/index.php/v2";

  Future getReferral(String number) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/get_referral_code_from_number",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
        }),
        data: jsonEncode({"number": number}),
      );

      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch(e) {
      if(e is DioError){
        showError(e.error.toString());
      }else{
        showError(e.error.toString());
      }
    }
  }

//    print("API HIT");
//    String url = "${status.baseUrl.value}/get_referral_code_from_number";
//    http.Response response = await http.post(
//      url,
//      headers: {
//        "Accept": "application/json",
//        "Content-Type": "application/json"
//      },
//      body: json.encode({"number": number}),
//      encoding: Encoding.getByName("utf-8"),
//    );
//    print(response.body);
//    print(response.statusCode);
//    if(response.statusCode == 200){
//      var jData = json.decode(response.body);
//      return jData;
//    }else{
//      print(response.headers);
//      showError("");
//      return null;
//    }
//  }

  Future registerPhone(String number) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/register/userphone",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
        }),
        data: jsonEncode({"mobile": number}),
      );

      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future login(String number, String pass) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/register/loginwith_phone",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
        }),
        data: jsonEncode({"number": number, "password": pass}),
      );

      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future checkUser(String number) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/check_user_exists",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
        }),
        data: jsonEncode({"mobile": number}),
      );

      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future verifyOtp(String number, String otp) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/register/verifyotp",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
        }),
        data: jsonEncode({"mobile": number, "otp": otp}),
      );

      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getToken(String deviceId, String fcmToken, String deviceType,
      String userid) async {
    print("API HIT getToken");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/push/gettoken",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
        }),
        data: jsonEncode({
          "info": {
            "deveiceUniqId": deviceId,
            "deviceTokenId": fcmToken,
            "deviceType": "1",
            "user_type": "merchant"
          },
          "userid": userid
        }),
      );

      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future sendComm(String userid) async {
    print("API HIT send comm");
    try {
      Response response = await dio.post("${STATUS.baseUrl.value}/send_comm",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
        }),
        data: jsonEncode({"userid": userid}),
      );

      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future updateProfile(String city, String lat, String lng, String location,
      String name, String mobile, String image, String userid) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/register/updatemerchantdetails",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
        }),
        data: jsonEncode({
          "alt_phone_no": "",
          "city": city,
          "lat": lat,
          "location": location,
          "lng": lng,
          "merchantname": name,
          "mobile": mobile,
          "storeImage": "",
          "ownerImage": "",
          "profile_pic": image,
          "userid": userid
        }),
      );

      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future setNewPass(String pass, String userid, String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/register/set_password",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"password": pass, "userid": userid}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getStoreTypes(String token) async {
    print("API HIT");
    try {
      Response response = await dio.get(
        "${STATUS.baseUrl.value}/get_store_types",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getStoreSubCat(String catId, String merchantid, String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/get_subcategory",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"category_id": catId, "merchant_id": merchantid}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getSubCategory(String catId, String merchantid, String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/getsubcategory",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode(
            {"categoryid": catId, "merchant_id": merchantid, "store_id": ""}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getMyCategory(String catId) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/getsubcategory",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "categoryid": catId,
          "merchant_id": merchantid,
          "store_id": storeid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future saveProductCat(String storeId, String merchantid, String subCats,
      String token) async {
    print("API HIT");
    print(jsonEncode({
      "store_id": storeId,
      "merchant_id": merchantid,
      "sub_categoryies": subCats
    }));
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/save_productcat",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "store_id": storeId,
          "merchant_id": merchantid,
          "sub_categoryies": subCats
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future addMoreCat(String subCats) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/add_more_categories",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "category_id": "1",
          "store_id": storeid,
          "merchant_id": merchantid,
          "sub_categoryies": subCats
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future saveStoreAddress(String address,
      String city,
      String storeId,
      String country,
      String lat,
      String lng,
      String locality,
      String pincode,
      String state,
      String storeLocation,
      String merchantid,
      String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/save_shop_address",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "address": address,
          "city": city,
          "country": country,
          "lat": lat,
          "lng": lng,
          "locality": locality,
          "merchant_id": merchantid,
          "pincode": pincode,
          "state": state,
          "store_id": storeId,
          "store_location": storeLocation
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        print("DIO ERROR:: ${e.response.data['message']}");
        showError(e.response.data['message']);
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future updateCustomerDetails(String orderType,
      String username,
      String userId,
      String address,
      String addressId,
      String lat,
      String lng,
      String house,
      String pincode,
      String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/update_customer_details",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "address_id": addressId,
          "address_lat": lat,
          "address_lng": lng,
          "delivery_address": address,
          "flat_houseno": house,
          "merchant_id": merchantid,
          "order_type": orderType,
          "pincode": pincode,
          "store_id": storeid,
          "userid": userId,
          "username": username
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        print("DIO ERROR:: ${e.response.data['message']}");
        showError(e.response.data['message']);
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future updateCustomerDetailsPickup(String userid, String deliveryAddress,
      String username, String orderType) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/update_customer_details",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "address_id": "",
          "address_lat": "",
          "address_lng": "",
          "delivery_address": deliveryAddress,
          "merchant_id": merchantid,
          "order_type": orderType,
          "store_id": storeid,
          "userid": userid,
          "username": username
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        print("DIO ERROR:: ${e.response.data['message']}");
        showError(e.response.data['message']);
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future saveShippingSettings(String deliveryCharge,
      String deliveryCloseTime,
      String deliveryRadius,
      String deliveryStartTime,
      String merchantId,
      String minDeliveryTime,
      String minOrder,
      String storeCloseTime,
      String storeClosingDays,
      String storeId,
      String storeOpenTime,
      String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/save_shipping_settings",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "delivery_charge": deliveryCharge,
          "delivery_close_time": deliveryCloseTime,
          "delivery_radius": deliveryRadius,
          "delivery_start_time": deliveryStartTime,
          "merchant_id": merchantId,
          "min_delivery_time": minDeliveryTime,
          "min_order": minOrder,
          "store_close_time": storeCloseTime,
          "store_closing_days": storeClosingDays,
          "store_id": storeId,
          "store_open_time": storeOpenTime
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        print("DIO ERROR:: ${e.response.data['message']}");
        showError(e.response.data['message']);
      } else {
        showError(e.error.toString());
      }
    }
  }


  Future addStoreSubCat(String subCatId, String merchantid, String subCategory,
      String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/add_subcategory",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "category_id": subCatId,
          "merchant_id": merchantid,
          "sub_category": subCategory
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getAllMerchantStores(String merchantid, String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/get_all_merchant_stores",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"merchant_id": merchantid,}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future addNewCat(String categoryName, String merchantid, String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/add_category",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"category": categoryName, "merchant_id": merchantid}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }


  Future saveStoreInfo(String aadharCard,
      String aadharCardImage,
      String categoryid,
      String description,
      String documentsAvail,
      String fssai,
      String gstImage,
      String gstNo,
      String logo,
      String merchantId,
      String panCard,
      String pancardImage,
      String shopName,
      String storeId,
      String storeImageAvail,
      String storeNumber,
      String storeType,
      String storeSubCategory,
      String subCatId,
      String merchantType,
      String storeImage1,
      String storeImage2,
      String storeImage3,
      String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/setup_store",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "aadhar_card": aadharCard,
          "aadhar_card_image": aadharCardImage,
          "categoryid": categoryid,
          "delivery_time": "",
          "description": description,
          "documents_avail": documentsAvail,
          "fssai": fssai,
          "gst_image": gstImage,
          "gst_no": gstNo,
          "logo": logo,
          "merchant_id": merchantId,
          "pan_card": panCard,
          "pancard_image": pancardImage,
          "shop_name": shopName,
          "store_id": storeId,
          "store_image_avail": storeImageAvail,
          "store_number": storeNumber,
          "store_type": storeType,
          "storeimage1_front": storeImage1,
          "storeimage2_inside1": storeImage2,
          "storeimage3_inside2": storeImage3,
          "storeimage4": "",
          "storeimage5": "",
          "store_type_sub_category": storeSubCategory,
          "store_type_sub_category_id": subCatId,
          "landline": "",
          "merchant_type": merchantType
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getNotifications() async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/get_sent_notifications",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"store_id": storeid}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future searchNumber(String number, String storeid, String merchantid,
      String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/search_numbers",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "merchant_id": merchantid,
          "phoneno": number,
          "store_id": storeid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future searchCustomerNumber(String number, String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/shopping/search_customers_number",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"phoneno": number}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getUserAddress(String userid, String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/get_all_address",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"user_id": userid}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getPosCat(String userId, String merchantid, String token) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/get_filtered_data_pos_categories",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "filter": "Instock",
          "merchant_id": merchantid,
          "store_id": storeid,
          "userid": userId
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getPosProduct(String catId, String subCatid, String userId, String max,
      String min) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/get_filtered_data_pos_products",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "catid": catId,
          "subcatid": subCatid,
          "max": max,
          "min": min,
          "filter": "Instock",
          "merchant_id": merchantid,
          "store_id": storeid,
          "userid": userId
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future addToCartPos(String deliveryType, String masterProductId,
      String merchantListId, String quantity, String userId,
      String token) async {
    print("API HIT");
    try {
      Response response = await dio.post("${STATUS.baseUrl.value}/addalltocart",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "cart": [
            {
              "masterproductid": masterProductId,
              "merchantlist_id": merchantListId,
              "offer_id": 0,
              "offer_price": "",
              "qty": quantity
            }
          ],
          "delivery_type": deliveryType,
          "merchant_id": merchantid,
          "store_id": storeid,
          "userid": userId
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future posViewCart(String deliveryType, String userId, String token) async {
    print("API HIT");
    try {
      Response response = await dio.post("${STATUS.baseUrl.value}/viewcart",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "delivery_type": deliveryType,
          "merchantid": merchantid,
          "storeid": storeid,
          "userid": userId
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

//  Future updateAddressPos(String deliveryType, String userId) async {
//    print("API HIT");
//    try {
//      Response response = await dio.post("${status.baseUrl.value}/viewcart",
//        options: Options(headers: {
//          HttpHeaders.contentTypeHeader: "application/json",
//          HttpHeaders.acceptHeader: "application/json",
//          HttpHeaders.authorizationHeader: "Bearer $token"
//        }),
//        data: jsonEncode({
//          "address": address,
//          "address_id": "3924",
//          "city": "Gurgaon",
//          "colony_street": "Naurangpur - Tauru Road , Bissar Akbarpur",
//          "flat_houseno": "5x",
//          "full_name": "Ajay",
//          "landmark": "Rdfg",
//          "lat": "28.473588185509",
//          "lng": "77.055438682437",
//          "pincode": "122103",
//          "state": "Haryana",
//          "user_id": "10370",
//          "username": "Ajay"
//        }),
//      );
//      print("DIO RES: ${response.data}");
//      var jData = response.data;
//      return jData;
//    } catch(e) {
//      if(e is DioError){
//        if(e.response.statusCode == 401){
//          loginExpired();
//        } else showError(e.error.toString());
//      }else{
//        showError(e.error.toString());
//      }
//    }
//  }

  Future posCheckout(String deliveryType, String paymentMode, String mobile,
      String userId, String token) async {
    print("API HIT");
    try {
      Response response = await dio.post("${STATUS.baseUrl.value}/checkoutcart",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "deliveryaddress": "",
          "deliverydate": "",
          "deliverytype": deliveryType,
          "max_time_to_deliver": "Jun 1 2020, 10:40 AM - 11:10 AM",
          "merchant_id": merchantid,
          "payment_mode": paymentMode,
          "response": "1",
          "store_id": storeid,
          "userid": userId,
          "usermobile": mobile,
          "xn_number": ""
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getAllOffers() async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/get_all_collections_offers",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode(
            {"merchant_id": merchantid, "store_id": storeid, "message": ""}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getPosOffers() async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/get_all_collections",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"merchant_id": merchantid, "store_id": storeid, "userid": userid}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future setCollection(String name, String description, String image1,
      String image2, String image3) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/set_collection_for_merchant",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "collection_desc": description,
          "collection_image1": image1,
          "collection_image2": image2,
          "collection_image3": image3,
          "collection_image4": "",
          "collection_image5": "",
          "collection_name": name,
          "merchant_id": merchantid,
          "store_id": storeid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getSystemFilters() async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/get_system_filters",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode(
            {"merchant_id": merchantid, "store_id": storeid, "message": ""}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getSubCategories(String catId) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/get_sub_categories_products",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "category_id": catId,
          "merchant_id": merchantid,
          "store_id": storeid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getProductCategories(String filter) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/getproductcategories",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "filter": filter,
          "merchant_id": merchantid,
          "store_id": storeid,
          "message": ""
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        showError(e.error.toString());
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getFilteredProducts(String filter, String catId, String subCatId,
      String max, String min) async {
    print("API HIT FILTER DATA PRODUCTS");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/get_filtered_data_products",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "cat_id": catId,
          "filter": filter,
          "max": max,
          "merchant_id": merchantid,
          "min": min,
          "store_id": storeid,
          "subcat_id": subCatId
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getFilteredData(String filterName) async {
    print("API HIT FILTER DATA");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/get_filtered_data",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "filter": filterName,
          "merchant_id": merchantid,
          "store_id": storeid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future addProductToCollection(String collectionId, String offerQty,
      List<dynamic> productList, String offerPrice) async {
    print("API HIT");
    print(productList);
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/add_products_to_collection",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "collection_id": collectionId,
          "inventory": offerQty,
          "merchant_product_ids": productList,
          "price": offerPrice
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getOfferInfo(String collectionId) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/get_one_collection_info",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "collection_id": collectionId,
          "merchant_id": merchantid,
          "store_id": storeid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future publishCollection(String collectionId, String validFrom,
      String validTo) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/publish_collection",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "collection_id": collectionId,
          "merchant_id": merchantid,
          "valid_from": validFrom,
          "valid_to": validTo
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getProfile() async {
    print("API HIT");
    try {
      Response response = await dio.get("${STATUS.baseUrl.value}/viewprofile",
          options: Options(headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.acceptHeader: "application/json",
            HttpHeaders.authorizationHeader: "Bearer $token"
          })
//        data: jsonEncode({
//          "collection_id": collectionId,
//          "merchant_id": merchantid,
//          "valid_from": validFrom,
//          "valid_to": validTo
//        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getTnC() async {
    print("API HIT");
    try {
      Response response = await dio.post(
          "${STATUS.baseUrl.value}/register/gettermsandconditions",
          options: Options(headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.acceptHeader: "application/json",
            HttpHeaders.authorizationHeader: "Bearer $token"
          })
//        data: jsonEncode({
//          "collection_id": collectionId,
//          "merchant_id": merchantid,
//          "valid_from": validFrom,
//          "valid_to": validTo
//        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getPrivacyPolicy() async {
    print("API HIT");
    try {
      Response response = await dio.post(
          "${STATUS.baseUrl.value}/register/getprivacy_policy",
          options: Options(headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.acceptHeader: "application/json",
            HttpHeaders.authorizationHeader: "Bearer $token"
          })
//        data: jsonEncode({
//          "collection_id": collectionId,
//          "merchant_id": merchantid,
//          "valid_from": validFrom,
//          "valid_to": validTo
//        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future updateInventoryNew(String masterProductId, String merchantListId,
      String mrp, String originalMrp, String qty, int sellingStatus) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/update_inventory_new",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "master_product_id": masterProductId,
          "merchant_product_id": merchantListId,
          "mpi": originalMrp,
          "product_mrp": mrp,
          "qty": qty,
          "selling_status": sellingStatus,
          "storeid": storeid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future addProductToInventoryNew(List<dynamic> productList) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/add_product_to_inventory_new",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "product": productList
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getProductCount() async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/getproducts_count",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode(
            {"merchant_id": merchantid, "store_id": storeid, "message": ""}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getProductCatFordp() async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/getproductcategories_fordp",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode(
            {"merchant_id": merchantid, "store_id": storeid, "message": ""}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getProductSubCatFordp(String catId) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/get_sub_categories_products_dp",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode(
            {"category_id": catId, "merchant_id": "0", "store_id": "0"}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future addProductUnderOwnBrand(String barcode,
      String brand,
      String company,
      String catId,
      String description,
      String gstTax,
      String mrp,
      String productName,
      String image1,
      String image2,
      String image3,
      String qty,
      String srp,
      String subCatId,
      String taxType,
      String unit) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/add_product_under_own_brand",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "barcode_no": barcode,
          "brand_name": brand,
          "category_id": catId,
          "company_name": company,
          "description": description,
          "gst_tax": gstTax,
          "merchant_id": merchantid,
          "mrp": mrp,
          "product_name": productName,
          "productimage1": image1,
          "productimage2": image2,
          "productimage3": image3,
          "qty": qty,
          "srp": srp,
          "store_id": storeid,
          "sub_category_id": subCatId,
          "tax_type": taxType,
          "unit": unit
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getStoreDetails() async {
    print("API HIT");
    try {
      Response response = await dio.post(
          "${STATUS.baseUrl.value}/product/get_merchant_store_detail",
          options: Options(headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.acceptHeader: "application/json",
            HttpHeaders.authorizationHeader: "Bearer $token"
          }),
          data: jsonEncode(
              {"merchant_id": merchantid, "store_id": storeid, "message": ""})
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getOrderCountByStatus() async {
    print("API HIT");
    try {
      Response response = await dio.post(
          "${STATUS.baseUrl.value}/get_orders_count_by_status",
          options: Options(headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.acceptHeader: "application/json",
            HttpHeaders.authorizationHeader: "Bearer $token"
          }),
          data: jsonEncode({"merchant_id": merchantid, "store_id": storeid})
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getOrdersByStatus(String orderStatus) async {
    print("API HIT");
    try {
      Response response = await dio.post(
          "${STATUS.baseUrl.value}/customerorder/get_orders_by_status",
          options: Options(headers: {
            HttpHeaders.contentTypeHeader: "application/json",
            HttpHeaders.acceptHeader: "application/json",
            HttpHeaders.authorizationHeader: "Bearer $token"
          }),
          data: jsonEncode({
            "merchant_id": merchantid,
            "status": orderStatus,
            "store_id": storeid
          })
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getOrderSummary(String orderId) async {
    print("API HIT FILTER DATA");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/shopping/get_order_summary",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"orderid": orderId}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future confirmOrder(String orderId) async {
    print("API HIT FILTER DATA");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/shopping/confirm_order",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"orderid": orderId}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future packOrder(String orderId) async {
    print("API HIT FILTER DATA");
    try {
      Response response = await dio.post("${STATUS.baseUrl.value}/pack_order",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"comments": "Order Packed", "orderid": orderId}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future addDiscount(String cartId, String orderType, String discount,
      String discountFlag, String grandTotal, String userId) async {
    print("API HIT ");
    try {
      Response response = await dio.post("${STATUS.baseUrl.value}/add_discount",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "cart_id": cartId,
          "delivery_type": orderType,
          "discount": discount,
          "discount_flag": discountFlag,
          "discount_input": discount,
          "merchant_id": merchantid,
          "store_id": storeid,
          "total_amount": grandTotal,
          "user_id": userId
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future searchProductFromBarcode(String barcode) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/get_product_details_from_barcode",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"barcode_no": barcode, "store_id": storeid}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future updateMerchantProfile(String name, String email, String mobile,
      String lat, String lng, String city, String image) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/updateprofile_merchant",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "city": city,
          "email": email,
          "lat": lat,
          "lng": lng,
          "name": name,
          "phone_no1": mobile,
          "phone_no2": "",
          "user_profile_image": image
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future searchMasterProduct(String input) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/search_master_product",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "merchant_id": merchantid,
          "search_input": input,
          "store_id": storeid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future searchProductPos(String input) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/search_product_pos",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "merchant_id": merchantid,
          "search_input": input,
          "store_id": storeid,
          "userid": userid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future searchProductCollection(String input) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/search_product",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "merchant_id": merchantid,
          "search_input": input,
          "store_id": storeid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getAllPlans(String merchantId, String storeId) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/get_all_plans",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"merchant_id": merchantId, "store_id": storeId}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getBarcodeScanResult(String barcodeString) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/get_product_details_from_barcode",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"barcode_no": barcodeString, "store_id": storeid}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future addProductFromBarcode(String barcodeString, String qty, String srp,
      String mrp, String mProductId) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/add_barcode_product_to_inventory",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "barcode": barcodeString,
          "qty": storeid,
          "selling_price": srp,
          "product_mrp": mrp,
          "merchant_product_id": mProductId
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future updateMerchantSettings(String channel, String industryType, String mid,
      String merchantKey) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/update_merchant_settings",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "channelID": channel,
          "industry_typeID": industryType,
          "mID": mid,
          "merchant_id": merchantid,
          "merchant_key": merchantKey
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getPaytmConfig() async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/get_merchant_settings",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"merchant_id": merchantid}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future pauseOffer(String collectionId) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/un_publish_collection",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode(
            {"collection_id": collectionId, "merchant_id": merchantid}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future resumeOffer(String collectionId, String validFrom,
      String validTo) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/product/publish_collection",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "collection_id": collectionId,
          "merchant_id": merchantid,
          "valid_from": validFrom,
          "valid_to": validTo
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future removeOffer(String collectionId) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/delete_collection",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "collection_id": collectionId,
          "merchant_id": merchantid,
          "store_id": storeid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future addDeliveryBoy(String email, String gender, String image) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/add_new_delivery_boy",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "delivery_boy_email": email,
          "delivery_boy_gender": gender,
          "delivery_boy_image": image
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future orderDelivered(String orderId) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/action_for_delivered",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "comments": "Delivered",
          "merchant_id": merchantid,
          "order_id": orderId,
          "reason": "Delivered",
          "storeid": storeid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future cancelOrder(String orderId) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/shopping/cancel_order",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode(
            {"order_id": orderId, "reason": "Delivery Boy Not Available,"}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future cancelRequest(String orderId, bool request) async {
    print("API HIT");
    String status = request ? "disapprove cancelled" : "approve cancelled";
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/action_orders",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "comments": "",
          "merchant_id": merchantid,
          "order_id": orderId,
          "status": status,
          "store_id": storeid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future returnRequest(String orderId, bool request) async {
    print("API HIT");
    String status = request ? "disapprove returned" : "approve returned";
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/action_orders",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({
          "comments": "",
          "merchant_id": merchantid,
          "order_id": orderId,
          "status": status,
          "store_id": storeid
        }),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getRevenue() async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/revenue",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"merchant_id": merchantid, "store_id": storeid,"duration":1}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }
  Future getInvoice(String orderId) async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/get_invoice",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"order_id": orderId}),
      );
      print("DIO RES TOKEN: $token");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else {
          showError(e.error.toString());
        }
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getOrders() async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/orders",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"merchant_id": merchantid, "store_id": storeid,"duration":7}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getStock() async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/products",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"merchant_id": merchantid, "store_id": storeid}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getReviews() async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/reviews",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"merchant_id": merchantid, "store_id": storeid}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }

  Future getReferralInfo() async {
    print("API HIT");
    try {
      Response response = await dio.post(
        "${STATUS.baseUrl.value}/get_referral_code",
        options: Options(headers: {
          HttpHeaders.contentTypeHeader: "application/json",
          HttpHeaders.acceptHeader: "application/json",
          HttpHeaders.authorizationHeader: "Bearer $token"
        }),
        data: jsonEncode({"merchant_id": merchantid}),
      );
      print("DIO RES: ${response.data}");
      var jData = response.data;
      return jData;
    } catch (e) {
      if (e is DioError) {
        if (e.response.statusCode == 401) {
          loginExpired();
        } else
          showError(e.error.toString());
      } else {
        showError(e.error.toString());
      }
    }
  }
}