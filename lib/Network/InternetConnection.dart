import 'dart:io';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../NoInternet.dart';

class CheckInternet {
  BuildContext context;

  CheckInternet(this.context);

  bool snackBar = false;

  checkInternet() async {
    DataConnectionChecker().onStatusChange.listen(onStatusChange);

    return await DataConnectionChecker().connectionStatus;
  }

  void onStatusChange(DataConnectionStatus status) {
    print(status.toString());
    switch (status) {
      case DataConnectionStatus.connected:
        print('Data connection is available.');
        if (Get.currentRoute.toString().contains("/NoInternet")) {
          Get.back();
          Get.snackbar("Online", "We are back online.",
              backgroundColor: Colors.green,
              colorText: Colors.white,
              icon: Icon(Icons.signal_wifi_4_bar, color: Colors.white),
              isDismissible: true,
              duration: Duration(seconds: 2),
              snackPosition: SnackPosition.TOP,
              snackbarStatus: (SnackbarStatus status) {
            print(status);
          });
//          snackBar = false;
        }
        break;
      case DataConnectionStatus.disconnected:
        print('You are disconnected from the internet.');
        Get.to(NoInternet());
        print("Route:: ${Get.currentRoute.toString()}");
        print("Prev Route:: ${Get.previousRoute.toString()}");
        break;
    }
  }
}
