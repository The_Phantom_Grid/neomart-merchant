import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:merchant/constants.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:search_map_place/search_map_place.dart';

import 'EditAddress.dart';

class SearchLocation extends StatefulWidget {
  @override
  _SearchLocationState createState() => _SearchLocationState();
}

class _SearchLocationState extends State<SearchLocation> {
  String androidApiKey = "AIzaSyCZ6Up6jrsmGiavYalS7ORRxKBdwuIH-as",
      iosApiKey = "AIzaSyCZ6Up6jrsmGiavYalS7ORRxKBdwuIH-as";
  double lat, lng;
  PermissionStatus permissionStatus;
  bool _loading = true, locating = false, accessDenied = false;
  LatLng cameraTarget, initialPosition;
  GoogleMapController gController;
  String placeInfo;
  List<Address> addresses;
  var marker = [].toSet();
  int times = 0;

  onCameraMove(CameraPosition position) {
    cameraTarget = position.target;
  }

  getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      cameraTarget = LatLng(position.latitude, position.longitude);
      initialPosition = cameraTarget;
      getFullAddress();
    }).catchError((e) {
      if (e.toString().contains("PERMISSION_DENIED")) {
//        showUpdateLocation();
        setState(() {
          _loading = false;
          locating = false;
          accessDenied = true;
        });
      }
    });
  }

  getFullAddress() async {
    final coordinates =
        Coordinates(cameraTarget.latitude, cameraTarget.longitude);
    addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    setState(() {
      placeInfo = addresses.first.addressLine;
      marker = [
        Marker(
            markerId: MarkerId('cur_loc'),
            infoWindow: InfoWindow(title: placeInfo),
            position: cameraTarget)
      ].toSet();
      _loading = false;
      locating = false;
      showUpdateLocation();
      print("LOADING: $_loading");
    });
    if (gController != null)
      gController.animateCamera(CameraUpdate.newLatLng(cameraTarget));
  }

  getPermission() async {
    PermissionStatus status = await Permission.location.status;
    print("PERMISSIONS STATUS: $status");
    if (status.isGranted)
      getCurrentLocation();
    else if (status.isDenied) {
      setState(() {
        accessDenied = true;
        _loading = false;
      });
      await Permission.location.request();
//      Permission.locationAlways.request();
//      await Permission.locationWhenInUse.request();
    } else if (status.isPermanentlyDenied) {
      setState(() {
        accessDenied = true;
        _loading = false;
      });
      await Permission.location.request();
    } else if (status.isRestricted) {
      setState(() {
        accessDenied = true;
        _loading = false;
      });
      await Permission.location.request();
    } else if (status.isUndetermined) {
      setState(() {
        accessDenied = true;
        _loading = false;
      });
      await Permission.location.request();
//      showLocationAccessModal();
    }
//    else if(status.isDenied){
//      getPermission();
//    }
  }

  setUserLocation() {
    Navigator.pop(context, addresses);
  }

  showUpdateLocation() {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        builder: (context) {
          return Container(
              padding: EdgeInsets.all(15),
//              height: MediaQuery
//                  .of(context)
//                  .size
//                  .height * .32,
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Update Location",
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 15,
                  ),
                  Text("Do you wish change your location to $placeInfo."),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: OutlineButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text("Cancel"),
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        flex: 1,
                        child: FlatButton(
                          onPressed: () {
                            setState(() {
                              locating = true;
                            });
//                          getFullAddress();
                            Navigator.pop(context);
                            setUserLocation();
                          },
                          child: Text("Update"),
                          textColor: Colors.white,
                          color: Colors.black,
                        ),
                      )
                    ],
                  )
                ],
              ));
        });
  }

//  showLocationAccessModal() {
//    showModalBottomSheet(
//        context: context,
//        isDismissible: false,
//        isScrollControlled: true,
//        builder: (context) {
//          return Container(
//              padding: EdgeInsets.all(15),
////              height: MediaQuery
////                  .of(context)
////                  .size
////                  .height * .32,
//              width: MediaQuery.of(context).size.width,
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                mainAxisSize: MainAxisSize.min,
//                children: <Widget>[
//                  Text("Cant't Access your location",
//                      style: bigTextStyle),
//                  SizedBox(
//                    height: 15,
//                  ),
//                  Text("In order to access your location you must provide location permission to application.", style: regularText,),
//                  Text("Goto Setting > NeoMart Seller > Location", style: subHeading,),
//                  SizedBox(
//                    height: 15,
//                  ),
//                  Row(
//                    children: <Widget>[
//                      Expanded(
//                        flex: 1,
//                        child: OutlineButton(
//                          onPressed: () {
//                            Get.off(EditAddress());
////                          Navigator.pop(context);
//                          },
//                          child: Text("Exit"),
//                        ),
//                      ),
//                      SizedBox(
//                        width: 15,
//                      ),
//                      Expanded(
//                        flex: 1,
//                        child: FlatButton(
//                          onPressed: () async {
//                            Navigator.pop(context);
//                            openAppSettings();
//                          },
//                          child: Text("Open Settings"),
//                          textColor: Colors.white,
//                          color: mColor,
//                        ),
//                      )
//                    ],
//                  )
//                ],
//              )
//          );
//        });
//  }

  Widget locationAccess() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(32.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text("Can't access your location", style: bigTextStyle),
            SizedBox(
              height: 15,
            ),
            Text(
              "In order to access your location you must provide location permission to application.",
              style: regularText,
            ),
            Text(
              "Goto Setting > NeoMart Seller > Location",
              style: subHeading,
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: OutlineButton(
                    onPressed: () {
                      setState(() {
                        accessDenied = false;
                        _loading = true;
                      });
                      getPermission();
//                          Navigator.pop(context);
                    },
                    child: Text("Try Again"),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                  flex: 1,
                  child: FlatButton(
                    onPressed: () async {
                      openAppSettings();
                    },
                    child: Text("Open Settings"),
                    textColor: Colors.white,
                    color: Colors.black,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    getPermission();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
          ),
          title: Text("Search Location")),
      floatingActionButton: accessDenied
          ? SizedBox()
          : FloatingActionButton.extended(
              onPressed: locating
                  ? null
                  : () {
                      setState(() {
                        locating = true;
                      });
                      getCurrentLocation();
                    },
              label: Text(
                "Get current location",
                style: TextStyle(color: Colors.blue),
              ),
              icon: Icon(Icons.my_location, color: Colors.blue),
              backgroundColor: Colors.white,
            ),
      body: _loading
          ? loadingCircular()
          : accessDenied
              ? locationAccess()
              : Stack(
                  children: <Widget>[
                    SizedBox.expand(
                      child: Stack(
                        children: <Widget>[
                          GoogleMap(
                            myLocationButtonEnabled: false,
                            rotateGesturesEnabled: true,
                            zoomGesturesEnabled: true,
                            scrollGesturesEnabled: true,
                            onTap: (LatLng value) {
                              cameraTarget = value;
                              getFullAddress();
                            },
                            initialCameraPosition: CameraPosition(
                              target: initialPosition,
                              zoom: 14.4746,
                              bearing: 30,
                            ),
                            markers: marker,
                            onCameraMove: onCameraMove,
                            mapType: MapType.normal,
                            onMapCreated: (GoogleMapController controller) {
                              gController = controller;
                              gController.animateCamera(
                                  CameraUpdate.newLatLng(initialPosition));
                            },
                          ),
                          Positioned(
                            top: 15,
                            right: 15,
                            left: 15,
                            child: SearchMapPlaceWidget(
//                  location: LatLng(cameraTarget.latitude, cameraTarget.longitude),
//                  radius: 30000,
                              placeType: PlaceType.address,
                              apiKey:
                                  Platform.isIOS ? iosApiKey : androidApiKey,
                              onSelected: (Place place) async {
                                final geolocation = await place.geolocation;
                                setState(() {
                                  cameraTarget = geolocation.coordinates;
                                  getFullAddress();
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: locating,
                      child: loadingCircular(),
                    )
                  ],
                ),
    );
  }
}
