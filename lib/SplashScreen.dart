import 'dart:async';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:merchant/Components/IntroductionScreen.dart';
import 'package:merchant/Getter/GetUser.dart';
import 'package:merchant/LoginScreenComponents/Login.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';

import 'GetStarted.dart';
import 'Network/InternetConnection.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  HttpRequests requests = HttpRequests();
  User user;

  initController() async {
    await CheckInternet(context).checkInternet();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    if (preferences.get('showIntro') == null) {
      Timer(Duration(seconds: 3), () => Get.off(IntroSreen()));
    } else if (preferences.get('mobile') != null &&
        preferences.get('password') != null) {
      mobile = preferences.get('mobile');
      password = preferences.get('password');
      login();
    } else {
      Timer(Duration(seconds: 3), () => Get.off(Login()));
    }
  }

  login() async {
    var res = await requests.login(mobile, password);
    if (res == null ||
        res['meta']['status'].toString().toLowerCase() == "fail") {
      Get.snackbar("Login failed", "${res['meta']['message']}");
      Get.offAll(Login());
    } else {
      setState(() {
        user = User.fromJson(res);
        print("USER: ${user.data.loginDetails.authKey}");
        userid = user.data.loginDetails.userId.toString();
      });
      setSharedPreference();
      Get.off(GetStarted(user.data.loginDetails.merchantId.toString(),
          user.data.loginDetails.authKey.toString()));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    initController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBody: true,
        resizeToAvoidBottomInset: false,
        body: Center(
          child: Hero(tag: 'neomart' ,child: Image.asset('assets/neomart_splash_image.png', height: 70,)),
        )
    );
  }
}
