import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:get/get.dart';
import 'package:merchant/GetStarted.dart';
import 'package:merchant/Getter/GetUser.dart';
import 'package:merchant/LoginScreenComponents/ForgotPassword.dart';
import 'package:merchant/LoginScreenComponents/Register.dart';
import 'package:merchant/Network/httpRequests.dart';

import '../CreateStore.dart';
import '../constants.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String userError, passError;
  bool hidePass = true, loading = false;
  HttpRequests requests = HttpRequests();
  User user;

  validateFields() {
    setState(() {
      if (phoneController.text.length < 10 ||
          int.parse(phoneController.text.toString()) < 6000000000 ||
          int.parse(phoneController.text.toString()) > 9999999999) {
        userError = "Please enter valid mobile number";
      } else if (passwordController.text.length < 8) {
        passError = "Password must be minimum 8 characters";
      } else {
        loading = true;
        login();
      }
    });
  }

  login() async {
    var res = await requests.login(
        phoneController.text.toString(), passwordController.text.toString());
    if (res == null ||
        res['meta']['status'].toString().toLowerCase() == "fail") {
      setState(() {
        loading = false;
      });
      Get.snackbar("Login failed", "${res['meta']['message']}");
    } else {
      setState(() {
        user = User.fromJson(res);
        print("USER: ${user.data.loginDetails.authKey}");
        userid = user.data.loginDetails.userId.toString();
        loading = false;
      });
      mobile = phoneController.text.toString();
      password = passwordController.text.toString();
      setSharedPreference();
      Get.off(GetStarted(user.data.loginDetails.merchantId.toString(),
          user.data.loginDetails.authKey.toString()));
    }
  }

  togglePass() {
    setState(() {
      if (hidePass)
        hidePass = false;
      else
        hidePass = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: loading,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              SizedBox(
                height: 50,
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                  child: Hero(
                      tag: 'neomart',
                      child: Image.asset(
                        'assets/neomart_splash_image.png',
                        height: 60,
                      ))),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  children: <Widget>[
                    Container(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Hello\nWelcome Back",
                          style: welcomeHeading,
                        ),
//                        Text("Welcome Back", style: welcomeHeading,),
                        SizedBox(
                          height: 10,
                        ),

                        Text(
                          "Phone Number",
                          style: h1,
                        ),
                        TextField(
                          keyboardType: TextInputType.numberWithOptions(),
                          controller: phoneController,
                          scrollPadding: EdgeInsets.all(8.0),
                          maxLength: 10,
                          maxLengthEnforced: true,
                          inputFormatters: [
                            WhitelistingTextInputFormatter(RegExp("[0-9]"))
                          ],
                          onChanged: (value) {
                            setState(() {
                              userError = null;
                            });
                            if (value.length == 10) {
                              FocusScope.of(context).requestFocus(FocusNode());
                            }
                          },
                          decoration: InputDecoration(
                            errorText: userError,
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 16, vertical: 16),
                            prefixIcon: Icon(
                              Icons.call,
                              color: Colors.black,
                            ),
                            labelStyle:
                                TextStyle(color: Colors.black.withOpacity(.4)),
                            filled: true,
                            fillColor: textBoxFillColor,
                            focusColor: Colors.white38,
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                                borderRadius: BorderRadius.circular(4)),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(4)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(4)),
                            counterText: "",
                            hintText: "00 0000 0000",
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),

                        Text(
                          "Password",
                          style: h1,
                        ),
                        TextField(
                          controller: passwordController,
                          scrollPadding: EdgeInsets.all(8.0),
                          onChanged: (val) {
                            setState(() {
                              passError = null;
                            });
                          },
                          obscureText: hidePass,
                          decoration: InputDecoration(
                            errorText: passError,
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 16, vertical: 16),
                            suffixIcon: IconButton(
                              icon: Icon(Icons.remove_red_eye,
                                  color: !hidePass
                                      ? Colors.black
                                      : Colors.black.withOpacity(.4)),
                              onPressed: togglePass,
                            ),
                            prefixIcon:
                                Icon(Icons.vpn_key, color: Colors.black),
                            labelStyle:
                                TextStyle(color: Colors.black.withOpacity(.4)),
                            filled: true,
                            fillColor: textBoxFillColor,
                            focusColor: Colors.white38,
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                                borderRadius: BorderRadius.circular(4)),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(4)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(4)),
                            counterText: "",
                            hintText: "********",
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ForgotPassword()));
                            },
                            child: Text("Forgot Password?", style: h1)),
                      ],
                    )),
                    SizedBox(
                      height: 20,
                    ),
                    Hero(
                      tag: 'getStarted',
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25),
                        child: ButtonTheme(
                          height: 45,
                          minWidth: 250,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6),
                          ),
                          child: RaisedButton(
                            elevation: 0,
                            textColor: Colors.white,
                            onLongPress: () {
                              requests.switchServer();
                              clearData();
                              clearSharedPreference();
                              Get.offAll(Login());
                            },
                            onPressed: () {
                              validateFields();
//                            Navigator.push(context, MaterialPageRoute(builder: (context) => GetStarted()));
                            },
                            child: loading
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      SizedBox(
                                        height: 15,
                                        width: 15,
                                        child: CircularProgressIndicator(
                                          strokeWidth: 2,
                                          backgroundColor: Colors.white,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 8,
                                      ),
                                      Text("Logging...")
                                    ],
                                  )
                                : Text(
                                    "Login",
                                    style: TextStyle(fontSize: 16),
                                  ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          color: Colors.transparent,
          elevation: 0.0,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: GestureDetector(
              onTap: () {
//              Get.off(CreateStore("12362", "RG6SKcifUsrT_iKH9_iQrm48AhQhxM2U"));
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Register()));
              },
              child: Text(
                "Create Account",
                style: h2,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
