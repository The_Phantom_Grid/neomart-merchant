import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:merchant/LoginScreenComponents/Login.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

import '../constants.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  TextEditingController phoneController = TextEditingController();
  TextEditingController otpController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController password2Controller = TextEditingController();
  bool showConfirmPassword = false, showVerifyOtp = false;
  String userError, p1Error, p2Error, userid, token;
  HttpRequests requests = HttpRequests();
  Timer timer;
  int start = 30;

  validateMobile() {
    setState(() {
      if (phoneController.text.length < 10 ||
          int.parse(phoneController.text.toString()) < 6000000000 ||
          int.parse(phoneController.text.toString()) > 9999999999) {
        userError = "Please enter valid mobile number";
      } else
        checkUser();
    });
  }

  validatePassword() {
    setState(() {
      if (passwordController.text.length < 8)
        p1Error = "Password must be minimum 8 characters!";
      else if (password2Controller.text.length < 8)
        p2Error = "Password must be minimum 8 characters!";
      else if (passwordController.text.toString() !=
          password2Controller.text.toString()) {
        p2Error = "Password does not match!";
      } else {
        setPassword();
      }
    });
  }

  setPassword() async {
    var res = await requests.setNewPass(
        passwordController.text.toString(), userid, token);
    if (res['status'].toString().toLowerCase() == "failed") {
      Fluttertoast.showToast(
          msg: res['message'],
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    } else {
      Fluttertoast.showToast(
          msg: res['message'],
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
      Get.off(Login());
    }
  }

  checkUser() async {
    var res = await requests.checkUser(phoneController.text.toString());

    if (res['status'].toString().toLowerCase() == "failed") {
      Get.snackbar("Failed", res['message']);
    } else {
      setState(() {
        showVerifyOtp = true;
      });
      startTimer();
    }
  }

  verifyOtp() async {
    var res = await requests.verifyOtp(
        phoneController.text.toString(), otpController.text.toString());
    if (res['status'].toString().toLowerCase() == "fail")
      Fluttertoast.showToast(
          msg: res['message'],
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    else {
      setState(() {
        userid = res['id'].toString();
        token = res['authkey'].toString();
        showConfirmPassword = true;
      });
      print("USERID: $userid | TOKEN: $token");
    }
  }

  startTimer() {
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        if (start < 1)
          timer.cancel();
        else
          start = start - 1;
      });
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    if (timer != null) timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              height: 50,
            ),
            Container(
                margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                child: Image.asset('assets/neomart_splash_image.png', height: 60,)
            ),
            showConfirmPassword? confirmPasswordCard(): showVerifyOtp? verifyOtpCard(): getOtpCard(),
          ],
        ),
      ),

    );
  }

  getOtpCard(){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("Forgot Password", style: welcomeHeading,),
          SizedBox(height: 10,),
          Text("Enter your phone number below to create new password", style: h1, textAlign: TextAlign.start,),
          SizedBox(height: 5,),
          Container(
              child: Column(
                children: <Widget>[
                  TextField(
                    keyboardType: TextInputType.numberWithOptions(),
                    controller: phoneController,
                    scrollPadding: EdgeInsets.all(8.0),
                    maxLength: 10,
                    maxLengthEnforced: true,
                    inputFormatters: [
                      WhitelistingTextInputFormatter(RegExp("[0-9]"))
                    ],
                    onChanged: (value) {
                  setState(() {
                    userError = null;
                  });
                  if (value.length == 10) {
                    FocusScope.of(context).requestFocus(FocusNode());
                  }
                },
                    decoration: InputDecoration(
                      errorText: userError,
                      contentPadding: EdgeInsets.all(16),
                      prefixIcon: Icon(Icons.call, color: Colors.black,),
                      labelStyle: TextStyle(
                          color: Colors.black.withOpacity(.4)),
                      filled: true,
                      fillColor: textBoxFillColor,
                      focusColor: Colors.white38,
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.circular(4)),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(4)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(4)),
                      counterText: "",
                      hintText: "00 0000 0000",
                    ),
                  ),
                ],
              )
          ),
          SizedBox(height: 20,),
          Hero(
            tag: 'getStarted',
            child: Center(
              child: ButtonTheme(
                height: 45,
                minWidth: 250,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6),
                ),
                child: RaisedButton(
                  elevation: 0,
                  textColor: Colors.white,
                  onPressed: () {
                    validateMobile();
//                    Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyOtp()));
                  },
                  child: Text("GET OTP", style: TextStyle(fontSize: 16),),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  verifyOtpCard(){
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: 25),
        child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("Forgot Password", style: welcomeHeading,),
              SizedBox(height: 15,),
              Text("Enter 4 digit OTP sent to your number to register as a merchant", style: h1, textAlign: TextAlign.start,),
              SizedBox(height: 15,),
              Container(
                child: Column(
                  children: <Widget>[
                    PinCodeTextField(
                      controller: otpController,
                      pinBoxBorderWidth: 0,
                      pinBoxDecoration: ProvidedPinBoxDecoration.defaultPinBoxDecoration,
                      pinBoxRadius: 2,
                      wrapAlignment: WrapAlignment.center,
                      pinBoxOuterPadding: EdgeInsets.symmetric(horizontal: 8),
                      pinBoxColor: textBoxFillColor,
                      keyboardType: TextInputType
                          .numberWithOptions(),
                      pinBoxHeight: 40,
                      maxLength: 4,
                      pinBoxWidth: 40,
                    ),
                    SizedBox(height: 15,),
                    GestureDetector(
                      onTap: start > 1 ? null : () {
                        setState(() {
                          start = 30;
                        });
                        checkUser();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          start < 1 ? SizedBox() : Text("$start | ",
                              style: TextStyle(fontWeight: FontWeight.bold,
                                  color: Colors.grey)),
                          Text("Resend", style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: start > 0 ? Colors.grey : Colors.black)),
                        ],
                      ),
                    ),
                    SizedBox(height: 20,),
                    Hero(
                      tag: 'getStarted',
                      child: Center(
                        child: ButtonTheme(
                          height: 50,
                          minWidth: 250,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(6),
                          ),
                          child: RaisedButton(
                            elevation: 0,
                            textColor: Colors.white,
                            onPressed: (){
                              verifyOtp();
//                             Navigator.push(context, MaterialPageRoute(builder: (context) => GetStarted()));
                            },
                            child: Text("VERIFY", style: TextStyle(fontSize: 16),),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ])
    );
  }

  bool p1 = true,
      p2 = true;

  toggleP1() {
    setState(() {
      if (p1)
        p1 = false;
      else
        p1 = true;
    });
  }

  toggleP2() {
    setState(() {
      if (p2)
        p2 = false;
      else
        p2 = true;
    });
  }

  confirmPasswordCard() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Forgot Password",
            style: welcomeHeading,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "New Password",
            style: h1,
          ),
          TextField(
            keyboardType: TextInputType.numberWithOptions(),
            controller: passwordController,
            scrollPadding: EdgeInsets.all(8.0),
            onChanged: (val) {
              setState(() {
                p1Error = null;
              });
            },
            obscureText: p1,
            decoration: InputDecoration(
              errorText: p1Error,
              contentPadding:
              EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              suffixIcon: IconButton(
                icon: Icon(Icons.remove_red_eye,
                    color: !p1 ? Colors.black : Colors.black.withOpacity(.4)),
                onPressed: toggleP1,
              ),
              prefixIcon: Icon(Icons.vpn_key, color: Colors.black),
              labelStyle: TextStyle(color: Colors.black.withOpacity(.4)),
              filled: true,
              fillColor: textBoxFillColor,
              focusColor: Colors.white38,
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(4)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(4)),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(4)),
              counterText: "",
              hintText: "********",
            ),
          ),
          SizedBox(height: 15,),
          Text(
            "Confirm Password",
            style: h1,
          ),
          TextField(
            keyboardType: TextInputType.numberWithOptions(),
            controller: password2Controller,
            scrollPadding: EdgeInsets.all(8.0),
            onChanged: (val) {
              setState(() {
                p2Error = null;
              });
            },
            obscureText: p2,
            decoration: InputDecoration(
              errorText: p2Error,
              contentPadding:
              EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              suffixIcon: IconButton(
                icon: Icon(Icons.remove_red_eye,
                    color: !p2 ? Colors.black : Colors.black.withOpacity(.4)),
                onPressed: toggleP2,
              ),
              prefixIcon: Icon(Icons.vpn_key, color: Colors.black),
              labelStyle: TextStyle(color: Colors.black.withOpacity(.4)),
              filled: true,
              fillColor: textBoxFillColor,
              focusColor: Colors.white38,
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(4)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(4)),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(4)),
              counterText: "",
              hintText: "********",
            ),
          ),
          SizedBox(height: 20,),
          Hero(
            tag: 'getStarted',
            child: Center(
              child: ButtonTheme(
                height: 50,
                minWidth: 250,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6),
                ),
                child: RaisedButton(
                  elevation: 0,
                  textColor: Colors.white,
                  onPressed: (){
                    validatePassword();
//                    Navigator.push(context, MaterialPageRoute(builder: (context) => CreateProfile()));
                  },
                  child: Text("REGISTER", style: TextStyle(fontSize: 16),),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

}
