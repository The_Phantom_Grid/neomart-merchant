import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:merchant/LoginScreenComponents/Login.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/constants.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

import '../CreateProfile.dart';

class VerifyOtp extends StatefulWidget {
  String phone;

  VerifyOtp(this.phone);

  @override
  _VerifyOtpState createState() => _VerifyOtpState();
}

class _VerifyOtpState extends State<VerifyOtp> {


  TextEditingController otpController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController password2Controller = TextEditingController();
  bool showConfirmPassword = false;
  HttpRequests requests = HttpRequests();
  String userid, token, p1Error, p2Error;

  verifyOtp() async {
    var res =
        await requests.verifyOtp(widget.phone, otpController.text.toString());
    if (res['status'].toString().toLowerCase() == "fail")
      Fluttertoast.showToast(
          msg: res['message'],
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    else {
      setState(() {
        token = res['authkey'];
        userid = res['id'].toString();
        showConfirmPassword = true;
      });
      print("USERID: $userid | TOKEN: $token");
    }
  }

  validatePassword() {
    setState(() {
      if (passwordController.text.length < 8)
        p1Error = "Password must be minimum 8 characters!";
      else if (password2Controller.text.length < 8)
        p2Error = "Password must be minimum 8 characters!";
      else if (passwordController.text.toString() !=
          password2Controller.text.toString()) {
        p2Error = "Password does not match!";
      } else {
        setPassword();
      }
    });
  }

  setPassword() async {
    var res = await requests.setNewPass(
        passwordController.text.toString(), userid, token);
    if (res['status'].toString().toLowerCase() == "failed") {
      Fluttertoast.showToast(
          msg: res['message'],
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    } else {
      Fluttertoast.showToast(
          msg: res['message'],
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
      Get.off(CreateProfile(widget.phone, userid, token));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            SizedBox(
              height: 50,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
              child: Image.asset('assets/neomart_splash_image.png', height: 60,)
            ),
            showConfirmPassword? confirmPasswordCard(): getOtpCard(),
          ],
        ),
      ),

      bottomNavigationBar: BottomAppBar(
        color: Colors.transparent,
        elevation: 0.0,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: GestureDetector(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
            },
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                children: [
                  TextSpan(text: "Already have an account? ", style: TextStyle(fontSize: 15, color: Colors.black)),
                  TextSpan(text: "LOGIN", style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.black)),
                ]
              ),
            ),
          ),
        ),
      ),
    );
  }

  getOtpCard(){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("Create\nNew Account", style: welcomeHeading,),
          SizedBox(height: 15,),
          Text("Enter 4 digit OTP sent to your number to register as a merchant", style: h1, textAlign: TextAlign.start,),
          SizedBox(height: 15,),
          Container(
              child: Column(
                children: <Widget>[
                  PinCodeTextField(
                    controller: otpController,
                    onTextChanged: (value) {

                    },
                    pinBoxBorderWidth: 0,
                    pinBoxDecoration: ProvidedPinBoxDecoration.defaultPinBoxDecoration,
                    pinBoxRadius: 2,
                    wrapAlignment: WrapAlignment.center,
                    pinBoxOuterPadding: EdgeInsets.symmetric(horizontal: 8),
                    pinBoxColor: textBoxFillColor,
                    keyboardType: TextInputType
                        .numberWithOptions(),
                    pinBoxHeight: 40,
                    maxLength: 4,
                    pinBoxWidth: 40,
                  ),
                  SizedBox(height: 15,),
                  Text("Resend", style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(height: 20,),
                  Hero(
                    tag: 'getStarted',
                    child: Center(
                      child: ButtonTheme(
                        height: 50,
                        minWidth: 250,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: RaisedButton(
                          elevation: 0,
                          textColor: Colors.white,
                          onPressed: (){
                            verifyOtp();
//                             Navigator.push(context, MaterialPageRoute(builder: (context) => GetStarted()));
                          },
                          child: Text("VERIFY", style: TextStyle(fontSize: 16),),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
          )
        ])
    );
  }

  bool p1 = true,
      p2 = true;

  toggleP1() {
    setState(() {
      if (p1)
        p1 = false;
      else
        p1 = true;
    });
  }

  toggleP2() {
    setState(() {
      if (p2)
        p2 = false;
      else
        p2 = true;
    });
  }


  confirmPasswordCard() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Setup\nLogin Password",
            style: welcomeHeading,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Enter Password",
            style: h1,
          ),
          TextField(
            controller: passwordController,
            scrollPadding: EdgeInsets.all(8.0),
            onChanged: (val) {
              setState(() {
                p1Error = null;
              });
            },
            obscureText: p1,
            decoration: InputDecoration(
              errorText: p1Error,
              contentPadding:
              EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              suffixIcon: IconButton(
                icon: Icon(Icons.remove_red_eye,
                    color: !p1 ? Colors.black : Colors.black.withOpacity(.4)),
                onPressed: toggleP1,
              ),
              prefixIcon: Icon(Icons.vpn_key, color: Colors.black),
              labelStyle: TextStyle(color: Colors.black.withOpacity(.4)),
              filled: true,
              fillColor: textBoxFillColor,
              focusColor: Colors.white38,
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(4)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(4)),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(4)),
              counterText: "",
              hintText: "********",
            ),
          ),
          SizedBox(height: 15,),
          Text(
            "Confirm Password",
            style: h1,
          ),
          TextField(
            controller: password2Controller,
            scrollPadding: EdgeInsets.all(8.0),
            onChanged: (val) {
              setState(() {
                p2Error = null;
              });
            },
            obscureText: p2,
            decoration: InputDecoration(
              errorText: p2Error,
              contentPadding:
              EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              suffixIcon: IconButton(
                icon: Icon(Icons.remove_red_eye,
                    color: !p2 ? Colors.black : Colors.black.withOpacity(.4)),
                onPressed: toggleP2,
              ),
              prefixIcon: Icon(Icons.vpn_key, color: Colors.black),
              labelStyle: TextStyle(color: Colors.black.withOpacity(.4)),
              filled: true,
              fillColor: textBoxFillColor,
              focusColor: Colors.white38,
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                  borderRadius: BorderRadius.circular(4)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(4)),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                  borderRadius: BorderRadius.circular(4)),
              counterText: "",
              hintText: "********",
            ),
          ),
          SizedBox(height: 20,),
          Hero(
            tag: 'getStarted',
            child: Center(
              child: ButtonTheme(
                height: 50,
                minWidth: 250,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6),
                ),
                child: RaisedButton(
                  elevation: 0,
                  textColor: Colors.white,
                  onPressed: () {
                    validatePassword();
//                   Navigator.push(context, MaterialPageRoute(builder: (context) => CreateProfile()));
                  },
                  child: Text("REGISTER", style: TextStyle(fontSize: 16),),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
