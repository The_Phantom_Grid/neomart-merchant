import 'package:device_info/device_info.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:merchant/CreateProfile.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/constants.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';

import 'Login.dart';
import 'VerifyOTP.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  TextEditingController phoneController = TextEditingController();
  TextEditingController referralController = TextEditingController();
  TextEditingController otpController = TextEditingController();
  String error, userid, deviceType = "1", fcmToken;
  bool showReferral = false;
  DatabaseReference dbRef;
  FirebaseMessaging firebaseMessaging = FirebaseMessaging();

//  PageController pageController = PageController();
  HttpRequests requests = HttpRequests();

  getReferral() async {
    var res = await requests.getReferral(phoneController.text.toString());
    if (res != null) {
      if (res['code'].toString().length != 0)
        setState(() {
          referralController.text = res['code'];
          showReferral = true;
        });
      else
        setState(() {
          referralController.text = "";
          showReferral = true;
        });
      getToken();
//    Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyOtp()));
    }
  }

  registerPhone() async {
    var res = await requests.registerPhone(phoneController.text.toString());
    if (res != null) {
      if (res['status'].toString().toLowerCase() == "failed!") {
        Get.snackbar("Failed", res['message']);
      } else {
        setState(() {
          userid = res['userid'].toString();
        });
        getReferral();
      }
    }
  }

  getToken() async {
    String deviceId = await _getId();
    print("DEVICE ID: $deviceId");
    var res = await requests.getToken(deviceId, fcmToken, deviceType, userid);
    if (res != null && res['status'].toString() == "success") {
      sendComm();
    }
  }

  sendComm() async {
    print("USER ID: $userid");
    var res = await requests.sendComm(userid);
    if (res != null && res['status'].toString() == "success") {
      Fluttertoast.showToast(
        msg: res['message'],
        timeInSecForIosWeb: 2,
        toastLength: Toast.LENGTH_LONG,
      );
    }
    Get.to(VerifyOtp(phoneController.text.toString()));
  }

  Future<String> _getId() async {
    print("getting device Id");
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      deviceType = "1";
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      deviceType = "1";
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  validateFields() {
    if (int.parse(phoneController.text.toString()) < 6000000000 ||
        int.parse(phoneController.text.toString()) > 9999999999)
      setState(() {
        error = "Please enter valid mobile number";
      });
    else {
      registerPhone();
    }
  }

  getOtpCard(){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 25),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("Create\nNew Account", style: welcomeHeading,),
          SizedBox(height: 10,),
          Text("Enter your phone number below to register as a merchant", style: h1, textAlign: TextAlign.start,),
          SizedBox(height: 5,),
          Container(
              child: Column(
                children: <Widget>[
                  TextField(
                    keyboardType: TextInputType.numberWithOptions(),
                    controller: phoneController,
                    scrollPadding: EdgeInsets.all(8.0),
                    maxLength: 10,
                    maxLengthEnforced: true,
                    inputFormatters: [
                      WhitelistingTextInputFormatter(RegExp("[0-9]"))
                    ],
                    onChanged: (value) {
                      setState(() {
                        error = null;
                      });
                      if (value.length == 10) {
                        FocusScope.of(context).requestFocus(FocusNode());
                      }
                    },
                    decoration: InputDecoration(
                      errorText: error,
                      errorMaxLines: 1,
                      contentPadding: EdgeInsets.all(16),
                      prefixIcon: Icon(Icons.call, color: Colors.black,),
                      labelStyle: TextStyle(color: Colors.black.withOpacity(.4)),
                      filled: true,
                      fillColor: textBoxFillColor,
                      focusColor: Colors.white38,
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black), borderRadius: BorderRadius.circular(4)),
                      border: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
                      counterText: "",
                      hintText: "00 0000 0000",
                    ),
                  ),
                  SizedBox(height: 8,),
                  !showReferral ? SizedBox() : TextField(
                    keyboardType: TextInputType.numberWithOptions(),
                    controller: referralController,
                    scrollPadding: EdgeInsets.all(8.0),
//                    maxLength: 10,
//                    maxLengthEnforced: true,
//                    inputFormatters: [
//                      WhitelistingTextInputFormatter(RegExp("[0-9]"))
//                    ],
                    onChanged: (value) {
                      if (value.length == 10) {
                        FocusScope.of(context).requestFocus(FocusNode());
                      }
                    },
                    decoration: InputDecoration(
                      enabled: false,
                      errorText: error,
                      errorMaxLines: 1,
                      contentPadding: EdgeInsets.all(16),
                      prefixIcon: Icon(FlutterIcons.flow_tree_ent, color: Colors.black,),
                      labelStyle: TextStyle(color: Colors.black.withOpacity(.4)),
                      filled: true,
                      fillColor: textBoxFillColor,
                      focusColor: Colors.white38,
                      disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
                      focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black), borderRadius: BorderRadius.circular(4)),
                      border: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
                      enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
                      counterText: "",
                      hintText: "No Referral Available",
                    ),
                  ),

                ],
              )
          ),
          SizedBox(height: 20,),
          Hero(
            tag: 'getStarted',
            child: Center(
              child: ButtonTheme(
                height: 50,
                minWidth: 250,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6),
                ),
                child: RaisedButton(
                  elevation: 0,
                  textColor: Colors.white,
                  onPressed: (){
                    validateFields();
//                    register();
//                    Navigator.push(context, MaterialPageRoute(builder: (context) => VerifyOtp()));
                  },
                  child: Text("GET OTP", style: TextStyle(fontSize: 16),),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dbRef = FirebaseDatabase.instance.reference().child("registeredUser");
    firebaseMessaging.getToken().then((value) {
      fcmToken = value;
      print("fcmToken -> $fcmToken");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 50,),
            Container(
                margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
                child: Image.asset(
                  'assets/neomart_splash_image.png', height: 60,)
            ),
            getOtpCard(),
          ],
        ),
      ),

      bottomNavigationBar: BottomAppBar(
        color: Colors.transparent,
        elevation: 0.0,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Login()));
            },
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  children: [
                    TextSpan(text: "Already have an account? ",
                        style: TextStyle(fontSize: 15, color: Colors.black)),
                    TextSpan(text: "LOGIN",
                        style: TextStyle(fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: Colors.black)),
                  ]
              ),
            ),
          ),
        ),
      ),
    );
  }
}

