import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:merchant/AddOwnBrand.dart';
import 'package:merchant/AddProduct.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'MyInventory.dart';
import 'QrScan.dart';
import 'ScanResult.dart';
import 'SearchProductBarcode.dart';
import 'constants.dart';

class MyStoreInventory extends StatefulWidget {
  @override
  _MyStoreInventoryState createState() => _MyStoreInventoryState();
}

class _MyStoreInventoryState extends State<MyStoreInventory> {

  Future _scan() async {
    PermissionStatus status = await Permission.camera.status;
    if (!status.isGranted)
      await Permission.camera.request();
    else {
      String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', "Cancel", true, ScanMode.BARCODE);

      // HapticFeedback.mediumImpact();
      if (barcodeScanRes == "-1")
        Get.back();
     else if (barcodeScanRes != null)
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ScanResult(barcodeScanRes)));
//      String barcode = await Get.to(QrScan());
//      print(barcode);
//      if(barcode != null)
//        Navigator.push(context,
//            MaterialPageRoute(builder: (context) => ScanResult(barcode)));
    }
//    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("My Store Inventory"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => MyInventory()));
              },
              child: Container(
                height: 100,
                child: Card(
                  elevation: 2.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: BorderSide(color: mColor)),
                  child: Center(
                    child: ListTile(
                      leading: Icon(
                        Icons.home,
                        size: 30,
                      ),
                      title: Text("My Product Listing", style: subHeading,),
                      trailing: Icon(Icons.arrow_forward_ios),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddProduct()));
              },
              child: Container(
                height: 100,
                child: Card(
                  elevation: 2.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: BorderSide(color: mColor)),
                  child: Center(
                    child: ListTile(
                      leading: Icon(
                        Icons.store_mall_directory,
                        size: 30,
                      ),
                      title: Text("Add Products From System Inventory",
                        style: subHeading,),
                      trailing: Icon(Icons.arrow_forward_ios),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AddOwnBrand()));
              },
              child: Container(
                height: 100,
                child: Card(
                  elevation: 2.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: BorderSide(color: mColor)),
                  child: Center(
                    child: ListTile(
                      leading: Icon(
                        FlutterIcons.list_thumbnails_fou,
                        size: 30,
                      ),
                      title: Text(
                        "List Your Own Brand Products",
                        style: subHeading,
                      ),
                      trailing: Icon(Icons.arrow_forward_ios),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: (){
                _scan();
              },
              child: Container(
                height: 100,
                child: Card(
                  elevation: 2.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                      side: BorderSide(color: mColor)),
                  child: Center(
                    child: ListTile(
                      leading: Icon(
                        FlutterIcons.qrcode_scan_mco,
                        size: 30,
                      ),
                      title: Text(
                        "Add Products with QR/Bar Code", style: subHeading,),
                      trailing: Icon(Icons.arrow_forward_ios),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
