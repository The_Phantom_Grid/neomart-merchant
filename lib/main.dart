import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:get/get.dart';
import 'package:merchant/ChooseCategory.dart';
import 'package:merchant/ChooseStore.dart';
import 'package:merchant/FillStoreDetails.dart';
import 'package:merchant/GetStarted.dart';
import 'package:merchant/SplashScreen.dart';
import 'package:merchant/constants.dart';

import 'AddDeliveryBoy.dart';
import 'LoginScreenComponents/Login.dart';
import 'StepsCreateStore.dart';

void main() => runApp(Phoenix(child: MyApp()));

const MaterialColor primaryBlack = MaterialColor(
  _blackPrimaryValue,
  <int, Color>{
    50: Color(0xFF000000),
    100: Color(0xFF000000),
    200: Color(0xFF000000),
    300: Color(0xFF000000),
    400: Color(0xFF000000),
    500: Color(_blackPrimaryValue),
    600: Color(0xFF000000),
    700: Color(0xFF000000),
    800: Color(0xFF000000),
    900: Color(0xFF000000),
  },
);
const int _blackPrimaryValue = 0xFF000000;

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
//        FocusScope.of(context).requestFocus(FocusNode());
        WidgetsBinding.instance.focusManager.primaryFocus.unfocus();
      },
      child: GetMaterialApp(
          title: 'NeoMart Seller',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            textTheme: TextTheme(
                title: TextStyle(fontFamily: 'Poppins-Regular', fontSize: 13),
                subtitle:
                    TextStyle(fontFamily: 'Poppins-Regular', fontSize: 13)),
            backgroundColor: textBoxFillColor,
            fontFamily: 'Poppins-Regular',
            appBarTheme: AppBarTheme(
                textTheme: TextTheme(
                    title:
                        TextStyle(fontFamily: 'Poppins-Regular', fontSize: 13),
                    subtitle: TextStyle(
                        fontFamily: 'Poppins-Regular', fontSize: 13))),
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: primaryBlack,
          ),
          home: Splash()),
    );
  }
}
