import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:merchant/Getter/GetBarcodeScanResult.dart';
import 'package:merchant/Network/httpRequests.dart';

import 'AddOwnBrand.dart';
import 'constants.dart';

class ScanResult extends StatefulWidget {
  String qrString;

  ScanResult(this.qrString);

  @override
  _ScanResultState createState() => _ScanResultState();
}

class _ScanResultState extends State<ScanResult> {
  String qrCode = "";
  BarcodeScanResult barcodeScanResult;
  HttpRequests requests = HttpRequests();
  int qty = 0;
  var mrpController = TextEditingController();
  var srpController = TextEditingController();
  var qtyController = TextEditingController();
  double mrp, srp;
  bool loading = true,
      notFound = false,
      adding = false;

  getScanResult() async {
    var res = await requests.getBarcodeScanResult(qrCode);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      barcodeScanResult = BarcodeScanResult.fromJson(res);
      setState(() {
        if (barcodeScanResult.product.isNotEmpty) {
          initValues();
        } else {
          loading = false;
          notFound = true;
        }
      });
    }
  }

  addToInventory() async {
    setState(() {
      adding = true;
    });
    var res = await requests.addProductFromBarcode(
        qrCode,
        qty.toString(),
        srp.toString(),
        mrp.toString(),
        barcodeScanResult.product[0].masterProductId.toString()
    );
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Fluttertoast.showToast(
          msg: "Product added to inventory",
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 2);
      Get.back();
    } else {
      setState(() {
        adding = false;
      });
    }
  }

  initValues() {
    setState(() {
      print("MRP: ${barcodeScanResult.product[0].mrp}");
      print("sRP: ${barcodeScanResult.product[0].sellingPrice}");
      mrp =
          double.tryParse(
              double.tryParse(barcodeScanResult.product[0].mrp).toStringAsFixed(
                  2));
      srp = double.tryParse(
          double.tryParse(barcodeScanResult.product[0].sellingPrice)
              .toStringAsFixed(2));
      qty = barcodeScanResult.product[0].totalLeft;
      mrpController.text = mrp.toString();
      srpController.text = srp.toString();
      qtyController.text = qty.toString();
      loading = false;
      notFound = false;
    });
  }

  validateDetails() {
    if (mrp <= 0) {
      Fluttertoast.showToast(
          msg: "Product MRP cannot be 0.",
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 2);
      initValues();
    } else if (mrp < srp) {
      Fluttertoast.showToast(
          msg: "Product MRP cannot be greater then Selling Price.",
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 2);
      initValues();
    } else if (srp <= 0) {
      Fluttertoast.showToast(
          msg: "Product Selling Price cannot be 0.",
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 2);
      initValues();
    } else {
      addToInventory();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    qrCode = widget.qrString;
    getScanResult();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: adding,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          automaticallyImplyLeading: true,
          title: Text("Scan Result"),
        ),

        body: loading ? loadingCircular() :
        notFound ? Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(FlutterIcons.qrcode_scan_mco, color: Colors.blueGrey,
                size: 70,),
              SizedBox(height: 8,),
              Text("oops!", style: bigTextStyle,),
              Text("No Product Found", style: h2),
              SizedBox(height: 50,),
              ButtonTheme(
                child: OutlineButton(
                  onPressed: () {
                    Get.off(AddOwnBrand());
                  },
                  borderSide: BorderSide(color: Colors.black),
                  splashColor: Colors.transparent,
                  child: Text("Add a new product", style: subHeading,),
                ),
              )
            ],
          ),
        ) :
        SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 2,
                          color: textBoxFillColor,
                          spreadRadius: 2,
                        )
                      ]),
                  child: FadeInImage.assetNetwork(
                    placeholder: 'assets/tsp.png',
                    image: barcodeScanResult.product[0].imagePath,
                    height: 200,
                    width: double.infinity,
                    fit: BoxFit.fitHeight,
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  barcodeScanResult.product[0].productname,
                  style: subHeading,
                ),
                Text(
                  barcodeScanResult.product[0].productdescription,
                  style: TextStyle(fontSize: 11, color: Colors.grey),
                ),
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(),
                              scrollPadding: EdgeInsets.all(8.0),
                              maxLength: 10,
                              maxLengthEnforced: true,
                              controller: mrpController,
                              inputFormatters: [
                                WhitelistingTextInputFormatter(RegExp("[0-9]")),
                              ],
                              onChanged: (value) {
                                setState(() {
                                  mrp = double.tryParse(double.tryParse(value)
                                      .toStringAsFixed(2));
                                });
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Icon(FlutterIcons.rupee_faw,
                                      size: 15, color: Colors.black),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 8),
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: true,
                                  fillColor: textBoxFillColor,
                                  focusColor: Colors.white38,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  border: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  counterText: "",
                                  hintText: "Enter MRP",
                                  labelText: "MRP"),
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(),
                              scrollPadding: EdgeInsets.all(8.0),
                              maxLength: 10,
                              controller: srpController,
                              maxLengthEnforced: true,
                              inputFormatters: [
                                WhitelistingTextInputFormatter(RegExp("[0-9]"))
                              ],
                              onChanged: (value) {
                                setState(() {
                                  srp = double.tryParse(double.tryParse(value)
                                      .toStringAsFixed(2));
                                });
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Icon(FlutterIcons.rupee_faw,
                                      size: 15, color: Colors.black),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 8),
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: true,
                                  fillColor: textBoxFillColor,
                                  focusColor: Colors.white38,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  border: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  counterText: "",
                                  hintText: "Enter Selling Price",
                                  labelText: "SRP"),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Row(
                              children: <Widget>[
                                Text(
                                  "Quantity: ",
                                  style: regularText,
                                ),
//                          IconButton(
//                            onPressed: qty <= 0? null: (){
//                              setState(() {
//                                qty--;
//                                qtyController.text = qty.toString();
//                              });
//                            },
//                            icon: Icon(Icons.remove, size: 15, color: Colors.black,),
//                          ),
                                SizedBox(
                                  width: 60,
                                  child: TextField(
                                    keyboardType:
                                    TextInputType.numberWithOptions(),
                                    scrollPadding: EdgeInsets.all(8.0),
                                    maxLength: 10,
                                    maxLengthEnforced: true,
                                    controller: qtyController,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter(
                                          RegExp("[0-9]"))
                                    ],
                                    onChanged: (value) {
                                      setState(() {
                                        qty = int.tryParse(value);
                                      });
                                    },
                                    decoration: InputDecoration(
                                      isDense: true,
//                              prefixIcon: Icon(Icons.remove, size: 15, color: Colors.black,),
//                              suffixIcon: Icon(Icons.add, size: 15, color: Colors.black,),
                                      contentPadding: EdgeInsets.symmetric(
                                          horizontal: 8, vertical: 8),
                                      labelStyle: TextStyle(
                                          color: Colors.black.withOpacity(.4)),
                                      filled: true,
                                      fillColor: textBoxFillColor,
                                      focusColor: Colors.white38,
                                      focusedBorder: OutlineInputBorder(
                                          borderSide:
                                          BorderSide(color: Colors.white),
                                          borderRadius:
                                          BorderRadius.circular(4)),
                                      border: OutlineInputBorder(
                                          borderSide:
                                          BorderSide(color: Colors.white),
                                          borderRadius:
                                          BorderRadius.circular(4)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide:
                                          BorderSide(color: Colors.white),
                                          borderRadius:
                                          BorderRadius.circular(4)),
                                      counterText: "",
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: ButtonTheme(
                              height: 35,
                              child: FlatButton(
                                onPressed: () {
                                  Get.back();
                                },
                                color: textBoxFillColor,
                                textColor: Colors.black,
                                child: Text("Cancel"),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: ButtonTheme(
                              height: 35,
                              child: FlatButton(
                                onPressed: () {
                                  validateDetails();
                                },
                                color: Colors.black,
                                textColor: textBoxFillColor,
                                child: Text("Add to inventory"),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),

        bottomNavigationBar: !adding ? SizedBox() : BottomAppBar(
          elevation: 0.0,
          color: Colors.white,
          child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(height: 30, width: 25, child: loadingCircular())
          ),
        ),
      ),
    );
  }
}
