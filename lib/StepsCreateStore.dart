import 'dart:convert';

import 'package:chips_choice/chips_choice.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/model.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:merchant/GetStarted.dart';
import 'package:merchant/Getter/GetStoreSubCat.dart';
import 'package:merchant/Getter/GetStoreTypes.dart';
import 'package:merchant/Network/httpRequests.dart';

import 'ChooseCategory.dart';
import 'Components/ImagePick.dart';
import 'FillStoreDetails.dart';
import 'Getter/GetSubCategory.dart';
import 'SearchLocation.dart';
import 'constants.dart';

_StepsCreateStoreState stepsCreateStoreState;

class StepsCreateStore extends StatefulWidget {
  String merchantid, token;

  StepsCreateStore(this.merchantid, this.token);

  @override
  _StepsCreateStoreState createState() {
    stepsCreateStoreState = _StepsCreateStoreState();
    return stepsCreateStoreState;
  }
}

class _StepsCreateStoreState extends State<StepsCreateStore> {

  PageController pageController = PageController();
  TextEditingController searchController = TextEditingController();
  int step = 0;

  var cNameController = TextEditingController();
  var numberController = TextEditingController();
  var cDescription = TextEditingController();
  var gstinController = TextEditingController();
  var subCatNameController = TextEditingController();
  var panController = TextEditingController();
  var aadharController = TextEditingController();
  var fssaiController = TextEditingController();
  List<Address> address;
  String cNameError,
      cDescError,
      storeType = "Choose store type",
      storeSubCat = "Choose store sub category",
      storeId;
  List<String> selected = [];
  bool homeDelivery = true;
  var ratio = [
    CropAspectRatioPreset.square,
    CropAspectRatioPreset.ratio3x2,
    CropAspectRatioPreset.original,
    CropAspectRatioPreset.ratio4x3,
    CropAspectRatioPreset.ratio16x9
  ];

  var house = TextEditingController(),
      streetName = TextEditingController(),
      fullNAme = TextEditingController(),
      pincode = TextEditingController(),
      city = TextEditingController(),
      state = TextEditingController(),
      storeLocation = TextEditingController(),
      country = TextEditingController();

  var deliveryTime = TextEditingController(),
      deliveryRadius = TextEditingController(),
      deliveryCharge = TextEditingController(),
      minOrder = TextEditingController(),
      openTime = TextEditingController(),
      closeTime = TextEditingController(),
      deliveryCloseTime = TextEditingController(),
      deliveryOpenTime = TextEditingController();

  String appBarTitle = "";
  HttpRequests requests = HttpRequests();
  StoreTypes storeTypes;
  StoreSubCat storeSubCategories;
  SubCategory subCategory;
  String categoryId,
      subCatId,
      aadharCard,
      aadharImage,
      description,
      documentsAvail,
      fssai,
      gstImage = "",
      gstNo,
      logo,
      panCard,
      panCardImage,
      shopName,
      storeNumber,
      storeImageAvail;

  getStoreTypes() async {
    print("TOKEN: ${widget.token}| MERCHANT: ${widget.merchantid}");
    var res = await requests.getStoreTypes(widget.token);
    if (res != null) {
      if (res['status'].toString().toLowerCase() == "success") {
        setState(() {
          storeTypes = StoreTypes.fromJson(res);
        });
      }
    }
  }

  getStoreSubCat() async {
    print("MID: ${widget.merchantid}");
    print("TOKEN: ${widget.token}");
    print("CAT ID SELECTED: $categoryId");
    var res = await requests.getStoreSubCat(
        categoryId, widget.merchantid, widget.token);
    if (res != null) {
      if (res['status'].toString().toLowerCase() == "success") {
        setState(() {
          storeSubCategories = StoreSubCat.fromJson(res);
        });
      }
    }
  }

  saveStoreDetails() async {
    print(aadharController.text);
    print(aadharImage);
    print(categoryId);
    print(cDescription.text);
    print(fssaiController.text);
    print(gstImage);
    print(gstinController.text);
    print(logo);
    print(widget.merchantid);
    print(panController.text);
    print(panCardImage);
    print(cNameController.text);
    print(numberController.text);
    print(storeType);
    print(storeSubCat);
    print(subCatId);
    print(widget.token);
    var res = await requests.saveStoreInfo(
        aadharController.text.toString(),
        aadharImage,
        categoryId,
        cDescription.text.toString(),
        "0",
        fssaiController.text.toString(),
        gstImage,
        gstinController.text.toString(),
        logo,
        widget.merchantid.toString(),
        panController.text.toString(),
        panCardImage,
        cNameController.text.toString(),
        "1",
        "1",
        numberController.text.toString(),
        storeType,
        storeSubCat,
        subCatId,
        "0",
        "",
        "",
        "",
        widget.token
    );
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      storeId = res['data']['store_id'].toString();
      getSubCategory();
      changeStep();
    } else {
//      Fluttertoast.showToast(msg: res['message'].toString(), timeInSecForIosWeb: 2, toastLength: Toast.LENGTH_LONG);
    }
  }

  addNewCat() async {
    var res = await requests.addNewCat(
        catNameController.text.toString(), widget.merchantid, widget.token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Fluttertoast.showToast(
          msg: res['message'].toString(),
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
      setState(() {
        categories.add(catNameController.text);
        catNameController.text = "";
        selectedCat[res['data']['category_id']] =
            res['data']['category'].toString();
      });
      print("SELECTED CAT: $selectedCat");
      getSubCategory();
    } else {
      Fluttertoast.showToast(
          msg: res['message'].toString(),
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    }
  }

  getSubCategory() async {
    print("CAT ID: $categoryId");
    setState(() {
      subCategory = null;
    });
    var res =
        await requests.getSubCategory("1", widget.merchantid, widget.token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        subCategory = SubCategory.fromJson(res);
        print("LENGTH: ${subCategory.info.length}");
      });
    } else {
      Fluttertoast.showToast(
          msg: res['message'].toString(),
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    }
  }

  String selectedSubCats;

  validateStep3() {
    selectedSubCats = selectedCat.keys
        .toList()
        .toString()
        .replaceAll('[', "")
        .replaceAll(']', "")
        .replaceAll(" ", "");
    print(selectedSubCats);
    if (selectedSubCats == null || selectedSubCats == "") {
      Fluttertoast.showToast(
          msg: "Please select at least one category to proceed.",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    } else
      saveProductCat();
  }

  saveProductCat() async {
    var res = await requests.saveProductCat(
        storeId, widget.merchantid, selectedSubCats, widget.token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      changeStep();
    }
  }

  showStoreTypes() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (_) {
          return AlertDialog(
            title: Text(
              "Select store type",
              style: smallHeading,
            ),
            content: Container(
              height: MediaQuery.of(context).size.height - 60,
              width: MediaQuery.of(context).size.width,
              child: GridView.count(
                shrinkWrap: true,
                childAspectRatio: 4 / 2,
                crossAxisCount: 2,
                crossAxisSpacing: 4,
                mainAxisSpacing: 12,
                children: storeTypes.data.map((e) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      setState(() {
                        storeType = e.catName.toString();
                        categoryId = e.id.toString();
                        storeSubCategories = null;
                        storeSubCat = "Choose store sub category";
                        subCatId = null;
                      });
                      getStoreSubCat();
                    },
                    child: Container(
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            color:
                                storeType == e.catName ? mColor : Colors.white,
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(color: mColor)),
                        child: Center(
                            child: Text(
                          e.catName.toString(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 12,
                              color: storeType == e.catName
                                  ? Colors.white
                                  : mColor),
                        ))),
                  );
                }).toList(),
                padding: EdgeInsets.symmetric(vertical: 4),
              ),
            ),
          );
        });
  }

  showStoreSubTypes() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (_) {
          return AlertDialog(
            title: Text(
              "Select store sub-category",
              style: smallHeading,
            ),
            content: Container(
              height: MediaQuery.of(context).size.height - 60,
              width: MediaQuery.of(context).size.width,
              child: GridView.count(
                shrinkWrap: true,
                childAspectRatio: 4 / 2,
                crossAxisCount: 2,
                crossAxisSpacing: 4,
                mainAxisSpacing: 12,
                children: storeSubCategories.data.map((e) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      setState(() {
                        storeSubCat = e.name.toString();
                        subCatId = e.id.toString();
                      });
                    },
                    child: Container(
                        padding: EdgeInsets.all(8),
                        decoration: BoxDecoration(
                            color:
                                storeSubCat == e.name ? mColor : Colors.white,
                            borderRadius: BorderRadius.circular(12),
                            border: Border.all(color: mColor)),
                        child: Center(
                            child: Text(
                          e.name.toString(),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 12,
                              color: storeSubCat == e.name
                                  ? Colors.white
                                  : mColor),
                        ))),
                  );
                }).toList(),
                padding: EdgeInsets.symmetric(vertical: 4),
              ),
            ),
          );
        });
  }

  showAddSubCat() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Text(
              "Add a sub-category",
              style: subHeading,
            ),
            content: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    controller: subCatNameController,
                    style: TextStyle(fontSize: 14),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        contentPadding: EdgeInsets.all(8),
                        focusColor: Colors.white38,
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black12),
                            borderRadius: BorderRadius.circular(4)),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black12),
                            borderRadius: BorderRadius.circular(4)),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black12),
                            borderRadius: BorderRadius.circular(4)),
                        counterText: "",
                        hintText: "Sub-Category name",
                        labelStyle: TextStyle(color: Colors.grey)),
                  ),
                ],
              ),
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
            actions: <Widget>[
              ButtonTheme(
                height: 40,
                minWidth: 100,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                child: RaisedButton(
                  onPressed: () {
                    Navigator.pop(context);
                    if (subCatNameController.text.isNotEmpty ||
                        subCatNameController.text.length > 0) addSubCat();
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.add,
                        color: Colors.white,
                      ),
                      Text(
                        "Add",
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        });
  }

  addSubCat() async {
    print("SELECTED SUB CAT: $subCatId");
    var res = await requests.addStoreSubCat(categoryId, widget.merchantid,
        subCatNameController.text.toString(), widget.token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Fluttertoast.showToast(
          msg: res['message'].toString(),
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
      setState(() {
        storeSubCategories = null;
        getStoreSubCat();
      });
    } else {
      Fluttertoast.showToast(
          msg: res['message'].toString(),
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    }
    setState(() {
      subCatNameController.text = "";
    });
  }

  saveStoreAddress() async {
    var res = await requests.saveStoreAddress(
        house.text.toString(),
        city.text.toString(),
        storeId,
        country.text.toString(),
        "28.2996699",
        "76.9742072",
        streetName.text.toString(),
        pincode.text.toString(),
        state.text.toString(),
        storeLocation.text.toString(),
        widget.merchantid,
        widget.token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      changeStep();
    }
    {
      Fluttertoast.showToast(
          msg: res['message'].toString(),
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    }
  }

  saveDeliverySettings() async {
    var res = await requests.saveShippingSettings(
        deliveryCharge.text.toString(),
        deliveryCloseTime.text.toString(),
        deliveryRadius.text.toString(),
        deliveryOpenTime.text.toString(),
        widget.merchantid,
        deliveryTime.text.toString(),
        minOrder.text.toString(),
        closeTime.text.toString(),
        offDays
            .toString()
            .replaceAll('[', "")
            .replaceAll(']', "")
            .replaceAll(" ", ""),
        storeId,
        openTime.text.toString(),
        widget.token);

    if (res != null && res['status'].toString().toLowerCase() == "success") {
      changeStep();
    } else {
      Fluttertoast.showToast(
          msg: "Something went wrong!",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    }
  }

  validateStep2() {
    if (cNameController.text == "" || cNameController.text.length < 3)
      Fluttertoast.showToast(msg: "Invalid detail: Store Name");
    else if (cDescription.text == "" || cNameController.text.length < 5)
      Fluttertoast.showToast(msg: "Invalid detail: Store Description");
    else if (numberController.text.length < 10)
      Fluttertoast.showToast(msg: "Invalid detail: Store Contact Number");
    else if (categoryId == null)
      Fluttertoast.showToast(msg: "Invalid detail: Select Store Type");
    else if (subCatId == null)
      Fluttertoast.showToast(msg: "Invalid detail: Select Store Sub-Category");
    else if (logo == null)
      Fluttertoast.showToast(msg: "Invalid detail: Upload Store Logo");
    else if (gstinController.text != "" && gstinController.text.length < 15)
      Fluttertoast.showToast(msg: "Invalid detail: GST Number");
    else if (panController.text == "" || panController.text.length < 10)
      Fluttertoast.showToast(msg: "Invalid detail: Pan Card Number");
    else if (aadharController.text == "" || aadharController.text.length < 12)
      Fluttertoast.showToast(msg: "Invalid detail: Aadhar Card Number");
    else if (fssaiController.text != "" && fssaiController.text.length < 10)
      Fluttertoast.showToast(msg: "Invalid detail: FSSAI Number");
    else if (aadharImage == null)
      Fluttertoast.showToast(msg: "Invalid detail: Upload Aadhar Card Image");
    else if (panCardImage == null)
      Fluttertoast.showToast(msg: "Invalid detail: Upload Pan Card Image");
    else
      saveStoreDetails();
  }

  validateStep4() {
    if (storeLocation.text == "" || storeLocation.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Store Location");
    } else if (house.text == "" || house.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Address Line");
    } else if (streetName.text == "" || streetName.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Locality");
    } else if (pincode.text == "" || pincode.text.length < 6) {
      Fluttertoast.showToast(msg: "Invalid detail: Pincode");
    } else if (city.text == "" || city.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: City");
    } else if (state.text == "" || state.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: State");
    } else if (country.text == "" || country.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Country/Region");
    } else {
      saveStoreAddress();
    }
  }

  validateStep5() {
    if (deliveryTime.text == "" || deliveryTime.text.length < 0) {
      Fluttertoast.showToast(msg: "Invalid detail: Min. Delivery Time");
    } else if (deliveryRadius.text == "" || deliveryRadius.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Set Delivery Radius");
    } else if (deliveryCharge.text == "" || deliveryCharge.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Delivery Charge");
    } else if (minOrder.text == "" || minOrder.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Minimum Order");
    } else if (openTime.text == "" || openTime.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Set Store Opening Time");
    } else if (closeTime.text == "" || closeTime.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Set Store Closing Time");
    } else if (deliveryCloseTime.text == "" ||
        deliveryCloseTime.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Set Delivery Closing Time");
    } else if (deliveryOpenTime.text == "" ||
        deliveryOpenTime.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Set Delivery Opening Time");
    } else {
      saveDeliverySettings();
    }
  }

  setImage(int imageType, String imageString) {
    switch (imageType) {
      case 1:
        logo = imageString;
        break;
      case 2:
        panCardImage = imageString;
        break;
      case 3:
        aadharImage = imageString;
        break;
    }
  }

  openBottomSheet(int imageType) {
    String base64ImageString;
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (context) {
          return Container(
              height: MediaQuery.of(context).size.height * .2,
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      image = await getImage(ImageSource.camera, ratio);
                      if (image != null) {
                        base64ImageString = convertImageToBase64(image);
                        setImage(imageType, base64ImageString);
                      }
                      setState(() {});
                    },
                    leading: Icon(Icons.camera_alt),
                    title: Text("Camera"),
                  ),
                  ListTile(
                    onTap: () async {
                      Navigator.pop(context);
                      image = await getImage(ImageSource.gallery, ratio);
                      if (image != null) {
                        base64ImageString = convertImageToBase64(image);
                        setImage(imageType, base64ImageString);
                      }
                      setState(() {});
                    },
                    leading: Icon(Icons.photo_library),
                    title: Text("Gallery"),
                  ),
                ],
              ));
        });
  }

  @override
  void initState() {
    // TODO: implement initState
    getStoreTypes();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBody: true,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0.0,
          automaticallyImplyLeading: false,
          title: Text(appBarTitle),
          leading: IconButton(
              onPressed: (){
                setState(() {
                  if(step > 0)
                    step--;
                  switch(step){
                    case 0: appBarTitle = "";
                    break;
                    case 1: appBarTitle = "Fill your store details";
                    break;
                    case 2: appBarTitle = "Choose category";
                    break;
                    case 3: appBarTitle = "Fill store address";
                    break;
                    case 4: appBarTitle = "Delivery settings";
                    break;
                    case 5: appBarTitle = "Store creation complete";
                    break;
                  }
                  pageController.animateToPage(step, duration: Duration(milliseconds: 200), curve: Curves.decelerate);
                });
              },
              icon: Icon(Icons.arrow_back, color: Colors.white)
          ),

        ),

        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: step == 5? SizedBox(): Column(
                children: <Widget>[
                  Text("Step ${step + 1} of 5" ,style: subHeading,),
                  SizedBox(height: 5,),
                  Row(
                    children: <Widget>[
                      Expanded(child: Container(
                        height: 4,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: step >= 0? mColor: Colors.grey,
                        ),
                      ),),
                      SizedBox(width: 2,),
                      Expanded(child: Container(
                        height: 4,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: step >= 1? mColor: Colors.grey,
                        ),
                      ),),
                      SizedBox(width: 2,),
                      Expanded(child: Container(
                        height: 4,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: step >= 2? mColor: Colors.grey,
                        ),
                      ),),
                      SizedBox(width: 2,),
                      Expanded(child: Container(
                        height: 4,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: step >= 3? mColor: Colors.grey,
                        ),
                      ),),
                      SizedBox(width: 2,),
                      Expanded(child: Container(
                        height: 4,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: step >= 4? mColor: Colors.grey,
                        ),
                      ),),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
              child: PageView(
                controller: pageController,
                scrollDirection: Axis.horizontal,
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  step1(),
                  step2(),
                  step3(),
                  step4(),
                  step5(),
                  congratulations(),
                ],
              ),
            ),
          ],
        ),

        bottomNavigationBar: step == 5? bottomNavigationBar2(): bottomNavigationBar()

    );
  }

  congratulations(){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 30,
          ),
          Placeholder(
            fallbackHeight: 180,
            fallbackWidth: 180,
          ),
          SizedBox(
            height: 80,
          ),
          Text("Congratulations", style: TextStyle(color: mColor, fontSize: 24, fontWeight: FontWeight.bold),),
          SizedBox(
            height: 20,
          ),
          Text("Your store is registered with us.", style: subHeading,
            textAlign: TextAlign.center,),
          Text("You may create your inventory.", style: subHeading,
              textAlign: TextAlign.center),
        ],
      ),
    );
  }

  List<String> offDays = [];
  Map<int, String> wDays = {
    1: "Mon",
    2: "Tue",
    3: "Wed",
    4: "Thu",
    5: "Fri",
    6: "Sat",
    7: "Sun",
  };

  selectDay(int key) {
    if(offDays.contains(key.toString())){
      setState(() {
        offDays.remove(key.toString());
      });
      print(
          offDays.toString().replaceAll('[', "").replaceAll(']', "").replaceAll(
              " ", ""));
    }else{
      setState(() {
        offDays.add(key.toString());
        print(offDays.toString().replaceAll('[', "")
            .replaceAll(']', "")
            .replaceAll(" ", ""));
      });
    }
  }

  Widget weekDays() {
    return Container(
        height: 50,
        child: ListView(
          itemExtent: 55,
          scrollDirection: Axis.horizontal,
          children: wDays.entries.map((e) {
            return GestureDetector(
              onTap: () {
                selectDay(e.key);
              },
              child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 2),
                  decoration: BoxDecoration(
                    color: offDays.contains(e.key.toString())
                        ? Colors.red
                        : Colors.green,
                  ),
                  child: Center(child: Text(e.value.toString(),
                    style: TextStyle(color: Colors.white),),)
              ),
            );
          }).toList(),
        )
    );
  }

  toggleHomeDelivery() {
    setState(() {
      if (homeDelivery) {
        homeDelivery = false;
        deliveryTime.text = "0";
        deliveryRadius.text = "0";
        deliveryOpenTime.text = "0";
        deliveryCloseTime.text = "0";
      } else {
        homeDelivery = true;
        deliveryTime.text = "";
        deliveryRadius.text = "";
        deliveryOpenTime.text = "";
        deliveryCloseTime.text = "";
      }
    });
  }

  step5() {
    return Column(
      children: <Widget>[
        Expanded(
          child: ListView(
            padding: EdgeInsets.only(bottom: 90, top: 8, left: 8, right: 8),
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Text("Home Delivery"),
                      Switch.adaptive(
//                  title: Text("Home Delivery"),
                        value: homeDelivery,
                        onChanged: (val) {
                          toggleHomeDelivery();
                        },
                      ),
                    ],
                  ),
                  !homeDelivery ? SizedBox() : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 8,),
                      Text(
                        "Set Minimum Delivery Time (In Minutes)",
                        textAlign: TextAlign.left,
                        style: smallHeading,
                      ),
                      TextFormField(
                        enabled: true,
                        controller: deliveryTime,
                        inputFormatters: [
                          WhitelistingTextInputFormatter(RegExp("[0-9]"))
                        ],
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 12, horizontal: 8),
                            isDense: true,
                            hasFloatingPlaceholder: true,
                            hintText: "15 minutes",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black))),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Text(
                        "Set Delivery Radius (In Kilometers)",
                        textAlign: TextAlign.left,
                        textScaleFactor: 1,
                        style: smallHeading,
                      ),
                      TextFormField(
                        enabled: true,
                        controller: deliveryRadius,
                        inputFormatters: [
                          WhitelistingTextInputFormatter(RegExp("[0-9]"))
                        ],
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 12, horizontal: 8),
                            isDense: true,
                            hasFloatingPlaceholder: true,
                            hintText: "4 km",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black))),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Delivery Opening Time",
                                  textAlign: TextAlign.left,
                                  textScaleFactor: 1,
                                  style: smallHeading,
                                ),
                                TextFormField(
                                  onTap: () {
                                    DatePicker.showTime12hPicker(
                                        context, onConfirm: (time) {
                                      setState(() {
                                        deliveryOpenTime.text =
                                        "${time.hour.toString()} : ${time.minute
                                            .toString()}";
                                      });
                                    });
                                  },
                                  controller: deliveryOpenTime,
                                  enabled: true,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter(
                                        RegExp("[0-9]"))
                                  ],
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.symmetric(
                                          vertical: 12, horizontal: 8),
                                      isDense: true,
                                      hasFloatingPlaceholder: true,
                                      hintText: "Tap to Set",
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black))),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Delivery Closing Time",
                                  textAlign: TextAlign.left,
                                  textScaleFactor: 1,
                                  style: smallHeading,
                                ),
                                TextFormField(
                                  onTap: () {
                                    DatePicker.showTime12hPicker(
                                        context, onConfirm: (time) {
                                      setState(() {
                                        deliveryCloseTime.text =
                                        "${time.hour.toString()} : ${time.minute
                                            .toString()}";
                                      });
                                    });
                                  },
                                  controller: deliveryCloseTime,
                                  enabled: true,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter(
                                        RegExp("[0-9]"))
                                  ],
                                  decoration: InputDecoration(
                                      contentPadding: EdgeInsets.symmetric(
                                          vertical: 12, horizontal: 8),
                                      isDense: true,
                                      hasFloatingPlaceholder: true,
                                      hintText: "Tap to Set",
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black))),
                                ),
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 8,),
                  Text(
                    "Select Store Close Days",
                    textAlign: TextAlign.left,
                    style: smallHeading,
                  ),
                  weekDays(),
                  SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Delivery Charge(In Rs.)",
                              textAlign: TextAlign.left,
                              textScaleFactor: 1,
                              style: smallHeading,
                            ),
                            TextFormField(
                              enabled: true,
                              controller: deliveryCharge,
                              inputFormatters: [
                                WhitelistingTextInputFormatter(RegExp("[0-9]"))
                              ],
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 12, horizontal: 8),
                                  isDense: true,
                                  hasFloatingPlaceholder: true,
                                  hintText: " Rs. 99",
                                  border: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.black))),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 5,),
                      Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Minimum Order(In Rs.)",
                              textAlign: TextAlign.left,
                              textScaleFactor: 1,
                              style: smallHeading,
                            ),
                            TextFormField(
                              enabled: true,
                              controller: minOrder,
                              inputFormatters: [
                                WhitelistingTextInputFormatter(RegExp("[0-9]"))
                              ],
                              decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 12, horizontal: 8),
                                  isDense: true,
                                  hasFloatingPlaceholder: true,
                                  hintText: " Rs. 399",
                                  border: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.black))),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    "Store Opening Time",
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: smallHeading,
                  ),
                  TextFormField(
                    onTap: () {
                      DatePicker.showTime12hPicker(context, onConfirm: (time) {
                        setState(() {
                          openTime.text =
                          "${time.hour.toString()} : ${time.minute.toString()}";
                        });
                      });
                    },
                    controller: openTime,
                    enabled: true,
                    inputFormatters: [
                      WhitelistingTextInputFormatter(RegExp("[0-9]"))
                    ],
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        isDense: true,
                        hasFloatingPlaceholder: true,
                        hintText: "Tap to Set",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text("Store Closing Time",
                    textAlign: TextAlign.left,
                    textScaleFactor: 1,
                    style: smallHeading,
                  ),
                  TextFormField(
                    onTap: () {
                      DatePicker.showTime12hPicker(context, onConfirm: (time) {
                        setState(() {
                          closeTime.text =
                          "${time.hour.toString()} : ${time.minute.toString()}";
                        });
                      });
                    },
                    controller: closeTime,
                    enabled: true,
                    inputFormatters: [
                      WhitelistingTextInputFormatter(RegExp("[0-9]"))
                    ],
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        isDense: true,
                        hasFloatingPlaceholder: true,
                        hintText: "Tap to Set",
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                ],
              ),
            ],
          ),
        )
      ],
    );
  }

  updateDetail(){
    setState(() {
      storeLocation.text = address.first.addressLine;
      streetName.text = "${address.first.subLocality}, ${address.first.locality}";
      pincode.text = address.first.postalCode;
      country.text = address.first.countryName;
      city.text = address.first.subLocality;
      state.text = address.first.locality;
    });
  }

  step4(){
    return Column(
      children: <Widget>[
        Expanded(
          child: ListView(
            padding: EdgeInsets.only(bottom: 90, top: 8, left: 8, right: 8),
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Add Store Location",
                    textAlign: TextAlign.left,
                    style: smallHeading,
                  ),
                  TextFormField(
                    controller: storeLocation,
                    enabled: true,
                    decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                        suffixIcon: IconButton(
                          onPressed: () async {
                            address = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SearchLocation()));
                            if (address != null) updateDetail();
                          },
                          icon: Icon(
                            Icons.my_location,
                            color: Colors.black,
                          ),
                        ),
                        hintText: "Enter your store location",
                        isDense: true,
                        hasFloatingPlaceholder: true,
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.black))),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    "Store Address (*Customers will pick up packages from this address)",
                    textAlign: TextAlign.left,
                    style: smallHeading,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        border: Border.all(color: Colors.black)),
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.topRight,
                            child: RichText(
                              text: TextSpan(children: <TextSpan>[
                                TextSpan(
                                    text: "*", style: TextStyle(color: mColor)),
                                TextSpan(
                                    text: " Mandatory fields",
                                    style: TextStyle(color: Colors.black)),
                              ]),
                            ),
                          ),
                          TextField(
                            controller: house,
                            scrollPadding: EdgeInsets.all(8.0),
                            inputFormatters: [
                              WhitelistingTextInputFormatter(
                                  RegExp("[a-zA-Z 0-9 - ,-/]"))
                            ],
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 12),
                              hintText: "Address Line*",
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          TextField(
                            controller: streetName,
                            scrollPadding: EdgeInsets.all(8.0),
                            inputFormatters: [
                              WhitelistingTextInputFormatter(
                                  RegExp("[a-zA-Z 0-9 - ,-/]"))
                            ],
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 12,),
                              hintText: "Locality*",
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          TextField(
                            controller: pincode,
                            maxLength: 6,
                            maxLengthEnforced: true,
                            inputFormatters: [
                              WhitelistingTextInputFormatter(RegExp("[0-9]"))
                            ],
                            keyboardType: TextInputType.numberWithOptions(),
                            scrollPadding: EdgeInsets.all(8.0),
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 12),
                              counterText: "",
                              hintText: "Pincode*",
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  controller: city,
                                  scrollPadding: EdgeInsets.all(8.0),
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter(
                                        RegExp("[a-zA-Z 0-9 - ,]"))
                                  ],
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(vertical: 12),
                                    hintText: "City*",
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Expanded(
                                flex: 1,
                                child: TextField(
                                  controller: state,
                                  scrollPadding: EdgeInsets.all(8.0),
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter(
                                        RegExp("[a-zA-Z 0-9 - ,]"))
                                  ],
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(vertical: 12),
                                    hintText: "State*",
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          TextField(
                            controller: country,
                            scrollPadding: EdgeInsets.all(8.0),
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 12),
                              hintText: "Country/Region*",
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }


  step2(){
    return Column(
      children: <Widget>[
        Expanded(
          child: ListView(
              padding: EdgeInsets.only(bottom: 90, top: 8, left: 8, right: 8),
              children: <Widget>[ Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: mColor)
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Store name",
                          textAlign: TextAlign.left,
                          style: smallHeading,
                        ),
                        TextFormField(
                          controller: cNameController,
                          enabled: true,
                          onChanged: (val) {
                            shopName = cNameController.text;
                          },
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 12),
                              errorText: cNameError,
                              isDense: true,
                              hasFloatingPlaceholder: true,
                              hintText: "Store Name",
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white)),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white))),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: mColor)
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "About Store",
                          textAlign: TextAlign.left,
                          style: smallHeading,
                        ),
                        TextFormField(
                          controller: cDescription,
                          enabled: true,
                          maxLines: 2,
                          maxLength: 150,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(vertical: 12),
                            errorText: cDescError,
                            isDense: true,
                            hasFloatingPlaceholder: true,
                            hintText: "Write short description about the store",

                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: mColor)
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Store Contact Number",
                          textAlign: TextAlign.left,
                          style: smallHeading,
                        ),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            WhitelistingTextInputFormatter(RegExp("[0-9]"))
                          ],
                          maxLengthEnforced: true,
                          maxLength: 10,
                          decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white)),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white)),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 12),
                              counterText: "",
//                              prefixIcon: Icon(Icons.phone_iphone),
                              hintText: "000 000 0000",
//                            labelText: "Enter Mobile Number",
                              hasFloatingPlaceholder: true,
                              labelStyle: TextStyle(color: Colors.grey)),
                          controller: numberController,
                          onChanged: (val) {
                            storeNumber = numberController.text;
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        border: Border.all(color: mColor)
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Store Type",
                          textAlign: TextAlign.left,
                          style: smallHeading,
                        ),
                        SizedBox(height: 4,),
                        Row(
                          children: <Widget>[
                            Text(storeType.toString(), maxLines: 1,
                              overflow: TextOverflow.ellipsis,),
                            SizedBox(width: 8,),
                            IconButton(
                              icon: Icon(Icons.arrow_drop_down),
                              onPressed: () {
                                showStoreTypes();
                              },
                            ),
//                            PopupMenuButton<String>(
//                              onCanceled: (){
//                                setState(() {
//                                  storeSubCat = "Choose store category";
//                                });
//                              },
//                              itemBuilder: (context) {
//                                return storeTypeList.map((str) {
//                                  return PopupMenuItem(
//                                    value: str,
//                                    child: Text(str),
//                                  );
//                                }).toList();
//                              },
//                              child: Icon(Icons.arrow_drop_down,
//                                  color: Colors.black),
//                              onSelected: (v) {
//                                setState(() {
//                                  storeType = v;
//                                  print(storeType);
//                                });
//                              },
//                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 8,),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          padding: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(color: mColor)
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Store Sub-Category",
                                textAlign: TextAlign.left,
                                style: smallHeading,
                              ),
                              SizedBox(height: 4,),
                              Row(
                                children: <Widget>[
                                  Text(storeSubCat.toString(), maxLines: 1,
                                    overflow: TextOverflow.ellipsis,),
                                  SizedBox(width: 8,),
                                  IconButton(
                                    icon: Icon(Icons.arrow_drop_down),
                                    onPressed: storeSubCategories == null
                                        ? null
                                        : () {
                                      showStoreSubTypes();
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      IconButton(
                        onPressed: storeSubCategories == null ? null : () {
                          showAddSubCat();
                        },
                        icon: Icon(
                          Icons.add_circle_outline, color: mColor, size: 28,),
                      )
                    ],
                  ),
                  SizedBox(height: 8,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Container(
                          height: 65,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(color: mColor)),
                          child: GestureDetector(
                            onTap: () {
                              openBottomSheet(1);
                            },
                            child: Container(
                                height: 65,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(color: Colors.black)),
                                child: logo == null
                                    ?
                                Center(
                                    child: Icon(FlutterIcons.add_a_photo_mdi))
                                    :
                                Image.memory(Base64Decoder().convert(logo))
//                              FadeInImage(
//                                placeholder: AssetImage('assets/tsp.png'),
//                                image: NetworkImage(storeDetails.info.storeDetail.logo),
//                                fit: BoxFit.cover,
//                              )
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 5,),
                      Expanded(
                        flex: 4,
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(color: mColor)),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Upload Store Images",
                                  textAlign: TextAlign.left,
                                  style: smallHeading,
                                ),
                                Text("You are required to upload store images from My Store setup screen later", style: regularText,)
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    "Update Store Validation Details",
                    textAlign: TextAlign.left,
                    style: subHeading,
                  ),
                  Text("(Minimum 1 required)", style: subHeading,),
                  Divider(),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(color: mColor)
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Enter GSTIN Number",
                                textAlign: TextAlign.left,
                                style: smallHeading,
                              ),
                              TextFormField(
                                inputFormatters: [
                                  WhitelistingTextInputFormatter(
                                      RegExp("[0-9, A-Z, a-z]"))
                                ],
                                maxLengthEnforced: true,
                                maxLength: 15,
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(
                                        vertical: 12),
                                    counterText: "",
                                    hintText: "enter gstin number here...",
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white)),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white)),
                                    hasFloatingPlaceholder: true,
                                    labelStyle: TextStyle(color: Colors.grey)),
                                controller: gstinController,
                                onChanged: (text) {
                                  gstNo = gstinController.text.toUpperCase();
                                },
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          children: <Widget>[

                            Expanded(
                              flex: 1,
                              child: Container(
                                height: 65,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(color: mColor)),
                                child: GestureDetector(
                                  onTap: () {
                                    openBottomSheet(2);
                                  },
                                  child: Container(
                                      height: 65,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              8),
                                          border: Border.all(
                                              color: Colors.black)),
                                      child: panCardImage == null ?
                                      Center(child: Icon(
                                          FlutterIcons.add_a_photo_mdi)) :
                                      Image.memory(Base64Decoder().convert(
                                          panCardImage))
//                              FadeInImage(
//                                placeholder: AssetImage('assets/tsp.png'),
//                                image: NetworkImage(storeDetails.info.storeDetail.logo),
//                                fit: BoxFit.cover,
//                              )
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 5,),
                            Expanded(
                              flex: 4,
                              child: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(color: mColor)
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Enter Pan Number",
                                      textAlign: TextAlign.left,
                                      style: smallHeading,
                                    ),
                                    TextFormField(
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter(
                                            RegExp("[0-9, A-Z, a-z]"))
                                      ],
                                      maxLengthEnforced: true,
                                      maxLength: 10,
                                      decoration: InputDecoration(
                                          contentPadding: EdgeInsets
                                              .symmetric(vertical: 12),
                                          counterText: "",
                                          hintText: "AAAPL1234C",
                                          enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white)),
                                          focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white)),
                                          border: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white)),
                                          hasFloatingPlaceholder: true,
                                          labelStyle: TextStyle(
                                              color: Colors.grey)),
                                      controller: panController,
                                      onChanged: (text) {
                                        panCard =
                                            panController.text.toUpperCase();
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          children: <Widget>[

                            Expanded(
                              flex: 1,
                              child: Container(
                                height: 65,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(color: mColor)),
                                child: GestureDetector(
                                  onTap: () {
                                    openBottomSheet(3);
                                },
                                  child: Container(
                                      height: 65,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              8),
                                          border: Border.all(
                                              color: Colors.black)),
                                      child: aadharImage == null ?
                                      Center(child: Icon(
                                          FlutterIcons.add_a_photo_mdi)) :
                                      Image.memory(Base64Decoder().convert(
                                          aadharImage))
//                              FadeInImage(
//                                placeholder: AssetImage('assets/tsp.png'),
//                                image: NetworkImage(storeDetails.info.storeDetail.logo),
//                                fit: BoxFit.cover,
//                              )
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 5,),
                            Expanded(
                              flex: 4,
                              child: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    border: Border.all(color: mColor)
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Enter Aadhaar Card Number",
                                      textAlign: TextAlign.left,
                                      style: smallHeading,
                                    ),
                                    TextFormField(
                                      keyboardType: TextInputType.number,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter(
                                            RegExp("[0-9]"))
                                      ],
                                      maxLengthEnforced: true,
                                      maxLength: 12,
                                      decoration: InputDecoration(
                                          contentPadding: EdgeInsets
                                              .symmetric(vertical: 12,),
                                          counterText: "",
                                          hintText: "0000-0000-0000",
                                          enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white)),
                                          focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white)),
                                          border: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white)),
                                          hasFloatingPlaceholder: true,
                                          labelStyle: TextStyle(
                                              color: Colors.grey)),
                                      controller: aadharController,
                                      onChanged: (text) {
                                        aadharCard = aadharController.text
                                            .toUpperCase();
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Container(
                          padding: EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              border: Border.all(color: mColor)
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Enter FSSAI Number",
                                textAlign: TextAlign.left,
                                style: smallHeading,
                              ),
                              TextFormField(
                                inputFormatters: [
                                  WhitelistingTextInputFormatter(
                                      RegExp("[0-9, A-Z, a-z]"))
                                ],
                                maxLengthEnforced: true,
                                maxLength: 15,
                                decoration: InputDecoration(
                                    contentPadding: EdgeInsets.symmetric(vertical: 12),
                                    counterText: "",
                                    hintText: "0000-0000-0000-00",
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white)),
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white)),
                                    hasFloatingPlaceholder: true,
                                    labelStyle: TextStyle(
                                        color: Colors.grey)),
                                controller: fssaiController,
                                onChanged: (text) {
                                  fssai = fssaiController.text.toUpperCase();
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ].toList(),
              ),
              ]
          ),
        ),
      ],
    );
  }

  List<Info> searchList = [];
  bool searching = false;

  searchCat(String val){
    searchList.clear();
    subCategory.info.forEach((element) {
      if(element.subCategoryName.toLowerCase().contains(val.toLowerCase())){
        searchList.add(element);
      }
    });
    setState(() {});
  }

  step3(){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SingleChildScrollView(
        padding: EdgeInsets.only(bottom: 90),
        child: Column(
          children: <Widget>[
            TextFormField(
              onChanged: (val){
                if(val.length == 0 || val == "") {
                  setState(() {
                    searching = false;
                    getSubCategory();
                  });
                } else {
                  setState(() {
                    searching = true;
                  });
                  searchCat(val);
                }
              },
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(16),
                  filled: true,
                  fillColor: textBoxFillColor,
                  focusColor: Colors.white38,
                  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
                  border: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
                  enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.white), borderRadius: BorderRadius.circular(4)),
                  counterText: "",
                  suffixIcon: Icon(Icons.search, color: Colors.black),
                  hintText: "Search Category",
                  labelStyle: TextStyle(color: Colors.grey)
              ),
              controller: searchController,
            ),
            SizedBox(
              height: 15,
            ),
//            ChipsChoice.single(
//                value: categories,
//                isWrapped: true,
//                itemConfig: ChipsChoiceItemConfig(
//                  showCheckmark: true,
//                  spacing: 5,
//                  unselectedColor: mColor,
//                ),
//                options: ChipsChoiceOption.listFrom<String, String>(
//                  source: categories,
//                  value: (i, v) => v,
//                  label: (i, v) => v,
//                ),
//                onChanged: (val){
//                  setState(() {
//                    categories.removeWhere((element) => element == val);
//                    selectedCat.removeWhere((key, value) => value == val);
//                  });
//                }),
            subCategory == null ? SizedBox() : Wrap(
              spacing: 8,
              alignment: WrapAlignment.start,
              runSpacing: 12,
              children: List.generate(searching? searchList.length + 1: subCategory.info.length + 1, (index) {
                return index == 0 ? addProductContainer() : buildContainer(
                    searching? searchList[index - 1]:  subCategory.info[index - 1]);
              }),
            )
          ],
        ),
      ),
    );
  }

  List<String> categories = [];
  Map<int, String> selectedCat = {};
  TextEditingController catNameController = TextEditingController();

  addProductPrompt() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Text("Add a product category", style: subHeading,),
            content: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    controller: catNameController,
                    style: TextStyle(fontSize: 14),
                    decoration: InputDecoration(
                        hintStyle: TextStyle(fontSize: 14),
                        contentPadding: EdgeInsets.all(8),
                        focusColor: Colors.white38,
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12), borderRadius: BorderRadius.circular(4)),
                        border: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12), borderRadius: BorderRadius.circular(4)),
                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.black12), borderRadius: BorderRadius.circular(4)),
                        counterText: "",
                        hintText: "Category name",
                        labelStyle: TextStyle(color: Colors.grey)),
                  ),
                ],
              ),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18)
            ),
            actions: <Widget>[
              ButtonTheme(
                height: 40,
                minWidth: 100,
                shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                child: RaisedButton(
                  onPressed: () {
                    Navigator.pop(context);
                    addNewCat();
//                    setState(() {
//                      categories.add(catNameController.text);
//                      catNameController.text = "";
//                    });
                  },
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.add, color: Colors.white,),
                      Text("Add",
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        }
    );
  }

  addProductContainer() {
    return GestureDetector(
      onTap: (){
        addProductPrompt();
      },
      child: Container(
        height: 100,
        width: 100,
        decoration: BoxDecoration(
            boxShadow: [BoxShadow(color: Colors.black12, offset: Offset(0, 0), blurRadius: 6, spreadRadius: 2
            )],
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: Colors.grey)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.add),
            SizedBox(height: 5,),
            Text("Add a product category", style: smallHeading, textAlign: TextAlign.center,)
          ],
        ),
      ),
    );
  }

  step1(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(16),
                      border: Border.all(color: mColor)
                  ),
                  margin: EdgeInsets.all(8),
                  height: MediaQuery.of(context).size.width / 2 - 12,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 60,
                        height: 60,
                        child: Placeholder(
                          fallbackHeight: 60,
                          fallbackWidth: 60,
                        ),
                      ),
                      SizedBox(height: 8,),
                      Text("Product", style: subHeading,)
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(16),
                      border: Border.all(color: Colors.grey)
                  ),
                  margin: EdgeInsets.all(8),
                  height: MediaQuery.of(context).size.width / 2 - 12,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 60,
                        height: 60,
                        child: Placeholder(
                          fallbackHeight: 60,
                          fallbackWidth: 60,
                        ),
                      ),
                      SizedBox(height: 8,),
                      Text("Service", style: subHeading,)
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  bottomNavigationBar2(){
    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: ButtonTheme(
                height: 50,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6),
                ),
                child: RaisedButton(
                  elevation: 0,
                  textColor: Colors.white,
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => FillStoreDetails()));
                  },
                  child: Text("Setup Store", style: TextStyle(fontSize: 16),),
                ),
              ),
            ),
            SizedBox(width: 5,),
            Expanded(
              child: ButtonTheme(
                height: 50,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(6),
                ),
                child: RaisedButton(
                  elevation: 0,
                  textColor: Colors.white,
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context) => GetStarted(widget.merchantid,
                            widget.token)));
                  },
                  child: Text("Continue", style: TextStyle(fontSize: 16),),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  changeStep() {
    setState(() {
      if (step < 5)
        step++;
      switch (step) {
        case 0:
          appBarTitle = "";
          break;
        case 1:
          appBarTitle = "Fill your store details";
          break;
        case 2:
          appBarTitle = "Choose category";
          break;
        case 3:
          appBarTitle = "Fill store address";
          break;
        case 4:
          appBarTitle = "Delivery settings";
          break;
        case 5:
          appBarTitle = "Store creation complete";
          break;
      }
    });
    pageController.animateToPage(
        step, duration: Duration(milliseconds: 200), curve: Curves.decelerate);
  }

  bottomNavigationBar() {
    return BottomAppBar(
      elevation: 0.0,
      color: Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ButtonTheme(
          height: 50,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: RaisedButton(
            onPressed: () {
              setState(() {
                switch(step){
                  case 0:
                    changeStep();
                    break;
                  case 1:
                    validateStep2();
                    break;
                  case 2:
                    validateStep3();
                    break;
                  case 3:
                    validateStep4();
                    break;
                  case 4:
                    validateStep5();
                    break;
                  case 5:
                    changeStep();
                    break;
                }
                pageController.animateToPage(step, duration: Duration(milliseconds: 200), curve: Curves.decelerate);
              });
            },
            child: Text(
              "Next",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
