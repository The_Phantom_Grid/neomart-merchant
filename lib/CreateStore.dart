import 'package:flutter/material.dart';
import 'package:merchant/StepsCreateStore.dart';

import 'constants.dart';

class CreateStore extends StatelessWidget {
  String merchantid, token;

  CreateStore(this.merchantid, this.token);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    final padding = MediaQuery.of(context).padding.top;
    return Scaffold(
//      appBar: AppBar(
//        automaticallyImplyLeading: false,
//      ),
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Image.asset('assets/neomart_getstarted.png', height: 30,),
        centerTitle: true,
        elevation: 0.0,
        automaticallyImplyLeading: false,
      ),

      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                SizedBox(
                  height: height * .2,
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30))
                    ),
                    padding: EdgeInsets.symmetric(vertical: padding),
                    width: width,
                    child: Center(
                      child: Column(
                        children: <Widget>[
                          Text("Welcome to NeoMart", style: TextStyle(fontSize: 16, color: Colors.white),),
                          Text("Create Your E-Store", style: TextStyle(fontSize: 14, color: Colors.white),),
                        ],
                      ),
                    ),
                  ),
                ),
//                Positioned(
//                  bottom: -70,
//                  left: 0,
//                  right: 0,
//                  child: Column(
//                    crossAxisAlignment: CrossAxisAlignment.center,
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      Container(
//                        height: 80,
//                        width: 80,
//                        decoration: BoxDecoration(
//                            color: Colors.yellow,
//                            shape: BoxShape.circle,
//                            border: Border.all(color: Colors.white, width: 5)
//                        ),
//                      ),
//                      Text("CHETNA STORE ", style: subHeading, maxLines: 1, overflow: TextOverflow.ellipsis, textAlign: TextAlign.center,)
//                    ],
//                  ),
//                )
              ],
            ),
            SizedBox(height: 85,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Stack(
                overflow: Overflow.visible,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 80,
                    child: Container(
                      margin: new EdgeInsets.only(left: 36.0, right: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: textBoxFillColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 30.0),
                        child: Align(alignment: Alignment.centerLeft,child: Text("SETUP YOUR STORE", style: h1, textAlign: TextAlign.start,),),
                      ),
                    ),
                  ),
                  Container(
                    height: 60,
                    width: 60,
                    child: Center(child: Image.asset('assets/getstarted/step-1.png', height: 20, color: Colors.white,)),
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.symmetric(vertical: 12),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 5),
                        color: Colors.teal
                    ),
                  ),
                ],
              ),
            ),

            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Stack(
                overflow: Overflow.visible,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 80,
                    child: Container(
                      margin: new EdgeInsets.only(left: 36.0, right: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: textBoxFillColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 30.0),
                        child: Align(alignment: Alignment.centerLeft,child: Text("PROFILE VERIFICATION AND APPROVAL", style: h1, textAlign: TextAlign.start,),),
                      ),
                    ),
                  ),
                  Container(
                    height: 60,
                    width: 60,
                    child: Center(child: Image.asset('assets/getstarted/step-2.png', height: 20, color: Colors.white,)),
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.symmetric(vertical: 12),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 5),
                        color: Colors.deepOrangeAccent
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Stack(
                overflow: Overflow.visible,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 80,
                    child: Container(
                      margin: new EdgeInsets.only(left: 36.0, right: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: textBoxFillColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 30.0),
                        child: Align(alignment: Alignment.centerLeft,child: Text("CREATE STORE INVENTORY", style: h1, textAlign: TextAlign.start,),),
                      ),
                    ),
                  ),
                  Container(
                    height: 60,
                    width: 60,
                    child: Center(child: Image.asset('assets/getstarted/step-3.png', height: 20, color: Colors.white,)),
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.symmetric(vertical: 12),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 5),
                        color: Colors.indigoAccent
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Stack(
                overflow: Overflow.visible,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 80,
                    child: Container(
                      margin: new EdgeInsets.only(left: 36.0, right: 0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: textBoxFillColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 30.0),
                        child: Align(alignment: Alignment.centerLeft,child: Text("START SELLING", style: h1, textAlign: TextAlign.start,),),
                      ),
                    ),
                  ),
                  Container(
                    height: 60,
                    width: 60,
                    child: Center(child: Image.asset('assets/getstarted/step-4.png', height: 20, color: Colors.white,)),
                    alignment: FractionalOffset.centerLeft,
                    margin: EdgeInsets.symmetric(vertical: 12),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 5),
                        color: Colors.amber
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.transparent,
        elevation: 0,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 50, vertical: 10),
          child: Hero(
            tag: 'getStarted',
            child: ButtonTheme(
              height: 45,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(25))
                ),
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) =>
                          StepsCreateStore(merchantid, token)));
                },
                color: Colors.black,
                textColor: Colors.white,
                child: Text("CREATE STORE"),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
