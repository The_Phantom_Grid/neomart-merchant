//import 'package:flutter/material.dart';
//import 'package:flutter_icons/flutter_icons.dart';
//import 'package:get/get.dart';
//import 'package:qr_code_scanner/qr_code_scanner.dart';
//
//class QrScan extends StatefulWidget {
//  @override
//  _QrScanState createState() => _QrScanState();
//}
//
//class _QrScanState extends State<QrScan> {
//  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
//  var qrText = "";
//  QRViewController controller;
//  bool flash = false, camera = false;
//
//  toggleFlash(){
//    print(flash);
//    setState(() {
//      if(flash) {
//        flash = false;
//      }
//      else{
//        flash = true;
//      }
//      controller.toggleFlash();
//    });
//  }
//
//  toggleCamera(){
//    setState(() {
//      if(camera)
//        camera = false;
//      else camera = true;
//
//      controller.flipCamera();
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      body: Column(
//        children: <Widget>[
//          Expanded(
//            flex: 5,
//            child: QRView(
//              key: qrKey,
//              onQRViewCreated: _onQRViewCreated,
//            ),
//          ),
//          Expanded(
//            flex: 1,
//            child: Container(
//              decoration: BoxDecoration(
//                color: Colors.blueGrey.withOpacity(.2),
//              ),
//              child: Center(
//                child: Row(
//                  mainAxisAlignment: MainAxisAlignment.spaceAround,
//                  children: <Widget>[
//                    GestureDetector(
//                      onTap: (){
//                        toggleFlash();
//                      },
//                      child: Icon(flash? FlutterIcons.flash_off_mdi: FlutterIcons.flash_on_mdi)),
//                    GestureDetector(
//                        onTap: (){
//                          toggleCamera();
//                        },
//                      child: Icon(Icons.switch_camera))
//                  ],
//                ),
//              ),
//            ),
//          )
//        ],
//      ),
//    );
//  }
//
//  void _onQRViewCreated(QRViewController controller) {
//    print("DONE");
//    this.controller = controller;
//    controller.scannedDataStream.listen((scanData) {
//      setState(() {
//        qrText = scanData;
//        Navigator.pop(context, qrText);
//      });
//    });
//  }
//
//  @override
//  void dispose() {
//    controller?.dispose();
//    super.dispose();
//  }
//}
