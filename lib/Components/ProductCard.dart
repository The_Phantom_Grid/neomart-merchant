import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:merchant/Getter/GetCollectionInfo.dart';
import 'package:merchant/Getter/GetOffers.dart';
import 'package:merchant/Getter/GetOrderSummary.dart';
import 'package:merchant/Getter/GetPosCart.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/PosCheckout.dart';

import '../constants.dart';

class ProductCard extends StatefulWidget {
  Product product;
  bool edit;
  String deliveryType, customerUserid;

  ProductCard(this.product, this.edit, this.deliveryType, this.customerUserid);

  @override
  _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> {
  int qty;
  HttpRequests requests = HttpRequests();

  updateProduct() async {
    var res = await requests.addToCartPos(
        widget.deliveryType,
        widget.product.masterproductid.toString(),
        widget.product.merchantlistid.toString(),
        qty.toString(),
        widget.customerUserid,
        token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      posCheckoutState.getCart();
    }
  }

  removeItem(){
    setState(() {
      if(qty > 0) {
        qty--;
        updateProduct();
      }
    });
  }


  addItem(){
    setState(() {
      if(qty == widget.product.totalLeft)
        showToast("Cannot add more of these item.", Colors.redAccent);
      else {
        qty++;
        updateProduct();
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    qty = widget.product.quantity;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          ClipRRect(
              borderRadius: BorderRadius.circular(4),
              child: FadeInImage.assetNetwork(
                  placeholder: 'assets/tsp.png',
                  image: widget.product.icon,
                  height: 55,
                  width: 55)),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text(
                    widget.product.productname,
                    style: productName,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.only(left: 4, right: 4),
                                decoration: BoxDecoration(color: mColor),
                                child: Text(
                                  widget.product.measure,
                                  style: TextStyle(
                                      fontSize: 11, color: Colors.white),
                                )),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.only(left: 4, right: 4),
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(15)),
                                    color: mColor),
                                width: 80,
                                height: 24,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    !widget.edit? SizedBox(): GestureDetector(
                                      onTap: (){ removeItem();},
                                      child: Icon(Icons.remove, color: Colors.white, size: 15,)
                                    ),
                                    Center(
                                        child: Text(
                                      "$qty",
                                      style: TextStyle(
                                          fontSize: 12.5, color: Colors.white),
                                    )),
                                    !widget.edit? SizedBox(): GestureDetector(
                                      onTap: (){ addItem();},
                                      child: Icon(Icons.add, color: Colors.white, size: 15,)
                                    ),
                                  ],
                                )),
                            SizedBox(
                              height: 5,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "SRP: ₹ ${double.tryParse(widget.product.productMrp.toString())}",
                        style: TextStyle(fontSize: 13),
                      ),
                      Text(
                        "₹ ${double.tryParse(widget.product.productMrp.toString())}",
                        style: TextStyle(fontSize: 13, color: mColor),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CollectionProductCard extends StatefulWidget {
  CollectionProductInfo collectionProduct;

  CollectionProductCard(this.collectionProduct);

  @override
  _CollectionProductCardState createState() => _CollectionProductCardState();
}

class _CollectionProductCardState extends State<CollectionProductCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          ClipRRect(
              borderRadius: BorderRadius.circular(4),
              child: FadeInImage.assetNetwork(
                  placeholder: 'assets/tsp.png',
                  image: widget.collectionProduct.productImage,
                  height: 55,
                  width: 55)),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: Text(
                      widget.collectionProduct.productName,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: subHeading,
                    ),
                  ),
                  Icon(
                    FlutterIcons.check_circle_faw,
                    color: Colors.green,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class OrderSummaryProductCard extends StatefulWidget {
  Datum data;

  OrderSummaryProductCard(this.data);

  @override
  _OrderSummaryProductCardState createState() =>
      _OrderSummaryProductCardState();
}

class _OrderSummaryProductCardState extends State<OrderSummaryProductCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          ClipRRect(
              borderRadius: BorderRadius.circular(4),
              child: FadeInImage.assetNetwork(
                  placeholder: 'assets/tsp.png',
                  image: widget.data.icon,
                  height: 55,
                  width: 55)),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text(
                    widget.data.productname,
                    style: productName,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.only(left: 4, right: 4),
                                decoration: BoxDecoration(color: mColor),
                                child: Text(
                                  widget.data.measure,
                                  style: TextStyle(
                                      fontSize: 11, color: Colors.white),
                                )),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment:
                          CrossAxisAlignment.end,
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.only(left: 4, right: 4),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(15)),
                                    color: mColor
                                ),
                                width: 80,
                                height: 24,
                                child: Center(child: Text(
                                  "${widget.data.quantity}", style: TextStyle(
                                    fontSize: 12.5, color: Colors.white),))
                            ),
                            SizedBox(height: 5,),
                            Text("₹ ${double.tryParse(
                                widget.data.mrp.toString()).toStringAsFixed(
                                2)}",
                              style: TextStyle(fontSize: 13, color: mColor),)
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}


