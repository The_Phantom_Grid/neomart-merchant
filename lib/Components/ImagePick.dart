import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;

File image;

Future<File> getImage(
    ImageSource source, List<CropAspectRatioPreset> ratio) async {
  var i = await ImagePicker.pickImage(source: source);
  i = await ImageCropper.cropImage(
      sourcePath: i.path,
      compressFormat: ImageCompressFormat.jpg,
      compressQuality: 0,
      aspectRatioPresets: ratio,
      androidUiSettings: AndroidUiSettings(
          toolbarTitle: 'Cropper',
          toolbarColor: Colors.black,
          toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.original,
          lockAspectRatio: false),
      iosUiSettings: IOSUiSettings(
        minimumAspectRatio: 1.0,
      ));
  if (i != null)
    return image = i;
  else
    return null;
}

String convertImageToBase64(File image) {
  String base64Image;
  if (image != null) {
    List<int> imageBytes = image.readAsBytesSync();
    base64Image = Base64Encoder().convert(imageBytes);
    return base64Image;
  } else {
    return base64Image = null;
  }
}

Future<String> getImageFromNetwork(String imageUrl) async {
  String base64Image;
  try {
    var res = await http.get(imageUrl);
    base64Image = base64Encode(res.bodyBytes);
    return base64Image;
  } catch (Exception) {
    print("EXCEPTION: $Exception");
    return base64Image = null;
  }
}

//getImageFromGallery() async {
//  var i = await ImagePicker.pickImage(source: ImageSource.gallery);
//  i = await ImageCropper.cropImage(
//      sourcePath: i.path,
//      compressFormat: ImageCompressFormat.jpg,
//      compressQuality: 0,
//      aspectRatioPresets: [
//        CropAspectRatioPreset.square,
//        CropAspectRatioPreset.ratio3x2,
//        CropAspectRatioPreset.original,
//        CropAspectRatioPreset.ratio4x3,
//        CropAspectRatioPreset.ratio16x9
//      ],
//      androidUiSettings: AndroidUiSettings(
//          toolbarTitle: 'Cropper',
//          toolbarColor: Colors.black,
//          toolbarWidgetColor: Colors.white,
//          initAspectRatio: CropAspectRatioPreset.original,
//          lockAspectRatio: false),
//      iosUiSettings: IOSUiSettings(
//        minimumAspectRatio: 1.0,
//      )
//  );
//    if (i != null) image = i;
//}
