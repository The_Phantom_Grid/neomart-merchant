import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:merchant/Getter/GetSearchProductPos.dart';
import 'package:merchant/Network/httpRequests.dart';

import '../ChoosInventory.dart';
import '../constants.dart';

class SearchProductPos extends StatefulWidget {
  String deliveryType, customerUserid;
  bool pos;

  SearchProductPos(this.deliveryType, this.customerUserid, this.pos);

  @override
  _SearchProductPosState createState() => _SearchProductPosState();
}

class _SearchProductPosState extends State<SearchProductPos> {
  HttpRequests requests = HttpRequests();
  ProductPos productPos;
  bool loading = false, notFound = false;

//  Map<int, dynamic> products = {};
  TextEditingController searchController = TextEditingController();

  searchMAsterProduct() async {
    var res = await requests.searchProductPos(searchController.text.toString());
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        productPos = ProductPos.fromJson(res);
        loading = false;
        if (productPos.info.isEmpty)
          notFound = true;
        else
          notFound = false;
      });
    }
  }

//  addProductsToInventory() async {
//    print("HIT");
//    var res = await requests.addProductToInventoryNew(products.values.toList());
//    if (res != null && res['status'].toString().toLowerCase() == "success") {
//      Fluttertoast.showToast(
//          msg: "Inventory Updated.",
//          timeInSecForIosWeb: 2,
//          toastLength: Toast.LENGTH_LONG);
//      setState(() {
//        products = {};
//      });
//    }
//  }

  setPageState() {
    if (mounted) setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Search Product",
          style: regularText,
        ),
        bottom: PreferredSize(
          preferredSize: Size(double.infinity, 60),
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: searchController,
                onFieldSubmitted: (text) {
                  setState(() {
                    loading = true;
                  });
                  searchMAsterProduct();
                },
                decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                    filled: true,
                    fillColor: textBoxFillColor,
                    focusColor: Colors.white38,
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(4)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(4)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(4)),
                    counterText: "",
                    suffixIcon: Icon(Icons.search, color: Colors.black),
                    hintText: "search product here...",
                    labelStyle: TextStyle(color: Colors.grey)),
              ),
            ),
          ),
        ),
      ),
      body: loading
          ? loadingCircular()
          : notFound
              ? Center(
                  child: Text(
                    "No result.",
                    style: subHeading,
                  ),
                )
              : productPos == null
                  ? SizedBox()
                  : ListView.separated(
                      padding: EdgeInsets.only(bottom: 70),
                      separatorBuilder: (context, index) {
                        return SizedBox(
                          height: 8,
                        );
                      },
                      itemCount: productPos.info.length,
                      itemBuilder: (context, index) {
                        return ProductPosCard(
                            productPos.info[index],
                            widget.deliveryType,
                            widget.customerUserid,
                            widget.pos);
                      }),
//      floatingActionButton: products.values.length == 0
//          ? SizedBox()
//          : FloatingActionButton.extended(
//          onPressed: () {
//            addProductsToInventory();
//          },
//          label: Text("Update (${products.length})")),
    );
  }
}

class ProductPosCard extends StatefulWidget {
  Info info;
  String customerUserid, deliveryType;
  bool pos;

  ProductPosCard(this.info, this.deliveryType, this.customerUserid, this.pos);

  @override
  _ProductPosCardState createState() => _ProductPosCardState();
}

class _ProductPosCardState extends State<ProductPosCard> {
  HttpRequests requests = HttpRequests();
  int qty = 0;

  addToCollection() {
    chooseInventoryState.products[widget.info.merchantListId] = {
      "merchant_product_id": widget.info.merchantListId.toString(),
      "merchant_product_qty": qty.toString()
    };
    print(chooseInventoryState.products);
  }

  addToPosCart() async {
    var res = await requests.addToCartPos(
        widget.deliveryType,
        widget.info.masterProductId.toString(),
        widget.info.merchantListId.toString(),
        qty.toString(),
        widget.customerUserid,
        token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
//      Fluttertoast.showToast(msg: "Done");
    } else {
      Fluttertoast.showToast(msg: "Something went wrong!");
    }
  }

  addProduct() async {
    if (widget.pos)
      addToPosCart();
    else
      addToCollection();
  }

//  removeProduct() {
//    searchMasterProductState.products.remove(widget.info.mproductId);
//    searchMasterProductState.setPageState();
//    print(searchMasterProductState.products);
//  }

//  addProduct() {
//    if (qty == 0)
//      removeProduct();
//    else {
//      searchMasterProductState.products[widget.info.mproductId] = {
//        "master_product_id": widget.info.mproductId.toString(),
//        "merchant_id": merchantid,
//        "qty": qty.toString(),
//        "srp": widget.info.mrp.toString(),
//        "store_id": storeid
//      };
//      searchMasterProductState.setPageState();
//      print(searchMasterProductState.products);
//    }
//  }

  incrementQty() {
    setState(() {
      if (qty < widget.info.totalLeft) {
        qty++;
        //        addProductToCollection();
        addProduct();
      } else {
        Fluttertoast.showToast(
            msg: "You cannot add more of these product.",
            toastLength: Toast.LENGTH_LONG,
            timeInSecForIosWeb: 2);
      }
    });
  }

  decrementQty() {
    setState(() {
      if (qty > 0) {
        qty--;
        addProduct();
//        addProductToCollection();
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
//    print("PRODUCT BARCODE: ${widget.data.barcode}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(8)),
      margin: EdgeInsets.symmetric(horizontal: 8),
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(4)),
              height: 70,
              width: 70,
              child: FadeInImage.assetNetwork(
                  placeholder: 'assets/tsp.png', image: widget.info.icon)),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.info.productname,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: smallHeading,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                      color: Colors.black,
                      padding: EdgeInsets.symmetric(horizontal: 4),
                      child: Text(
                        widget.info.measure,
                        style: TextStyle(fontSize: 12, color: Colors.white),
                      )),
                  Row(
                    children: <Widget>[
                      Expanded(
                          child: RichText(
                        text: TextSpan(children: [
                          TextSpan(
                              text: "Mrp: ",
                              style:
                                  TextStyle(fontSize: 12, color: Colors.black)),
                          TextSpan(
                              text: "₹ ${widget.info.mrp.toString()}",
                              style: mrpTextStyle),
                        ]),
                      )),
                      Container(
                        height: 36,
                        width: 120,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(18),
                            border: Border.all(color: Colors.black)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: IconButton(
                                onPressed: qty <= 0
                                    ? null
                                    : () {
                                        decrementQty();
                                      },
                                icon: Icon(
                                  Icons.remove,
                                  size: 18,
                                ),
                              ),
                            ),
                            Expanded(
                                child: Center(
                                    child: Text(
                              "$qty",
                              style: TextStyle(fontSize: 12),
                            ))),
                            Expanded(
                              child: IconButton(
                                onPressed: () {
                                  incrementQty();
                                },
                                icon: Icon(
                                  Icons.add,
                                  size: 18,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
