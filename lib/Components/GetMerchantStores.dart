// To parse this JSON data, do
//
//     final merchantStores = merchantStoresFromJson(jsonString);

import 'dart:convert';

MerchantStores merchantStoresFromJson(String str) =>
    MerchantStores.fromJson(json.decode(str));

String merchantStoresToJson(MerchantStores data) => json.encode(data.toJson());

class MerchantStores {
  MerchantStores({
    this.info,
    this.status,
  });

  Info info;
  String status;

  factory MerchantStores.fromJson(Map<String, dynamic> json) => MerchantStores(
        info: Info.fromJson(json["info"]),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": info.toJson(),
        "status": status,
      };
}

class Info {
  Info({
    this.allStores,
    this.status,
  });

  List<AllStore> allStores;
  String status;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        allStores: List<AllStore>.from(
            json["all_stores"].map((x) => AllStore.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "all_stores": List<dynamic>.from(allStores.map((x) => x.toJson())),
        "status": status,
      };
}

class AllStore {
  AllStore({
    this.id,
    this.storeName,
    this.storeAddress,
    this.storeStatus,
    this.storeImage,
    this.categories,
    this.systemCategoryId,
    this.inventorySet,
  });

  int id;
  String storeName;
  String storeAddress;
  String storeStatus;
  String storeImage;
  int categories;
  int systemCategoryId;
  int inventorySet;

  factory AllStore.fromJson(Map<String, dynamic> json) => AllStore(
        id: json["id"],
        storeName:
            json["store_name"] == null ? "" : json["store_name"].toString(),
        storeAddress: json["store_address"] == null
            ? ""
            : json["store_address"].toString(),
        storeStatus: json["store_status"].toString(),
        storeImage: json["store_image"].toString(),
        categories: json["categories"],
        systemCategoryId: json["system_category_id"],
        inventorySet: json["inventory_set"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "store_name": storeName == null ? "" : storeName,
        "store_address": storeAddress == null ? "" : storeAddress,
        "store_status": storeStatus,
        "store_image": storeImage,
        "categories": categories,
        "system_category_id": systemCategoryId,
        "inventory_set": inventorySet,
      };
}
