import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:merchant/LoginScreenComponents/Login.dart';

class IntroSreen extends StatefulWidget {
  @override
  _IntroSreenState createState() => _IntroSreenState();
}

class _IntroSreenState extends State<IntroSreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
      ),
      body: IntroductionScreen(
        pages: <PageViewModel>[
          PageViewModel(
            title: "Connect With Your Favorite Retailers",
            bodyWidget: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/neomart_splash_image.png',
                  height: 60,
                )
              ],
            ),
            image: Center(child: Image.asset('assets/connect.gif')),
          ),
          PageViewModel(
            title: "Exciting Offers & Discounts",
            bodyWidget: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/neomart_splash_image.png',
                  height: 60,
                )
              ],
            ),
            image: Center(child: Image.asset('assets/offer.gif')),
          ),
        ],
        showSkipButton: true,
        skip: Text("Skip"),
        onSkip: () {
          Get.offAll(Login());
        },
        next: Text("Next"),
        showNextButton: true,
        done: Text("Done", style: TextStyle(fontWeight: FontWeight.w600)),
        onDone: () {
          Get.offAll(Login());
        },
        globalBackgroundColor: Colors.white,
        curve: Curves.bounceOut,
        dotsDecorator: DotsDecorator(
            size: const Size.square(10.0),
            activeSize: const Size(20.0, 10.0),
            activeColor: Colors.black,
            color: Colors.black26,
            spacing: const EdgeInsets.symmetric(horizontal: 3.0),
            activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0))),
      ),
    );
  }
}
