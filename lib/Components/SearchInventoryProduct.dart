import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:merchant/Getter/GetSearchProductPos.dart';
import 'package:merchant/Network/httpRequests.dart';

import '../constants.dart';

_SearchInventoryProductState searchInventoryProductState;

class SearchInventoryProduct extends StatefulWidget {
  @override
  _SearchInventoryProductState createState() {
    searchInventoryProductState = _SearchInventoryProductState();
    return searchInventoryProductState;
  }
}

class _SearchInventoryProductState extends State<SearchInventoryProduct> {
  HttpRequests requests = HttpRequests();
  ProductPos productPos;
  bool loading = false, notFound = false;

//  Map<int, dynamic> products = {};
  TextEditingController searchController = TextEditingController();

  searchMAsterProduct() async {
    var res = await requests.searchProductPos(searchController.text.toString());
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        productPos = ProductPos.fromJson(res);
        loading = false;
        if (productPos.info.isEmpty)
          notFound = true;
        else
          notFound = false;
      });
    }
  }

//  addProductsToInventory() async {
//    print("HIT");
//    var res = await requests.addProductToInventoryNew(products.values.toList());
//    if (res != null && res['status'].toString().toLowerCase() == "success") {
//      Fluttertoast.showToast(
//          msg: "Inventory Updated.",
//          timeInSecForIosWeb: 2,
//          toastLength: Toast.LENGTH_LONG);
//      setState(() {
//        products = {};
//      });
//    }
//  }

  setPageState() {
    if (mounted) setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Search Inventory",
          style: regularText,
        ),
        bottom: PreferredSize(
          preferredSize: Size(double.infinity, 60),
          child: Container(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: searchController,
                onFieldSubmitted: (text) {
                  setState(() {
                    loading = true;
                  });
                  searchMAsterProduct();
                },
                decoration: InputDecoration(
                    contentPadding:
                    EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                    filled: true,
                    fillColor: textBoxFillColor,
                    focusColor: Colors.white38,
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(4)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(4)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(4)),
                    counterText: "",
                    suffixIcon: Icon(Icons.search, color: Colors.black),
                    hintText: "search product here...",
                    labelStyle: TextStyle(color: Colors.grey)),
              ),
            ),
          ),
        ),
      ),
      body: loading
          ? loadingCircular()
          : notFound
          ? Center(
        child: Text(
          "No result.",
          style: subHeading,
        ),
      )
          : productPos == null
          ? SizedBox()
          : ListView.separated(
          padding: EdgeInsets.only(bottom: 70),
          separatorBuilder: (context, index) {
            return SizedBox(
              height: 8,
            );
          },
          itemCount: productPos.info.length,
          itemBuilder: (context, index) {
            return ProductPosCard(
                productPos.info[index]
            );
          }),
//      floatingActionButton: products.values.length == 0
//          ? SizedBox()
//          : FloatingActionButton.extended(
//          onPressed: () {
//            addProductsToInventory();
//          },
//          label: Text("Update (${products.length})")),
    );
  }
}

class ProductPosCard extends StatefulWidget {
  Info info;

  ProductPosCard(this.info);

  @override
  _ProductPosCardState createState() => _ProductPosCardState();
}

class _ProductPosCardState extends State<ProductPosCard> {
  HttpRequests requests = HttpRequests();
  int qty = 0;

  var mrpController = TextEditingController();
  var srpController = TextEditingController();
  var qtyController = TextEditingController();
  double mrp, srp;
  bool sellingStatus = true;

  showEditProduct() {
    Get.bottomSheet(StatefulBuilder(
      builder: (context, setState) {
        return Container(
          padding: EdgeInsets.all(16),
          width: double.infinity,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(8), topLeft: Radius.circular(8))),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 2,
                          color: textBoxFillColor,
                          spreadRadius: 2,
                        )
                      ]),
                  child: FadeInImage.assetNetwork(
                    placeholder: 'assets/tsp.png',
                    image: widget.info.icon,
                    height: 200,
                    width: double.infinity,
                    fit: BoxFit.fitHeight,
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  widget.info.productname,
                  style: subHeading,
                ),
                Text(
                  widget.info.productdescription,
                  style: TextStyle(fontSize: 11, color: Colors.grey),
                ),
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              scrollPadding: EdgeInsets.all(8.0),
                              maxLength: 10,
                              maxLengthEnforced: true,
                              controller: mrpController,
                              inputFormatters: [amountInputFormatter],
                              onChanged: (value) {
                                setState(() {
                                  mrp = double.tryParse(double.tryParse(value)
                                      .toStringAsFixed(2));
                                });
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Icon(FlutterIcons.rupee_faw,
                                      size: 15, color: Colors.black),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 8),
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: true,
                                  fillColor: textBoxFillColor,
                                  focusColor: Colors.white38,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  border: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  counterText: "",
                                  hintText: "Enter MRP",
                                  labelText: "MRP"),
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              scrollPadding: EdgeInsets.all(8.0),
                              maxLength: 10,
                              controller: srpController,
                              maxLengthEnforced: true,
                              inputFormatters: [
                                amountInputFormatter
                              ],
                              onChanged: (value) {
                                setState(() {
                                  srp = double.tryParse(double.tryParse(value)
                                      .toStringAsFixed(2));
                                });
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Icon(FlutterIcons.rupee_faw,
                                      size: 15, color: Colors.black),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 8),
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: true,
                                  fillColor: textBoxFillColor,
                                  focusColor: Colors.white38,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  border: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  counterText: "",
                                  hintText: "Enter Selling Price",
                                  labelText: "SRP"),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Row(
                              children: <Widget>[
                                Text(
                                  "Quantity: ",
                                  style: regularText,
                                ),
//                          IconButton(
//                            onPressed: qty <= 0? null: (){
//                              setState(() {
//                                qty--;
//                                qtyController.text = qty.toString();
//                              });
//                            },
//                            icon: Icon(Icons.remove, size: 15, color: Colors.black,),
//                          ),
                                SizedBox(
                                  width: 60,
                                  child: TextField(
                                    keyboardType:
                                    TextInputType.numberWithOptions(),
                                    scrollPadding: EdgeInsets.all(8.0),
                                    maxLength: 10,
                                    maxLengthEnforced: true,
                                    controller: qtyController,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter(
                                          RegExp("[0-9]"))
                                    ],
                                    onChanged: (value) {
                                      setState(() {
                                        qty = int.tryParse(value);
                                      });
                                    },
                                    decoration: InputDecoration(
                                      isDense: true,
//                              prefixIcon: Icon(Icons.remove, size: 15, color: Colors.black,),
//                              suffixIcon: Icon(Icons.add, size: 15, color: Colors.black,),
                                      contentPadding: EdgeInsets.symmetric(
                                          horizontal: 8, vertical: 8),
                                      labelStyle: TextStyle(
                                          color: Colors.black.withOpacity(.4)),
                                      filled: true,
                                      fillColor: textBoxFillColor,
                                      focusColor: Colors.white38,
                                      focusedBorder: OutlineInputBorder(
                                          borderSide:
                                          BorderSide(color: Colors.white),
                                          borderRadius:
                                          BorderRadius.circular(4)),
                                      border: OutlineInputBorder(
                                          borderSide:
                                          BorderSide(color: Colors.white),
                                          borderRadius:
                                          BorderRadius.circular(4)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide:
                                          BorderSide(color: Colors.white),
                                          borderRadius:
                                          BorderRadius.circular(4)),
                                      counterText: "",
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                              child: Row(
                                children: <Widget>[
                                  Text("Selling Status: "),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  SizedBox(
                                    width: 40,
                                    child: Switch(
                                      activeColor: Colors.green,
                                      inactiveThumbColor: Colors.red,
                                      onChanged: (val) {
                                        setState(() {
                                          if (sellingStatus)
                                            sellingStatus = false;
                                          else
                                            sellingStatus = true;
                                        });
                                      },
                                      value: sellingStatus,
                                    ),
                                  )
                                ],
                              )),
//                          IconButton(
//                            onPressed: (){
//                              print("ADD");
//                              setState(() {
//                                qty++;
//                                qtyController.text = qty.toString();
//                              });
//                            },
//                            icon: Icon(Icons.add, size: 15, color: Colors.black,),
//                          ),
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: ButtonTheme(
                              height: 35,
                              child: FlatButton(
                                onPressed: () {
                                  Get.back();
                                },
                                color: textBoxFillColor,
                                textColor: Colors.black,
                                child: Text("Cancel"),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: ButtonTheme(
                              height: 35,
                              child: FlatButton(
                                onPressed: () {
                                  validateDetails();
                                  Get.back();
                                },
                                color: Colors.black,
                                textColor: textBoxFillColor,
                                child: Text("Update"),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    ),
        isDismissible: true,
        isScrollControlled: true,
        enableDrag: true
    );
  }

//  addToCollection() {
//    chooseInventoryState.products[widget.info.merchantListId] = {
//      "merchant_product_id": widget.info.merchantListId.toString(),
//      "merchant_product_qty": qty.toString()
//    };
//    print(chooseInventoryState.products);
//  }
//
//  addToPosCart() async {
//    var res = await requests.addToCartPos(
//        widget.deliveryType,
//        widget.info.masterProductId.toString(),
//        widget.info.merchantListId.toString(),
//        qty.toString(),
//        widget.customerUserid,
//        token);
//    if (res != null && res['status'].toString().toLowerCase() == "success") {
////      Fluttertoast.showToast(msg: "Done");
//    } else {
//      Fluttertoast.showToast(msg: "Something went wrong!");
//    }
//  }

//  addProduct() async {
//    if (widget.pos)
//      addToPosCart();
//    else
//      addToCollection();
//  }

//  removeProduct() {
//    searchMasterProductState.products.remove(widget.info.mproductId);
//    searchMasterProductState.setPageState();
//    print(searchMasterProductState.products);
//  }

//  addProduct() {
//    if (qty == 0)
//      removeProduct();
//    else {
//      searchMasterProductState.products[widget.info.mproductId] = {
//        "master_product_id": widget.info.mproductId.toString(),
//        "merchant_id": merchantid,
//        "qty": qty.toString(),
//        "srp": widget.info.mrp.toString(),
//        "store_id": storeid
//      };
//      searchMasterProductState.setPageState();
//      print(searchMasterProductState.products);
//    }
//  }

  updateInventory() async {
    var res = await requests.updateInventoryNew(
        widget.info.masterProductId.toString(),
        widget.info.merchantListId.toString(),
        widget.info.mrp.toString(),
        widget.info.sellingPrice.toString(),
        qty.toString(),
        widget.info.isactive);

    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Fluttertoast.showToast(msg: "Inventory Updated!");
    }
  }

  incrementQty() {
    setState(() {
      if (qty < widget.info.totalLeft) {
        qty++;
        qtyController.text = qty.toString();
//        addProductToCollection();
        validateDetails();
      } else {
        Fluttertoast.showToast(
            msg: "You cannot add more of these product.",
            toastLength: Toast.LENGTH_LONG,
            timeInSecForIosWeb: 2);
      }
    });
  }

  decrementQty() {
    setState(() {
      if (qty > 0) {
        qty--;
        qtyController.text = qty.toString();
//        addProductToCollection();
        validateDetails();
      }
    });
  }

  initValues() {
    setState(() {
      print("SRP MRP ${widget.info.sellingPrice}");
      print("SRP MRP ${widget.info.mrp}");
      mrp =
          double.tryParse(double.tryParse(widget.info.mrp.toString()).toStringAsFixed(2));
      srp = double.tryParse(
          double.tryParse(widget.info.sellingPrice).toStringAsFixed(2));
      qty = widget.info.totalLeft;
      mrpController.text = mrp.toString();
      srpController.text = srp.toString();
      qtyController.text = qty.toString();
      sellingStatus = widget.info.status == 1 ? true : false;
    });
  }

  validateDetails() {
    if (mrp <= 0) {
      Fluttertoast.showToast(
          msg: "Product MRP cannot be 0.",
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 2);
      initValues();
    } else if (mrp < srp) {
      Fluttertoast.showToast(
          msg: "Product MRP cannot be greater then Selling Price.",
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 2);
      initValues();
    } else if (srp <= 0) {
      Fluttertoast.showToast(
          msg: "Product Selling Price cannot be 0.",
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 2);
      initValues();
    } else
      updateInventory();
  }


  @override
  void initState() {
    // TODO: implement initState
//    print("PRODUCT BARCODE: ${widget.data.barcode}");
    initValues();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        showEditProduct();
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        margin: EdgeInsets.symmetric(horizontal: 8),
        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(4)),
                height: 70,
                width: 70,
                child: FadeInImage.assetNetwork(
                    placeholder: 'assets/tsp.png', image: widget.info.icon)),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 4),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.info.productname,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: smallHeading,
                    ),
                    Text(
                      widget.info.productdescription,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: TextStyle(fontSize: 12, color: Colors.black54),
                    ),
                    SizedBox(height: 5,),
                    Container(
                        color: Colors.black,
                        padding: EdgeInsets.symmetric(horizontal: 4),
                        child: Text(
                          widget.info.measure,
                          style: TextStyle(fontSize: 12, color: Colors.white),
                        )
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
