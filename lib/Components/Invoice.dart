import 'dart:io';

import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:merchant/constants.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:open_file/open_file.dart';


import 'package:path_provider/path_provider.dart';

class Invoice extends StatefulWidget {
  String invoiceUrl, orderId;

  Invoice(this.invoiceUrl, this.orderId);

  @override
  _InvoiceState createState() => _InvoiceState();
}

class _InvoiceState extends State<Invoice> {

  Dio dio = Dio();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  initNotificationSettings(){
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var android = AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOS = IOSInitializationSettings(
        requestAlertPermission: true,
        requestBadgePermission: true,
        requestSoundPermission: true,
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initSettings = InitializationSettings(android, iOS);
    flutterLocalNotificationsPlugin.initialize(initSettings,
        onSelectNotification: onSelectNotification);
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(body),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: Text('Ok'),
              onPressed: () async {
                Navigator.of(context, rootNavigator: true).pop();
              })
        ],
      ),
    );
  }

  Future onSelectNotification(String payload) async {
    print('Working on select');
    print('PAYLOAD: $payload');
    openPdf();
  }
  openPdf() async {
    print("Opening file");

    OpenFile.open(filePath);
//    UrlLauncher.launch(
//      path,F
//    );
  }
  showNotification(String name) async {
    var android = AndroidNotificationDetails(
        'channel id', 'channel NAME', 'CHANNEL DESCRIPTION');

    var iOS = IOSNotificationDetails();
    var platform = NotificationDetails(android, iOS);
    print("${platform.iOS.toString()} platform");

    await flutterLocalNotificationsPlugin.show(
        0,
        "Invoice Downloaded",
        "$name",
        platform);
  }
  String filePath;
  // PDFDocument document;
  getAppDirectory() async {
    Directory dir;
    if (Platform.isIOS)
      dir = await getApplicationDocumentsDirectory();
    else if (Platform.isAndroid)
      dir = await getExternalStorageDirectory();
    filePath = dir.path +
        "/invoice-${widget.orderId.toString()}.pdf";
    print("FILE PATH: $filePath");
    download2(widget.invoiceUrl, filePath);
  }

  Future download2(String url, String savePath) async {
    try {
      Response response = await dio.get(
        url,
        onReceiveProgress: showDownloadProgress,
        //Received data with List<int>
        options: Options(
            responseType: ResponseType.bytes,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            }),
      );
      print(response.headers);
      File file = File(savePath);
      var raf = file.openSync(mode: FileMode.write);
      // response.data is List<int> type
      raf.writeFromSync(response.data);
      await raf.close();
      showNotification("invoice-${widget.orderId.toString()}.pdf");
      showToast("Invoice downloaded & saved in phone storage.", Colors.green);
    } catch (e) {
      print("EXCEPTION: $e");
      showToast("Something went wrong. Cannot download Invoice", Colors.red);
    }
  }

  void showDownloadProgress(received, total) {
    print("DOWNLOADIN>> $received || $total");
    print("DOWNLOAD PROGRESS: " + (received / total * 100).toStringAsFixed(0) +
        "%");
    if (total != -1) {
      print(
          "DOWNLOAD PROGRESS: " + (received / total * 100).toStringAsFixed(0) +
              "%");
    }
//    Get.snackbar("Invoice", "Downloading", progressIndicatorController: )
  }

  @override
  void initState() {
    initNotificationSettings();
    super.initState();
    print("invoice"+widget.invoiceUrl);
    loadDocument();
    // loadDocument();
  }
  PDFDocument document;
  bool _isLoading=true;
  loadDocument() async {
    document = await PDFDocument.fromURL(widget.invoiceUrl);

    setState(() => _isLoading = false);
  }
  // bool _isLoading = true;
  // PDFDocument document;
  // loadDocument() async {
  //   document = await PDFDocument.fromURL(
  //       "http://conorlastowka.com/book/CitationNeededBook-Sample.pdf");
  //
  //   setState(() => _isLoading = false);
  // }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor:Colors.black,

          title: Text(
            "Invoice",
            style: regularText,
          ),
          centerTitle: true,
          automaticallyImplyLeading: true,
          actions: <Widget>[
            IconButton(
              onPressed: () {
                getAppDirectory();
              },
              icon: Icon(
                Icons.save_alt,
                color: Colors.white,
              ),
            )
          ],
        ),
        body:  Center(
          child: _isLoading
              ? Center(child: CircularProgressIndicator())
              :
          PDFViewer(
            document: document,
            zoomSteps: 1,
            //uncomment below line to preload all pages
            // lazyLoad: false,
            // uncomment below line to scroll vertically
            // scrollDirection: Axis.vertical,

            //uncomment below code to replace bottom navigation with your own
            /* navigationBuilder:
                      (context, page, totalPages, jumpToPage, animateToPage) {
                    return ButtonBar(
                      alignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.first_page),
                          onPressed: () {
                            jumpToPage()(page: 0);
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.arrow_back),
                          onPressed: () {
                            animateToPage(page: page - 2);
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.arrow_forward),
                          onPressed: () {
                            animateToPage(page: page);
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.last_page),
                          onPressed: () {
                            jumpToPage(page: totalPages - 1);
                          },
                        ),
                      ],
                    );
                  }, */
          ),
        ));
  }
}
