import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';

class NotificationHandler {
  FirebaseMessaging firebaseMessaging;

  void setupFirebaseNotification() {
    firebaseMessaging = FirebaseMessaging();
    firebaseListner();
  }

  void firebaseListner() async {
    if (Platform.isIOS) iOS_Permission();
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("on message: $message");
//        myBackgroundMessageHandler(message);
        return;
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
        myBackgroundMessageHandler(message);
        return;
      },
//        onLaunch: (Map<String, dynamic> message) async {
//          print('on launch $message');
//          myBackgroundMessageHandler(message);
//          return;
//        },
    );
  }

  Future<dynamic> myBackgroundMessageHandler(
      Map<String, dynamic> message) async {
    if (message.containsKey('data')) {
      print("Data -> ");
      final dynamic data = message['data'];
//      if (data['screen'] == "Chat")
//        Navigator.push(
//            context, MaterialPageRoute(builder: (context) => Chats(widget.user)));
      print(data);
      if (data['screen'].toString().contains("Chat"))
        print("chat");
      else {
        print("order");
      }
    }

    if (message.containsKey('notification')) {
      print("notification -> ");
      final dynamic notification = message['notification'];
      print(notification);
    }

    // Or do other work.
  }

  void iOS_Permission() {
    firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }
}
