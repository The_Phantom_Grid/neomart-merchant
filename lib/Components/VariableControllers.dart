import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:merchant/Getter/GetProductCategory.dart';
import 'package:merchant/Getter/GetProductSubCategory.dart';
import 'package:merchant/Getter/GetStoreDetails.dart';
import 'package:merchant/Getter/GetUserProfile.dart';
import 'package:merchant/Network/httpRequests.dart';

import '../constants.dart';
import 'GetMerchantStores.dart';

HttpRequests requests = HttpRequests();
UserProfile userProfile;

MerchantStores merchantStores;

ProductCategory addProdCats;

ProductSubCategory addProdSubCats;

StoreDetails storeDetails;

bool detailsUpdated = true;

getStoreDetails() async {
  var res = await requests.getStoreDetails();
  if (res != null && res['status'].toString().toLowerCase() == "success") {
    storeDetails = StoreDetails.fromJson(res);
    storeLocation = storeDetails.info.storeDetail.location;
  } else {
    Fluttertoast.showToast(
        msg: res['message'].toString(),
        timeInSecForIosWeb: 2,
        toastLength: Toast.LENGTH_LONG);
  }
}

getMerchantProfile() async {
  var res = await requests.getProfile();
  if (res != null && res['status'].toString().toLowerCase() == "success") {
    userProfile = UserProfile.fromJson(res);
  }
}

getPlans() async {
//  var res = await requests.getAllPlans();
//  if (res != null && res['status'].toString().toLowerCase() == "success") {
//    userProfile = UserProfile.fromJson(res);
//  }
}

class VarController extends GetxController {
  RxInt pCount = 0.obs;
  RxBool devServer = true.obs;
  RxString baseUrl = "https://www.neomart.com/rest/web/index.php/v2".obs;
}
