import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:merchant/AddProduct.dart';
import 'package:merchant/ChoosInventory.dart';
import 'package:merchant/Getter/GetFilteredProduct.dart';
import 'package:merchant/Getter/GetProductCategory.dart';
import 'package:merchant/Getter/GetProductSubCategory.dart';
import 'package:merchant/Network/httpRequests.dart';

import '../constants.dart';

class CollectionProductCard extends StatefulWidget {
  Datum data;

  CollectionProductCard(this.data);

  @override
  _CollectionProductCardState createState() => _CollectionProductCardState();
}

class _CollectionProductCardState extends State<CollectionProductCard> {
  HttpRequests requests = HttpRequests();
  int qty = 0;

  addProductToCollection() async {
    addProduct();
    print(
        "Added product id ${widget.data.merchantListId}|| QTY: $qty || CID: ${chooseInventoryState.widget.collectionId}");
//    var res = await requests.addProductToCollection(
//        chooseInventoryState.widget.collectionId, "1",
//        widget.data.merchantListId.toString(), qty.toString(), widget.data.mrp.toString());
//    if(res != null && res['status'].toString().toLowerCase() == "success"){
//      print("Added product ${widget.data.productname}|| QTY: $qty");
//    }
  }

  addProduct() {
    chooseInventoryState.products[widget.data.merchantListId] = {
      "merchant_product_id": widget.data.merchantListId.toString(),
      "merchant_product_qty": qty.toString()
    };
    print(chooseInventoryState.products);
  }

  incrementQty() {
    setState(() {
      if (qty < widget.data.totalLeft) {
        qty++;
        addProductToCollection();
      } else {
        Fluttertoast.showToast(
            msg: "You cannot add more of these product.",
            toastLength: Toast.LENGTH_LONG,
            timeInSecForIosWeb: 2);
      }
    });
  }

  decrementQty() {
    setState(() {
      if (qty > 0) {
        qty--;
        addProductToCollection();
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(8)),
      margin: EdgeInsets.symmetric(horizontal: 8),
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(4)),
              height: 70,
              width: 70,
              child: FadeInImage.assetNetwork(
                  placeholder: 'assets/tsp.png', image: widget.data.icon)),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.data.productname,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: smallHeading,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                      color: Colors.black,
                      padding: EdgeInsets.symmetric(horizontal: 4),
                      child: Text(
                        widget.data.measure,
                        style: TextStyle(fontSize: 12, color: Colors.white),
                      )),
                  Row(
                    children: <Widget>[
                      Expanded(
                          child: RichText(
                        text: TextSpan(children: [
                          TextSpan(
                              text: "Mrp: ",
                              style:
                                  TextStyle(fontSize: 12, color: Colors.black)),
                          TextSpan(
                              text: "₹ ${widget.data.mrp.toString()}",
                              style: mrpTextStyle),
                        ]),
                      )),
                      Container(
                        height: 36,
                        width: 120,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(18),
                            border: Border.all(color: Colors.black)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: IconButton(
                                onPressed: qty <= 0
                                    ? null
                                    : () {
                                        decrementQty();
                                      },
                                icon: Icon(
                                  Icons.remove,
                                  size: 18,
                                ),
                              ),
                            ),
                            Expanded(
                                child: Center(
                                    child: Text(
                              "$qty",
                              style: TextStyle(fontSize: 12),
                            ))),
                            Expanded(
                              child: IconButton(
                                onPressed: () {
                                  incrementQty();
                                },
                                icon: Icon(
                                  Icons.add,
                                  size: 18,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class AddProductCard extends StatefulWidget {
  Datum data;

  AddProductCard(this.data);

  @override
  _AddProductCardState createState() => _AddProductCardState();
}

class _AddProductCardState extends State<AddProductCard> {
  HttpRequests requests = HttpRequests();
  int qty = 0;

  addProductToCollection() async {
//    addProduct();
    print(
        "Added product id ${widget.data.merchantListId}|| QTY: $qty || CID: ${chooseInventoryState.widget.collectionId}");
//    var res = await requests.addProductToCollection(
//        chooseInventoryState.widget.collectionId, "1",
//        widget.data.merchantListId.toString(), qty.toString(), widget.data.mrp.toString());
//    if(res != null && res['status'].toString().toLowerCase() == "success"){
//      print("Added product ${widget.data.productname}|| QTY: $qty");
//    }
  }

  removeProduct() {
    addProductState.products.remove(widget.data.masterProductId);
    addProductState.setPageState();
    print(addProductState.products);
  }

  addProduct() {
    if (qty == 0)
      removeProduct();
    else {
      addProductState.products[widget.data.masterProductId] = {
        "master_product_id": widget.data.masterProductId.toString(),
        "merchant_id": merchantid,
        "qty": qty.toString(),
        "srp": widget.data.sellingPrice.toString(),
        "store_id": storeid
      };
      addProductState.setPageState();
      print(addProductState.products);
    }
  }

  incrementQty() {
    setState(() {
//      if(qty < widget.data.totalLeft) {
      qty++;
//        addProductToCollection();
      addProduct();
//      }
//      else {
//        Fluttertoast.showToast(msg: "You cannot add more of these product.", toastLength: Toast.LENGTH_LONG, timeInSecForIosWeb: 2);
//      }
    });
  }

  decrementQty() {
    setState(() {
      if (qty > 0) {
        qty--;
        addProduct();
//        addProductToCollection();
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
//    print("PRODUCT BARCODE: ${widget.data.barcode}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(8)),
      margin: EdgeInsets.symmetric(horizontal: 8),
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(4)),
              height: 70,
              width: 70,
              child: FadeInImage.assetNetwork(
                  placeholder: 'assets/tsp.png', image: widget.data.icon)),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(left: 4),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.data.productname,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: smallHeading,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                      color: Colors.black,
                      padding: EdgeInsets.symmetric(horizontal: 4),
                      child: Text(
                        widget.data.measure,
                        style: TextStyle(fontSize: 12, color: Colors.white),
                      )),
                  Row(
                    children: <Widget>[
                      Expanded(
                          child: RichText(
                        text: TextSpan(children: [
                          TextSpan(
                              text: "Mrp: ",
                              style:
                                  TextStyle(fontSize: 12, color: Colors.black)),
                          TextSpan(
                              text: "₹ ${widget.data.originalMrp.toString()}",
                              style: mrpTextStyle),
                        ]),
                      )),
                      Container(
                        height: 36,
                        width: 120,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(18),
                            border: Border.all(color: Colors.black)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: IconButton(
                                onPressed: qty <= 0
                                    ? null
                                    : () {
                                        decrementQty();
                                      },
                                icon: Icon(
                                  Icons.remove,
                                  size: 18,
                                ),
                              ),
                            ),
                            Expanded(
                                child: Center(
                                    child: Text(
                              "$qty",
                              style: TextStyle(fontSize: 12),
                            ))),
                            Expanded(
                              child: IconButton(
                                onPressed: () {
                                  incrementQty();
                                },
                                icon: Icon(
                                  Icons.add,
                                  size: 18,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class MyInventoryProductCard extends StatefulWidget {
  Datum data;

  MyInventoryProductCard(this.data);

  @override
  _MyInventoryProductCardState createState() => _MyInventoryProductCardState();
}

class _MyInventoryProductCardState extends State<MyInventoryProductCard> {
  HttpRequests requests = HttpRequests();
  int qty = 0;
  var mrpController = TextEditingController();
  var srpController = TextEditingController();
  var qtyController = TextEditingController();
  double mrp, srp;
  bool sellingStatus = true;

//  addProductToCollection() async {
//    addProduct();
//    print(
//        "Added product id ${widget.data.merchantListId}|| QTY: $qty || CID: ${chooseInventoryState.widget.collectionId}");
////    var res = await requests.addProductToCollection(
////        chooseInventoryState.widget.collectionId, "1",
////        widget.data.merchantListId.toString(), qty.toString(), widget.data.mrp.toString());
////    if(res != null && res['status'].toString().toLowerCase() == "success"){
////      print("Added product ${widget.data.productname}|| QTY: $qty");
////    }
//  }

//  addProduct() {
//    chooseInventoryState.products[widget.data.merchantListId] = {
//      "merchant_product_id": widget.data.merchantListId.toString(),
//      "merchant_product_qty": qty.toString()
//    };
//    print(chooseInventoryState.products);
//  }

  incrementQty() {
    setState(() {
      if (qty < widget.data.totalLeft) {
        qty++;
        qtyController.text = qty.toString();
//        addProductToCollection();
        validateDetails();
      } else {
        Fluttertoast.showToast(
            msg: "You cannot add more of these product.",
            toastLength: Toast.LENGTH_LONG,
            timeInSecForIosWeb: 2);
      }
    });
  }

  decrementQty() {
    setState(() {
      if (qty > 0) {
        qty--;
        qtyController.text = qty.toString();
//        addProductToCollection();
        validateDetails();
      }
    });
  }

  initValues() {
    setState(() {
      print("MRP: ${widget.data.mrp}");
      print("sRP: ${widget.data.sellingPrice}");
      mrp =
          double.tryParse(double.tryParse(widget.data.mrp).toStringAsFixed(2));
      srp = double.tryParse(
          double.tryParse(widget.data.sellingPrice).toStringAsFixed(2));
      qty = widget.data.totalLeft;
      mrpController.text = mrp.toString();
      srpController.text = srp.toString();
      qtyController.text = qty.toString();
      sellingStatus = widget.data.status == 1 ? true : false;
    });
  }

  validateDetails() {
    if (mrp <= 0) {
      Fluttertoast.showToast(
          msg: "Product MRP cannot be 0.",
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 2);
      initValues();
    } else if (mrp < srp) {
      Fluttertoast.showToast(
          msg: "Product MRP cannot be greater then Selling Price.",
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 2);
      initValues();
    } else if (srp <= 0) {
      Fluttertoast.showToast(
          msg: "Product Selling Price cannot be 0.",
          toastLength: Toast.LENGTH_LONG,
          timeInSecForIosWeb: 2);
      initValues();
    } else
      updateInventory();
  }

  updateInventory() async {
    print("MRP: $mrp || SRP: $srp");
    var res = await requests.updateInventoryNew(
        widget.data.masterProductId.toString(),
        widget.data.merchantListId.toString(),
        mrp.toString(),
        srp.toString(),
        qty.toString(),
        sellingStatus ? 1 : 0);

    if (res != null && res['status'].toString().toLowerCase() == "success") {
      Fluttertoast.showToast(msg: "Inventory Updated!");
    }
    setPageState();
  }

  setPageState() {
    setState(() {});
  }

  @override
  void initState() {
    initValues();
    // TODO: implement initState
    super.initState();
  }

  showEditProduct() {
    Get.bottomSheet(StatefulBuilder(
      builder: (context, setState) {
        return Container(
          padding: EdgeInsets.all(16),
          width: double.infinity,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(8), topLeft: Radius.circular(8))),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 2,
                          color: textBoxFillColor,
                          spreadRadius: 2,
                        )
                      ]),
                  child: FadeInImage.assetNetwork(
                    placeholder: 'assets/tsp.png',
                    image: widget.data.icon,
                    height: 200,
                    width: double.infinity,
                    fit: BoxFit.fitHeight,
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  widget.data.productname,
                  style: subHeading,
                ),
                Text(
                  widget.data.productdescription,
                  style: TextStyle(fontSize: 11, color: Colors.grey),
                ),
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              scrollPadding: EdgeInsets.all(8.0),
                              maxLength: 10,
                              maxLengthEnforced: true,
                              controller: mrpController,
                              inputFormatters: [amountInputFormatter],
                              onChanged: (value) {
                                setState(() {
                                  mrp = double.tryParse(double.tryParse(value)
                                      .toStringAsFixed(2));
                                });
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Icon(FlutterIcons.rupee_faw,
                                      size: 15, color: Colors.black),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 8),
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: true,
                                  fillColor: textBoxFillColor,
                                  focusColor: Colors.white38,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  border: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  counterText: "",
                                  hintText: "Enter MRP",
                                  labelText: "MRP"),
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: TextField(
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              scrollPadding: EdgeInsets.all(8.0),
                              maxLength: 10,
                              controller: srpController,
                              maxLengthEnforced: true,
                              inputFormatters: [
                                amountInputFormatter
                              ],
                              onChanged: (value) {
                                setState(() {
                                  srp = double.tryParse(double.tryParse(value)
                                      .toStringAsFixed(2));
                                });
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Icon(FlutterIcons.rupee_faw,
                                      size: 15, color: Colors.black),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 8),
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: true,
                                  fillColor: textBoxFillColor,
                                  focusColor: Colors.white38,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  border: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  counterText: "",
                                  hintText: "Enter Selling Price",
                                  labelText: "SRP"),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Row(
                              children: <Widget>[
                                Text(
                                  "Quantity: ",
                                  style: regularText,
                                ),
//                          IconButton(
//                            onPressed: qty <= 0? null: (){
//                              setState(() {
//                                qty--;
//                                qtyController.text = qty.toString();
//                              });
//                            },
//                            icon: Icon(Icons.remove, size: 15, color: Colors.black,),
//                          ),
                                SizedBox(
                                  width: 60,
                                  child: TextField(
                                    keyboardType:
                                        TextInputType.numberWithOptions(),
                                    scrollPadding: EdgeInsets.all(8.0),
                                    maxLength: 10,
                                    maxLengthEnforced: true,
                                    controller: qtyController,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter(
                                          RegExp("[0-9]"))
                                    ],
                                    onChanged: (value) {
                                      setState(() {
                                        qty = int.tryParse(value);
                                      });
                                    },
                                    decoration: InputDecoration(
                                      isDense: true,
//                              prefixIcon: Icon(Icons.remove, size: 15, color: Colors.black,),
//                              suffixIcon: Icon(Icons.add, size: 15, color: Colors.black,),
                                      contentPadding: EdgeInsets.symmetric(
                                          horizontal: 8, vertical: 8),
                                      labelStyle: TextStyle(
                                          color: Colors.black.withOpacity(.4)),
                                      filled: true,
                                      fillColor: textBoxFillColor,
                                      focusColor: Colors.white38,
                                      focusedBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.white),
                                          borderRadius:
                                              BorderRadius.circular(4)),
                                      border: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.white),
                                          borderRadius:
                                              BorderRadius.circular(4)),
                                      enabledBorder: OutlineInputBorder(
                                          borderSide:
                                              BorderSide(color: Colors.white),
                                          borderRadius:
                                              BorderRadius.circular(4)),
                                      counterText: "",
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                              child: Row(
                            children: <Widget>[
                              Text("Selling Status: "),
                              SizedBox(
                                width: 5,
                              ),
                              SizedBox(
                                width: 40,
                                child: Switch(
                                  activeColor: Colors.green,
                                  inactiveThumbColor: Colors.red,
                                  onChanged: (val) {
                                    setState(() {
                                      if (sellingStatus)
                                        sellingStatus = false;
                                      else
                                        sellingStatus = true;
                                    });
                                  },
                                  value: sellingStatus,
                                ),
                              )
                            ],
                          )),
//                          IconButton(
//                            onPressed: (){
//                              print("ADD");
//                              setState(() {
//                                qty++;
//                                qtyController.text = qty.toString();
//                              });
//                            },
//                            icon: Icon(Icons.add, size: 15, color: Colors.black,),
//                          ),
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: ButtonTheme(
                              height: 35,
                              child: FlatButton(
                                onPressed: () {
                                  Get.back();
                                },
                                color: textBoxFillColor,
                                textColor: Colors.black,
                                child: Text("Cancel"),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                            child: ButtonTheme(
                              height: 35,
                              child: FlatButton(
                                onPressed: () {
                                  validateDetails();
                                  Get.back();
                                },
                                color: Colors.black,
                                textColor: textBoxFillColor,
                                child: Text("Update"),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    ),
        isDismissible: true,
        isScrollControlled: true,
        enableDrag: true
    );
  }

  Widget gridCard() {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(8)),
//      margin: EdgeInsets.symmetric(horizontal: 8),
//      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(4)),
              height: 70,
              width: 70,
              child: FadeInImage.assetNetwork(
                  placeholder: 'assets/tsp.png', image: widget.data.icon)),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                widget.data.productname,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                style: smallHeading,
              ),
              SizedBox(
                height: 5,
              ),
              Container(
                  color: Colors.black,
                  padding: EdgeInsets.symmetric(horizontal: 4),
                  child: Text(
                    widget.data.measure,
                    style: TextStyle(fontSize: 12, color: Colors.white),
                  )),
            ],
          ),
        ],
      ),
    );
  }

//  Widget listCard() {
//    return Container(
//      decoration: BoxDecoration(
//          color: Colors.white, borderRadius: BorderRadius.circular(8)),
//      margin: EdgeInsets.symmetric(horizontal: 8),
//      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
//      child: Row(
//        mainAxisAlignment: MainAxisAlignment.start,
//        crossAxisAlignment: CrossAxisAlignment.start,
//        children: <Widget>[
//          Container(
//              decoration: BoxDecoration(
//                  color: Colors.transparent,
//                  borderRadius: BorderRadius.circular(4)),
//              height: 70,
//              width: 70,
//              child: FadeInImage.assetNetwork(
//                  placeholder: 'assets/tsp.png', image: widget.data.icon)),
//          Expanded(
//            child: Container(
//              padding: EdgeInsets.only(left: 4),
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Text(
//                    widget.data.productname,
//                    overflow: TextOverflow.ellipsis,
//                    maxLines: 2,
//                    style: smallHeading,
//                  ),
//                  SizedBox(
//                    height: 5,
//                  ),
//                  Container(
//                      color: Colors.black,
//                      padding: EdgeInsets.symmetric(horizontal: 4),
//                      child: Text(
//                        widget.data.measure,
//                        style: TextStyle(fontSize: 12, color: Colors.white),
//                      )),
//                  Row(
//                    children: <Widget>[
//                      Expanded(
//                          child: RichText(
//                        text: TextSpan(children: [
//                          TextSpan(
//                              text: "Mrp: ",
//                              style:
//                                  TextStyle(fontSize: 12, color: Colors.black)),
//                          TextSpan(text: "₹ $mrp", style: mrpTextStyle),
//                        ]),
//                      )),
//                      Container(
//                        height: 36,
//                        width: 120,
//                        decoration: BoxDecoration(
//                            borderRadius: BorderRadius.circular(18),
//                            border: Border.all(color: Colors.black)),
//                        child: Row(
//                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                          crossAxisAlignment: CrossAxisAlignment.center,
//                          children: <Widget>[
//                            Expanded(
//                              child: IconButton(
//                                onPressed: qty <= 0
//                                    ? null
//                                    : () {
//                                        decrementQty();
//                                      },
//                                icon: Icon(
//                                  Icons.remove,
//                                  size: 18,
//                                ),
//                              ),
//                            ),
//                            Expanded(
//                                child: Center(
//                                    child: Text(
//                              "$qty",
//                              style: TextStyle(fontSize: 12),
//                            ))),
//                            Expanded(
//                              child: IconButton(
//                                onPressed: () {
//                                  incrementQty();
//                                },
//                                icon: Icon(
//                                  Icons.add,
//                                  size: 18,
//                                ),
//                              ),
//                            )
//                          ],
//                        ),
//                      )
//                    ],
//                  )
//                ],
//              ),
//            ),
//          )
//        ],
//      ),
//    );
//  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          showEditProduct();
        },
        child: gridCard());
  }
}

//class InventoryProductCardGrid extends StatefulWidget {
//  Datum data;
//  InventoryProductCardGrid(this.data);
//  @override
//  _InventoryProductCardGridState createState() => _InventoryProductCardGridState();
//}
//
//class _InventoryProductCardGridState extends State<InventoryProductCardGrid> {
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      decoration: BoxDecoration(
//          color: Colors.white,
//          borderRadius: BorderRadius.circular(8)
//      ),
////      margin: EdgeInsets.symmetric(horizontal: 8),
////      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
//      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
//      child: Column(
//        mainAxisAlignment: MainAxisAlignment.center,
//        children: <Widget>[
//          Container(
//            alignment: Alignment.center,
//              decoration: BoxDecoration(
//                  color: Colors.transparent,
//                  borderRadius: BorderRadius.circular(4)
//              ),
//              height: 70,
//              width: 70,
//              child: FadeInImage.assetNetwork(placeholder: 'assets/tsp.png', image: widget.data.icon)
//          ),
//          Column(
//            crossAxisAlignment: CrossAxisAlignment.start,
//            children: <Widget>[
//              Text(widget.data.productname, overflow: TextOverflow.ellipsis, maxLines: 2, style: smallHeading,),
//              SizedBox(height: 5,),
//              Container(
//                  color: Colors.black,
//                  padding: EdgeInsets.symmetric(horizontal: 4),
//                  child: Text(widget.data.measure, style: TextStyle(fontSize: 12, color: Colors.white),)
//              ),
//            ],
//          ),
//        ],
//      ),
//    );
//  }
//}
