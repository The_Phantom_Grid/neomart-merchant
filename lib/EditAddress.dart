import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:merchant/Network/httpRequests.dart';

import 'Components/VariableControllers.dart';
import 'SearchLocation.dart';
import 'constants.dart';

class EditAddress extends StatefulWidget {
  @override
  _EditAddressState createState() => _EditAddressState();
}

class _EditAddressState extends State<EditAddress> {
  var house = TextEditingController(),
      streetName = TextEditingController(),
      fullNAme = TextEditingController(),
      pincode = TextEditingController(),
      city = TextEditingController(),
      state = TextEditingController(),
      storeLocation = TextEditingController(),
      country = TextEditingController();
  HttpRequests requests = HttpRequests();
  bool loading = true;
  double lat, lng;
  List<Address> address;

  validate() {
    if (storeLocation.text == "" || storeLocation.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Store Location");
    } else if (house.text == "" || house.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Address Line");
    } else if (streetName.text == "" || streetName.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Locality");
    } else if (pincode.text == "" || pincode.text.length < 6) {
      Fluttertoast.showToast(msg: "Invalid detail: Pincode");
    } else if (city.text == "" || city.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: City");
    } else if (state.text == "" || state.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: State");
    } else if (country.text == "" || country.text.length < 1) {
      Fluttertoast.showToast(msg: "Invalid detail: Country/Region");
    } else {
      saveStoreAddress();
    }
  }

  saveStoreAddress() async {
    var res = await requests.saveStoreAddress(
        house.text.toString(),
        city.text.toString(),
        storeid,
        country.text.toString(),
        lat.toString(),
        lng.toString(),
        streetName.text.toString(),
        pincode.text.toString(),
        state.text.toString(),
        storeLocation.text.toString(),
        merchantid,
        token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      detailsUpdated = true;
      Fluttertoast.showToast(
          msg: "Details Updated!",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    } else {
      Fluttertoast.showToast(
          msg: "Something went wrong",
          timeInSecForIosWeb: 2,
          toastLength: Toast.LENGTH_LONG);
    }
  }

  initControllers() {
    setState(() {
      house.text = storeDetails.info.storeDetail.address;
      streetName.text = storeDetails.info.storeDetail.locality;
      city.text = storeDetails.info.storeDetail.city;
      country.text = storeDetails.info.storeDetail.country;
      pincode.text = storeDetails.info.storeDetail.pincode;
      state.text = storeDetails.info.storeDetail.state;
      storeLocation.text = storeDetails.info.storeDetail.location;
      lat = storeDetails.info.storeDetail.lat;
      lng = storeDetails.info.storeDetail.lng;
    });
  }

  getDetails() async {
    print(detailsUpdated);
    if (detailsUpdated) {
      await getStoreDetails();
      setState(() {
        loading = false;
        detailsUpdated = false;
        initControllers();
      });
    } else {
      setState(() {
        initControllers();
        loading = false;
      });
    }
  }

  updateDetail() {
    setState(() {
      storeLocation.text = address.first.addressLine;
      pincode.text = address.first.postalCode;
      state.text = address.first.adminArea;
      streetName.text =
          address.first.featureName + "," + address.first.locality;
      city.text = address.first.locality;
      country.text = address.first.countryName;
      lat = address.first.coordinates.latitude;
      lng = address.first.coordinates.longitude;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    getDetails();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Edit Address"),
      ),
      body: loading
          ? loadingCircular()
          : Column(
              children: <Widget>[
                Expanded(
                  child: ListView(
                    padding: EdgeInsets.all(16),
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Add Store Location",
                            textAlign: TextAlign.left,
                            style: smallHeading,
                          ),
                          TextFormField(
                            controller: storeLocation,
                            enabled: true,
                            onTap: () async {
                              address = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SearchLocation()));
                              if (address != null) updateDetail();
                            },
                            decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 12, horizontal: 8),
                                suffixIcon: IconButton(
                                  onPressed: () {},
                                  icon: Icon(
                                    Icons.my_location,
                                    color: Colors.black,
                                  ),
                                ),
                                hintText: "Enter your store location",
                                isDense: true,
                                hasFloatingPlaceholder: true,
                                border: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.black))),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Text(
                            "Store Address (*Customers will pick up packages from this address)",
                            textAlign: TextAlign.left,
                            style: smallHeading,
                          ),
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                border: Border.all(color: Colors.black)),
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Column(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: RichText(
                                      text: TextSpan(children: <TextSpan>[
                                        TextSpan(
                                            text: "*",
                                            style: TextStyle(
                                                color: mColor, fontSize: 12)),
                                        TextSpan(
                                            text: " Mandatory fields",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 12)),
                                      ]),
                                    ),
                                  ),
                                  TextField(
                                    controller: house,
                                    scrollPadding: EdgeInsets.all(8.0),
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter(
                                          RegExp("[a-zA-Z 0-9 ,-/#]"))
                                    ],
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 12),
                                      hintText: "Address Line*",
                                    ),
                                  ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  TextField(
                                    controller: streetName,
                                    scrollPadding: EdgeInsets.all(8.0),
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter(
                                          RegExp("[a-zA-Z 0-9 - ,]"))
                                    ],
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.symmetric(
                                        vertical: 12,
                                      ),
                                      hintText: "Locality*",
                                    ),
                                  ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  TextField(
                                    enabled: false,
                                    controller: pincode,
                                    maxLength: 6,
                                    maxLengthEnforced: true,
                                    inputFormatters: [
                                      WhitelistingTextInputFormatter(
                                          RegExp("[0-9]"))
                                    ],
                                    keyboardType:
                                        TextInputType.numberWithOptions(),
                                    scrollPadding: EdgeInsets.all(8.0),
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 12),
                                      counterText: "",
                                      hintText: "Pincode*",
                                    ),
                                  ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: TextField(
                                          enabled: false,
                                          controller: city,
                                          scrollPadding: EdgeInsets.all(8.0),
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter(
                                                RegExp("[a-zA-Z 0-9 - ,]"))
                                          ],
                                          decoration: InputDecoration(
                                            contentPadding:
                                                EdgeInsets.symmetric(
                                                    vertical: 12),
                                            hintText: "City*",
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 15,
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: TextField(
                                          enabled: false,
                                          controller: state,
                                          scrollPadding: EdgeInsets.all(8.0),
                                          inputFormatters: [
                                            WhitelistingTextInputFormatter(
                                                RegExp("[a-zA-Z 0-9 - ,]"))
                                          ],
                                          decoration: InputDecoration(
                                            contentPadding:
                                                EdgeInsets.symmetric(
                                                    vertical: 12),
                                            hintText: "State*",
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  TextField(
                                    enabled: false,
                                    controller: country,
                                    scrollPadding: EdgeInsets.all(8.0),
                                    decoration: InputDecoration(
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 12),
                                      hintText: "Country/Region*",
                                    ),
                                  ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ButtonTheme(
          height: 50,
          splashColor: Colors.white38,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          child: RaisedButton(
            onPressed: () {
              validate();
            },
            child: Text(
              "UPDATE",
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
