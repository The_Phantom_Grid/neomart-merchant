import 'dart:math';

import 'package:flutter/material.dart';
import 'package:merchant/Getter/GetNotifications.dart';
import 'package:merchant/constants.dart';
import 'Network/httpRequests.dart';

class NotificationCenter extends StatefulWidget {
  @override
  _NotificationCenterState createState() => _NotificationCenterState();
}

class _NotificationCenterState extends State<NotificationCenter> {
  HttpRequests requests = HttpRequests();
  UserNotifications notification;
  int randomColorIndex = 1;
  List colors = [
    Colors.red,
    Colors.green,
    Colors.blue,
    Colors.black45,
    Colors.amber,
    Colors.brown,
    Colors.cyan,
    Colors.deepPurpleAccent,
    Colors.indigoAccent
  ];
  Random random = new Random();
  String orderId = "", letter;
  bool loading = true;

  getNotification() async {
    var res = await requests.getNotifications();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        notification = UserNotifications.fromJson(res);
        loading = false;
      });
    }
  }

  String getLetter(int index) {
    letter = notification.data[index].sender
        .replaceRange(1, notification.data[index].sender.length, "")
        .toUpperCase();
    randomColorIndex = random.nextInt(colors.length);
    return letter;
  }

  String getTimeDuration(int index) {
    var sentAt = notification.data[index].sentAt.toString();
    var now = DateTime.now();
    int diff = now.difference(DateTime.parse(sentAt)).inSeconds;
    if (diff <= 59) {
      return "$diff seconds ago";
    }
    diff = now.difference(DateTime.parse(sentAt)).inMinutes;
    if (diff <= 60) {
      return "$diff minutes ago";
    }
    diff = now.difference(DateTime.parse(sentAt)).inHours;
    if (diff <= 24) {
      return "$diff hours ago";
    }
    diff = now.difference(DateTime.parse(sentAt)).inDays;
    return "$diff days ago";
  }

  @override
  void initState() {
    // TODO: implement initState
    getNotification();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Notifications"),
      ),
      body: loading
          ? loadingCircular()
          : notification == null || notification.data.isEmpty
              ? Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.notification_important,
                        size: 35,
                      ),
                      Text(
                        "No Notifications",
                        style: regularText,
                      )
                    ],
                  ),
                )
              : Container(
                  color: Colors.white,
                  child: ListView.separated(
                    separatorBuilder: (context, index) {
                      return Divider(
                        indent: 15,
                        endIndent: 15,
                        height: 0,
                      );
                    },
                    itemCount: notification.data.length,
                    itemBuilder: (context, index) {
                  return InkWell(
                    onTap:
                        notification.data[index].orderId != null ? () {} : null,
                    child: Stack(
                      children: <Widget>[
                        Card(
                            elevation: 0.0,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Container(
                                      color: colors[randomColorIndex],
                                      height: 60,
                                      width: 60,
                                      child: Center(
                                          child: Text(
                                        getLetter(index),
                                        style: TextStyle(
                                            fontSize: 26, color: Colors.white),
                                      )),
                                    ),
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 8.0, right: 8.0),
                                        child: Container(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Text(
                                                "${notification.data[index].sender}",
                                                style: TextStyle(
                                                    fontSize: 14,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                              Text(
                                                "${notification.data[index].messageContent}",
                                                style: TextStyle(fontSize: 11),
                                              ),
                                              Text(
                                                getTimeDuration(index),
                                                style: TextStyle(
                                                    fontSize: 11,
                                                    color: mColor),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )),
                        Visibility(
                          visible: getTimeDuration(index)
                                      .toString()
                                      .contains("seconds") ||
                                  getTimeDuration(index)
                                      .toString()
                                      .contains("minutes")
                              ? true
                              : false,
                          child: Positioned(
                              top: 9,
                              right: 9,
                              child: Container(
                                padding: EdgeInsets.fromLTRB(5, 2, 5, 2),
                                color: mColor,
                                child: Text(
                                  "NEW",
                                  style: TextStyle(
                                      fontSize: 9, color: Colors.white),
                                ),
                              )),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
    );
  }
}
