import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:merchant/Components.dart';
import 'package:merchant/Components/SearchInventoryProduct.dart';
import 'package:merchant/Components/StoreInventoryProductCard.dart';
import 'package:merchant/Getter/GetFilteredData.dart';
import 'package:merchant/Getter/GetFilteredProduct.dart';
import 'package:merchant/Getter/GetProductCategory.dart';
import 'package:merchant/Getter/GetProductSubCategory.dart';

import 'Getter/GetSystemFilter.dart';
import 'Network/httpRequests.dart';
import 'constants.dart';

_MyInventoryState myInventoryState;

class MyInventory extends StatefulWidget {
  @override
  _MyInventoryState createState() {
    myInventoryState = _MyInventoryState();
    return myInventoryState;
  }
}

class _MyInventoryState extends State<MyInventory>
    with SingleTickerProviderStateMixin {
  var _tabController;
  int selectedChip = 0;
  bool pageState = false, loading = true, loadMore = false;
  PageController pageController = PageController();
  HttpRequests requests = HttpRequests();
  SystemFilter systemFilter;
  String filterName, catId, subCatId;

//  ProductCategory productCategory;
  ProductSubCategory productSubCategory;
  Product filteredProduct, tempProducts;
  FilteredData filteredData;
  Map<int, dynamic> products = {};
  int MIN = 0, MAX = 10;

  selectedOption(int index) {
    setState(() {
      MIN = 0;
      selectedChip = index;
      pageState = false;
      catId = filteredData.info.category[index].prodCategoryId.toString();
      pageState = false;
      pageController.animateToPage(0,
          duration: Duration(milliseconds: 200), curve: Curves.decelerate);
    });
    getSubCategory();
  }

//  showSaveChages(){
//    Get.defaultDialog(
//        barrierDismissible: false,
//        title: "Save Changes",
//        content: Text("Do you want to save changes and update inventory."),
//        confirm: Text("Save"),
//        cancel: Text("Discard"),
//        onCancel: (){
//          Get.back();
//        },
//        onConfirm: (){
//          upd
//        }
//    );
//  }

  getFilters() async {
    var res = await requests.getSystemFilters();
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        systemFilter = SystemFilter.fromJson(res);
        filterName = systemFilter.info[0].filterName;
        _tabController = TabController(
            length: systemFilter.info.length, initialIndex: 0, vsync: this);
//        getProductCategories();
        getFilteredData();
      });
    }
  }

  getFilteredData() async {
    print("FILTER NAME: $filterName");
    var res = await requests.getFilteredData(filterName);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        filteredData = FilteredData.fromJson(res);
        catId = filteredData.info.category[0].prodCategoryId;
//        _tabController = TabController(length: filteredData.info.category.length, initialIndex: 0, vsync: this);
//        getProductCategories();
        getSubCategory();
      });
    }
  }

//  getProductCategories() async {
//    var res = await requests.getProductCategories(filterName);
//    if(res != null && res['status'].toString().toLowerCase() == "success") {
//      setState(() {
//        productCategory = ProductCategory.fromJson(res);
//        catId = productCategory.info[0].prodCategoryId.toString();
//        getSubCategory();
//      });
//    }
//  }

  getSubCategory() async {
    var res = await requests.getSubCategories(catId);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        productSubCategory = ProductSubCategory.fromJson(res);
        subCatId = productSubCategory.info[0].subcatId.toString();
        loading = false;
      });
    }
  }

  getProducts() async {
    print(filterName);
    var res = await requests.getFilteredProducts(
        filterName, catId, subCatId, "10", "0");
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        filteredProduct = Product.fromJson(res);
        pageState = true;
        print("FIltered: ${filteredProduct.info.data}");
        pageController.animateToPage(1,
            duration: Duration(milliseconds: 200), curve: Curves.decelerate);
      });
    }
  }

  getMoreProducts() async {
    MIN += MAX;
    print(filterName);
    tempProducts = null;
    var res = await requests.getFilteredProducts(
        filterName, catId, subCatId, MAX.toString(), MIN.toString());
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      setState(() {
        tempProducts = Product.fromJson(res);
        if(tempProducts.info.data.isEmpty)
          showToast("No more products", Colors.black);
        filteredProduct.info.data.addAll(tempProducts.info.data);
        loadMore = false;
        pageState = true;
        print("FIltered: ${filteredProduct.info.data}");
        pageController.animateToPage(1,
            duration: Duration(milliseconds: 200), curve: Curves.decelerate);
      });
    }
  }

  Widget showMore() {
    return GestureDetector(
      onTap: () {
        setState(() {
          loadMore = true;
        });
        getMoreProducts();
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        padding: EdgeInsets.all(16),
        child: loadMore
            ? Center(
                child: Text("Loading...",
                    textAlign: TextAlign.center, style: subHeading))
            : Center(
                child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(FlutterIcons.add_mdi),
                  Text(
                    "Show more",
                    textAlign: TextAlign.center,
                    style: subHeading,
                  ),
                ],
              )),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    getFilters();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: textBoxFillColor,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "My Inventory",
              style: regularText,
            ),
            Text(
              storeName,
              style: TextStyle(color: mColor, fontSize: 14),
            ),
          ],
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              Get.to(SearchInventoryProduct());
            },
          )
        ],
        bottom: systemFilter == null ? PreferredSize(
          child: SizedBox(),
          preferredSize: Size(0, 0),
        ) : TabBar(
          isScrollable: true,
          indicatorColor: mColor,
          indicatorWeight: 3,
          tabs: systemFilter.info.map((e) =>
              Tab(child: Text(e.filterName, style: regularText,),)).toList(),
          controller: _tabController,
          onTap: loading
                    ? null
                    : (val) {
                        setState(() {
                          filterName = systemFilter.info[val].filterName;
                          loading = true;
                          filteredData = null;
                          productSubCategory = null;
                          print("FILTER: $filterName");
                          pageState = false;
                          pageController.animateToPage(0,
                              duration: Duration(milliseconds: 200),
                              curve: Curves.decelerate);
//              getProductCategories();
            });
          },
        ),
      ),
      body: loading ? loadingCircular() : Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Container(
                height: 25,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: filteredData.info.category.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        selectedOption(index);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: selectedChip == index ? Colors.black : Colors
                                .grey,
                            borderRadius: BorderRadius.circular(8)
                        ),
                        padding: EdgeInsets.symmetric(horizontal: 8),
                        margin: EdgeInsets.symmetric(horizontal: 8),
                        child: Center(child: Text(
                          "${filteredData.info.category[index]
                              .prodCategoryName} (${filteredData.info
                              .category[index].productCount})",
                          style: TextStyle(fontSize: 11,
                              color: selectedChip == index
                                  ? Colors.white
                                  : Colors.black),)),
                      ),
                    );
                  },
                ),
              ),
            ),

            !pageState? Container(
              padding: EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text("My Brands: 4 items"),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.arrow_drop_up,
                          size: 30,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ): GestureDetector(
                onTap: (){
                  setState(() {
                    MIN = 0;
                    pageState = false;
                    filteredProduct = null;
                  });
                  pageController.animateToPage(0, duration: Duration(milliseconds: 200), curve: Curves.decelerate);
                },
                child: SizedBox(height: 30, child: Icon(Icons.keyboard_backspace))),
            Expanded(
              child: PageView(
                physics: NeverScrollableScrollPhysics(),
                scrollDirection: Axis.horizontal,
                controller: pageController,
                children: <Widget>[
                  myBrandList(),
                  filteredProduct == null ?
                  SizedBox() :
                  GridView.builder(
                      padding: EdgeInsets.only(bottom: 70),
                      itemCount: filteredProduct.info.data.length < MIN
                          ? filteredProduct.info.data.length
                          : filteredProduct.info.data.length + 1,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          mainAxisSpacing: 4,
                          crossAxisSpacing: 4,
                          childAspectRatio: 6 / 8
                      ),
                      itemBuilder: (context, index) {
                        return index == filteredProduct.info.data.length
                            ? showMore()
                            : MyInventoryProductCard(
                            filteredProduct.info.data[index]);
                      })
                ],
              ),
            )
          ],
        ),
      ),

//      floatingActionButton: FloatingActionButton.extended(
//          onPressed: () {},
//          label: Row(
//            children: <Widget>[Text("NEXT"), Icon(Icons.arrow_forward)],
//          )
//      ),
    );
  }

  myBrandList() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView.separated(
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: 5,
                );
              },
              itemCount: productSubCategory.info.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      subCatId = productSubCategory.info[index].subcatId
                          .toString();
//                      filteredProduct = null;
                    });
                    getProducts();
                  },
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0)),
                    elevation: 2.0,
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border(
                              left: BorderSide(color: mColor, width: 4))),
                      child: ListTile(
                        title: Text("${productSubCategory.info[index]
                            .name}(${productSubCategory.info[index]
                            .productCount})", style: defaultTextStyle,),
                        trailing: Icon(Icons.arrow_forward_ios),
                      ),
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

//
//class brandProductList extends StatefulWidget {
//  List<Datum> data;
//  brandProductList(this.data);
//
//  @override
//  _brandProductListState createState() => _brandProductListState();
//}
//
//class _brandProductListState extends State<brandProductList> {
//  @override
//  Widget build(BuildContext context) {
//    return GridView.builder(
//      padding: EdgeInsets.only(bottom: 70),
//      itemCount: widget.data.length,
//        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//          crossAxisCount: 3,
//          mainAxisSpacing: 4,
//          crossAxisSpacing: 4,
//          childAspectRatio: 6/8
//        ),
//        itemBuilder: (context, index){
//          return MyInventoryProductCard(widget.data[index]);
//        });
////    return ListView.separated(
////        padding: EdgeInsets.only(bottom: 70),
////        separatorBuilder: (context, index){
////          return SizedBox(height: 8,);
////        },
////        itemCount: widget.data.length,
////      itemBuilder: (context, index){
////        return MyInventoryProductCard(widget.data[index]);
////      }
////    );
//  }
//}

//class AddProductList extends StatefulWidget {
//
//  List<Datum> data;
//  AddProductList(this.data);
//
//  @override
//  _AddProductListState createState() => _AddProductListState();
//}
//
//class _AddProductListState extends State<AddProductList> {
//  @override
//  Widget build(BuildContext context) {
//    return widget.data.isEmpty? Center(child: Text("No Products.", style: subHeading,),): ListView.separated(
//        padding: EdgeInsets.only(bottom: 70),
//        separatorBuilder: (context, index){
//          return SizedBox(height: 8,);
//        },
//        itemCount: widget.data.length,
//        itemBuilder: (context, index){
//          return AddProductCard(widget.data[index]);
//        }
//    );
//  }
//}



