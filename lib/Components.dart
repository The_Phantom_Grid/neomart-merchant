import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:merchant/Components/VariableControllers.dart';
import 'package:merchant/Getter/GetPosOffer.dart';
import 'package:merchant/Getter/GetPosProduct.dart';
import 'package:merchant/Network/httpRequests.dart';
import 'package:merchant/OffersByCategory.dart';
import 'package:merchant/constants.dart';

class productCard extends StatefulWidget {
  Datum data;
  String deliveryType, customerUserid;

  productCard(this.data, this.deliveryType, this.customerUserid);

  @override
  _productCardState createState() => _productCardState();
}

class _productCardState extends State<productCard> {
  HttpRequests requests = HttpRequests();
  int qty = 0;
  VarController v = Get.put(VarController());
  var mrpController = TextEditingController();
  var srpController = TextEditingController();
  var qtyController = TextEditingController();
  double mrp, srp;
  bool sellingStatus = false;

  addtoCart() async {
    var res = await requests.addToCartPos(
        widget.deliveryType,
        widget.data.masterProductId.toString(),
        widget.data.merchantListId.toString(),
        qty.toString(),
        widget.customerUserid,
        token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      offersByCategoryState.setPageState();
    }
  }

  incrementQty() {
    setState(() {
      if (qty < widget.data.totalLeft) {
        qty++;
        v.pCount.value++;
        addtoCart();
      } else {
        Fluttertoast.showToast(
            msg: "You cannot add more of these product.",
            toastLength: Toast.LENGTH_LONG,
            timeInSecForIosWeb: 2);
      }
    });
  }

  decrementQty() {
    setState(() {
      if (qty > 0) {
        v.pCount.value--;
        qty--;
        addtoCart();
      }
    });
  }

  showEditProduct() {
    Get.bottomSheet(StatefulBuilder(
      builder: (context, setState) {
        return Container(
          padding: EdgeInsets.all(16),
          width: double.infinity,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(8), topLeft: Radius.circular(8))),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 2,
                          color: textBoxFillColor,
                          spreadRadius: 2,
                        )
                      ]),
                  child: FadeInImage.assetNetwork(
                    placeholder: 'assets/tsp.png',
                    image: widget.data.icon,
                    height: 200,
                    width: double.infinity,
                    fit: BoxFit.fitHeight,
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  widget.data.productname,
                  style: subHeading,
                ),
                Text(
                  widget.data.productdescription,
                  style: TextStyle(fontSize: 11, color: Colors.grey),
                ),
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        height: 8,
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                            color: Colors.black,
                            padding: EdgeInsets.symmetric(horizontal: 4),
                            child: Text(
                              widget.data.measure,
                              style:
                                  TextStyle(fontSize: 12, color: Colors.white),
                            )),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        children: <Widget>[
                          Text("SKU: "),
                          SizedBox(
                            width: 5,
                          ),
                          Text(widget.data.sku),
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
//                      SizedBox(
//                        height: 8,
//                      ),
//                      Row(
//                        children: <Widget>[
//                          Expanded(
//                            child: Row(
//                              children: <Widget>[
//                                Text(
//                                  "Quantity: ",
//                                  style: regularText,
//                                ),
////                          IconButton(
////                            onPressed: qty <= 0? null: (){
////                              setState(() {
////                                qty--;
////                                qtyController.text = qty.toString();
////                              });
////                            },
////                            icon: Icon(Icons.remove, size: 15, color: Colors.black,),
////                          ),
//                                SizedBox(
//                                  width: 60,
//                                  child: TextField(
//                                    keyboardType:
//                                    TextInputType.numberWithOptions(),
//                                    scrollPadding: EdgeInsets.all(8.0),
//                                    maxLength: 10,
//                                    maxLengthEnforced: true,
//                                    controller: qtyController,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter(
//                                          RegExp("[0-9]"))
//                                    ],
//                                    onChanged: (value) {
//                                      setState(() {
//                                        qty = int.tryParse(value);
//                                      });
//                                    },
//                                    decoration: InputDecoration(
//                                      isDense: true,
////                              prefixIcon: Icon(Icons.remove, size: 15, color: Colors.black,),
////                              suffixIcon: Icon(Icons.add, size: 15, color: Colors.black,),
//                                      contentPadding: EdgeInsets.symmetric(
//                                          horizontal: 8, vertical: 8),
//                                      labelStyle: TextStyle(
//                                          color: Colors.black.withOpacity(.4)),
//                                      filled: true,
//                                      fillColor: textBoxFillColor,
//                                      focusColor: Colors.white38,
//                                      focusedBorder: OutlineInputBorder(
//                                          borderSide:
//                                          BorderSide(color: Colors.white),
//                                          borderRadius:
//                                          BorderRadius.circular(4)),
//                                      border: OutlineInputBorder(
//                                          borderSide:
//                                          BorderSide(color: Colors.white),
//                                          borderRadius:
//                                          BorderRadius.circular(4)),
//                                      enabledBorder: OutlineInputBorder(
//                                          borderSide:
//                                          BorderSide(color: Colors.white),
//                                          borderRadius:
//                                          BorderRadius.circular(4)),
//                                      counterText: "",
//                                    ),
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ),
//                          Expanded(
//                              child: Row(
//                                children: <Widget>[
//                                  Text("Selling Status: "),
//                                  SizedBox(
//                                    width: 5,
//                                  ),
//                                  SizedBox(
//                                    width: 40,
//                                    child: Switch(
//                                      activeColor: Colors.green,
//                                      inactiveThumbColor: Colors.red,
//                                      onChanged: (val) {
//                                        setState(() {
//                                          if (sellingStatus)
//                                            sellingStatus = false;
//                                          else
//                                            sellingStatus = true;
//                                        });
//                                      },
//                                      value: sellingStatus,
//                                    ),
//                                  )
//                                ],
//                              )),
////                          IconButton(
////                            onPressed: (){
////                              print("ADD");
////                              setState(() {
////                                qty++;
////                                qtyController.text = qty.toString();
////                              });
////                            },
////                            icon: Icon(Icons.add, size: 15, color: Colors.black,),
////                          ),
//                        ],
//                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: TextField(
                              enabled: false,
                              keyboardType: TextInputType.numberWithOptions(),
                              scrollPadding: EdgeInsets.all(8.0),
                              maxLength: 10,
                              maxLengthEnforced: true,
                              controller: mrpController,
                              inputFormatters: [
                                WhitelistingTextInputFormatter(RegExp("[0-9]")),
                              ],
                              onChanged: (value) {
                                setState(() {
                                  mrp = double.tryParse(double.tryParse(value)
                                      .toStringAsFixed(2));
                                });
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Icon(FlutterIcons.rupee_faw,
                                      size: 15, color: Colors.black),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 8),
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: true,
                                  fillColor: textBoxFillColor,
                                  focusColor: Colors.white38,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  border: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  counterText: "",
                                  hintText: "Enter MRP",
                                  labelText: "MRP"),
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: TextField(
                              enabled: false,
                              keyboardType: TextInputType.numberWithOptions(),
                              scrollPadding: EdgeInsets.all(8.0),
                              maxLength: 10,
                              controller: srpController,
                              maxLengthEnforced: true,
                              inputFormatters: [
                                WhitelistingTextInputFormatter(RegExp("[0-9]"))
                              ],
                              onChanged: (value) {
                                setState(() {
                                  srp = double.tryParse(double.tryParse(value)
                                      .toStringAsFixed(2));
                                });
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Icon(FlutterIcons.rupee_faw,
                                      size: 15, color: Colors.black),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 8),
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: true,
                                  fillColor: textBoxFillColor,
                                  focusColor: Colors.white38,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  border: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  counterText: "",
                                  hintText: "Enter Selling Price",
                                  labelText: "SRP"),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
//                      Row(
//                        children: <Widget>[
//                          Expanded(
//                            child: ButtonTheme(
//                              height: 35,
//                              child: FlatButton(
//                                onPressed: () {
//                                  Get.back();
//                                },
//                                color: textBoxFillColor,
//                                textColor: Colors.black,
//                                child: Text("Cancel"),
//                              ),
//                            ),
//                          ),
//                          SizedBox(
//                            width: 8,
//                          ),
//                          Expanded(
//                            child: ButtonTheme(
//                              height: 35,
//                              child: FlatButton(
//                                onPressed: () {
////                                  validateDetails();
//                                  Get.back();
//                                },
//                                color: Colors.black,
//                                textColor: textBoxFillColor,
//                                child: Text("Update"),
//                              ),
//                            ),
//                          )
//                        ],
//                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    ));
  }

  @override
  void initState() {
    // TODO: implement initState
    qty = widget.data.cartCount;
    v.pCount.value += qty;
    print(v.pCount);
    mrpController.text = widget.data.mrp;
    srpController.text = widget.data.sellingPrice;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showEditProduct();
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        margin: EdgeInsets.symmetric(horizontal: 8),
        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(4)
                ),
                height: 70,
                width: 70,
                child: FadeInImage.assetNetwork(
                    placeholder: 'assets/tsp.png', image: widget.data.icon)
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 4),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.data.productname, overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: smallHeading,),
                    SizedBox(height: 5,),
                    Container(
                        color: Colors.black,
                        padding: EdgeInsets.symmetric(horizontal: 4),
                        child: Text(widget.data.measure,
                          style: TextStyle(
                              fontSize: 12, color: Colors.white),)),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: RichText(
                              text: TextSpan(
                                  children: [
                                    TextSpan(text: "Mrp: ",
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.black)),
                                    TextSpan(
                                        text: "₹ ${widget.data.mrp.toString()}",
                                        style: mrpTextStyle),
                                  ]
                              ),
                            )
                        ),
                        Container(
                          height: 36,
                          width: 120,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18),
                              border: Border.all(color: Colors.black)
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: IconButton(
                                  onPressed: qty <= 0 ? null : () {
                                    decrementQty();
                                  },
                                  icon: Icon(Icons.remove, size: 18,),
                                ),
                              ),
                              Expanded(
                                  child: Center(child: Text(
                                    "$qty", style: TextStyle(fontSize: 12),))
                              ),
                              Expanded(
                                child: IconButton(
                                  onPressed: () {
                                    incrementQty();
                                  },
                                  icon: Icon(Icons.add, size: 18,),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class PosOfferCard extends StatefulWidget {
  String deliveryType, customerUserid;
  PosOfferInfo data;
  PosOfferCard(this.deliveryType, this.customerUserid, this.data);

  @override
  _PosOfferCardState createState() => _PosOfferCardState();
}

class _PosOfferCardState extends State<PosOfferCard> {
  HttpRequests requests = HttpRequests();
  int qty = 0;
  VarController v = Get.put(VarController());
  var mrpController = TextEditingController();
  var srpController = TextEditingController();
  var qtyController = TextEditingController();
  double mrp, srp;
  bool sellingStatus = false;

  addtoCart() async {
    var res = await requests.addToCartPos(
        widget.deliveryType,
        widget.data.collectionProducts[0].masterProductId.toString(),
        widget.data.collectionProducts[0].merchantProductId.toString(),
        qty.toString(),
        widget.customerUserid,
        token);
    if (res != null && res['status'].toString().toLowerCase() == "success") {
      offersByCategoryState.setPageState();
    }
  }

  incrementQty() {
    setState(() {
      if (qty < widget.data.inventory) {
        qty++;
        v.pCount.value++;
        addtoCart();
      } else {
        Fluttertoast.showToast(
            msg: "You cannot add more of these product.",
            toastLength: Toast.LENGTH_LONG,
            timeInSecForIosWeb: 2);
      }
    });
  }

  decrementQty() {
    setState(() {
      if (qty > 0) {
        v.pCount.value--;
        qty--;
        addtoCart();
      }
    });
  }

  showEditProduct() {
    Get.bottomSheet(StatefulBuilder(
      builder: (context, setState) {
        return Container(
          padding: EdgeInsets.all(16),
          width: double.infinity,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(8), topLeft: Radius.circular(8))),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 2,
                          color: textBoxFillColor,
                          spreadRadius: 2,
                        )
                      ]),
                  child: FadeInImage.assetNetwork(
                    placeholder: 'assets/tsp.png',
                    image: widget.data.collectionImages[0].imageUrl,
                    height: 200,
                    width: double.infinity,
                    fit: BoxFit.fitHeight,
                  ),
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  widget.data.collectionName,
                  style: subHeading,
                ),
                Text(
                  widget.data.collectionDescr,
                  style: TextStyle(fontSize: 11, color: Colors.grey),
                ),
                Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        height: 8,
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                            color: Colors.black,
                            padding: EdgeInsets.symmetric(horizontal: 4),
                            child: Text(
                              "Expires: ${widget.data.validTo}",
                              style:
                              TextStyle(fontSize: 12, color: Colors.white),
                            )),
                      ),
//                      SizedBox(
//                        height: 8,
//                      ),
//                      Row(
//                        children: <Widget>[
//                          Text("SKU: "),
//                          SizedBox(
//                            width: 5,
//                          ),
//                          Text(widget.data.sku),
//                        ],
//                      ),
                      SizedBox(
                        height: 16,
                      ),
//                      SizedBox(
//                        height: 8,
//                      ),
//                      Row(
//                        children: <Widget>[
//                          Expanded(
//                            child: Row(
//                              children: <Widget>[
//                                Text(
//                                  "Quantity: ",
//                                  style: regularText,
//                                ),
////                          IconButton(
////                            onPressed: qty <= 0? null: (){
////                              setState(() {
////                                qty--;
////                                qtyController.text = qty.toString();
////                              });
////                            },
////                            icon: Icon(Icons.remove, size: 15, color: Colors.black,),
////                          ),
//                                SizedBox(
//                                  width: 60,
//                                  child: TextField(
//                                    keyboardType:
//                                    TextInputType.numberWithOptions(),
//                                    scrollPadding: EdgeInsets.all(8.0),
//                                    maxLength: 10,
//                                    maxLengthEnforced: true,
//                                    controller: qtyController,
//                                    inputFormatters: [
//                                      WhitelistingTextInputFormatter(
//                                          RegExp("[0-9]"))
//                                    ],
//                                    onChanged: (value) {
//                                      setState(() {
//                                        qty = int.tryParse(value);
//                                      });
//                                    },
//                                    decoration: InputDecoration(
//                                      isDense: true,
////                              prefixIcon: Icon(Icons.remove, size: 15, color: Colors.black,),
////                              suffixIcon: Icon(Icons.add, size: 15, color: Colors.black,),
//                                      contentPadding: EdgeInsets.symmetric(
//                                          horizontal: 8, vertical: 8),
//                                      labelStyle: TextStyle(
//                                          color: Colors.black.withOpacity(.4)),
//                                      filled: true,
//                                      fillColor: textBoxFillColor,
//                                      focusColor: Colors.white38,
//                                      focusedBorder: OutlineInputBorder(
//                                          borderSide:
//                                          BorderSide(color: Colors.white),
//                                          borderRadius:
//                                          BorderRadius.circular(4)),
//                                      border: OutlineInputBorder(
//                                          borderSide:
//                                          BorderSide(color: Colors.white),
//                                          borderRadius:
//                                          BorderRadius.circular(4)),
//                                      enabledBorder: OutlineInputBorder(
//                                          borderSide:
//                                          BorderSide(color: Colors.white),
//                                          borderRadius:
//                                          BorderRadius.circular(4)),
//                                      counterText: "",
//                                    ),
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ),
//                          Expanded(
//                              child: Row(
//                                children: <Widget>[
//                                  Text("Selling Status: "),
//                                  SizedBox(
//                                    width: 5,
//                                  ),
//                                  SizedBox(
//                                    width: 40,
//                                    child: Switch(
//                                      activeColor: Colors.green,
//                                      inactiveThumbColor: Colors.red,
//                                      onChanged: (val) {
//                                        setState(() {
//                                          if (sellingStatus)
//                                            sellingStatus = false;
//                                          else
//                                            sellingStatus = true;
//                                        });
//                                      },
//                                      value: sellingStatus,
//                                    ),
//                                  )
//                                ],
//                              )),
////                          IconButton(
////                            onPressed: (){
////                              print("ADD");
////                              setState(() {
////                                qty++;
////                                qtyController.text = qty.toString();
////                              });
////                            },
////                            icon: Icon(Icons.add, size: 15, color: Colors.black,),
////                          ),
//                        ],
//                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: TextField(
                              enabled: false,
                              keyboardType: TextInputType.numberWithOptions(),
                              scrollPadding: EdgeInsets.all(8.0),
                              maxLength: 10,
                              maxLengthEnforced: true,
                              controller: mrpController,
                              inputFormatters: [
                                WhitelistingTextInputFormatter(RegExp("[0-9]")),
                              ],
                              onChanged: (value) {
                                setState(() {
                                  mrp = double.tryParse(double.tryParse(value)
                                      .toStringAsFixed(2));
                                });
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Icon(FlutterIcons.rupee_faw,
                                      size: 15, color: Colors.black),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 8),
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: true,
                                  fillColor: textBoxFillColor,
                                  focusColor: Colors.white38,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  border: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  counterText: "",
                                  hintText: "Enter MRP",
                                  labelText: "MRP"),
                            ),
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Expanded(
                            child: TextField(
                              enabled: false,
                              keyboardType: TextInputType.numberWithOptions(),
                              scrollPadding: EdgeInsets.all(8.0),
                              maxLength: 10,
                              controller: srpController,
                              maxLengthEnforced: true,
                              inputFormatters: [
                                WhitelistingTextInputFormatter(RegExp("[0-9]"))
                              ],
                              onChanged: (value) {
                                setState(() {
                                  srp = double.tryParse(double.tryParse(value)
                                      .toStringAsFixed(2));
                                });
                              },
                              decoration: InputDecoration(
                                  prefixIcon: Icon(FlutterIcons.rupee_faw,
                                      size: 15, color: Colors.black),
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 8),
                                  labelStyle: TextStyle(color: Colors.black),
                                  filled: true,
                                  fillColor: textBoxFillColor,
                                  focusColor: Colors.white38,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  border: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                      BorderSide(color: Colors.white),
                                      borderRadius: BorderRadius.circular(4)),
                                  counterText: "",
                                  hintText: "Enter Selling Price",
                                  labelText: "SRP"),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
//                      Row(
//                        children: <Widget>[
//                          Expanded(
//                            child: ButtonTheme(
//                              height: 35,
//                              child: FlatButton(
//                                onPressed: () {
//                                  Get.back();
//                                },
//                                color: textBoxFillColor,
//                                textColor: Colors.black,
//                                child: Text("Cancel"),
//                              ),
//                            ),
//                          ),
//                          SizedBox(
//                            width: 8,
//                          ),
//                          Expanded(
//                            child: ButtonTheme(
//                              height: 35,
//                              child: FlatButton(
//                                onPressed: () {
////                                  validateDetails();
//                                  Get.back();
//                                },
//                                color: Colors.black,
//                                textColor: textBoxFillColor,
//                                child: Text("Update"),
//                              ),
//                            ),
//                          )
//                        ],
//                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    ));
  }

  @override
  void initState() {
    // TODO: implement initState
    qty = widget.data.cartCount;
    v.pCount.value += qty;
    print(v.pCount);
    mrpController.text = widget.data.comboPrice.toString();
    srpController.text = widget.data.comboPrice.toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showEditProduct();
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        margin: EdgeInsets.symmetric(horizontal: 8),
        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(4)
                ),
                height: 70,
                width: 70,
                child: FadeInImage.assetNetwork(
                    placeholder: 'assets/tsp.png', image: widget.data.collectionImages[0].imageUrl)
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 4),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.data.collectionName, overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: smallHeading,),
                    SizedBox(height: 5,),
                    Container(
                        color: Colors.black,
                        padding: EdgeInsets.symmetric(horizontal: 4),
                        child: Text("Expires on: ${widget.data.validTo}",
                          style: TextStyle(
                              fontSize: 12, color: Colors.white),)),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: RichText(
                              text: TextSpan(
                                  children: [
                                    TextSpan(text: "Mrp: ",
                                        style: TextStyle(
                                            fontSize: 12, color: Colors.black)),
                                    TextSpan(
                                        text: "₹ ${widget.data.comboPrice.toString()}",
                                        style: mrpTextStyle),
                                  ]
                              ),
                            )
                        ),
                        Container(
                          height: 36,
                          width: 120,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18),
                              border: Border.all(color: Colors.black)
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                child: IconButton(
                                  onPressed: qty <= 0 ? null : () {
                                    decrementQty();
                                  },
                                  icon: Icon(Icons.remove, size: 18,),
                                ),
                              ),
                              Expanded(
                                  child: Center(child: Text(
                                    "$qty", style: TextStyle(fontSize: 12),))
                              ),
                              Expanded(
                                child: IconButton(
                                  onPressed: () {
                                    incrementQty();
                                  },
                                  icon: Icon(Icons.add, size: 18,),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

